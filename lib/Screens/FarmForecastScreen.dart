import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:fms_mobileapp/services/farmforecastinformer.dart';

class FarmForecastViewer extends StatefulWidget {
  @override
  _FarmForecastViewerState createState() => _FarmForecastViewerState();
}

class _FarmForecastViewerState extends State<FarmForecastViewer> {
  List<Widget> cards = [];
  FarmForecastInformer fcst;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cardBuilder();
  }

  @override
  Widget build(BuildContext contex) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20.0),
      height: 300,
      child: ListView(
          // This next line does the trick.
          scrollDirection: Axis.horizontal,
          children: cards),
    );
    // Container(child:
    //   ListView(
    //     scrollDirection: Axis.vertical,
    //     shrinkWrap: true,
    //     children: cards,
    // ));
  }

  void cardBuilder() {
    int length = FarmForecastInformer.farmweatherData['days'].length;
    int flag = 0;
    for (int i = 0; i < length; i++) {
      fcst = new FarmForecastInformer();
      fcst.buildForecast(i);
      flag = flag == 0 ? 1 : 0;
      print("flag :$flag");
      setState(() {
        cards.add(forecastCard(flag));
      });
    }
  }
  

  Widget forecastCard(int flag) {

    return Row(children: [
      SingleChildScrollView(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
            child: Card(
              child: Container(
                  height: 300,
                  width: 180,
                  margin: EdgeInsets.only(top: 0.0, bottom: 2.0),
                  child: Row(
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Icon(fcst.weatherIcon,size: 40.0,),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(fcst.weatherCondition, softWrap: true,) ,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text("Date: " + fcst.date),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(fcst.temperature),
                              ),
                              Text(fcst.tempMeteric)
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: Image.network(
                                  'https://thumbs.dreamstime.com/b/vector-icon-thermometer-up-arrow-rise-temperature-temperature-sensor-simple-design-vector-icon-175423814.jpg',
                                  height: 30,
                                  width: 30,
                                ),
                              ),
                              Text(fcst.minTemp + " °C"),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: Image.asset(
                                  'assets/images/decreasing_temp.jpg',
                                  height: 25,
                                  width: 30,
                                ),
                              ),
                              Text(fcst.maxTemp  + " °C"),
                            ],
                          ),
                          Row(
                            children: [
                              IconButton(
                                  onPressed: null,
                                  icon: new Icon(
                                    WeatherIcons.humidity,
                                    color: Colors.black,
                                  )),
                              Text(fcst.humidity + " %"),
                            ],
                          ),
                          Row(
                            children: [
                              IconButton(
                                  onPressed: null,
                                  icon: new Icon(
                                    Icons.umbrella,
                                    color: Colors.black,
                                  )),
                              Text(fcst.preciption),
                            ],
                          ),
                        ],
                      )
                    ],
                  )

                  //  Padding(
                  //   padding: const EdgeInsets.all(6.0),
                  //   child:Row(

                  //       children: <Widget>[

                  //         // Image.network(
                  //         //     'https://openweathermap.org/img/w/${forecast.clouds}.png'),
                  //         Column(children: [
                  //            Text(fcst.date,
                  //             textAlign: TextAlign.left,
                  //             style: new TextStyle(color: Colors.black)),
                  //            Row(
                  //           children: [
                  //             Image.network(
                  //               'https://thumbs.dreamstime.com/b/vector-icon-thermometer-up-arrow-rise-temperature-temperature-sensor-simple-design-vector-icon-175423814.jpg',
                  //               height: 30,
                  //               width: 30,
                  //             ),
                  //             Padding(
                  //               padding: const EdgeInsets.all(2.0),
                  //               child: Text(
                  //                 fcst.maxTemp,
                  //                 style: TextStyle(fontSize: 14),
                  //               ),
                  //             ),
                  //           ],
                  //         ),
                  //         Row(
                  //           children: [
                  //             Image.asset(
                  //               'assets/images/decreasing_temp.jpg',
                  //               height: 25,
                  //               width: 30,
                  //             ),
                  //             Padding(
                  //               padding: const EdgeInsets.all(2.0),
                  //               child: Text(
                  //                 fcst.minTemp,
                  //                 style: TextStyle(fontSize: 14),
                  //               ),
                  //             ),
                  //           ],
                  //         ),
                  //         Row(
                  //           children: [
                  //             Image.network(
                  //               'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-9zkVUhH0-kAEbNdT6Ei-4sC3dAe7nBDnodDyLLrJ4kb1H7ajeDJ5S9PnePRC7eM0uGw&usqp=CAU',
                  //               height: 28,
                  //               width: 30,
                  //             ),
                  //             Padding(
                  //               padding: const EdgeInsets.all(2.0),
                  //               child: Text(
                  //                 fcst.humidity,
                  //                 style: TextStyle(fontSize: 14),
                  //               ),
                  //             ),
                  //           ],
                  //         ),
                  //         ],)

                  //         // Row(
                  //         //   children: [
                  //         //     // Image.network(
                  //         //     //     'https://th.bing.com/th/id/OIP.L8uzDXZg3bPW6PbBfXlp8AHaI5?pid=ImgDet&rs=1',height: 28,width: 30,),
                  //         //     Padding(
                  //         //       padding: const EdgeInsets.all(2.0),
                  //         //       child: Icon(Icons.cloud),
                  //         //     ),
                  //         //     Padding(
                  //         //       padding: const EdgeInsets.fromLTRB(2.0, 0.0, 8.0, 8.0),
                  //         //       child: Text(
                  //         //         forecast.main,
                  //         //         style: TextStyle(fontSize: 18),
                  //         //       ),
                  //         //     ),
                  //         //   ],
                  //         // ),
                  //       ],
                  //     ),
                  //   ),
                  // ]),
                  ),
            ),
          )
        ]),
      )
    ]);

    // Card(
    //   margin: EdgeInsets.only(top: 0.0, bottom: 2.0),
    //   // clipBehavior: Clip.antiAlias,
    //   child: Stack(
    //     alignment: Alignment.center,
    //     children: [
    //       // Ink.image(
    //       //   image: flag == 1
    //       //       ? AssetImage('assets/images/SunRising2.jpg')
    //       //       : AssetImage('assets/images/ForecastImg2.jpg'),
    //       //   height: 250,
    //       //   fit: BoxFit.cover,
    //       //   colorFilter: ColorFilter.mode(
    //       //       Colors.black.withOpacity(0.6), BlendMode.dstATop),
    //       // ),
    //       Padding(
    //         padding: const EdgeInsets.all(20.0),
    //         child: Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: [

    //             // Column(
    //             //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             //     children: [
    //             //       Row(
    //             //         crossAxisAlignment: CrossAxisAlignment.start,
    //             //         mainAxisAlignment: MainAxisAlignment.start,
    //             //         children: [
    //             //           Text(
    //             //             fcst.date, // Time
    //             //             style: TextStyle(
    //             //                 fontSize: 20.0, fontWeight: FontWeight.w500),
    //             //           ),
    //             //         ],
    //             //       ),
    //             //       Row(
    //             //         children: [
    //             //           Row(
    //             //             mainAxisAlignment: MainAxisAlignment.spaceAround,
    //             //             textBaseline: TextBaseline.ideographic,
    //             //             crossAxisAlignment: CrossAxisAlignment.baseline,
    //             //             children: [
    //             //               Text(fcst.temperature,
    //             //                   style: TextStyle(
    //             //                       fontSize: 45.0,
    //             //                       fontWeight: FontWeight.w600)),
    //             //               Text(
    //             //                 "°C",
    //             //                 style: TextStyle(
    //             //                     fontSize: 30.0,
    //             //                     fontWeight: FontWeight.w400),
    //             //               )
    //             //             ],
    //             //           ),
    //             //         ],
    //             //       ),
    //             //       Row(
    //             //         crossAxisAlignment: CrossAxisAlignment.start,
    //             //         children: [
    //             //           Container(
    //             //             padding: EdgeInsets.only(right: 5.0, bottom: 6.0),
    //             //             child: Icon(
    //             //               fcst.weatherIcon,
    //             //               size: 50.0,
    //             //             ),
    //             //           ),
    //             //         ],
    //             //       ),
    //             //     ]),
    //             // SizedBox(
    //             //   height: 15,
    //             // ),
    //             // Divider(
    //             //   color: Colors.white,
    //             //   height: 20,
    //             //   thickness: 2,
    //             // ),
    //             // Row(
    //             //   children: [
    //             //     Column(
    //             //       crossAxisAlignment: CrossAxisAlignment.start,
    //             //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             //       children: [
    //             //         Text(
    //             //           fcst.humidity,
    //             //           overflow: TextOverflow.ellipsis,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.preciption,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.uvIndex,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.windSpeed,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.minTemp,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //       ],
    //             //     ),
    //             //     SizedBox(
    //             //       width: 40.0,
    //             //     ),
    //             //     Column(
    //             //       crossAxisAlignment: CrossAxisAlignment.start,
    //             //       children: [
    //             //         Text(
    //             //           fcst.dewPoint,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.cloudCover,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.pressure,
    //             //           overflow: TextOverflow.ellipsis,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.windDirect,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //         SizedBox(
    //             //           height: 5.0,
    //             //         ),
    //             //         Text(
    //             //           fcst.maxTemp,
    //             //           style: TextStyle(
    //             //               fontSize: 16.0, fontWeight: FontWeight.w400),
    //             //         ),
    //             //       ],
    //             //     )
    //             //   ],
    //             // )
    //           ],
    //         ),
    //       )
    //     ],
    //   ),
    // );
  }
}
