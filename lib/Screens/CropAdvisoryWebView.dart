import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'dart:io';

class CropAdvisoryWebView extends StatefulWidget {
  String url;


  CropAdvisoryWebView({this.url});

   @override
  CropAdvisoryWebViewState createState() {
    return CropAdvisoryWebViewState(url: this.url);
  }
}

class CropAdvisoryWebViewState extends State<CropAdvisoryWebView> {
  WebView webView;
  String url;
  String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";


  CropAdvisoryWebViewState({this.url});

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(leading: BackButton(
            onPressed: () => Navigator.pop(context, CropAdvisoryScreen()),
          ),backgroundColor: Colors.green,),
        body:  WebView(  
                    allowsInlineMediaPlayback: true,
                    javascriptMode: JavascriptMode.unrestricted,  
                    initialUrl: this.url,
                    zoomEnabled: true,
                    userAgent: newUA,
                    gestureNavigationEnabled: false,
                    ),
        
    );
  }
}