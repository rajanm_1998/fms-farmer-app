import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fms_mobileapp/models/FRMModel/FROSurveyanalysismodel.dart';
import 'package:fms_mobileapp/models/FRMModel/GetFarmerListByFroIdModel.dart';
import 'package:fms_mobileapp/models/KhasaraChakModel.dart';
import 'package:fms_mobileapp/models/SurveyStatusModel.dart';
import 'package:fms_mobileapp/utils/FRMutils/farmerlistbyfrooperation.dart';
import 'package:fms_mobileapp/utils/FRMutils/frmsurveyoperation.dart';
import 'package:http/http.dart' as http;

class FarmerListByFROScreen extends StatefulWidget {
  final fromodel;
  FarmerListByFROScreen(this.fromodel, {Key key}) : super(key: key);
  @override
  FarmerListByFROScreenState createState() =>
      FarmerListByFROScreenState(this.fromodel);
}

class FarmerListByFROScreenState extends State<FarmerListByFROScreen> {
  bool _hasMore;
  int _pageNumber;
  bool _error;
  bool _loading;
  String _SearchBar;
  int _VillageId;
  int _SearchBy;
  int _FroId;

  // final int _defaultPagePerPageCount = 10;
  List<FarmerListByFroId> _farmerList;
  final int _nextPageThreshold = 5;

  Set<String> VillageList;
  Set<String> SearchByList;
  Set<String> KhasaraList;
  Set<String> ChakList;

  List<KhasaraChakResponse> _khasaraChaklist;
  List<SurveyStatusResponse> SurveyStatusList;
  List<FarmerListByFroId> _photos;
  bool _isDone = false;
  dynamic fromodel;
  FarmerListByFROScreenState(this.fromodel);

  @override
  void initState() {
    super.initState();
    setState(() {
      _hasMore = true;
      _pageNumber = 0;
      _error = false;
      _loading = true;
      _photos = [];
      _SearchBar = '';
      _VillageId = 0;
      _SearchBy = 0;
    });
    getDropDownListAsync().then((value) => fetchGetFarmerListByFroId());
  }

  Future<void> getDropDownListAsync() async {
    try {
      var listData = fetchFROKhasaraChakList();
      _khasaraChaklist = new List<KhasaraChakResponse>();
      _khasaraChaklist = await listData;
      SurveyStatusList = new List<SurveyStatusResponse>();
      var surveyStatus = fetchSurveyStatusList();
      SurveyStatusList = await surveyStatus;

      VillageList = Set<String>();
      SearchByList = Set<String>();
      KhasaraList = Set<String>();
      ChakList = Set<String>();
      setState(() {
        VillageList.add("All Village");
        VillageList.addAll(_khasaraChaklist.map((e) => e.villageName).toList());
        KhasaraList.addAll(_khasaraChaklist.map((e) => e.roRSurveyNo).toList());
        ChakList.addAll(_khasaraChaklist.map((e) => e.chakNo).toList());

        SearchByList.add("Select Search By");
        SearchByList.add("Farmer Name");
        SearchByList.add("Khasara Number");
        SearchByList.add("Chak Number");

        selVillage = VillageList.first;
        selSurveyStatus = SurveyStatusList.first.statusId.toString();
        selSearchBy = SearchByList.first;
        selKhasara = KhasaraList.first;
        selChak = ChakList.first;
        _isDone = true;
      });
      setState(() {
        _hasMore = true;
        _pageNumber = 0;
        _error = false;
        _loading = true;
        _farmerList = [];
      });
    } catch (_, ex) {}
  }

  String selVillage, selSurveyStatus, selSearchBy, selKhasara, selChak, _Search;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (_isDone)
      return Scaffold(
        appBar: AppBar(
          title: Text(
            fromodel.froname ?? '',
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
            textAlign: TextAlign.left,
          ),
          backgroundColor: Colors.green,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              children: [
                Row(
                  children: [
                    if (VillageList.length != 0)
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: selVillage,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.green),
                            underline: Container(
                              height: 2,
                              color: Colors.green,
                            ),
                            items: VillageList.map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value,
                                      style: TextStyle(fontSize: 14)),
                                  value: value);
                            }).toList(),
                            onChanged: (value) async {
                              setState(() {
                                selVillage = value;
                              });
                              KhasaraList.clear();
                              ChakList.clear();

                              if (selVillage != 'All Village') {
                                setState(() {
                                  _VillageId = 0;
                                  KhasaraList.addAll(_khasaraChaklist
                                      .where((e) => e.villageName == selVillage)
                                      .map((e) => e.roRSurveyNo)
                                      .toList());
                                  ChakList.addAll(_khasaraChaklist
                                      .where((e) => e.villageName == selVillage)
                                      .map((e) => e.chakNo)
                                      .toList());
                                  selChak = ChakList.first;
                                  selKhasara = KhasaraList.first;
                                });
                              } else {
                                setState(() {
                                  _VillageId = _khasaraChaklist
                                      .where((e) => e.villageName == selVillage)
                                      .first
                                      .villageId;
                                  KhasaraList.addAll(_khasaraChaklist
                                      .map((e) => e.roRSurveyNo)
                                      .toList());
                                  ChakList.addAll(_khasaraChaklist
                                      .map((e) => e.chakNo)
                                      .toList());
                                  selChak = ChakList.first;
                                  selKhasara = KhasaraList.first;
                                });
                                setState(() {
                                  _pageNumber = 0;
                                  _photos.clear();
                                  
                                });
                                fetchGetFarmerListByFroId();
                              }
                            },
                          ),
                        ),
                      ),
                    if (SurveyStatusList.length != 0)
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: selSurveyStatus,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.green),
                            underline: Container(
                              height: 2,
                              color: Colors.green,
                            ),
                            items: SurveyStatusList.map((value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value.status,
                                      style: TextStyle(fontSize: 14)),
                                  value: value.statusId.toString());
                            }).toList(),
                            onChanged: (value) async {
                              setState(() {
                                selSurveyStatus = value;
                              });
                              setState(() {
                                _pageNumber = 0;
                                _photos.clear();
                                
                              });
                              fetchGetFarmerListByFroId();
                            },
                          ),
                        ),
                      )
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                        child: DropdownButton<String>(
                          isExpanded: true,
                          value: selSearchBy,
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: const TextStyle(color: Colors.green),
                          underline: Container(
                            height: 2,
                            color: Colors.green,
                          ),
                          items: SearchByList.map<DropdownMenuItem<String>>(
                              (String value) {
                            return DropdownMenuItem<String>(
                                child:
                                    Text(value, style: TextStyle(fontSize: 14)),
                                value: value);
                          }).toList(),
                          onChanged: (value) async {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                });
                            setState(() {
                              switch (selSearchBy) {
                                case "Select Search By":
                                  _SearchBy = 0;
                                  break;
                                case "Farmer Name":
                                  _SearchBy = 1;
                                  break;
                                case "Khasara Number":
                                  _SearchBy = 2;
                                  _SearchBar = selKhasara;
                                  _pageNumber = 0;
                                  _photos.clear();
                                  fetchGetFarmerListByFroId();

                                  break;
                                case "Chak Number":
                                  _SearchBy = 3;
                                  _SearchBar = selChak;
                                  _pageNumber = 0;
                                  _photos.clear();
                                  fetchGetFarmerListByFroId();

                                  break;
                              }
                              selSearchBy = value;
                              Navigator.pop(context);
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                if (KhasaraList.length != 0 && selSearchBy == 'Khasara Number')
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: selKhasara,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.green),
                            underline: Container(
                              height: 2,
                              color: Colors.green,
                            ),
                            items: KhasaraList.map<DropdownMenuItem<String>>(
                                (String v) {
                              return DropdownMenuItem<String>(
                                  child: Text(v), value: v);
                            }).toList(),
                            onChanged: (value) async {
                              setState(() {
                                selKhasara = value;
                                _SearchBar = value;
                              });
                              setState(() {
                                _pageNumber = 0;
                                _photos.clear();
                              });
                              fetchGetFarmerListByFroId();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                if (ChakList.length != 0 && selSearchBy == 'Chak Number')
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: DropdownButton<String>(
                            isExpanded: true,
                            value: selChak,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.green),
                            underline: Container(
                              height: 2,
                              color: Colors.green,
                            ),
                            items: ChakList.map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value,
                                      style: TextStyle(fontSize: 14)),
                                  value: value);
                            }).toList(),
                            onChanged: (value) async {
                              setState(() {
                                selChak = value;
                                _SearchBar = value;
                              });
                              setState(() {
                                _pageNumber = 0;
                                _photos.clear();
                              });
                              fetchGetFarmerListByFroId();
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                if (selSearchBy == 'Farmer Name')
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          initialValue: '',
                          decoration: const InputDecoration(
                              hintText: 'Search Farmer Name',
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.text,
                          onChanged: (newValue) async {
                            setState(() {
                              _SearchBar = newValue;
                            });
                          },
                        ),
                      ),
                      Container(width: 20),
                      ElevatedButton(
                        child: Center(child: const Text('Search')),
                        style: ElevatedButton.styleFrom(
                          primary: Colors.green,
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          padding: EdgeInsets.all(20),
                        ),
                        onPressed: () async {
                          setState(() {
                            _pageNumber = 0;
                            _photos.clear();
                            
                          });
                          fetchGetFarmerListByFroId();
                        },
                      ),
                    ],
                  ),
                Container(height: 20),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  color: Colors.green,
                  child: Center(
                    child: Text(
                      "Farmer List",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ),
                if (_photos.length != 0) Expanded(child: getBody()),
                if (_photos.length == 0)
                  Container(
                    height: MediaQuery.of(context).size.height *0.30,
                    alignment: Alignment.center,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
              ],
            ),
          ),
        ),
      );
    else
      return Scaffold(
          body: Center(
        child: CircularProgressIndicator(),
      ));
  }

  Widget getBody() {
    if (_photos.isEmpty) {
      if (_loading) {
        return Center(
            child: Padding(
          padding: const EdgeInsets.all(8),
          child: CircularProgressIndicator(),
        ));
      } else if (_error) {
        return Container(
          alignment: Alignment.centerLeft,
          child: InkWell(
            onTap: () {setState(
              () {
                _loading = true;
                _error = false;
                
              },
            );
            fetchGetFarmerListByFroId();
            },
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Text("Error while loading photos, tap to try agin"),
            ),
          ),
        );
      }
    } else {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: _photos.length + (_hasMore ? 1 : 0),
        itemBuilder: (context, index) {
          if (index == _photos.length - _nextPageThreshold) {
            fetchGetFarmerListByFroId();
          }
          if (index == _photos.length) {
            if (_error) {
              return InkWell(
                onTap: () => setState(
                  () {
                    _loading = true;
                    _error = false;
                    fetchGetFarmerListByFroId();
                  },
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text("Error while loading photos, tap to try agin"),
                ),
              );
            } else {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: CircularProgressIndicator(),
                ),
              );
            }
          }
          var photo = _photos[index];
          return Card(
            child: InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return Center(
                        child: Container(
                            color: Colors.white,
                            padding: EdgeInsets.all(8),
                            child: Text(
                              photo.farmerName,
                              style: TextStyle(fontSize: 14),
                            )),
                      );
                    });
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      photo.farmerName,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
    return Container();
  }

  final int _defaultPhotosPerPageCount = 10;
  Future<void> fetchGetFarmerListByFroId() async {
    var search = base64.encode(utf8.encode(_SearchBar));
    final response = await http.get(Uri.parse(
        ('http://fmsapi.seprojects.in/api/SurveyAnalysis/GetFarmerListByFroId?Search=$search&SearchType=$_SearchBy&VillageId=$_VillageId&Fro_id=555220&StatusId=$selSurveyStatus&pageIndex=$_pageNumber&pageSize=$_defaultPhotosPerPageCount')));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      var data = GetFarmerListByFroId.fromJson(jsonDecode(response.body));
      var fetchedPhotos = data.data.response.toList();
      setState(
        () {
          _hasMore = fetchedPhotos.length == _defaultPhotosPerPageCount;
          _loading = false;
          _pageNumber = _pageNumber + 1;
          _photos.addAll(fetchedPhotos);
        },
      );
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Modal Data');
    }
  }
}
