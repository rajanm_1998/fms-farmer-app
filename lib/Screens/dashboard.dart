// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:fms_mobileapp/Screens/ChakIrrigationSchedule.dart';
import 'package:fms_mobileapp/Screens/CropWaterRequirement.dart';
import 'package:fms_mobileapp/Screens/CurrentCropPattern.dart';
import 'package:fms_mobileapp/Screens/FarmerDetails.dart';
import 'package:fms_mobileapp/Screens/FarmerProfileScreen/FarmerProfileScreen.dart';
import 'package:fms_mobileapp/Screens/SprinklerRequirement.dart';
import 'package:fms_mobileapp/Screens/WUA.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/farmerMap.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/MainWeather.dart';
import 'package:fms_mobileapp/Screens/PreviousYrCropPattern.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:fms_mobileapp/services/storage.dart';

class DashBoardScreen extends StatefulWidget {
  DashBoardScreen({Key key}) : super(key: key);

  @override
  _DashBoardState createState() => _DashBoardState();
}

enum TtsState { playing, stopped, paused, continued }

class _DashBoardState extends State<DashBoardScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  bool _isPlaying = true;

  @override
  void initState() {
    super.initState();
    initTts();
    _controller = AnimationController(
      lowerBound: 0.3,
      vsync: this,
      duration: Duration(seconds: 3),
    )..repeat();
    if (SecureStorage.getUserData() == null) {
      Navigator.of(context).popUntil((route) => route.isFirst);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  ErrorBuilder() {
    // ignore: prefer_const_constructors
    return Center(
      child: Text("LandHolding Details Not Found "),
    );
  }

  Widget ErrorViewBuilder(BuildContext context, String error) {
    return Center(
      child: Text(error),
    );
  }

  GifController controller;

  FlutterTts flutterTts = FlutterTts();

  Future<FarmerDetailsModel> farmer;
  FarmerDetailsModel fd;
  var postData;

  TtsState ttsState = TtsState.stopped;

  initTts() {
    flutterTts = FlutterTts();

    flutterTts.setCompletionHandler(() {
      setState(() {
        print("Complete");
        ttsState = TtsState.stopped;
      });
    });
  }

  var data = [];
  String strData = "";
  var l = null;
  ConvertFarmerDetails() async {
    farmer = fetchFarmerDetailsResponse();
    fd = await farmer;
    return fd;
  }

  getDataLength() async {
    farmer = fetchFarmerDetailsResponse();
    fd = await farmer;
    data.add(fd.response.farmerName + " , " + fd.response.contactNo);
    print(data);
    strData = DemoLocalization.of(context).translate('name') +
        " " +
        fd.response.farmerName +
        " , " +
        DemoLocalization.of(context).translate('contactno') +
        " " +
        fd.response.contactNo;
    print(strData);
    // print(strData.length);
    // print(data.length);
    l = strData.split(',');
    print(l.length);
  }

  String btnText = "START";

  Future speak() async {
    // print(await flutterTts.getLanguages);

    await flutterTts.setLanguage("hi-IN");
    await flutterTts.setSpeechRate(0.2);
    await flutterTts.awaitSpeakCompletion(true);
    await getDataLength();
    // for (int i = 0; i < l.length; i++) {
    //   await flutterTts.speak(l[i]);
    //   print(l[i]);
    //   if (i == (l.length - 1) && _isPlaying = false) {
    //     _isPlaying = false;
    //     btnText = "Stop";

    //   }
    // }
    if (btnText == "START") {
      await setState(() => btnText = "STOP");
      for (int i = 0; i < l.length; i++) {
        await flutterTts.speak(l[i]);
        // print(l[i]);
        await flutterTts.awaitSpeakCompletion(true);

        print(i);
        if (i == (l.length - 1)) {
          await setState(() => btnText = "START");
        }
      }
    } else if (btnText == "STOP") {
      flutterTts.stop();
      await setState(() => btnText = "START");
    }
    return false;
  }

  Future stop() async {
    await flutterTts.stop();
    setState(() {
      _isPlaying = true;
    });
    setState(() => btnText = "START");
    // if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  VoidCallback _changeLanguage(String language) {
    Locale _temp;
    switch (language) {
      case 'hi':
        _temp = Locale(language, 'IN');
        break;

      default:
        _temp = Locale(language, 'US');
    }

    // MyApp.setLocale(context, _temp);
  }

  Future<void> showInformationDialog(BuildContext context) async {
    List<FruitsList> fList = [
      FruitsList(index: 1, name: "English", langcode: "en", countrycode: "US"),
      FruitsList(index: 2, name: "Hindi", langcode: "hi", countrycode: "IN"),
      FruitsList(
        index: 3,
        name: "Marathi",
      ),
      FruitsList(
        index: 4,
        name: "Odia",
      ),
    ];
    String radioItem = 'English';

    int id = 1;
    return await showDialog(
        context: context,
        builder: (context) {
          bool isChecked = false;
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text(
                  DemoLocalization.of(context).translate('LanguageSelection')),
              actions: [
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text('OK'),
                )
              ],
              content: Form(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: fList
                    .map((data) => RadioListTile(
                          title: Text("${data.name}"),
                          groupValue: id,
                          value: data.index,
                          activeColor: Colors.green,
                          onChanged: (val) {
                            setState(() {
                              radioItem = data.name;
                              id = data.index;
                              Locale _temp =
                                  Locale(data.langcode, data.countrycode);
                              MyApp.setLocale(context, _temp);
                              this.setState(() {
                                MyApp.setLocale(context, _temp);
                              });
                            });
                          },
                        ))
                    .toList(),
              )),
              // title: Text('Stateful Dialog'),
              // actions: <Widget>[
              //   InkWell(
              //     child: Text('OK   '),
              //     onTap: () {
              //       if (_formKey.currentState.validate()) {
              //         // Do something like updating SharedPreferences or User Settings etc.
              //         Navigator.of(context).pop();
              //       }
              //     },
              //   ),
              // ],
            );
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    // final double categoryHeight = size.height * 0.30;

    return Scaffold(
        appBar: AppBar(
          title: Text(DemoLocalization.of(context).translate('title')),
          backgroundColor: Colors.green,

          // Padding(
          //     padding: EdgeInsets.all(8.0),
          //     child: DropdownButton(
          //       onChanged: (Language language) {
          //         _changeLanguage(language);
          //       },
          //       underline: SizedBox(),
          //       icon: Icon(
          //         Icons.language,
          //         color: Colors.white,
          //       ),
          //       items: Language.languageList()
          //           .map<DropdownMenuItem<Language>>((lang) =>
          //               DropdownMenuItem(
          //                 value: lang,
          //                 child: Row(
          //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
          //                   children: <Widget>[Text(lang.name)],
          //                 ),
          //               ))
          //           .toList(),
          //     ))
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                padding: EdgeInsets.all(50.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                      image: AssetImage("assets/images/Logo.png"),
                      fit: BoxFit.fitHeight,
                      alignment: Alignment.center),
                ),
              ),
              ListTile(
                leading: Icon(
                  Icons.info_outline,
                  color: Colors.green,
                ),
                title:
                    Text(DemoLocalization.of(context).translate('mydetails')),
                onTap: () {
                  // Update the state of the app.
                  // ...
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FarmerDetailsScreen()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.crop,
                  color: Colors.green,
                ),
                title: Text(
                    DemoLocalization.of(context).translate('cropadvisory')),
                onTap: () {
                  // Update the state of the app.
                  // ...
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CropAdvisoryScreen()));
                },
              ),
              ListTile(
                  leading: Icon(
                    Icons.language,
                    color: Colors.green,
                  ),
                  title: Text(DemoLocalization.of(context)
                      .translate('LanguageSelection')),
                  onTap: () {
                    //
                    Navigator.pop(context);
                    showInformationDialog(context);
                  }

                  // children: <Widget>[
                  //   ListTile(
                  //     title: const Text("English"),
                  //     onTap: () {
                  //       _changeLanguage('en');
                  //     },
                  //   ),
                  //   ListTile(
                  //     title: const Text("Hindi"),
                  //     onTap: () {
                  //       _changeLanguage('hi');
                  //     },
                  //   ),
                  // ],
                  ),
              ListTile(
                leading: Icon(Icons.volume_up_rounded, color: Colors.green),
                title: Text(
                    DemoLocalization.of(context).translate('VoiceSelection')),
                onTap: () {
                  Navigator.pop(context);
                  showVoiceSelectDialog(context);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Colors.green,
                ),
                title: Text(DemoLocalization.of(context).translate('logout')),
                onTap: () {
                  // Update the state of the app.
                  // ...

                  SecureStorage.setUserData(null);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new MyCustomForm()));
                  // Navigator.of(context).popUntil((route) => route.isFirst);
                },
              ),
            ],
          ),
        ),
        body: Container(
          color: Colors.grey,
          height: size.height,
          width: size.width,
          padding: EdgeInsets.all(5.0),

          // width: MediaQuery.of(context).size.width,

          child: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                verticalDirection: VerticalDirection.down,
                children: <Widget>[
                  Card(
                    shadowColor: Colors.black,
                    child: Row(
                      children: [
                        Expanded(
                          // width: MediaQuery.of(context).size.width/2,
                          child: (Image.asset(
                            "assets/images/Logo.png",
                            fit: BoxFit.contain,
                            height: 100,
                          )),
                        ),
                        Wrap(children: [
                          FutureBuilder(
                            future: ConvertFarmerDetails(),
                            builder: (context, FarmerDetails) {
                              if (FarmerDetails.hasData) {
                                return Container(
                                  width: size.width * 0.65,
                                  padding: const EdgeInsets.all(30.0),
                                  child: Column(
                                    children: [
                                      Text(DemoLocalization.of(context)
                                              .translate('name') +
                                          ": " +
                                          fd.response.farmerName),
                                      Text(DemoLocalization.of(context)
                                              .translate('contactno') +
                                          ": " +
                                          fd.response.contactNo),
                                      Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    0.0, 10.0, 0.0, 0.0),
                                                child: ElevatedButton(
                                                  style:
                                                      ElevatedButton.styleFrom(
                                                    primary: Colors.green,
                                                    elevation: 3,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        9.0)),
                                                  ),
                                                  child: Text(btnText),
                                                  onPressed: () async {
                                                    await speak();
                                                  },
                                                )),
                                            Expanded(
                                              child: ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                    primary: Colors.transparent,
                                                    shadowColor:
                                                        Colors.transparent,
                                                         ),
                                                child: Text(
                                                  "View More->",
                                                  style: TextStyle(
                                                      color: Colors.grey, backgroundColor:Colors.transparent,)
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              FarmerDetailsScreen()));
                                                },
                                              ),
                                            ),
                                          ]),
                                    ],
                                  ),
                                );
                              } else if (FarmerDetails.hasError) {
                                return Text("${FarmerDetails.error}");
                              }
                              return CircularProgressIndicator();
                            },
                          ),
                        ]),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        FarmerProfileDetailScreen()));
                          },
                          child: Container(
                          
                              child: Column(
                                children: [
                                  Image.asset(
                                    "assets/images/F_Icon.png",
                                    height: 100,
                                    width: 150,
                                  ),
                                  Center(
                                    child: Flexible(flex: 1, child: Text(
                                        DemoLocalization.of(context)
                                            .translate('FARMERPROFILE'),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(color: Colors.green))),
                                  ),
                                ],
                              ),
                     
                          ),
                        )),
                      ),
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Expanded(
                          child: Card(
                              child: new InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          FarmerDetailsScreen()));
                            },
                            child: Container(
                                child: Column(
                                  children: [
                                    Image.asset(
                                      "assets/images/LandHolding.png",
                                      height: 100,
                                      width: 150,
                                    ),
                                    Center(
                                      child: Flexible(flex: 1,child: Text(
                                          DemoLocalization.of(context)
                                              .translate('LandHoldingDetails'),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Colors.green),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                            ),
                          )),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 200,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        PreviousYrCropPatternScreen()));
                          },
                          child: Container(
                            
                              child: Column(
                                children: [
                                  Image.asset(
                                    "assets/images/leves.png",
                                    height: 80,
                                    width: 150,
                                  ),
                                  Center(
                                    child: Flexible(flex: 1,child: Text(
                                          DemoLocalization.of(context).translate(
                                              'PreviousYearCroppingPattern&EstimatedEconomics'),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Colors.green)),
                                    ),
                                  ),
                                ],
                              ),
                            
                          ),
                        )),
                      ),
                      Container(
                        width: size.width * 0.485,
                        height: 200,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        CurrentYrCropPatternScreen()));
                          },
                          child: Container(

                              child: Column(
                                children: [
                                  Image.asset(
                                    "assets/images/leves.png",
                                    height: 80,
                                    width: 150,
                                  ),
                                  Center(
                                    child: Flexible(flex: 1,child: Text(
                                          DemoLocalization.of(context).translate(
                                              'CurrentYearCroppingPattern&EstimatedEconomics'),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Colors.green)),
                                    ),
                                  ),
                                ],
                              ),
                      
                          ),
                        )),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                          child: new InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CropAdvisoryScreen()));
                            },
                            child: Container(
                                child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/cropadvi.png",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('CROPAdvisory'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            )),
                          ),
                        ),
                      ),
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        SprinklerRequirementScreen()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/SPRINKLER_icon.png",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('SprinklerRequirement'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        SubChakIrrigationScheduleScreen()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/CHAK_IRRIGATION_icon.jpg",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('ChakIrrigationSchedule'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                      ),
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        KMLAPP()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/map.png",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('FarmerMAP'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MainWeatherScreen()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/weather.png",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('Weather'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                        // Crop Water Requirement
                      ),
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => WUAScreen()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/WUA_icon.jpg",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('WaterUserAssociation'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => WUAScreen()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/calender.png",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('CropCalendar'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                        // Crop Water Requirement
                      ),
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                            child: new InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CropWaterScreen()));
                          },
                          child: Container(
                            child: Column(
                              children: [
                                Image.asset(
                                  "assets/images/WATER_REQU.png",
                                  height: 100,
                                  width: 150,
                                ),
                                Center(
                                  child: Text(
                                      DemoLocalization.of(context)
                                          .translate('CropWaterRequirement'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green)),
                                ),
                              ],
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                ]),
            //  Container(
            //    constraints: BoxConstraints.expand(),
            //   child: Card( child: Text('data1'),),
            // ),
          ),
        ));
    // body: Container(
    //   height: size.height,
    //   // width: MediaQuery.of(context).size.width,
    //   child: Column(
    //     children: <Widget>[
    //   Row(
    //     children: [
    //       Wrap(
    //         children: [
    //           FutureBuilder (
    //     future: ConvertFarmerDetails(),
    //     builder: (context,FarmerDetails) {
    //      if(FarmerDetails.hasData){
    //        return Padding(
    //          padding: const EdgeInsets.all(30.0),
    //          child: Column(
    //            children: [
    //             Text(DemoLocalization.of(context).translate('name') + ": " + fd.response.farmerName),
    //             Text(DemoLocalization.of(context).translate('farmerreg') + ":" + fd.response.registrationNo),
    //             Text(DemoLocalization.of(context).translate('contactno') + ": " + fd.response.contactNo),
    //             Text(DemoLocalization.of(context).translate('address') + ": " + fd.response.address),
    //             Text(DemoLocalization.of(context).translate('network') + ": " + fd.response.network),
    //             Text(DemoLocalization.of(context).translate('project_name') + ":" + fd.response.projectName),
    //             ElevatedButton(
    //         style: ElevatedButton.styleFrom(
    //           primary: Colors.blue,
    //           elevation: 3,
    //           shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.circular(32.0)),
    //           padding: EdgeInsets.all(20),
    //         ),
    //         onPressed:  () {
    //           speak();
    //         },
    //         child: const Text('PLAY VOICE', style: TextStyle()),

    //       ),
    //            ],
    //          ),
    //        );

    //      }else if(FarmerDetails.hasError){
    //        return Text("${FarmerDetails.error}");
    //      }
    //      return CircularProgressIndicator();
    //     },
    //   ),
    //   ]),
    //   Expanded(
    //     // width: MediaQuery.of(context).size.width/2,
    //     child: (
    //       Image.asset("assets/images/Farmer_ani1.gif",
    //       fit: BoxFit.cover,
    //       height: 300,)
    //     ),
    //   ),
    //    Expanded(
    //             child: FutureBuilder(
    //                 future: fetchFarmerLandResponse(),
    //                 builder: (context, landDetails) {
    //                   if (landDetails.hasData) {
    //                     return listViewBuilder(context, landDetails.data);
    //                   }
    //                 }),
    //       ),
    //   ],
    //   )
    //     ],)

    // ),
  }
}

class FruitsList extends _DashBoardState {
  String name;
  int index;
  String langcode;
  String countrycode;
  FruitsList({this.name, this.index, this.langcode, this.countrycode});
}

Future<void> showVoiceSelectDialog(BuildContext context) async {
  List<FruitsList> fList = [
    FruitsList(
      index: 1,
      name: "MALE",
    ),
    FruitsList(
      index: 2,
      name: "FEMALE",
    ),
  ];
  String radioItem = 'FEMALE';

  int id = 1;
  return await showDialog(
      context: context,
      builder: (context) {
        bool isChecked = false;
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            title:
                Text(DemoLocalization.of(context).translate('VoiceSelection')),
            content: Form(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: fList
                  .map((data) => RadioListTile(
                        title: Text("${data.name}"),
                        groupValue: id,
                        value: data.index,
                        activeColor: Colors.green,
                        onChanged: (val) {
                          setState(() {
                            radioItem = data.name;
                            id = data.index;
                          });
                        },
                      ))
                  .toList(),
            )),
            // title: Text('Stateful Dialog'),
            // actions: <Widget>[
            //   InkWell(
            //     child: Text('OK   '),
            //     onTap: () {
            //       if (_formKey.currentState.validate()) {
            //         // Do something like updating SharedPreferences or User Settings etc.
            //         Navigator.of(context).pop();
            //       }
            //     },
            //   ),
            // ],
          );
        });
      });
}

class FruitsListVoice {
  String name;
  int index;
  FruitsListVoice({this.name, this.index});
}
