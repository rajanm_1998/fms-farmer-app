import 'package:flutter/material.dart';
import 'package:fms_mobileapp/models/FRMModel/FRMSurveyanalysismodel.dart';
import 'package:fms_mobileapp/services/extension/piechartwidget.dart';
import 'package:fms_mobileapp/utils/FRMutils/frmsurveyoperation.dart';


class SurveyPage extends StatefulWidget {
  const SurveyPage({Key key}) : super(key: key);

  @override
  _SurveyPageState createState() => _SurveyPageState();
}

double surveyed = 0;
double notsurveyed = 0;
double approved = 0;

class _SurveyPageState extends State<SurveyPage> {
  @override
  void initState() {
    super.initState();
  }

  Future<Surveyanalysis> _survey;
  Surveyanalysis survey;
  ConvertSurveyAnalysis() async {
    _survey = fetchFRMsurveyanalysisreponse();
    var data = await _survey;
    setState(() {
      survey = data;
    });
    return survey;
  }

  Widget listViewBuilder(BuildContext context, dynamic values) {
    // final height = MediaQuery.of(context).size.height;
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
          alignment: Alignment.bottomCenter,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.black),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 2, 2, 2),
                    child: Text("Project Name :",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                        textAlign: TextAlign.left),
                  ),
                  Expanded(
                      child: Text(
                    values.projectName,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black),
                  ))
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 2, 2, 2),
                    child: Text("Command Area :",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                        textAlign: TextAlign.left),
                  ),
                  Expanded(
                      child: Text(
                    values.cCA.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black),
                  ))
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 2, 2, 2),
                    child: Text("Area Allocated :",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                        textAlign: TextAlign.left),
                  ),
                  Expanded(
                      child: Text(
                    values.areaAllocated.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black),
                  ))
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 2, 2, 2),
                    child: Text("No.Of FRO :",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                        textAlign: TextAlign.left),
                  ),
                  Expanded(
                      child: Text(
                    values.noOfFROs.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black),
                  ))
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 2, 2, 2),
                    child: Text("No. Of Farmers :",
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                        textAlign: TextAlign.left),
                  ),
                  Expanded(
                      child: Text(
                    values.noOfFarmer.toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        color: Colors.black),
                  ))
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.1,
        ),
        Container(
          padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
          alignment: Alignment.bottomCenter,
          child: PieChartView(
            jsonData: values.sampleData,
          ),
        ),
      ],
      
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Survey Analysis"),
        backgroundColor: Colors.green,
        leading: BackButton(
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: FutureBuilder(
                future: ConvertSurveyAnalysis(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return listViewBuilder(context, survey);
                  } else if (snapshot.hasError) {
                    return Center(child: Text("No data found"));
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                }),
          )),
    );
  }
}

// class PieChartView extends StatelessWidget {
//   const PieChartView({this.jsonData});

//   final Map<String, double> jsonData;

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: PieChart(
//       dataMap: jsonData,
//       chartType: ChartType.ring,
//       chartRadius: MediaQuery.of(context).size.width / 1.8,
//       colorList: [Colors.orange, Colors.red, Colors.green],
//       legendOptions: LegendOptions( 
//       showLegendsInRow: true,
//       legendPosition: LegendPosition.bottom,
//       showLegends: true,
//       legendTextStyle: TextStyle(
//       fontWeight: FontWeight.bold,
//           )
//       )
//     )
//     );
//    }
//  }
