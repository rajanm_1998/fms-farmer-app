import 'package:flutter/material.dart';

class WUAScreen extends StatefulWidget {
  const WUAScreen({ Key key }) : super(key: key);

  @override
  _WUAScreenState createState() => _WUAScreenState();
}

class _WUAScreenState extends State<WUAScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Water User Association(WUA)"),
        backgroundColor: Colors.green,
        ),
        body: Container(
        child: Column(
          children: [
            Padding(padding:EdgeInsets.all(30)),
            Text("Water User Association(WUA)" ,style: TextStyle(color: Colors.green,fontSize:25,fontWeight:FontWeight.bold  ),),
            Image.asset("assets/images/WUA.jpg"),
            
            
          ],
        ),
        
       ),
    
      
    );
      
    
  }
}