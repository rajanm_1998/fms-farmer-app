import 'package:flutter/material.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/styles.dart';
import 'dart:convert';
// ignore: file_names
import 'package:flutter/cupertino.dart';
import 'package:fms_mobileapp/Screens/Home.dart';
import 'package:fms_mobileapp/models/User.dart';
import 'package:fms_mobileapp/models/Login.dart'; 
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/styles.dart';
import 'package:fms_mobileapp/Screens/OTP.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';



class HomeScreen extends StatelessWidget{
  String username;
  HomeScreen({Key key, this.username}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: Text('Home Screen'),backgroundColor: Colors.green),
        drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              child: Text('FMS Mobile App',style: TextStyle(fontSize: 30),),
            ),
            ListTile(
              title: const Text('My Details'),
              onTap: () {
                // Update the state of the app.
                // ...
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>  CropAdvisoryScreen()));
              },
            ),
            ListTile(
              title: const Text('Crop Advisory'),
              onTap: () {
                // Update the state of the app.
                // ...
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>  CropAdvisoryScreen()));
              },
            ),
            ListTile(
              title: const Text('Logout'),
              onTap: () {
                // Update the state of the app.
                // ...
                   Navigator.pop(context, MaterialPageRoute(builder: (context) => MyCustomForm()));
                
              },
            ),
          ],
        ),
      ),
    );
  }
}