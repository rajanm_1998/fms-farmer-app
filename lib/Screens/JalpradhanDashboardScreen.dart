import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/FarmerDetails.dart';
import 'package:fms_mobileapp/Screens/dashboard.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/DashboardMenuItem.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/projectAuthorityModal.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:fms_mobileapp/utils/Operation.dart';

class DynamicDashboardScreen extends StatefulWidget {
  DynamicDashboardScreen({Key key}) : super(key: key);

  @override
  _DynamicDashboardScreenState createState() => _DynamicDashboardScreenState();
}

enum TtsState { playing, stopped, paused, continued }

class _DynamicDashboardScreenState extends State<DynamicDashboardScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  bool _isPlaying = true;

  @override
  void initState() {
    super.initState();
    initTts();

    ConvertFarmerDetails();
    _controller = AnimationController(
      lowerBound: 0.3,
      vsync: this,
      duration: Duration(seconds: 3),
    )..repeat();
    /*if (SecureStorage.getUserData() == null) {
      Navigator.of(context).popUntil((route) => route.isFirst);
    }
    else
      checkRole();*/
  }

  List<MenuItem> menuList;
  getDashboardContent(List<MenuItem> values) {
    ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: Container(
                child: new InkWell(
                    child: Container(
              child: ListTile(
                // leading: SizedBox(
                //     height: 150.0,
                //     width: 100.0,
                //     child: Image.network(
                //       'http://fms.seprojects.in/' +
                //           (values[index].path).toString(),
                //       fit: BoxFit.fitWidth,
                //       alignment: Alignment.centerLeft,
                //     )),
                title: Text(("Menu Name : " + values[index].Menu),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 19.0,
                        color: Colors.black),
                    textAlign: TextAlign.start),
                // subtitle: Text(
                //   "Season :" + values[index].seasonName.toString(),
                //   style: TextStyle(
                //       fontSize: 16,
                //       color: getColor(values[index].recommendation)),
                // ),
                // trailing: Icon(Icons.arrow_forward,
                //     color: getColor(values[index].recommendation)),
              ),
            ))),
          );
        });
  }

  @override
  GifController controller;
  FlutterTts flutterTts = FlutterTts();
  TtsState ttsState = TtsState.stopped;
  Future<FarmerDetailsModel> farmer;
  List<AuthorityResponse> authority;
  FarmerDetailsModel fd;

  initTts() {
    flutterTts = FlutterTts();
    flutterTts.setCompletionHandler(() {
      setState(() {
        print("Complete");
        ttsState = TtsState.stopped;
      });
    });
  }

  // checkRole() async {
  //   UserModel fetched = await SecureStorage.getUserData();
  //   if (fetched.data.roleName.toLowerCase() == 'farmer') {
  //     //Navigator.of(context).pop();
  //     Navigator.of(context).pushReplacement(MaterialPageRoute(
  //         builder: (BuildContext context) => DashBoardScreen()));
  //   } else if (fetched.data.roleName.toLowerCase() == 'frm' ||
  //       fetched.data.roleName.toLowerCase() == 'fro') {
  //     //Navigator.of(context).pop();
  //     Navigator.of(context).pushReplacement(MaterialPageRoute(
  //         builder: (BuildContext context) => DynamicDashboardScreen()));
  //   } else {
  //     SecureStorage.setUserData(null);
  //     Navigator.pushReplacement(
  //         context,
  //         MaterialPageRoute(
  //             builder: (BuildContext context) => new MyCustomForm()));
  //   }
  // }


  Widget screen;
  bool _IsLoading = false;
  var data = [];
  String strData = "";
  var l = null;
  Future<bool> ConvertFarmerDetails() async {
    try {
      await fetchFarmerDetailsResponse().then((value) => fd = value);

      // int count = authority.length;
      int count = 2;
      print(count);
      //authority = await _authority;

      dynamic fetch = await SecureStorage.getUserData();
      var menu = new DashboardMenuItem(context);
      // LandHoldingModel Land = LandHoldingModel.fromJson(jsonDecode(val));
        switch (fetch.data.roleName.toLowerCase()) {
      case 'farmer':
        setState(() {
          menuList = menu.farmermenuList;
        });
        break;
      case 'frm':
        setState(() {
          menuList = menu.frmMenuList;
        });
        break;
      case 'fro':
        setState(() {
          menuList = menu.frmMenuList;
        });
        break;
    }
    } on Exception catch (_, ex) {}
    return false;
  }

  String name = "";
  getDataLength() async {
    farmer = fetchFarmerDetailsResponse();
    fd = await farmer;
    data.add(fd.response.farmerName +
        " , " +
        fd.response.contactNo +
        " , " +
        fd.response.roleName);
    name = fd.response.roleName.toString();
    print(data);
    strData = DemoLocalization.of(context).translate('name') +
        " " +
        fd.response.farmerName +
        " , " +
        DemoLocalization.of(context).translate('contactno') +
        " " +
        fd.response.contactNo +
        " , " +
        DemoLocalization.of(context).translate('role') +
        " " +
        fd.response.roleName;
    print(strData);

    l = strData.split(',');
    print(l.length);
  }

  String btnText = "START";
  Future speak() async {
    // print(await flutterTts.getLanguages);

    await flutterTts.setLanguage("hi-IN");
    await flutterTts.setSpeechRate(0.2);
    await flutterTts.awaitSpeakCompletion(true);
    await getDataLength();
    // for (int i = 0; i < l.length; i++) {
    //   await flutterTts.speak(l[i]);
    //   print(l[i]);
    //   if (i == (l.length - 1) && _isPlaying = false) {
    //     _isPlaying = false;
    //     btnText = "Stop";

    //   }
    // }
    if (btnText == "START") {
      await setState(() => btnText = "STOP");
      for (int i = 0; i < l.length; i++) {
        await flutterTts.speak(l[i]);
        // print(l[i]);
        await flutterTts.awaitSpeakCompletion(true);

        print(i);
        if (i == (l.length - 1)) {
          await setState(() => btnText = "START");
        }
      }
    } else if (btnText == "STOP") {
      flutterTts.stop();
      await setState(() => btnText = "START");
    }
    return false;
  }

  Future stop() async {
    await flutterTts.stop();
    setState(() {
      _isPlaying = true;
    });
    setState(() => btnText = "START");
  }

  VoidCallback _changeLanguage(String language) {
    Locale _temp;
    switch (language) {
      case 'hi':
        _temp = Locale(language, 'IN');
        break;
      default:
        _temp = Locale(language, 'US');
    }
    // MyApp.setLocale(context, _temp);
  }

  Future<void> showInformationDialog(BuildContext context) async {
    List<FruitsList> fList = [
      FruitsList(index: 1, name: "English", langcode: "en", countrycode: "US"),
      FruitsList(index: 2, name: "Hindi", langcode: "hi", countrycode: "IN"),
      FruitsList(
        index: 3,
        name: "Marathi",
      ),
      FruitsList(
        index: 4,
        name: "Odia",
      ),
    ];
     String radioItem = 'English';
    int id = 1;
    return await showDialog(
        context: context,
        builder: (context) {
          bool isChecked = false;
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text(
                  DemoLocalization.of(context).translate('LanguageSelection')),
              actions: [
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text('OK'),
                )
              ],
              content: Form(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: fList
                    .map((data) => RadioListTile(
                          title: Text("${data.name}"),
                          groupValue: id,
                          value: data.index,
                          activeColor: Colors.green,
                          onChanged: (val) async {
                            await this.setState(() {
                              radioItem = data.name;
                              id = data.index;
                              Locale _temp;

                              _temp = Locale(data.langcode, data.countrycode);
                              MyApp.setLocale(context, _temp);

                              MyApp.setLocale(context, _temp);
                            });
                          },
                        ))
                    .toList(),
              )),
              // title: Text('Stateful Dialog'),
            );
          });
        });
  }

final color = Colors.blue;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text(DemoLocalization.of(context).translate('title')),
          backgroundColor: Colors.green,
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color: Colors.green,
                    // image: DecorationImage(
                    //     image: AssetImage("assets/images/Logo.png"),
                    //     fit: BoxFit.fitHeight,
                    //     alignment: Alignment.center),
                  ),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Material(
                                  color: Colors.grey[50],
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(90.0)),
                                  child: Image.asset(
                                    'assets/images/Logo.png',
                                    height: 100,
                                    width: 100,
                                  )),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        18.0, 0.0, 0.0, 0.0),
                                    child: Text("Saisanket Industries",
                                        style: TextStyle(
                                            fontSize: 18, color: Colors.white)),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        18.0, 3.0, 0.0, 0.0),
                                    child: Text(
                                      'FMS Farmer Application',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        19.0, 3.0, 0.0, 0.0),
                                    child: Text('App Version-v1.1',
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.white)),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
              ListTile(
                leading: Icon(
                  Icons.info_outline,
                  color: Colors.green,
                ),
                trailing: Icon(
                  Icons.arrow_right_sharp,
                  color: Colors.green,
                  size: 20,
                ),
                title:
                    Text(DemoLocalization.of(context).translate('mydetails')),
                onTap: () {
                  // Update the state of the app.
                  // ...
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FarmerDetailsScreen()));
                },
              ),
              Divider(
                thickness: 1,
                color: Colors.green,
              ),
              ListTile(
                leading: Icon(
                  Icons.eco,
                  size: 27,
                  color: Colors.green,
                ),
                trailing: Icon(
                  Icons.arrow_right_sharp,
                  color: Colors.green,
                  size: 20,
                ),
                title: Text(
                    DemoLocalization.of(context).translate('cropadvisory')),
                onTap: () {
                  // Update the state of the app.
                  // ...
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CropAdvisoryScreen()));
                },
              ),
              Divider(
                thickness: 1,
                color: Colors.green,
              ),
              ListTile(
                  leading: Icon(
                    Icons.language,
                    color: Colors.green,
                  ),
                  trailing: Icon(
                    Icons.arrow_right_sharp,
                    color: Colors.green,
                    size: 20,
                  ),
                  title: Text(DemoLocalization.of(context)
                      .translate('LanguageSelection')),
                  onTap: () {
                    //

                    Navigator.pop(context);
                    showInformationDialog(context);
                  }),
              Divider(
                thickness: 1,
                color: Colors.green,
              ),
              ListTile(
                leading: Icon(Icons.volume_up_rounded, color: Colors.green),
                trailing: Icon(
                  Icons.arrow_right_sharp,
                  color: Colors.green,
                  size: 20,
                ),
                title: Text(
                    DemoLocalization.of(context).translate('VoiceSelection')),
                onTap: () {
                  Navigator.pop(context);
                  showVoiceSelectDialog(context);
                },
              ),
              Divider(
                thickness: 1,
                color: Colors.green,
              ),
              ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Colors.green,
                ),
                trailing: Icon(
                  Icons.arrow_right_sharp,
                  color: Colors.green,
                  size: 20,
                ),
                title: Text(DemoLocalization.of(context).translate('logout')),
                onTap: () {
                  // Update the state of the app.
                  // ...

                  SecureStorage.setUserData(null);
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => new MyApp()));
                  // Navigator.of(context).popUntil((route) => route.isFirst);
                },
              ),
            ],
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                  "https://cdn.pixabay.com/photo/2016/09/21/04/46/barley-field-1684052_960_720.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          height: size.height,
          width: size.width,
          padding: EdgeInsets.all(5.0),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                Card(
                  shadowColor: Colors.black,
                  child: Row(
                    children: [
                      Expanded(
                        // width: MediaQuery.of(context).size.width/2,
                        child: (Image.asset(
                          "assets/images/Logo.png",
                          fit: BoxFit.contain,
                          height: 100,
                        )),
                      ),
                      Wrap(children: [
                        FutureBuilder(
                          future: ConvertFarmerDetails(),
                          builder: (context, FarmerDetails) {
                            if (FarmerDetails.hasData) {
                              return Container(
                                width: size.width * 0.65,
                                padding: const EdgeInsets.all(30.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      DemoLocalization.of(context)
                                              .translate('name') +
                                          " : " +
                                          fd.response.farmerName,
                                      textAlign: TextAlign.left,
                                    ),
                                    Text(
                                      DemoLocalization.of(context)
                                              .translate('contactno') +
                                          ": " +
                                          fd.response.contactNo,
                                      textAlign: TextAlign.left,
                                    ),
                                    Text(
                                      DemoLocalization.of(context)
                                              .translate('role') +
                                          ": " +
                                          fd.response.roleName,
                                      textAlign: TextAlign.left,
                                    ),
                                    Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0.0, 10.0, 0.0, 0.0),
                                              child: ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                  primary: Colors.green,
                                                  elevation: 3,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0)),
                                                ),
                                                child: Text(btnText),
                                                onPressed: () async {
                                                  await speak();
                                                },
                                              )),
                                          Expanded(
                                            child: ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                  primary: Colors.transparent,
                                                  shadowColor:
                                                      Colors.transparent),
                                              child: Text(
                                                "View More->",
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            FarmerDetailsScreen()));
                                              },
                                            ),
                                          ),
                                        ]),
                                  ],
                                ),
                              );
                            } else if (FarmerDetails.hasError) {
                              return Text("${FarmerDetails.error}");
                            }
                            return CircularProgressIndicator();
                          },
                        ),
                      ]),
                    ],
                  ),
                ),
                //getDashboardContent(menu.menuList)

                GridView.count(
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  primary: false,
                  padding: const EdgeInsets.all(1.5),
                  childAspectRatio: 1.10,
                  mainAxisSpacing: 1.1,
                  crossAxisSpacing: 1.0,
                  children: [
                    if (menuList != null)
                      for (dynamic value in menuList)
                        Card(
                            child: new InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              value.ScreenName));
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  verticalDirection: VerticalDirection.up,
                                  children: [
                                    Expanded(
                                      child: Center(
                                        child: Image.asset(
                                          value.ImagePath,
                                          height: 120,
                                          width: 100,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Center(
                                        child: Text(
                                          value.Menu,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.green,
                                              fontSize: 12),
                                          softWrap: true,
                                        ),
                                      ),
                                    ),
                                  ],
                                ))),
                  ],
                )

                // ListView.builder(
                //     shrinkWrap: true,
                //     itemCount: menu.menuList.length,
                //     itemBuilder: (BuildContext context, int index) {
                //       return Container(
                //         width: 100,
                //         height: 170,
                //         child: Card(
                //           child: new InkWell(
                //             onTap: () {
                //               Navigator.push(
                //                   context,
                //                   MaterialPageRoute(
                //                       builder: (context) =>
                //                            menu.menuList[index].ScreenName));
                //             },
                //             child: Container(
                //               child: GridView.count(
                //                 crossAxisCount: 2,
                //                 scrollDirection: Axis.horizontal,
                //                 children: [
                //                  Flexible(fit: FlexFit.loose,child: Image.asset(
                //                   "assets/images/cropadvi.png",
                //                   height: 150,
                //                   width: 100,
                //                 )),
                //                 Center(
                //                   child: Flexible(fit: FlexFit.loose,child: Text(
                //                       menu.menuList[index].Menu,
                //                       textAlign: TextAlign.center,
                //                       style: TextStyle(color: Colors.green, fontSize: 10))),
                //                 ),
                //                 ],
                //               ),
                //             )
                //             // child: Container(
                //             //     child: Column(
                //             //   children: [
                //             //     Flexible(fit: FlexFit.tight,child: Image.asset(
                //             //       "assets/images/cropadvi.png",
                //             //       height: 150,
                //             //       width: 100,
                //             //     )),
                //             //     Center(
                //             //       child: Flexible(fit: FlexFit.loose,child: Text(
                //             //           menu.menuList[index].Menu,
                //             //           textAlign: TextAlign.center,
                //             //           style: TextStyle(color: Colors.green, fontSize: 10))),
                //             //     ),
                //             //   ],
                //             // )),
                //           ),
                //         ),
                //       );
                //     }
                // )
                // ListView.builder(
                // itemCount: menu.menuList.length,
                // itemBuilder: (BuildContext context, int index) {
                //   return Card(
                //     child: Container(
                //         child: new InkWell(
                //             onTap: () {

                //             },
                //             child: Container(
                //               child: ListTile(
                //                 leading: SizedBox(
                //                     height: 150.0,
                //                     width: 100.0,
                //                     child: Image.network(
                //                       'http://fms.seprojects.in/' +
                //                           (values[index].path).toString(),
                //                       fit: BoxFit.fitWidth,
                //                       alignment: Alignment.centerLeft,
                //                     )),
                //                 title: Text(
                //                     ("Crop : " + values[index].cropName.toString()),
                //                     style: TextStyle(
                //                         fontWeight: FontWeight.w600,
                //                         fontSize: 19.0,
                //                         color: Colors.black),
                //                     textAlign: TextAlign.start),
                //                 subtitle: Text(
                //                   "Season :" + values[index].seasonName.toString(),
                //                   style: TextStyle(
                //                       fontSize: 16,
                //                       color: getColor(values[index].recommendation)),
                //                 ),
                //                 trailing: Icon(Icons.arrow_forward,
                //                     color: getColor(values[index].recommendation)),
                //               ),
                //             ))),
                //   );
                // })

                /*
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                      Container(
                        width: size.width * 0.485,
                        height: 170,
                        child: Card(
                          child: new InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CropAdvisoryScreen()));
                            },
                            child: Container(
                                child: Column(
                              children: [
                                Flexible(fit: FlexFit.tight,child: Image.asset(
                                  "assets/images/cropadvi.png",
                                  height: 150,
                                  width: 100,
                                )),
                                Center(
                                  child: Flexible(fit: FlexFit.loose,child: Text(
                                      DemoLocalization.of(context)
                                          .translate('CROPAdvisory'),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.green, fontSize: 10))),
                                ),
                              ],
                            )),
                          ),
                        ),
                      ),
                      
                    Container(
                      width: size.width * 0.485,
                      height: 170,
                      child: Card(
                          child: new InkWell(
                        //  onTap: () {
                        //  Navigator.push(
                        //      context,
                        //    MaterialPageRoute(
                        //      builder: (context) =>
                        //        CropEconomuicsScreen()));
                        //},
                        child: Container(
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/images/CropA.jpg",
                                height: 100,
                                width: 150,
                              ),
                              Center(
                                child: Text(
                                    DemoLocalization.of(context)
                                        .translate('SprinklerRequirement'),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.green)),
                              ),
                            ],
                          ),
                        ),
                      )),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: size.width * 0.485,
                      height: 170,
                      child: Card(
                          child: new InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => WUAScreen()));
                        },
                        child: Container(
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/images/WUA_icon.jpg",
                                height: 100,
                                width: 150,
                              ),
                              Center(
                                child: Text(
                                    DemoLocalization.of(context)
                                        .translate('WaterUserAssociation'),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.green)),
                              ),
                            ],
                          ),
                        ),
                      )),
                      // Crop Water Requirement
                    ),
                    Container(
                      width: size.width * 0.485,
                      height: 170,
                      child: Card(
                          child: new InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CropCalenderScreen()));
                        },
                        child: Container(
                          child: Column(
                            children: [
                              Image.asset(
                                "assets/images/calender.png",
                                height: 100,
                                width: 150,
                              ),
                              Center(
                                child: Text(
                                    DemoLocalization.of(context)
                                        .translate('CropCalendar'),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.green)),
                              ),
                            ],
                          ),
                        ),
                      )),
                    ),
                  ],
                ),
                Container(
                  width: size.width * 0.485,
                  height: 170,
                  child: Card(
                      child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CropWaterScreen()));
                    },
                    child: Container(
                      child: Column(
                        children: [
                          Image.asset(
                            "assets/images/WATER_REQU.png",
                            height: 100,
                            width: 150,
                          ),
                          Center(
                            child: Text(
                                DemoLocalization.of(context)
                                    .translate('CropWaterRequirement'),
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.green)),
                          ),
                        ],
                      ),
                    ),
                  )),
                )
              */
              ],
            ),
          ),
        ));
  }
}

class FruitsList extends _DynamicDashboardScreenState {
  String name;
  int index;
  String langcode;
  String countrycode;
  FruitsList({this.name, this.index, this.langcode, this.countrycode});
}

Future<void> showVoiceSelectDialog(BuildContext context) async {
  List<FruitsList> fList = [
    FruitsList(
      index: 1,
      name: "MALE",
    ),
    FruitsList(
      index: 2,
      name: "FEMALE",
    ),
  ];
  String radioItem = 'FEMALE';

  int id = 1;
  return await showDialog(
      context: context,
      builder: (context) {
        bool isChecked = false;
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            title:
                Text(DemoLocalization.of(context).translate('VoiceSelection')),
            content: Form(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: fList
                  .map((data) => RadioListTile(
                        title: Text("${data.name}"),
                        groupValue: id,
                        value: data.index,
                        activeColor: Colors.green,
                        onChanged: (val) {
                          setState(() {
                            radioItem = data.name;
                            id = data.index;
                          });
                        },
                      ))
                  .toList(),
            )),
            // title: Text('Stateful Dialog'),
            // actions: <Widget>[
            //   InkWell(
            //     child: Text('OK   '),
            //     onTap: () {
            //       if (_formKey.currentState.validate()) {
            //         // Do something like updating SharedPreferences or User Settings etc.
            //         Navigator.of(context).pop();
            //       }
            //     },
            //   ),
            // ],
          );
        });
      });
}

class FruitsListVoice {
  String name;
  int index;
  FruitsListVoice({this.name, this.index});
}
