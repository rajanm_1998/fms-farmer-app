import 'package:dynamic_text_highlighting/dynamic_text_highlighting.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:fms_mobileapp/Screens/CroppingPattern.dart';
import 'package:fms_mobileapp/Screens/Iconfarmermap.dart';
import 'package:fms_mobileapp/classes/language.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:translator/translator.dart';
import 'package:fms_mobileapp/services/storage.dart';

class FarmerDetailsScreen extends StatefulWidget {
  FarmerDetailsScreen({Key key}) : super(key: key);

  @override
  _FarmerDetailsState createState() => _FarmerDetailsState();
}

class _FarmerDetailsState extends State<FarmerDetailsScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  bool _isPlaying = true;

  @override
  void initState() {
    super.initState();
    getDataLength();

    _controller = AnimationController(
      lowerBound: 0.3,
      vsync: this,
      duration: Duration(seconds: 3),
    )..repeat();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  ErrorBuilder() {
    // ignore: prefer_const_constructors
    return Center(
      child: Text("LandHolding Details Not Found "),
    );
  }

  Widget listViewBuilder(BuildContext context, List<dynamic> values) {
    final textSize = MediaQuery.of(context).textScaleFactor * 15;
    return ListView.builder(
        itemCount: values.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return new Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              child: Card(
                  child: new InkWell(
                      child: Container(
                color: Colors.greenAccent,
                child: //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
                    Container(
                  decoration: BoxDecoration(
                      border: Border(
                    left: BorderSide(width: 2.0, color: Colors.green),
                    bottom: BorderSide(width: 2.0, color: Colors.green),
                  )),
                  width: double.maxFinite,
                  child: ListTile(
                    onTap: () async {
                      await SecureStorage.setLandId(values[index].landId);
                    },
                    title: Container(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: DynamicTextHighlighting(
                                text: DemoLocalization.of(context)
                                        .translate('khasaraNo') +
                                    " : " +
                                    values[index].roRSurveyNo.toString(),
                                softWrap: true,
                                highlights: [
                                  if (highlightsText != null) highlightsText
                                ],
                                color: Colors.yellow,
                                caseSensitive: false,
                                style: TextStyle(fontSize: textSize),
                              )),
                              Expanded(
                                  child: DynamicTextHighlighting(
                                text: DemoLocalization.of(context)
                                        .translate('area') +
                                    " : " +
                                    values[index]
                                        .roRTotalArea
                                        .toStringAsFixed(2),
                                softWrap: true,
                                highlights: [
                                  if (highlightsText != null) highlightsText
                                ],
                                color: Colors.yellow,
                                caseSensitive: false,
                                style: TextStyle(fontSize: textSize),
                              )),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: DynamicTextHighlighting(
                                text: DemoLocalization.of(context)
                                        .translate('villageName') +
                                    " : " +
                                    values[index].villageName.toString(),
                                softWrap: true,
                                highlights: [
                                  if (highlightsText != null) highlightsText
                                ],
                                color: Colors.yellow,
                                caseSensitive: false,
                                style: TextStyle(fontSize: textSize),
                              )),
                              Expanded(
                                  child: DynamicTextHighlighting(
                                text: DemoLocalization.of(context)
                                        .translate('chakno') +
                                    " : " +
                                    values[index].chakno.toString(),
                                softWrap: true,
                                highlights: [
                                  if (highlightsText != null) highlightsText
                                ],
                                color: Colors.yellow,
                                caseSensitive: false,
                                style: TextStyle(fontSize: textSize),
                              )),
                            ],
                          ),
                          Row(
                            children: [
                              Expanded(
                                  child: DynamicTextHighlighting(
                                text: DemoLocalization.of(context)
                                        .translate('soiltype') +
                                    " : " +
                                    values[index].soilType.toString(),
                                softWrap: true,
                                highlights: [
                                  if (highlightsText != null) highlightsText
                                ],
                                color: Colors.yellow,
                                caseSensitive: false,
                                style: TextStyle(fontSize: textSize),
                              )),
                            ],
                          ),
                          // Column(
                          //   children: [Icon(Icons.directions,
                          //   color: Colors.blueAccent,size: 31,)
                          //   ],
                          // )
                        ],
                      ),
                    ),
                    trailing: GestureDetector(
                      child: Icon(
                        Icons.directions,
                        color: Colors.blueAccent,
                        size: 31,
                      ),
                      onTap: () {
                        {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Farmermap()));
                        }
                      },
                    ),
                    // trailing: Icon(
                    //       Icons.directions,
                    //         color: Colors.blueAccent, size: 31),

                    // subtitle: Text(
                    //   DemoLocalization.of(context).translate('roRTotalArea') +
                    //       " : " +
                    //       values[index].roRTotalArea.toString() +
                    //       "\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('villageName') +
                    //       " : " +
                    //       values[index].villageName +
                    //       "\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('projectName') +
                    //       " : " +
                    //       values[index].projectName +
                    //       "\n" +
                    //       DemoLocalization.of(context).translate('chakno') +
                    //       " : " +
                    //       values[index].chakno.toString(),
                    //   style: TextStyle(
                    //       color: Colors.black, fontWeight: FontWeight.w500),
                    // ),
                    // leading: Icon(
                    //   Icons.album,
                    //   color: Colors.green,
                    // ),
                    // trailing: Icon(
                    //   Icons.arrow_forward,
                    //   size: 20,
                    //   color: Colors.green,
                    // ),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ))));

          // ListTile(
          //   leading: Text((values[index].id).toString()),
          //   title: Text((values[index].stateName).toString()),
          // );
        });
  }

  Widget rorDesignDetails(BuildContext context, List<dynamic> values) {
    final textSize = MediaQuery.of(context).textScaleFactor * 15;
    return ListView.builder(
        itemCount: values.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return new Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              child: Card(
                  child: new InkWell(
                      child: Container(
                color: Colors.greenAccent,
                child: //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
                    Container(
                  decoration: BoxDecoration(
                      border: Border(
                    left: BorderSide(width: 2.0, color: Colors.green),
                    bottom: BorderSide(width: 0.0, color: Colors.green),
                  )),
                  width: double.maxFinite,
                  child: ListTile(
                    onTap: () async {
                      await SecureStorage.setLandId(values[index].landId);
                    },
                    title: Container(
                        child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                DemoLocalization.of(context)
                                        .translate('khasaraNo') +
                                    " : " +
                                    values[index].khasaraNo.toString(),
                                softWrap: true,
                              ),
                            ),
                            Expanded(
                                child: Text(
                              DemoLocalization.of(context).translate('area') +
                                  " : " +
                                  values[index].totalArea.toStringAsFixed(2),
                              softWrap: true,
                            )),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                              DemoLocalization.of(context).translate('chakno') +
                                  " : " +
                                  values[index].chakno.toString(),
                              softWrap: true,
                            )),
                          ],
                        ),
                      ],
                    )),
                    trailing: GestureDetector(
                      child: Icon(
                        Icons.directions,
                        color: Colors.blueAccent,
                        size: 31,
                      ),
                      onTap: () {
                        {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Farmermap()));
                        }
                      },
                    ),

                    // subtitle: Text(
                    //   DemoLocalization.of(context).translate('roRTotalArea') +
                    //       " : " +
                    //       values[index].roRTotalArea.toString() +
                    //       "\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('villageName') +
                    //       " : " +
                    //       values[index].villageName +
                    //       "\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('projectName') +
                    //       " : " +
                    //       values[index].projectName +
                    //       "\n" +
                    //       DemoLocalization.of(context).translate('chakno') +
                    //       " : " +
                    //       values[index].chakno.toString(),
                    //   style: TextStyle(
                    //       color: Colors.black, fontWeight: FontWeight.w500),
                    // ),
                    // leading: Icon(
                    //   Icons.album,
                    //   color: Colors.green,
                    // ),
                    // trailing: Icon(
                    //   Icons.arrow_forward,
                    //   size: 20,
                    //   color: Colors.green,
                    // ),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ))));

          /*
          return new Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              child: Card(
                child: new InkWell(
                      child: Container(
                
                color: Colors.greenAccent,
                child: //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
                    Container(
                  decoration: BoxDecoration(
                      border: Border(
                    left: BorderSide(width: 3.0, color: Colors.green),
                    bottom: BorderSide(width: 4.0, color: Colors.green),
                  )),
                  width: double.maxFinite,
                  child: ListTile(
                    onTap: () async {
                      await SecureStorage.setLandId(values[index].landId);
                    },
                    title:Column(
                      children: [Row(children: [
                        Align(child: FittedBox(
                          fit: BoxFit.contain,
                          child: Text(
                       DemoLocalization.of(context).translate('khasaraNo') +
                            " : " +
                            values[index].roRSurveyNo.toString() + "\t\t",
                            textAlign: TextAlign.right,  
                            style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),),
                        ), alignment: Alignment.topLeft,),
                        Align(child: FittedBox(
                          fit: BoxFit.contain,
                          child: Text(
                       DemoLocalization.of(context).translate('totalArea') +
                            " : " +
                            values[index].roRTotalArea.toStringAsFixed(2),
                            textAlign: TextAlign.right,
                              style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),),
                        ), alignment: Alignment.topRight,),],),

                       (Row(children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Align(child: FittedBox(
                              fit: BoxFit.contain,
                      //         child: Text(
                      //  DemoLocalization.of(context).translate('chakno') +
                      //         " : " +
                      //         values[index].chakno.toString() +  "\t\t\t",
                      //           style: TextStyle(
                      //         color: Colors.black, fontWeight: FontWeight.w500),),
                            ), alignment: Alignment.bottomLeft,),
                          ),
                        Align(child: FittedBox(
                          fit: BoxFit.contain,
                      //     child: Text(
                      //  DemoLocalization.of(context).translate('subChakno') +
                      //       " : " +
                      //       values[index].subChakno.toString(),
                      //       style: TextStyle(
                      //       color: Colors.black, fontWeight: FontWeight.w500),),
                        ), alignment: Alignment.bottomRight),],)
                          ), 
                       
                    ]), 
                    // Text(
                    //    DemoLocalization.of(context).translate('surveyNo') +
                    //       " : " +
                    //       values[index].roRSurveyNo.toString() +
                    //       "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
                    //       DemoLocalization.of(context).translate('area') +
                    //       " : " +
                    //       values[index].roRTotalArea.toString() +
                    //       "\n\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('chakno') +
                    //       " : " +
                    //       values[index].chakno.toString() +
                    //       "\t\t\t\t\t\t\t" +
                    //       DemoLocalization.of(context).translate('subChakno') +
                    //       " : " +
                    //       values[index].subChakno.toString(),
                    //   style: TextStyle(
                    //       color: Colors.black, fontWeight: FontWeight.w500),
                    // ),
                    trailing: Icon(Icons.directions,color: Colors.blueAccent,size: 31),

                    // subtitle: Text(
                    //   DemoLocalization.of(context).translate('roRTotalArea') +
                    //       " : " +
                    //       values[index].roRTotalArea.toString() +
                    //       "\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('villageName') +
                    //       " : " +
                    //       values[index].villageName +
                    //       "\n" +
                    //       DemoLocalization.of(context)
                    //           .translate('projectName') +
                    //       " : " +
                    //       values[index].projectName +
                    //       "\n" +
                    //       DemoLocalization.of(context).translate('chakno') +
                    //       " : " +
                    //       values[index].chakno.toString(),
                    //   style: TextStyle(
                    //       color: Colors.black, fontWeight: FontWeight.w500),
                    // ),
                    // leading: Icon(
                    //   Icons.album,
                    //   color: Colors.green,
                    // ),
                    // trailing: Icon(
                    //   Icons.arrow_forward,
                    //   size: 20,
                    //   color: Colors.green,
                    // ),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              )
              )
              )
              );
              
            */

          // ListTile(
          //   leading: Text((values[index].id).toString()),
          //   title: Text((values[index].stateName).toString()),
          // );
        });
  }

  Widget ErrorViewBuilder(BuildContext context, String error) {
    return Center(
      child: Text(error),
    );
  }

  GifController controller;

  FlutterTts flutterTts = FlutterTts();

  Future<FarmerDetailsModel> farmer;
  FarmerDetailsModel fd;

  Future<List<FarmerLandModel>> land;
  List<FarmerLandModel> ld;
  var postData;

  var data = [];
  String strData = "";
  var l = null;

  ConvertFarmerDetails() async {
    farmer = fetchFarmerDetailsResponse();
    fd = await farmer;
    return fd;
  }

  ConvertLandDetails() async {
    land = fetchFarmerLandResponse();
    ld = await land;
    return fd;
  }

  getDataLength() async {
    var _farmer = fetchFarmerDetailsResponse();
    fd = await _farmer;
    var _land = fetchFarmerLandResponse();
    ld = await _land;
    await setState(() {
      farmer = _farmer;
      land = _land;
    });
    data.add(fd.response.farmerName +
        "," +
        fd.response.registrationNo +
        "," +
        fd.response.contactNo +
        "," +
        fd.response.address +
        "," +
        fd.response.network +
        "," +
        fd.response.projectName +
        "," +
        ld.first.roRSurveyNo +
        "," +
        ld.first.roRTotalArea.toStringAsFixed(2) +
        "," +
        ld.first.villageName +
        "," +
        ld.first.chakno);
    print(data);
    strData = DemoLocalization.of(context).translate('name') +
        "," +
        fd.response.farmerName +
        "," +
        DemoLocalization.of(context).translate('farmerreg') +
        "," +
        fd.response.registrationNo +
        "," +
        DemoLocalization.of(context).translate('contactno') +
        "," +
        fd.response.contactNo +
        "," +
        DemoLocalization.of(context).translate('address') +
        "," +
        fd.response.address +
        "," +
        DemoLocalization.of(context).translate('network') +
        "," +
        fd.response.network +
        "," +
        DemoLocalization.of(context).translate('projectName') +
        "," +
        fd.response.projectName;
    if (ld != null) {
      for (var ldetails in ld) {
        strData = strData +
            "," +
            DemoLocalization.of(context).translate('khasaraNo') +
            "," +
            ldetails.roRSurveyNo +
            "," +
            DemoLocalization.of(context).translate('villageName') +
            "," +
            ldetails.villageName +
            "," +
            DemoLocalization.of(context).translate('area') +
            "," +
            ldetails.totalArea.toStringAsFixed(2) +
            "," +
            DemoLocalization.of(context).translate('chakno') +
            "," +
            ldetails.chakno +
            "," +
            DemoLocalization.of(context).translate('soiltype') +
            "," +
            ldetails.soilType;
        ;
      }
    }

    print(strData);
    // print(strData.length);
    // print(data.length);
    l = strData.split(",");
    print(l.length);
  }

  String btnText = "PLAY";
  String imageLink = "assets/images/Farmer_ani1.png";
  dynamic index = 0;

  Color getColor(number) {
    if (number > 0 && number < 100) return Colors.red;
    if (number >= 100 && number < 200) return Colors.blue;
  }

  Future speak() async {
    // print(await flutterTts.getLanguages);

    await flutterTts.setLanguage("hi-IN");
    await flutterTts.setSpeechRate(0.2);
    await flutterTts.awaitSpeakCompletion(true);

    if (btnText == "PLAY") {
      await setState(() => btnText = "PAUSE");

      await setState(() {
        imageLink = "assets/images/Farmer_ani1.gif";
      });
      for (int i = index != 0 ? index + 1 : index; i < l.length; i++) {
        await flutterTts
            .awaitSpeakCompletion(true)
            .then((value) => setState(() {
                  highlightsText = l[i];
                }));

        await flutterTts.speak(l[i]);
        // await flutterTts.awaitSynthCompletion(true).then((value) => setState(() {
        //       highlightsText = l[i];
        //     }));
        await flutterTts.awaitSpeakCompletion(true).whenComplete(() {
          index = i;

          print(l[i]);
        });

        print(i);
        if (i == (l.length - 1)) {
          btnText = "PLAY";
          setState(() {
            imageLink = "assets/images/Farmer_ani1.png";
            highlightsText = null;
          });
        }
      }
    } else if (btnText == "PAUSE") {
      flutterTts.stop();
      print(index);
      btnText = "PLAY";
      setState(() {
        imageLink = "assets/images/Farmer_ani1.png";
      });
      // for(int i = index; i < l.length; i++){
      //  await flutterTts.speak(l[i]);
      // }
    }

    return false;
  }

  // Future speak() async {
  //   // print(await flutterTts.getLanguages);
  //   setState(() {
  //     imageLink = "assets/images/Farmer_ani1.gif";
  //   });

  //   await flutterTts.setLanguage("hi-IN");
  //   await flutterTts.setSpeechRate(0.2);
  //   await flutterTts.speak(DemoLocalization.of(context).translate('name') +
  //       ", " +
  //       fd.response.farmerName +
  //       ". " +
  //       DemoLocalization.of(context).translate('farmerreg') +
  //       ", " +
  //       fd.response.registrationNo +
  //       ". " +
  //       DemoLocalization.of(context).translate('contactno') +
  //       ", " +
  //       fd.response.contactNo +
  //       ". " +
  //       DemoLocalization.of(context).translate('address') +
  //       ", " +
  //       fd.response.address +
  //       ". " +
  //       DemoLocalization.of(context).translate('network') +
  //       ", " +
  //       fd.response.network +
  //       ". " +
  //       DemoLocalization.of(context).translate('project_name') +
  //       ", " +
  //       fd.response.projectName);
  // }

  Future stop() async {
    await flutterTts.stop();
    await setState(() {
      _isPlaying = true;
      index = 0;
      imageLink = "assets/images/Farmer_ani1.png";
      btnText = "PLAY";
      highlightsText = null;
    });
  }

  GoogleTranslator translator = new GoogleTranslator();

  VoidCallback _changeLanguage(String language) {
    Locale _temp;
    switch (language) {
      case 'hi':
        _temp = Locale(language, 'IN');
        break;

      default:
        _temp = Locale(language, 'US');
    }

    // MyApp.setLocale(context, _temp);
  }

  String highlightsText;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    // final double categoryHeight = size.height * 0.30;
    // onButtonPressed(String value) {
    //   setState(() {
    //   imageLink = value;
    //   });
    // }
    final textSize = MediaQuery.of(context).textScaleFactor * 15;
    return Scaffold(
        appBar: AppBar(
          title: Text(DemoLocalization.of(context).translate('title')),
          backgroundColor: Colors.green,
        ),
        // drawer: Drawer(
        //   child: ListView(
        //     padding: EdgeInsets.zero,
        //     children: [
        //       const DrawerHeader(
        //         padding: EdgeInsets.all(50.0),
        //         decoration: BoxDecoration(
        //           color: Colors.white,
        //           image: DecorationImage(
        //               image: AssetImage("assets/images/Logo.png"),
        //               fit: BoxFit.fitHeight,
        //               alignment: Alignment.center),
        //         ),
        //       ),
        //       ListTile(
        //         leading: Icon(
        //           Icons.info_outline,
        //           color: Colors.green,
        //         ),
        //         title:
        //             Text(DemoLocalization.of(context).translate('mydetails')),
        //         onTap: () {
        //           // Update the state of the app.
        //           // ...
        //           Navigator.push(
        //               context,
        //               MaterialPageRoute(
        //                   builder: (context) => FarmerDetailsScreen()));
        //         },
        //       ),
        //       ListTile(
        //         leading: Icon(
        //           Icons.crop,
        //           color: Colors.green,
        //         ),
        //         title: Text(
        //             DemoLocalization.of(context).translate('cropadvisory')),
        //         onTap: () {
        //           // Update the state of the app.
        //           // ...
        //           Navigator.push(
        //               context,
        //               MaterialPageRoute(
        //                   builder: (context) => CropAdvisoryScreen()));
        //         },
        //       ),
        //       ExpansionTile(
        //         leading: Icon(
        //           Icons.language,
        //           color: Colors.green,
        //         ),
        //         title: Text("Language"),
        //         children: <Widget>[
        //           ListTile(
        //             title: const Text("English"),
        //             onTap: () {
        //               _changeLanguage('en');
        //             },
        //           ),
        //           ListTile(
        //             title: const Text("Hindi"),
        //             onTap: () {
        //               _changeLanguage('hi');
        //             },
        //           ),
        //         ],
        //       ),
        //       ListTile(
        //         leading: Icon(
        //           Icons.logout,
        //           color: Colors.green,
        //         ),
        //         title: Text(DemoLocalization.of(context).translate('logout')),
        //         onTap: () {
        //           // Update the state of the app.
        //           // ...

        //           SecureStorage.setUserData(null);
        //           // Navigator.pushReplacement(
        //           //     context,
        //           //     MaterialPageRoute(
        //           //         builder: (BuildContext context) => new MyCustomForm()));
        //           Navigator.of(context).popUntil((route) => route.isFirst);
        //         },
        //       ),
        //     ],
        //   ),
        // ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      "https://cdn.pixabay.com/photo/2016/09/21/04/46/barley-field-1684052_960_720.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              width: size.width,
              height: size.height,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      FutureBuilder(
                        future: farmer,
                        builder: (context, FarmerDetails) {
                          if (FarmerDetails.hasData) {
                            return Container(
                              width: size.width * 0.72,
                              padding: EdgeInsets.all(5.0),
                              child: Container(
                                child: DynamicTextHighlighting(
                                  text: DemoLocalization.of(context)
                                          .translate('name') +
                                      ": " +
                                      "\n" +
                                      fd.response.farmerName +
                                      "\n" +
                                      DemoLocalization.of(context)
                                          .translate('farmerreg') +
                                      ":" +
                                      fd.response.registrationNo +
                                      "\n" +
                                      DemoLocalization.of(context)
                                          .translate('contactno') +
                                      ": " +
                                      fd.response.contactNo +
                                      "\n" +
                                      DemoLocalization.of(context)
                                          .translate('address') +
                                      ": " +
                                      fd.response.address +
                                      "\n" +
                                      DemoLocalization.of(context)
                                          .translate('network') +
                                      ": " +
                                      fd.response.network +
                                      "\n" +
                                      DemoLocalization.of(context)
                                          .translate('project_name') +
                                      ":" +
                                      fd.response.projectName,
                                  softWrap: true,
                                  highlights: [
                                    if (highlightsText != null) highlightsText
                                  ],
                                  color: Colors.yellow,
                                  caseSensitive: false,
                                  style: TextStyle(fontSize: textSize),
                                ),
                              ),
                            );
                          } else if (FarmerDetails.hasError) {
                            return Text("${FarmerDetails.error}");
                          }
                          return CircularProgressIndicator();
                        },
                      ),
                      Expanded(
                        child: Column(children: [
                          Image.asset(
                            imageLink,
                            fit: BoxFit.fitHeight,
                            height: 170,
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.blue,
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(9.0)),
                                ),
                                child: Text(btnText,
                                    style: TextStyle(height: 0.2)),
                                onPressed: () async {
                                  // await setState(() => _isPlaying = false);
                                  // await setState(() => btnText = "Stop");
                                  // await setState(() {imageLink = "assets/images/Farmer_ani1.gif"; });

                                  // await getDataLength();
                                  await speak();
                                  //setState(() => _isPlaying = false);
                                  //setState(() => btnText = "Stop");
                                  //_controller.reset();
                                  //flutterTts.completionHandler;
                                  // setState(() => _isPlaying = true);
                                  // setState(() {
                                  //   imageLink = "assets/images/Farmer_ani1.png";
                                  // });
                                  // setState(() => btnText = "Play");

                                  //_controller.repeat();
                                },
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.blue,
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(9.0)),
                                ),
                                child:
                                    Text("STOP", style: TextStyle(height: 0.2)),
                                onPressed: () async {
                                  await stop();
                                  // setState(() => _isPlaying = true);
                                  // setState(() {
                                  //   imageLink = "assets/images/Farmer_ani1.png";
                                  // });
                                  // setState(() => btnText = "Play");

                                  //_controller.repeat();
                                },
                              ))
                        ]),
                      ),
                    ],
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                            width: size.width,
                            color: Colors.blue[900],
                            child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(60.0, 0.0, 0.0, 0.0),
                                child: Text(
                                  DemoLocalization.of(context)
                                      .translate("LANDHOLDINGDETAILS(ROR)"),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ))),
                        FutureBuilder(
                          future: land,
                          builder: (context, landDetails) {
                            if (landDetails.hasData) {
                              return listViewBuilder(context, landDetails.data);
                            } else if (landDetails.hasError) {
                              return Text("No Land Details Found");
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                          width: size.width,
                          color: Colors.blue[900],
                          child: Padding(
                              padding: EdgeInsets.fromLTRB(60.0, 0.0, 0.0, 0.0),
                              child: Text(
                                DemoLocalization.of(context)
                                    .translate("LANDHOLDINGDETAILS(RORDESIGN)"),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ))),
                      FutureBuilder(
                        future: fetchFarmerLandDetailsResponse(),
                        builder: (context, landDetails) {
                          if (landDetails.hasData) {
                            return rorDesignDetails(context, landDetails.data);
                          } else if (landDetails.hasError) {
                            return Text("No Land Details Found");
                          } else {
                            return Center(child: CircularProgressIndicator());
                          }
                        },
                      ),
                    ],
                  )
                ],
              )),
        )
        // body: Container(
        //   height: size.height,
        //   // width: MediaQuery.of(context).size.width,
        //   child: Column(
        //     children: <Widget>[
        //   Row(
        //     children: [
        //       Wrap(
        //         children: [
        //           FutureBuilder (
        //     future: ConvertFarmerDetails(),
        //     builder: (context,FarmerDetails) {
        //      if(FarmerDetails.hasData){
        //        return Padding(
        //          padding: const EdgeInsets.all(30.0),
        //          child: Column(
        //            children: [
        //             Text(DemoLocalization.of(context).translate('name') + ": " + fd.response.farmerName),
        //             Text(DemoLocalization.of(context).translate('farmerreg') + ":" + fd.response.registrationNo),
        //             Text(DemoLocalization.of(context).translate('contactno') + ": " + fd.response.contactNo),
        //             Text(DemoLocalization.of(context).translate('address') + ": " + fd.response.address),
        //             Text(DemoLocalization.of(context).translate('network') + ": " + fd.response.network),
        //             Text(DemoLocalization.of(context).translate('project_name') + ":" + fd.response.projectName),
        //             ElevatedButton(
        //         style: ElevatedButton.styleFrom(
        //           primary: Colors.blue,
        //           elevation: 3,
        //           shape: RoundedRectangleBorder(
        //             borderRadius: BorderRadius.circular(32.0)),
        //           padding: EdgeInsets.all(20),
        //         ),
        //         onPressed:  () {
        //           speak();
        //         },
        //         child: const Text('PLAY VOICE', style: TextStyle()),

        //       ),
        //            ],
        //          ),
        //        );

        //      }else if(FarmerDetails.hasError){
        //        return Text("${FarmerDetails.error}");
        //      }
        //      return CircularProgressIndicator();
        //     },
        //   ),
        //   ]),
        //   Expanded(
        //     // width: MediaQuery.of(context).size.width/2,
        //     child: (
        //       Image.asset("assets/images/Farmer_ani1.gif",
        //       fit: BoxFit.cover,
        //       height: 300,)
        //     ),
        //   ),
        //    Expanded(
        //             child: FutureBuilder(
        //                 future: fetchFarmerLandResponse(),
        //                 builder: (context, landDetails) {
        //                   if (landDetails.hasData) {
        //                     return listViewBuilder(context, landDetails.data);
        //                   }
        //                 }),
        //       ),
        //   ],
        //   )
        //     ],)

        // ),
        );
  }
}
