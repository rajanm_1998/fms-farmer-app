
import 'package:flutter/material.dart';

class CropCalenderScreen extends StatefulWidget {
  const CropCalenderScreen({Key key}) : super(key: key);

  @override
  _CropCalenderScreenState createState() => _CropCalenderScreenState();
}

String dropdownValue = 'KHASARA NO:-___';

class _CropCalenderScreenState extends State<CropCalenderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          "SUB CHAK IRRIGATION CALENDAR",
          style: TextStyle(fontSize: 16.0),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Container(
           decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 0.3, color: Colors.black),
                      bottom: BorderSide(color: Colors.black, width: 0.2),
                      right: BorderSide(width: 0.3, color: Colors.black),
                      left: BorderSide(width: 0.3, color: Colors.black),
                    )),
          child: Column(
            children: [
                Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 0.0, 10.0, 0.0),
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      icon: const Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.green),
                      underline: Container(
                        height: 2,
                        color: Colors.green
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                        });
                      },
                      items: <String>['KHASARA NO:-___', 'Two', 'Free', 'Four']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )),
             Align(
               alignment: Alignment.topLeft,
               child: Padding(
                 padding: EdgeInsets.fromLTRB(20, 0.0, 0.0, 0.0),
                 child: Text("SUB CHAK CALENDAR",style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600),),
               ),
             ),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.black, width: 0.4,),
                                   )
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text('CHAK NO:-'),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(60, 0.0, 0.0, 0.0),
                              child: Text('SUB CHAK NO:-'),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                
                
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.black, width: 0.4,),
                     )
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child:Padding(
                          padding:EdgeInsets.all(4.0),
                          child: Text("TODAY :- DD/MM/YYYY"),
                          )),
                          Align(
                            alignment: Alignment.topLeft ,
                            child: Padding(
                              padding: EdgeInsets.all(4.0),
                              child:Text("TIME :- HH:MM  TO  HH:MM")
                            ),
                          ),
                          Align(
                          alignment: Alignment.topLeft,
                            child: Padding(
                              padding:EdgeInsets.all(4.0),
                              child: Text("DURATION :- 999 Min."),
                            ),
                          )
                    ],
                  ),
                ),  
              ),
              Container(
                decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.black, width: 0.4,),
                 )
                ),
                child: Column(
                  children: [
                  Row(
                    children: [
                      RaisedButton(
                         color: Colors.green,
                        child: Align(
                          child: Padding(
                            padding: EdgeInsets.all(0.0),
                            child: Text("PERVIOUS 8 DAYS", style: TextStyle(fontSize: 10,color: Colors.white),),                      
                          ),
                        ),
                        
                        
                      ),
                      Align(
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text("IRRIGATION SCHEDULE",style: TextStyle(fontSize: 13),),
                        ),
                      ),
                      Expanded(
                        child: RaisedButton(
                          child: Align(
                            child: Padding(
                              padding: EdgeInsets.all(0.0),
                              child: Text("NEXT 8 DAYS",style: TextStyle(fontSize: 10,color: Colors.white, )),
                            ),
                          ),
                                
                        ),
                      )
                      
                    ],
                  )
                  ],
                ),
              ),
               Container(
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 15),
                      child: FutureBuilder(
                        // future: fetchFamilyDetailsResponse(),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          print(snapshot.data);
                          if (snapshot.hasData) {
                            return CalendarDate(
                                context, snapshot.data.data.response);
                          } else if (snapshot.hasData == null) {
                            return Text("No Details Found");
                          } else if (snapshot.hasError) {
                            return Text("Error Occurs");
                          } else {
                            return Center(child: CircularProgressIndicator());
                          }
                        },
                      )),
            ],            
          ),
        ),
      ),
    );
  }

    Widget CalendarDate(BuildContext context, List<dynamic> values) {
    final Size size = MediaQuery.of(context).size;
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                child: Table(border: TableBorder.all(color: Colors.black),
                    // Allows to add a border decoration around your table
                    children: [
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            values[index].memberName.toString(),
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.w700),
                            textAlign: TextAlign.center,
                          ),
                        ),
                       Align(
                         alignment: Alignment.centerLeft,
                         child:Padding(
                           padding:EdgeInsets.all(0.0),
                           child:Text("DD/MM/YYYY")
                         )
                       ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Text("TIME :- HH:MM  TO  HH:MM"),
                        ),
                      ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child:Padding(
                            padding: EdgeInsets.all(0.0),
                            child:Text("DURATION :- 999 Min")
                          )),
                      ]),
                    ]),
              ),
            ],
          );
        });

   
  }
}
