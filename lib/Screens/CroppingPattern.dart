import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/FarmerDetails.dart';
import 'package:fms_mobileapp/classes/language.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/CroppingPatternModel_SubChakWise.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/styles.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:http/http.dart';
import 'package:translator/translator.dart';

class CroppingPatternDetailsScreen extends StatefulWidget {
  // final String chakNo;
  
  final String roRSurveyNo;
  CroppingPatternDetailsScreen({Key key, this.roRSurveyNo}) : super(key: key);

  @override
  CroppingPatternDetailsState createState() =>
      CroppingPatternDetailsState(roRSurveyNo: roRSurveyNo);
}

class CroppingPatternDetailsState extends State<CroppingPatternDetailsScreen> {
  CroppingPatternDetailsState({this.roRSurveyNo});
  String roRSurveyNo;

  Widget listViewBuilder(
      BuildContext context, List<CroppingPatternModel_SubChakWise> values) {
    return ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return new Padding(
              padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
              child: Card(
                  color: Colors.white10,
                  child: new InkWell(
                    child: Container(
                       decoration: BoxDecoration(
                         
                      border: Border(
                    bottom: BorderSide(width:3.0, color: Colors.black),
                    top: BorderSide(width: 3.0,color: Colors.black)
                  )),
                      height: 180,

                      padding: EdgeInsets.zero,
                      margin: EdgeInsets.zero,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("SubChak : "+
                            values[index].subChakno.toString()+
                            "\n"+ 
                            "Total Area :" +
                            values[index].croppingPatternsList[index].totalArea.toString(),
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w800,
                                color: Colors.black),
                          ),
                          Expanded(
                            child: Container(
                              
                              width: MediaQuery.of(context).size.width,
                              child: SizedBox(
                                height: 150,
                                // card height
                                child: PageView.builder(
                                  itemCount:
                                      values[index].croppingPatternsList.length,
                                  controller:
                                      PageController(viewportFraction: 0.7),
                                  onPageChanged: (int index) =>
                                      setState(() => _index = index),
                                  itemBuilder: (_, i) {
                                    return Transform.scale(
                                      scale: i == _index ? 1 : 0.9,
                                      child: Card(
                                        color: Colors.lightGreenAccent[50],
                                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                        elevation: 6,
                                        // shape: RoundedRectangleBorder(
                                        //     borderRadius:
                                        //         BorderRadius.circular(20)),
                                        child: Center(
                                          // child: Text(
                                          //   "Year :" +
                                          //       values[index]
                                          //           .croppingPatternsList[i]
                                          //           .year
                                          //           .toString() +
                                          //       "\n" +
                                          //       "CropName: " +
                                          //       values[index]
                                          //           .croppingPatternsList[i]
                                          //           .cropName
                                          //           .toString() +
                                          //       "\n"
                                          //           "Season Name :" +
                                          //       values[index]
                                          //           .croppingPatternsList[i]
                                          //           .seasonName
                                          //           .toString() +
                                          //       "\n"
                                          //           "Area :" +
                                          //       values[index]
                                          //           .croppingPatternsList[i]
                                          //           .area
                                          //           .toString() +
                                          //       "\n"
                                          //           "Yield :" +
                                          //       values[index]
                                          //           .croppingPatternsList[i]
                                          //           .yield
                                          //           .toString()
                                          //           .toString() +
                                          //       "\n"
                                          //           "InputCost :" +
                                          //       values[index]
                                          //           .croppingPatternsList[i]
                                          //           .inputCost
                                          //           .toString(),
                                          //   style: TextStyle(
                                          //       fontSize: 20,
                                          //       color: Colors.green,
                                          //       fontWeight: FontWeight.w400  ),
                                          //       ),   
                                        child: RichText(
                                          
                                          text: new TextSpan(
                                          
                                            children:<TextSpan>[   
                                              TextSpan(text: "Year: ",style: new TextStyle(color: Colors.green,fontSize:16) ),
                                              TextSpan(text: values[index]
                                                    .croppingPatternsList[i]
                                                    .year
                                                    .toString() + "\n" ,style: new TextStyle(color: Colors.green ,fontSize: 16) ),
                                              TextSpan(text: "Crop Name: ",style: new TextStyle(color: Colors.grey) ),
                                              TextSpan(text: values[index]
                                                    .croppingPatternsList[i]
                                                    .cropName
                                                    .toString() + "\n" ,style: new TextStyle(color: Colors.black) ),
                                              TextSpan(text: "Season Name: ",style: new TextStyle(color: Colors.grey) ),
                                              TextSpan(text: values[index]
                                                    .croppingPatternsList[i]
                                                    .seasonName
                                                    .toString() + "\n" ,style: new TextStyle(color: Colors.black) ),
                                              TextSpan(text: "Area: ",style: new TextStyle(color: Colors.grey) ),
                                              TextSpan(text: values[index]
                                                    .croppingPatternsList[i]
                                                    .area
                                                    .toString() + "\n" ,style: new TextStyle(color: Colors.black) ),
                                              TextSpan(text: "Yield: ",style: new TextStyle(color: Colors.grey) ),
                                              TextSpan(text: values[index]
                                                    .croppingPatternsList[i]
                                                    .yield
                                                    .toString() + "\n" ,style: new TextStyle(color: Colors.black) ),
                                              TextSpan(text: "InputCost: ",style: new TextStyle(color: Colors.grey) ),
                                              TextSpan(text: values[index]
                                                    .croppingPatternsList[i]
                                                    .inputCost
                                                    .toString() + "\n" ,style: new TextStyle(color: Colors.black)
                                                     ),
                                                     
                                            ]
                                            
                                            
                                          )
                                          
                                        )
                                        
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),

                              /*ListTile(
                          title: Text(("Year: " + values[index].landName + "\n" + 
                                       "Season Name: " + values[index].seasonName + "\n" + 
                                       "Crop Name: " + values[index].cropName + "\n" + 
                                       "Area: " + values[index].area + "\n").toString(),
                              style: TextStyle(
                                  fontSize: 15.0, color: Colors.black),
                              textAlign: TextAlign.start),
                        )*/
                            ),
                          ),
                        ],
                      ),

                      //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),

                      alignment: Alignment.center,
                    ),
                  )));
        });
  }

   void _changeLanguage(Language language) {
    Locale _temp;
    switch (language.languageCode) {
      case 'en':
        _temp = Locale(language.languageCode, 'US');
        break;

      case 'hi':
        _temp = Locale(language.languageCode, 'IN');
        break;

      default:
        _temp = Locale(language.languageCode, 'US');
    }

    // MyApp.setLocale(context, _temp);
  }

  int _index = 0;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    // final double categoryHeight = size.height * 0.30;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              /*Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FarmerDetailsScreen()));
            */
              Navigator.pop(context);
            },
          ),
          title: Text("ChakNo: " + roRSurveyNo),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.all(8.0),
                child: DropdownButton(
                  onChanged: (Language language) {
                    _changeLanguage(language);
                  },
                  underline: SizedBox(),
                  icon: Icon(
                    Icons.language,
                    color: Colors.white,
                  ),
                  items: Language.languageList()
                      .map<DropdownMenuItem<Language>>((lang) =>
                          DropdownMenuItem(
                            value: lang,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[Text(lang.name)],
                            ),
                          ))
                      .toList(),
                ))
          ],
        ),
        body: Container(
            decoration: BoxDecoration(image: backgroundImage
                // image: AssetImage("assets/images/light_image1.jpg"),
                // fit: BoxFit.cover),
                ),
            height: size.height,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: FutureBuilder(
                    future: fetchCroppingPatternListResponse(),
                    builder: (context, landDetails) {
                      if (landDetails.hasData) {
                        return listViewBuilder(context, landDetails.data);
                      } else if (landDetails.hasError) {
                        return Text("${landDetails.error}");
                      }
                      return Center(
                        child: new CircularProgressIndicator(),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}

//  class CroppingPatternDetailsState extends State<CroppingPatternDetailsScreen> {

Widget listViewBuilder(BuildContext context, List<dynamic> values) {
  return ListView.builder(
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        return new Padding(
            padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
            child: Card(
                child: new InkWell(
                    child: Container(
              height: 150,
              child: //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: ListTile(

                        title: Text(
                            ("Year: " +
                                    values[index].landName +
                                    "\n" +
                                    "Season Name: " +
                                    values[index].seasonName +
                                    "\n" +
                                    "Crop Name: " +
                                    values[index].cropName +
                                    "\n" +
                                    "Area: " +
                                    values[index].area +
                                    "\n")
                                .toString(),
                            style:
                                TextStyle(fontSize: 15.0, color: Colors.green),
                              
                                
                            textAlign: TextAlign.start),
                      )),
              alignment: Alignment.center,
            ))));
      });
}

@override
Widget build(BuildContext context) {
  final Size size = MediaQuery.of(context).size;

  // final double categoryHeight = size.height * 0.30;
  return Scaffold(
      appBar: AppBar(
        title: Text(DemoLocalization.of(context).translate('title')),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.all(8.0),
              child: DropdownButton(
                // onChanged: (Language language) {
                //   _changeLanguage(language);
                // },
                underline: SizedBox(),
                icon: Icon(
                  Icons.language,
                  color: Colors.white,
                ),
                items: Language.languageList()
                    .map<DropdownMenuItem<Language>>((lang) => DropdownMenuItem(
                          value: lang,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[Text(lang.name)],
                          ),
                        ))
                    .toList(),
              ))
        ],
      ),
      body: Container(
          decoration: BoxDecoration(
            image: backgroundImage,
          ),
          height: size.height,
          child: Column(
            children: <Widget>[
              Expanded(
                child: FutureBuilder(
                  future: fetchCroppingPatternResponse(),
                  builder: (context, landDetails) {
                    if (landDetails.hasData) {
                      return listViewBuilder(context, landDetails.data);
                    } else if (landDetails.hasError) {
                      return Text("${landDetails.error}");
                    }
                    return Center(
                      child: new CircularProgressIndicator(),
                    );
                  },
                ),
              ),
            ],
          )));
}
