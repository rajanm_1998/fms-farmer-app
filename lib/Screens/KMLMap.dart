// import 'dart:collection';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

// class KMLMAP extends StatefulWidget {
//   KMLMAP({Key key}) : super(key: key);

//   @override
//   _GMapState createState() => _GMapState();
// }

// class _GMapState extends State<KMLMAP> {
//   Set<Marker> _markers = HashSet<Marker>();
//   Set<Polygon> _polygons = HashSet<Polygon>();
//   List<LatLng> polygonLatLongs = List<LatLng>();
//   int _polygonIdCounter = 1;

//   static final CameraPosition _cameraPosition = CameraPosition(
//       bearing: 192.8334901395799,
//       target: LatLng(76.18521944699553, 24.32527449600105),
//       tilt: 59.440717697143555,
//       zoom: 19.151926040649414);

//   bool _showMapStyle = false;
//   bool _isPolygon = true;

//   GoogleMapController _mapController;
//   BitmapDescriptor _markerIcon;

//   @override
//   void initState() {
//     super.initState();
//     _setMarkerIcon();
//     _setPolygons();
//   }

//   void _setMarkerIcon() async {
//     _markerIcon = await BitmapDescriptor.fromAssetImage(
//         ImageConfiguration(), 'assets/noodle_icon.png');
//   }

//   void _toggleMapStyle() async {
//     //String style = await DefaultAssetBundle.of(context).loadString('assets/map_style.json');

//     {
//       _mapController.setMapStyle(null);
//     }
//   }

//   void _setPolygons() {
//     polygonLatLongs.add(LatLng(76.18521944699553, 24.32527449600105));
//     polygonLatLongs.add(LatLng(76.18522485724836, 24.3253397605078));
//     polygonLatLongs.add(LatLng(76.18522636239433, 24.32562108749878));
//     polygonLatLongs.add(LatLng(76.18522254368726, 24.32569187030174));
//     polygonLatLongs.add(LatLng(76.18520554743729, 24.32583987968718));
//     polygonLatLongs.add(LatLng(76.18520409295442, 24.32592960596005));
//     final String polygonIdVal = 'polygon_id_$_polygonIdCounter';
//     _polygons.add(
//       Polygon(
//         polygonId: PolygonId(polygonIdVal),
//         points: polygonLatLongs,
//         fillColor: Colors.yellow.withOpacity(0.15),
//         strokeWidth: 2,
//         strokeColor: Colors.yellow,
//       ),
//     );
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     _mapController = controller;

//     setState(() {
//       _markers.add(
//         Marker(
//             markerId: MarkerId("0"),
//             position: LatLng(76.18521944699553, 24.32527449600105),
//             infoWindow: InfoWindow(
//               title: "Borivali",
//               snippet: "National Park",
//             ),
//             icon: _markerIcon),
//       );
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         children: <Widget>[
//           GoogleMap(
//             onMapCreated: _onMapCreated,
//             initialCameraPosition: CameraPosition(
//               target: LatLng(76.18521944699553, 24.32527449600105),
//               zoom: 12,
//             ),
//             mapType: MapType.hybrid,
//             markers: _markers,
//             polygons: _polygons,
//             myLocationEnabled: true,
//             onTap: (point){
//               if(_isPolygon){
//                 setState(() {
//                   polygonLatLongs.add(point);
//                   _setPolygons();
//                 });
//               }
//             },
//           ),
//         ],
//       ),
//       floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//       floatingActionButton: FloatingActionButton(
//         tooltip: 'Increment',
//         child: Icon(Icons.map),
//         onPressed: () {
//           setState(() {
//             _showMapStyle = !_showMapStyle;
//           });

//           _toggleMapStyle();
//         },
//       ),
//     );
//   }
// }

import 'package:fms_mobileapp/models/FarmerMap/KmlFileTypeModel';
import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/models/FarmerMap/KmlMapModel.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:fms_mobileapp/utils/FarmerMapOperations/KMLOperations.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class KMLAPP extends StatefulWidget {
  @override
  _KMLAPPState createState() => _KMLAPPState();
}

class _KMLAPPState extends State<KMLAPP> {
  _KMLAPPState() {}

  @override
  void initState() {
    super.initState();
    getDropDataAsync();
  }

  GoogleMapController myController;

  String dropdownValue = "0";
  String filetype = "0";
  String Chakno = '';
  String SubChakno ='';

  void _onMapCreated(GoogleMapController controller) {
    myController = controller;
  }

  Future<List<FarmerLandModel>> land;
  Future<List<FarmerLandModel>> landData;
  List<FarmerLandModel> ld;
  Future<KmlFileTypeModel> kmlFile;
  KmlFileTypeModel kmlFileData;
  ConvertLandDetails() async {
    land = fetchFarmerLandResponse();
    ld = await land;
    return ld;
  }

  var landDetails;
  getDropDataAsync() async {
    landData = fetchFarmerLandDetailsResponse();
    var _ld = await landData;
    landDetails = _ld;
    kmlFile = fetchKmlFileType();
    var _kmlFileData = await kmlFile;
    try {
      await setState(() {
        ld = _ld;
        kmlFileData = _kmlFileData;
        dropdownValue = ld.first.landId.toString();
        filetype = kmlFileData.response.first.typeId.toString();
        Chakno = ld.first.chakno.toString();
        SubChakno = ld.first.subChakno.toString();
      });
      await _setPolygon(dropdownValue, filetype);
    } on Exception catch (_, ex) {}
  }
  // Maps

  Set<Polygon> _polygons = HashSet<Polygon>();
  Set<Marker> _markers = HashSet<Marker>();
  GoogleMapController _googleMapController;
  BitmapDescriptor _markerIcon;
  List<LatLng> polygonLatLngs = List<LatLng>();
  List<LatLng> markerLatLngs = List<LatLng>();
  double radius;

  //ids
  int _polygonIdCounter = 1;

  // Type controllers
  bool _isPolygon = true; //Default

  // Draw Polygon to the map
  Future<KmlMapModel> _setPolygon(String dropdownValue, String filetype) async {
    Future<KmlMapModel> kmlMap;
    try {
      _polygons = new Set<Polygon>();
      kmlMap =
          fetchKmlMapCoordinates(int.parse(dropdownValue), int.parse(filetype));
      var _kmlMapData = await kmlMap;
      dynamic fetched = await SecureStorage.getUserData();
      int uid = 475067;
      print(_kmlMapData.response.length);
      if(_kmlMapData.response.where((e)=> e.farmerName == uid).length != 0)
      _kmlMapData.response = _kmlMapData.response.where((e)=> e.farmerName == uid).toList();
      print(_kmlMapData.response.length);
      await setState(() {
        List<String> list =
            _kmlMapData.response.first.coordinates.split(',').toList();
        _kGooglePlex = CameraPosition(
          target: LatLng(double.parse(list[1]), double.parse(list[0])),
          zoom: 14.4746,
        );
      });

      String CoordinateStr = '';
      int counter = 1;
      for (var model in _kmlMapData.response) {
        polygonLatLngs = new List<LatLng>();
        markerLatLngs = new List<LatLng>();

        var data = model.coordinates.replaceAll(',0 ', ',');
        List<String> list = data.split(',').toList();
        list.remove("");
        int i = 0;
        while (i < list.length) {
          setState(() {
            polygonLatLngs
                .add(LatLng(double.parse(list[i + 1]), double.parse(list[i])));
            markerLatLngs
                .add(LatLng(double.parse(list[i + 1]), double.parse(list[i])));
          });

          //print(i.toString());
          i += 2;
        }

        await setState(() {
          final String polygonIdVal = 'polygon_id_$counter';
          _polygons.add(Polygon(
            polygonId: PolygonId(polygonIdVal),
            points: polygonLatLngs,
            strokeWidth: 1,
            strokeColor: Colors.yellow,
            fillColor: Colors.yellow.withOpacity(0.15),
          ));
          _markers.add(Marker(
            markerId: MarkerId(polygonIdVal),
            position: LatLng(markerLatLngs.first.latitude,markerLatLngs.first.longitude),
          ));
          
        });
        counter++;
      }
    } on Exception catch (_, ex) {}
  }

  // Widget _fabPolygon() {
  //   return FloatingActionButton.extended(
  //     onPressed: () {
  //       setState(() {
  //         polygonLatLngs.removeLast();
  //       });
  //     },
  //     icon: Icon(Icons.undo),
  //     label: Text('Undo point'),
  //     backgroundColor: Colors.orange,
  //   );
  // }

  static CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(24.32527449600105, 76.18521944699553),
    zoom: 14.4746,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Farmer Map"),
          backgroundColor: Colors.green,
          leading: BackButton(onPressed:() => Navigator.of(context).pop(),
        ),),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  DropdownButton<String>(
                    value: dropdownValue,
                    icon: const Icon(Icons.arrow_downward),
                    elevation: 16,
                    style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (String newValue) async {
                      var landmodel = ld.where((e) => e.landId.toString() == newValue).first;
                      setState(() {
                        dropdownValue = newValue;
                        Chakno = landmodel.chakno;
                        SubChakno = landmodel.subChakno;
                      });
                      await _setPolygon(dropdownValue, filetype);
                    },
                    items: ld != null
                        ? ld.map((item) {
                            return new DropdownMenuItem(
                              child: new Text(
                                item.khasaraNo,
                                style: TextStyle(fontSize: 13.0),
                              ),
                              value: item.landId.toString(),
                            );
                          }).toList()
                        : <String>["0"]
                            .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                  ),
                  DropdownButton<String>(
                    value: filetype,
                    icon: const Icon(Icons.arrow_downward),
                    elevation: 16,
                    style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (String newValue) async {
                      setState(() {
                        filetype = newValue;
                      });
                      await _setPolygon(dropdownValue, filetype);
                    },
                    items: kmlFileData != null
                        ? kmlFileData.response.map((item) {
                            return new DropdownMenuItem(
                              child: new Text(
                                item.type,
                                style: TextStyle(fontSize: 13.0),
                              ),
                              value: item.typeId.toString(),
                            );
                          }).toList()
                        : <String>["0"]
                            .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                  ),
                ],
              ),
               Table(
                 children: [
                  TableRow(
                    children: [ 
                      Text("Chakno: $Chakno",style: TextStyle(fontSize: 13.0),),

                      Text("Subchak No: $SubChakno",style: TextStyle(fontSize: 13.0)),
                    ]
                  )
                ],
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.75,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    style: BorderStyle.solid,
                  ),
                ),
                child: GoogleMap(
                  mapType: MapType.hybrid,
                  initialCameraPosition: _kGooglePlex,
                  polygons: _polygons,
                  markers: _markers 
                ),
              ),
            ],
          ),
        ),
      );
  }
}
