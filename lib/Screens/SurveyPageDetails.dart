import 'package:flutter/material.dart';

class SurveyPageDetails  extends StatefulWidget {
  const SurveyPageDetails ({ Key key }) : super(key: key);

  @override
  _SurveyPageDetailsState createState() => _SurveyPageDetailsState();
}

class _SurveyPageDetailsState extends State<SurveyPageDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Survey Page Details"),
        backgroundColor: Colors.green,
        ),
        body: Container(
        child: Column(
          children: [
            Padding(padding:EdgeInsets.all(30)),
            Text("Survey Page Details" ,style: TextStyle(color: Colors.green,fontSize:25,fontWeight:FontWeight.bold  ),),
            Image.asset("assets/images/survey_icon.png"),
            
            
          ],
        ),
        
       ),
    
      
    );
      
    
  }
}