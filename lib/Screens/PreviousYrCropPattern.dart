import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:fms_mobileapp/Screens/CroppingPattern.dart';
import 'package:fms_mobileapp/classes/language.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternDropDownModel.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternModel.dart';
import 'package:fms_mobileapp/models/CroppingPatternModel_SubChakWise.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:translator/translator.dart';
import 'package:fms_mobileapp/services/storage.dart';

class PreviousYrCropPatternScreen extends StatefulWidget {
  PreviousYrCropPatternScreen({Key key}) : super(key: key);

  @override
  _PreviousYrCropPatternState createState() => _PreviousYrCropPatternState();
}

class _PreviousYrCropPatternState extends State<PreviousYrCropPatternScreen> {
  String area = '';
  String yield = '';
  String marketPlace = '';
  String marketRate = '';
  String irrigationTypeName = '';
  String irrigationFacilityName = '';
  String actualIncome = '';
  String actualExpenses = '';
  String actualNetProfit = '';
  String estimateFloodGrossIncome = '';
  String estimateFloodYield = '';
  String estimateSprinklerGrossIncome = '';
  String estimateSprinklerYield = '';

  String seasondropdownValue = "0";
  String yeardropdownValue = "0";
  String surveydropdownValue = "0";
  String cropdropdownValue = "0";
  _PreviousYrCropPatternState() {}

  @override
  void initState() {
    getdropDownDataAsync();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  CropPatternDropDownModel _list;
  List<CommonList> _unsortedList = new List<CommonList>();
  List<CommonList> YearList = new List<CommonList>();
  List<CommonList> SeasonList = new List<CommonList>();
  List<CommonList> SurveyList = new List<CommonList>();
  List<CommonList> CropList = new List<CommonList>();
  CommonList SelectedValue = new CommonList();

  CropPatternDropDownModel cpPattern;
  Future<CropPatternDropDownModel> cropPattern;
  CropPatternDropDownModel cp = new CropPatternDropDownModel();
  Future<CroppingPatternModel> cropPatternDetails;
  CroppingPatternModel cpDetails = new CroppingPatternModel();

  getCropPatternDetailsAsync() async {
    Future.delayed(Duration(seconds: 5));
    try {
      cropPatternDetails = fetchCropPatternDetailResponse(
          int.tryParse(yeardropdownValue),
          int.tryParse(seasondropdownValue),
          int.tryParse(surveydropdownValue),
          int.tryParse(cropdropdownValue));
      if (cropPatternDetails != null) {
        var data = await cropPatternDetails;
        if (data != null) {
          await setState(() {
            cpDetails = data;
            area = data.area;
            yield = data.yield;
            marketPlace = data.marketPlace;
            marketRate = data.marketRate;
            irrigationTypeName = data.irrigationTypeName;
            irrigationFacilityName = data.irrigationFacilityName;
            actualIncome = data.actualIncome.toString();
            actualExpenses = data.actualExpenses.toString();
            actualNetProfit = data.actualNetProfit.toString();
            estimateFloodGrossIncome = data.estimateFloodGrossIncome.toString();
            estimateFloodYield = data.estimateFloodYield.toString();
            estimateSprinklerGrossIncome =
                data.estimateSprinklerGrossIncome.toString();
            estimateSprinklerYield = data.estimateSprinklerYield.toString();
          });
        }
      } else
        cropPatternDetails = null;
    } on Exception catch (_, ex) {}
    return cropPatternDetails;
  }

  getdropDownDataAsync() async {
    try {
      cropPattern = fetchCropPatternDropDownResponse();
      if (cropPattern != null) {
        cp = await cropPattern;
        if (cp != null) {
          await setState(() {
            _list = cp;
            _unsortedList = cp.list;
            YearList = cp.yearList;
            SeasonList = cp.seasonList;
            SurveyList = cp.surveyList;
            CropList = cp.cropList;
            SelectedValue = CropList.first;
            yeardropdownValue = SelectedValue.year.toString();
            seasondropdownValue = SelectedValue.seasonId.toString();
            surveydropdownValue = SelectedValue.landId.toString();
            cropdropdownValue = SelectedValue.cropId.toString();
            _CropName = SelectedValue.cropName;
          });
          await getCropPatternDetailsAsync();
        }
      }
    } on Exception catch (_, ex) {}
  }

  ErrorBuilder() {
    // ignore: prefer_const_constructors
    return Center(
      child: Text("Details Not Found "),
    );
  }

  Widget ErrorViewBuilder(BuildContext context, String error) {
    return Center(
      child: Text(error),
    );
  }

  // GifController controller;

  // VoidCallback _changeLanguage(String language) {
  //   Locale _temp;
  //   switch (language) {
  //     case 'hi':
  //       _temp = Locale(language, 'IN');
  //       break;

  //     default:
  //       _temp = Locale(language, 'US');
  //   }

  //   MyApp.setLocale(context, _temp);
  // }

  final _Cropkey = GlobalKey();
  String _CropName = '';
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    // final double categoryHeight = size.height * 0.30;

    // onButtonPressed(String value) {
    //   setState(() {
    //     imageLink = value;
    //   });
    // }

    return Scaffold(
        appBar: AppBar(
          title: Text(DemoLocalization.of(context).translate('title')),
          backgroundColor: Colors.green,
        ),
        body: SingleChildScrollView(
            child: Container(
                width: size.width,
                height: size.height,
                child: SingleChildScrollView(
                  child: Column(
                    children: [ 
                      Container(
                        width: size.width,
                        color: Colors.blue[900],
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(40.0, 0.0, 0.0, 0.0),
                            child: Text(
                              (DemoLocalization.of(context).translate(
                                  'PREVIOUSYEARSCROPPINGPATTERN&ESTIMATED')),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 18),
                            ))),
                      Wrap(
                        spacing: 10.0,
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              child: DropdownButton<String>(
                                value: yeardropdownValue,
                                icon: const Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.deepPurple),
                                underline: Container(
                                  height: 2,
                                  color: Colors.deepPurpleAccent,
                                ),
                                onChanged: (String newValue) async {
                                  try {
                                    var _model = _unsortedList
                                        .where((element) =>
                                            element.year.toString() == newValue)
                                        .first;
                                
                                    var _seasonList = cp.seasonList
                                        .where(
                                            (s) => s.seasonId == _model.seasonId)
                                        .toList();
                                    var _surveyList = cp.surveyList
                                        .where((s) => s.landId == _model.landId)
                                        .toList();
                                    var _cropList = cp.cropList
                                        .where((s) => s.cropId == _model.cropId)
                                        .toList();
                                    setState(() {
                                      yeardropdownValue = newValue;
                                      SeasonList = _seasonList;
                                      seasondropdownValue =
                                          _seasonList.first.seasonId.toString();
                                      SurveyList = _surveyList;
                                      surveydropdownValue =
                                          _surveyList.first.landId.toString();
                                      CropList = _cropList;
                                      cropdropdownValue =
                                          _cropList.first.cropId.toString();
                                      _CropName = _cropList.first.cropName;
                                    });
                                    await getCropPatternDetailsAsync();
                                  } on Exception catch (_, ex) {}
                                },
                                items: YearList != null
                                    ? YearList.map((item) {
                                        return new DropdownMenuItem(
                                          child: new Text(
                                            item.year.toString(),
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          value: item.year.toString(),
                                        );
                                      }).toList()
                                    : <String>["0"].map<DropdownMenuItem<String>>(
                                        (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              child: DropdownButton<String>(
                                value: seasondropdownValue,
                                icon: const Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.deepPurple),
                                underline: Container(
                                  height: 2,
                                  color: Colors.deepPurpleAccent,
                                ),
                                onChanged: (String newValue) async {
                                  try {
                                    var _model = _unsortedList
                                        .where((element) =>
                                            element.seasonId.toString() ==
                                                newValue &&
                                            element.year.toString() ==
                                                yeardropdownValue)
                                        .first;
                                
                                    var _surveyList = cp.surveyList
                                        .where((s) => s.landId == _model.landId)
                                        .toList();
                                    var _cropList = cp.cropList
                                        .where((s) => s.cropId == _model.cropId)
                                        .toList();
                                    await setState(() {
                                      seasondropdownValue = newValue;
                                      SurveyList = _surveyList;
                                      surveydropdownValue =
                                          SurveyList.first.landId.toString();
                                      CropList = _cropList;
                                      cropdropdownValue =
                                          CropList.first.cropId.toString();
                                      _CropName = CropList.first.cropName;
                                    });
                                    await getCropPatternDetailsAsync();
                                  } on Exception catch (_, ex) {}
                                },
                                items: SeasonList != null
                                    ? SeasonList.map((item) {
                                        return new DropdownMenuItem(
                                          child: new Text(
                                            item.seasonName,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          value: item.seasonId.toString(),
                                        );
                                      }).toList()
                                    : <String>["0"].map<DropdownMenuItem<String>>(
                                        (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              child: DropdownButton<String>(
                                value: surveydropdownValue,
                                icon: const Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.deepPurple),
                                underline: Container(
                                  height: 2,
                                  color: Colors.deepPurpleAccent,
                                ),
                                onChanged: (String newValue) async {
                                  try {
                                    var _model = _unsortedList
                                        .where((element) =>
                                            element.landId.toString() ==
                                                newValue &&
                                            element.seasonId.toString() ==
                                                seasondropdownValue &&
                                            element.year.toString() ==
                                                yeardropdownValue)
                                        .first;
                                
                                    var _cropList = cp.cropList
                                        .where((s) => s.cropId == _model.cropId)
                                        .toList();
                                    await setState(() {
                                      surveydropdownValue = newValue;
                                      CropList = _cropList;
                                      cropdropdownValue =
                                          CropList.first.cropId.toString();
                                      _CropName = CropList.first.cropName;
                                    });
                                    await getCropPatternDetailsAsync();
                                  } on Exception catch (_, ex) {}
                                },
                                items: SurveyList != null
                                    ? SurveyList.map((item) {
                                        return new DropdownMenuItem(
                                          child: new Text(
                                            item.surveyNo,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          value: item.landId.toString(),
                                        );
                                      }).toList()
                                    : <String>["0"].map<DropdownMenuItem<String>>(
                                        (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              child: DropdownButton<String>(
                                value: cropdropdownValue,
                                key: _Cropkey,
                                icon: const Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.deepPurple),
                                underline: Container(
                                  height: 2,
                                  color: Colors.deepPurpleAccent,
                                ),
                                onChanged: (String newValue) async {
                                  try {
                                    setState(() {
                                      cropdropdownValue = newValue;
                                      _CropName = CropList.where((c) =>
                                              c.cropId.toString() == newValue)
                                          .first
                                          .cropName;
                                    });
                                    await getCropPatternDetailsAsync();
                                  } on Exception catch (_, ex) {}
                                },
                                items: CropList != null
                                    ? CropList.map((item) {
                                        return new DropdownMenuItem(
                                          child: new Text(
                                            item.cropName,
                                            style: TextStyle(fontSize: 13.0),
                                          ),
                                          value: item.cropId.toString(),
                                        );
                                      }).toList()
                                    : <String>["0"].map<DropdownMenuItem<String>>(
                                        (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                              )),
                        ],
                      ),
                      FutureBuilder(
                          future: cropPatternDetails,
                          builder: (context, snap) {
                            try {
                              if (snap.hasData) {
                                if (snap.data != null)
                                  return Padding(
                                    padding: EdgeInsets.all(0.0),
                                    child: Column(
                                      children: [
                                        Wrap(
                                          alignment: WrapAlignment.center,
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    0.0, 0.0, 0.0, 0.0),
                                                child: Card(
                                                    child: Container(
                                                        decoration: BoxDecoration(
                                                            color: Colors
                                                                .greenAccent,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  width: 0.2,
                                                                  color: Colors
                                                                      .black),
                                                              bottom: BorderSide(
                                                                  width: 0.2,
                                                                  color: Colors
                                                                      .black),
                                                              right: BorderSide(
                                                                  width: 0.2,
                                                                  color: Colors
                                                                      .black),
                                                              left: BorderSide(
                                                                  width: 0.2,
                                                                  color: Colors
                                                                      .black),
                                                            )),
                                                        child: Padding(
                                                          padding: EdgeInsets.all(
                                                              10.0),
                                                          child: Text(
                                                            DemoLocalization.of(
                                                                            context)
                                                                        .translate(
                                                                            'cropName') +
                                                                    " : " +
                                                                    "\n" +
                                                                    _CropName ??
                                                                '',
                                                            style: TextStyle(
                                                                fontSize: 10,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        )))),
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    0.0, 0.0, 0.0, 0.0),
                                                child: Card(
                                                  child: Container(
                                                      decoration: BoxDecoration(
                                                          color:
                                                              Colors.greenAccent,
                                                          border: Border(
                                                            top: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                            bottom: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                            right: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                            left: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                          )),
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.all(10.0),
                                                        child: Text(
                                                          DemoLocalization.of(
                                                                          context)
                                                                      .translate(
                                                                          'area') +
                                                                  " : " +
                                                                  "\n" +
                                                                  cpDetails.area
                                                                      .toString() ??
                                                              '',
                                                          style: TextStyle(
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      )),
                                                )),
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    0.0, 0.0, 0.0, 0.0),
                                                child: Card(
                                                  child: Container(
                                                      decoration: BoxDecoration(
                                                          color:
                                                              Colors.greenAccent,
                                                          border: Border(
                                                            top: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                            bottom: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                            right: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                            left: BorderSide(
                                                                width: 0.2,
                                                                color:
                                                                    Colors.black),
                                                          )),
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.all(10.0),
                                                        child: Text(
                                                          DemoLocalization.of(
                                                                          context)
                                                                      .translate(
                                                                          'MarketRateRS/QTL') +
                                                                  " : " +
                                                                  "\n" +
                                                                  cpDetails
                                                                      .marketRate ??
                                                              '',
                                                          style: TextStyle(
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      )),
                                                )),
                                          ],
                                        ),
                                        Container(
                                            width: size.width,
                                            color: Colors.blue[900],
                                            child: Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    40.0, 0.0, 0.0, 0.0),
                                                child: Text(
                                                  DemoLocalization.of(context)
                                                      .translate(
                                                          'ActualCropEconomics'),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 17),
                                                ))),
                                        Container(
                                            width: size.width,
                                            decoration: BoxDecoration(
                                                color: getColor(cpDetails.irrigationTypeName),
                                                border: Border(
                                                  top: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  bottom: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  right: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  left: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                )),
                                            child: Column(
                                              children: [
                                                Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        2.0, 5.0, 0.0, 5.0),
                                                    child: Text(
                                                      DemoLocalization.of(context)
                                                              .translate(
                                                                  'IrrigationType') +
                                                          " : " +
                                                          cpDetails
                                                              .irrigationTypeName,
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ),
                                                Align(
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        2.0, 5.0, 0.0, 5.0),
                                                    child: Text(
                                                      DemoLocalization.of(context)
                                                              .translate(
                                                                  'IrrigationFacility') +
                                                          " : " +
                                                          cpDetails
                                                              .irrigationFacilityName,
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  alignment: Alignment.topLeft,
                                                ),
                                                Align(
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        2.0, 5.0, 0.0, 5.0),
                                                    child: Text(
                                                      DemoLocalization.of(context)
                                                              .translate(
                                                                  'ActualYield') +
                                                          " : " +
                                                          cpDetails.yield,
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  alignment: Alignment.topLeft,
                                                ),
                                                Align(
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        2.0, 5.0, 0.0, 5.0),
                                                    child: Text(
                                                      DemoLocalization.of(context)
                                                              .translate(
                                                                  'INCOME') +
                                                          " : " +
                                                          cpDetails.actualIncome
                                                              .toString(),
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  alignment: Alignment.topLeft,
                                                ),
                                                Align(
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        2.0, 5.0, 0.0, 5.0),
                                                    child: Text(
                                                      DemoLocalization.of(context)
                                                              .translate(
                                                                  'EXPENSES') +
                                                          " : " +
                                                          cpDetails.actualExpenses
                                                              .toString(),
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  alignment: Alignment.topLeft,
                                                ),
                                                Align(
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        2.0, 5.0, 0.0, 5.0),
                                                    child: Text(
                                                      DemoLocalization.of(context)
                                                              .translate(
                                                                  'NETPROFIT') +
                                                          " : " +
                                                          cpDetails
                                                              .actualNetProfit
                                                              .toString(),
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  alignment: Alignment.topLeft,
                                                ),
                                              ],
                                            )),
                                        Container(
                                            width: size.width,
                                            color: Colors.blue[900],
                                            child: Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                    40.0, 0.0, 0.0, 0.0),
                                                child: Text(
                                                  DemoLocalization.of(context)
                                                      .translate(
                                                          'EstimatedCropEconomics'),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 17),
                                                ))),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 0.0, 0.0, 0.0),
                                          child: Container(
                                            width: size.width,
                                            decoration: BoxDecoration(
                                                color: Colors.orangeAccent,
                                                border: Border(
                                                  top: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  bottom: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  right: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  left: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                )),
                                            child: Column(
                                              children: [
                                              Align(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      2.0, 5.0, 0.0, 5.0),
                                                  child: Text(
                                                    DemoLocalization.of(context)
                                                            .translate(
                                                                'IrrigationType') +
                                                        ":" +
                                                        "FLOW/FLOOD",
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                alignment: Alignment.topLeft,
                                              ),
                                              Align(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      2.0, 5.0, 0.0, 5.0),
                                                  child: Text(
                                                    DemoLocalization.of(context)
                                                            .translate(
                                                                'ActualYield') +
                                                        " : " +
                                                        cpDetails.yield,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                alignment: Alignment.topLeft,
                                              ),
                                              Align(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      2.0, 5.0, 0.0, 5.0),
                                                  child: Text(
                                                    DemoLocalization.of(context)
                                                            .translate(
                                                                'GrossIncome') +
                                                        " : " +
                                                        cpDetails
                                                            .estimateFloodGrossIncome
                                                            .toString(),
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                alignment: Alignment.topLeft,
                                              ),
                                            ]),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 0.0, 0.0, 0.0),
                                          child: Container(
                                            width:
                                                MediaQuery.of(context).size.width,
                                            decoration: BoxDecoration(
                                                color: Colors.greenAccent,
                                                border: Border(
                                                  top: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  bottom: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  right: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                  left: BorderSide(
                                                      width: 0.2,
                                                      color: Colors.black),
                                                )),
                                            child: Column(
                                              verticalDirection: VerticalDirection.down,children: [
                                              Align(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      2.0, 5.0, 0.0, 5.0),
                                                  child: Text(
                                                    DemoLocalization.of(context)
                                                            .translate(
                                                                'IrrigationType') +
                                                        ": " +
                                                        "SPRINKLER",
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                alignment: Alignment.topLeft,
                                              ),
                                              Align(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      2.0, 5.0, 0.0, 5.0),
                                                  child: Text(
                                                    DemoLocalization.of(context)
                                                            .translate(
                                                                'ActualYield') +
                                                        " : " +
                                                        cpDetails
                                                            .estimateFloodYield
                                                            .toString(),
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                alignment: Alignment.topLeft,
                                              ),
                                              Align(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      2.0, 5.0, 0.0, 5.0),
                                                  child: Text(
                                                    DemoLocalization.of(context)
                                                            .translate(
                                                                'GrossIncome') +
                                                        " : " +
                                                        cpDetails
                                                            .estimateSprinklerGrossIncome
                                                            .toString(),
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                alignment: Alignment.topLeft,
                                              ),
                                            ]),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                else
                                  throw new Exception();
                              } else if (snap.hasError)
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              else if (!snap.hasData)
                                return Container(
                                    height: size.height * 0.50,
                                    child: Center(
                                      child: CircularProgressIndicator(),
                                    ));
                              else
                                return Container(
                                  child: CircularProgressIndicator(),
                                );
                            } on Exception catch (_, ex) {
                              return Center(
                                child: Text("No Data Found"),
                              );
                            }
                          }),
                      //    Expanded(child: FutureBuilder(
                      //   future: fetchFarmerLandResponse(),
                      //   builder: (context, landDetails) {
                      //     if (landDetails.hasData) {
                      //       return listViewBuilder(context, landDetails.data);
                      //     } else if (landDetails.hasError) {
                      //       return ErrorViewBuilder(
                      //           context, "No Land Details Found");
                      //     }
                      //     return Center(
                      //       child: CircularProgressIndicator(),
                      //     );
                      //   },
                      // ),
                      // )
                    ],
                  ),
                )
            
                //  FutureBuilder(
                //      future: fetchCropPatternDropDownResponse(),
                //      builder: (context, landDetails) {
                //           if (landDetails.hasData) {
                //             cpPattern = landDetails.data;
            
                //           } else if (landDetails.hasError) {
                //             return Container(
                //                 child: Text( "No Land Details Found"),
                //             );
                //           }
                //           return Center(
                //             child: CircularProgressIndicator(),
                //           );
                //         },
                //        ),
            
                )));
  }
}



getColor(String irrigationType) {
  Color color;
  switch (irrigationType) {
    case 'Flow / Flood':
      color = Colors.orange;
      break;
    case 'Sprinkler':
      // recommend = Colors.redAccent.toString();
      color = Colors.greenAccent[500];
      break;
    default:
      color = Colors.blue[900];
      break;
  }
  return color;
}

//CALLING STATE API HERE
// Get year information by API
//   List yearList;
//   String _myYear;

//   // String stateInfoUrl = 'http://cleanions.bestweb.my/api/location/get_state';
//   Future<String> _getYearList() async {
// //     await http.post(stateInfoUrl, headers: {
// //       'Content-Type': 'application/x-www-form-urlencoded'
// //     }, body: {
// //       "api_key": '25d55ad283aa400af464c76d713c07ad',
// //     }).then((response) {
// //       var data = json.decode(response.body);

// // //      print(data);
// //       setState(() {
// //         statesList = data['state'];
// //       });
// //     });
//       // setState(() {
//       //   yearList = ["2020","2021"];
//       // });
//   }

  // Get State information by API
//   List citiesList;
//   String _myCity;

//   String cityInfoUrl =
//       'http://cleanions.bestweb.my/api/location/get_city_by_state_id';
//   Future<String> _getCitiesList() async {
//     await http.post(cityInfoUrl, headers: {
//       'Content-Type': 'application/x-www-form-urlencoded'
//     }, body: {
//       "api_key": '25d55ad283aa400af464c76d713c07ad',
//       "state_id": _myState,
//     }).then((response) {
//       var data = json.decode(response.body);

//       setState(() {
//         citiesList = data['cities'];
//       });
//     });
//   }
// }