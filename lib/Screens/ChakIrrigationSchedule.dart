import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:fms_mobileapp/Screens/CroppingPattern.dart';
import 'package:fms_mobileapp/classes/language.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/models/LandHoldingRoRDesign/LandHoldingRoRDesignModel.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/models/subchakIrrigationSchedulingModel.dart';
import 'package:fms_mobileapp/utils/CroppingPatternUtility/Operation.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:translator/translator.dart';
import 'package:fms_mobileapp/services/storage.dart';

class SubChakIrrigationScheduleScreen extends StatefulWidget {
  SubChakIrrigationScheduleScreen({Key key}) : super(key: key);

  @override
  SubChakIrrigationScheduleScreenState createState() =>
      SubChakIrrigationScheduleScreenState();
}

class SubChakIrrigationScheduleScreenState
    extends State<SubChakIrrigationScheduleScreen>
    with SingleTickerProviderStateMixin {
  Future<ChakIrrigationModel> farmer;
  ChakIrrigationModel fd;

  String surveydropdownValue = "0";
  var ChakIrrigationList;
  List<ChakIrrigationResponse> sortedChakIrrigationList;

  List<RoRDesignResponse> landList;
  List<RoRDesignResponse> surveyList;

  String MonthYear = '';
  ConvertFarmerDetails() async {
    try {
      farmer = fetchSubChakIrrigationDetailsResponse();
      var data = await farmer;
       await setState(() {
          fd = data;
        });
      
      if (data.status == "Ok") {
        /*final DateTime now = DateTime.now();
      final DateFormat formatter = DateFormat('dd');
      final String formatted = formatter.format(now);
      int currDate = int.parse(formatted);

      await setState(() {
        sortedChakIrrigationList = fd.data.response
            .where((e) => e.day >= currDate && e.day <= currDate + 7)
            .toList();
      });*/

        final DateFormat monthYearFormatter = DateFormat('MMMM y');

        final DateTime now = DateTime.now();
        final DateFormat formatter = DateFormat('dd');
        final String formatted = formatter.format(now);
        int currDate = int.parse(formatted);
        MonthYear = monthYearFormatter.format(now);
        dynamic values =
            data.data.response.where((e) => e.day == currDate).first;

        setState(() {
          _currentChakIrrigation = values;
          IsVisible = true;
          sortedChakIrrigationList = fd.data.response
              .where((e) =>
                  e.day >= currDate && e.day <= currDate + 7 && e != values)
              .toList();
        });
      }
      return fd;
    } catch (ex) {}
  }

  // ConvertIrrigationDetails() async {
  //   farmer = fetchSubChakIrrigationDetailsResponse();
  //   var temp = await farmer;
  //   setState(() {
  //     fd = temp;
  //   });
  //   return farmer;
  // }

  AnimationController _controller;
  bool _hasBeenPressed = true;

  @override
  void initState() {
    super.initState();
    ConvertDropdownDetails();
  }

  @override
  void dispose() {
    super.dispose();
  }

  ErrorBuilder() {
    // ignore: prefer_const_constructors
    return Center(
      child: Text("SubChak Irrigation Details Not Found "),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'SUB CHAK IRRIGATION CALENDAR',
            style: TextStyle(fontSize: 15),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.contact_support_outlined,
                color: Colors.white,
              ),
              onPressed: () {
                showColorsInformationDialog(context);
              },
            )
          ],
          backgroundColor: Colors.green),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: DropdownButton<String>(
                isExpanded: true,
                value: surveydropdownValue,
                icon: const Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                onChanged: (String newValue) async {
                  setState(() {
                    var tempList = surveyList
                        .where((s) => s.landId.toString() == newValue)
                        .toList();
                    _selectedlandarea = tempList.first.totalArea;
                    surveydropdownValue = newValue;
                    Chakno = tempList.first.chakno;
                    SubChakno = tempList.first.subChakno;
                  });
                  ConvertFarmerDetails();
                },
                items: surveyList != null
                    ? surveyList.map((item) {
                        return new DropdownMenuItem(
                          child: new Text(
                            item.khasaraNo.toString(),
                            style: TextStyle(fontSize: 13.0),
                          ),
                          value: item.landId.toString(),
                        );
                      }).toList()
                    : <String>["0"]
                        .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                            child: Text(value), value: value);
                      }).toList(),
              ),
            ),
            /*FutureBuilder(
              future: ConvertDropdownDetails(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return DropDownIrrigation(context, snapshot.data);
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),*/
            if (surveyList != null)
              DropDownIrrigation(context, null)
            else
              CircularProgressIndicator(),
            if (fd != null )
              if(fd.status == 'Ok')
                Irrigation(context, fd.data.response)
              else
                Container(
                  height: size.height*0.5,
                child: Center(child: Text("No Chak Irrgiation Schedule found !"),))
            else
              CircularProgressIndicator(),
            /*FutureBuilder(
              future: ConvertFarmerDetails(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (fd != null) {
                  final DateTime now = DateTime.now();
                  final DateFormat formatter = DateFormat('dd');
                  final String formatted = formatter.format(now);
                  int currDate = int.parse(formatted);
                  return Irrigation(context,
                      fd.data.response.where((e) => e.day == currDate).first);
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            */
            if (sortedChakIrrigationList != null)
              SingleChildScrollView(
                child: IrrigationList(context, sortedChakIrrigationList),
                scrollDirection: Axis.vertical,
              ),
           

            /*FutureBuilder(
              future: farmer,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return IrrigationList(context, sortedChakIrrigationList);
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),*/
          ],
        ),
      ),
    );
  }

  double _projectDuty = 0.0;
  double _selectedlandarea = 0.0;
  ConvertDropdownDetails() async {
    var land = fetchDropdownLandHoldingRoRDesignDetailResponse();
    var data = await land;

    var _project =
        fetchChakProjectDetailsResponse(data.data.response.first.projectId);
    var project = await _project;

    setState(() {
      _projectDuty = project.duty;
      if (data.data.response.length != 0) {
        surveyList = data.data.response;
        surveydropdownValue = surveyList.first.landId.toString();
        _selectedlandarea = surveyList.first.totalArea;
        Chakno = surveyList.first.chakno;
        SubChakno = surveyList.first.subChakno;
      }
    });
    ConvertFarmerDetails();
    return land;
  }

  double calculateWaterDelivered(dynamic val) {
    var calc = 0.0;
    try {
      var TotalWater = _projectDuty * _selectedlandarea * 3.6;
      var duration = val.valMin / 60;
      calc = TotalWater * duration;
    } catch (Exception) {}
    return calc;
  }

  bool IsVisible = true;
  String Chakno = '';
  String SubChakno = '';
  // getDropDownDataAsync() async {
  //   try {
  //     var land = fetchDropdownLandHoldingRoRDesignDetailResponse();
  //     var _ld = await land;
  //     setState(() {
  //       // areaList = _ld.data.response;
  //     });
  //   } on Exception catch (_, ex) {}
  // }

  Widget DropDownIrrigation(BuildContext context, dynamic values) {
    //     if (values.landId == 0)
    //   setState(() {
    //     if (landList.data.response
    //             .where((element) => element.landId == values.landId)
    //             .length ==
    //         0)
    //       Landdropdownvalue = landList.data.response.first.landId.toString();
    //     else
    //       Landdropdownvalue = landList.data.response
    //           .where((element) => element.landId == values.landId)
    //           .first
    //           .landId
    //           .toString();
    //   });
    // else
    //   Landdropdownvalue = landList.data.response.first.landId.toString();
    return Container(
        padding: EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 0.0),
        // width: size.width,
        decoration: BoxDecoration(
            border: Border(
          top: BorderSide(width: 0.2, color: Colors.black),
          right: BorderSide(width: 0.2, color: Colors.black),
          left: BorderSide(width: 0.2, color: Colors.black),
          bottom: BorderSide(width: 0.4, color: Colors.black),
        )),
        child: Column(
          children: [
            // DropdownButton<String>(
            //   isExpanded: true,
            //   value: surveydropdownValue,
            //   icon: const Icon(Icons.arrow_downward),
            //   iconSize: 24,
            //   elevation: 16,
            //   style: const TextStyle(color: Colors.deepPurple),
            //   underline: Container(
            //     height: 2,
            //     color: Colors.deepPurpleAccent,
            //   ),
            //   onChanged: (String newValue) async {
            //     await setState(() {
            //       var tempList = surveyList
            //           .where((s) => s.landId.toString() == newValue)
            //           .toList();

            //       surveydropdownValue = newValue;
            //       Chakno = tempList.first.chakno;
            //       SubChakno = tempList.first.subChakno;
            //     });
            //   },
            //   items: surveyList != null
            //       ? surveyList.map((item) {
            //           return new DropdownMenuItem(
            //             child: new Text(
            //               item.khasaraNo.toString(),
            //               style: TextStyle(fontSize: 13.0),
            //             ),
            //             value: item.landId.toString(),
            //           );
            //         }).toList()
            //       : <String>["0"].map<DropdownMenuItem<String>>((String value) {
            //           return DropdownMenuItem<String>(
            //               child: Text(value), value: value);
            //         }).toList(),
            // ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Text(
                'SUB CHAK CALENDAR',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 7.0),
              decoration: BoxDecoration(
                  border: Border(
                // top: BorderSide(width: 0.2, color: Colors.black),
                // right: BorderSide(width: 0.2, color: Colors.black),
                bottom: BorderSide(
                  width: 0.2,
                  color: Colors.black87,
                ),
              )),
              child: Row(
                children: [
                  Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8.0, 10, 0.0, 0.0),
                        child: Text("CHAK NO :-" + Chakno),
                      )),
                  Expanded(
                    child: Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 10, 0.0, 0.0),
                          child: Text("SUB CHAK NO :-" + SubChakno),
                        )),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  ColorsDaySchedule(dynamic values) {
    MaterialColor color;
    DateTime currtime = new DateTime.now();
    DateFormat formatter = DateFormat('dd');
    String currentDay = formatter.format(currtime);
    if (values.day == int.parse(currentDay)) {
      //String DisplayStr = 'A ,C ,D 12:00 - 22:00';
      String tempStr = values.displayStr
          .substring(values.displayStr.length - 13, values.displayStr.length);
      String StartTime = tempStr.substring(0, 5);
      String EndTime = tempStr.substring(tempStr.length - 5, tempStr.length);

      formatter = DateFormat('HHmm');
      String formatted = formatter.format(currtime);

      //  String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(currtime);

      if (int.parse(formatted) > int.parse(StartTime.replaceAll(":", "")) &&
          int.parse(formatted) < int.parse(EndTime.replaceAll(":", ""))) {
        color = Colors.blue;
      } else if (int.parse(formatted) >
          int.parse(EndTime.replaceAll(":", ""))) {
        color = Colors.green;
      } else if (int.parse(formatted) <
          int.parse(EndTime.replaceAll(":", ""))) {
        color = Colors.orange;
      } else {
        color = Colors.red;
      }
    } else if (values.day > int.parse(currentDay)) {
      color = Colors.orange;
    } else {
      if (values.displayStr != null)
        color = Colors.green;
      else {
        color = Colors.red;
      }
    }
    return color;
  }

  dynamic _currentChakIrrigation;
  bool isButtonPressed = false;
  Widget Irrigation(BuildContext context, List<ChakIrrigationResponse> data) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd');
    final String formatted = formatter.format(now);
    int currDate = int.parse(formatted);
    dynamic values = data.where((e) => e.day == currDate).first;
    _currentChakIrrigation = values;
    return Container(
        padding: EdgeInsets.fromLTRB(5.0, 15.0, 5.0, 0.0),
        // width: size.width,
        decoration: BoxDecoration(
            border: Border(
          top: BorderSide(width: 0.2, color: Colors.black),
          right: BorderSide(width: 0.2, color: Colors.black),
          left: BorderSide(width: 0.2, color: Colors.black),
        )),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 6.0),
              decoration: BoxDecoration(
                  color: Colors.blue[300],
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: Colors.black87,
                    ),
                  )),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Text("TODAY :-" +
                            currDate.toString() +
                            ' ' +
                            MonthYear)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Text('TIME :- ' + values.displayStr.toString())),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text('DURATION :- ' +
                          (values.valMin / 60).toStringAsFixed(1) +
                          " Hr"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text('WATER DELIVERED :-' +
                          (calculateWaterDelivered(values).toStringAsFixed(3))
                              .toString() +
                          " M3"),
                    ),
                  ),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 5.0),
                decoration: BoxDecoration(
                    border: Border(
                  // top: BorderSide(width: 0.2, color: Colors.black),
                  // right: BorderSide(width: 0.2, color: Colors.black),
                  bottom: BorderSide(
                    width: 0.5,
                    color: Colors.black,
                  ),
                )),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (IsVisible)
                        RaisedButton(
                          child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                "PERVIOUS 8 DAYS",
                                style: TextStyle(
                                    fontSize: 10, color: Colors.white),
                              )),
                          color: _hasBeenPressed ? Colors.red : Colors.grey,
                          onPressed: () async {
                            // isButtonPressed ? Colors.orange : Colors.red;
                            final DateTime now = DateTime.now();
                            final DateFormat formatter = DateFormat('dd');
                            final String formatted = formatter.format(now);
                            int currDate = int.parse(formatted);
                            await setState(() {
                              IsVisible = false;
                              sortedChakIrrigationList = fd.data.response
                                  .where((e) =>
                                      e.day >= currDate - 7 &&
                                      e.day <= currDate &&
                                      e != _currentChakIrrigation)
                                  .toList();
                            });
                          },
                          textColor: Colors.yellow,
                          splashColor: Colors.grey,
                        ),
                      // Container(
                      //   width: MediaQuery.of(context).size.width * 0.35,
                      //   child: Padding(
                      //     padding: const EdgeInsets.all(5.0),
                      //     child: Text(
                      //       'IRRIGATION SCHEDULE',
                      //       style: TextStyle(fontSize: 13),
                      //     ),
                      //   ),
                      // ),
                      if (!IsVisible)
                        RaisedButton(
                          child: Align(
                              alignment: Alignment.topRight,
                              child: Text(
                                "Next 8 DAYS",
                                style: TextStyle(
                                    fontSize: 10, color: Colors.white),
                              )),
                          color: _hasBeenPressed ? Colors.red : Colors.grey,
                          onPressed: () async {
                            final DateTime now = DateTime.now();
                            final DateFormat formatter = DateFormat('dd');
                            final String formatted = formatter.format(now);
                            int currDate = int.parse(formatted);
                            await setState(() {
                              IsVisible = true;
                              sortedChakIrrigationList = fd.data.response
                                  .where((e) =>
                                      e.day >= currDate &&
                                      e.day <= currDate + 7 &&
                                      e != _currentChakIrrigation)
                                  .toList();
                            });
                          },
                        )
                    ])),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Align(
                    child: Text(
                  "IRRIGATION SCHEDULE",
                  style: TextStyle(fontSize: 18),
                ))
              ],
            )
          ],
        ));
  }

  // void IrrigationDay(values) {
  //   if (values.first.day == values) {
  //     return values;
  //   } else {
  //     values = false;
  //   }
  // }

  Widget IrrigationList(
      BuildContext context, List<ChakIrrigationResponse> values) {
    return ListView.builder(
        itemCount: values.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.all(0.0),
            child: Container(
              decoration: BoxDecoration(
                color: ColorsDaySchedule(values[index]),
              ),
              child: Column(
                children: [
                  Wrap(
                    alignment: WrapAlignment.center,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                          child: Card(
                              child: Container(
                            decoration: BoxDecoration(
                                // color: ColorsDaySchedule(values[index]),
                                border: Border(
                              top: BorderSide(width: 0.2, color: Colors.black),
                              bottom:
                                  BorderSide(width: 0.2, color: Colors.black),
                              right:
                                  BorderSide(width: 0.2, color: Colors.black),
                              left: BorderSide(width: 0.2, color: Colors.black),
                            )),
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  values[index].day.toString() +
                                      ' ' +
                                      MonthYear,
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ))),
                      // Padding(
                      //   padding: const EdgeInsets.all(5.0),
                      //   child: Align(
                      //       alignment: Alignment.topLeft,
                      //       child: Text(
                      //           values[index].day.toString() +
                      //           ' ' +
                      //           MonthYear)),
                      // ),

                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Text('TIME :-' +
                                values[index].displayStr.toString())),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text('DURATION :-' +
                              (values[index].valMin / 60).toStringAsFixed(1) +
                              " Hr"),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Text('WATER DELIVERED :' +
                                (calculateWaterDelivered(values[index])
                                        .toStringAsFixed(3))
                                    .toString() +
                                " M3")),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> showColorsInformationDialog(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.55,
                  child: Text('Irrigation Status'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.close,
                          color: Colors.red,
                          size: 20,
                        ))
                  ],
                ),
              ],
            ),
            actions: [
              Divider(
                thickness: 2,
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.blue),
                title: Text('IN IRRIGATION'),
              ),
              const Divider(
                thickness: 1.0,
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.green),
                title: Text('IRRIGATION COMPLETED'),
              ),
              const Divider(
                thickness: 1.0,
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.orange),
                title: Text('WAITING FOR IRRIGATION'),
              ),
              const Divider(
                thickness: 1.0,
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.red),
                title: Text('NO IRRIGATION'),
              ),
            ],
          );
        });
  }
}
  // void pervious() {
  //   final DateTime now = DateTime.now();
  //   final DateFormat formatter = DateFormat('dd');
  //   final String formatted = formatter.format(now);
  //   int currDate = int.parse(formatted);

  //   setState(() {
  //    fd.data.response
  //         .where((e) => e.day >= currDate - 7 && e.day <= currDate)
  //         .toList();
  //   });
  // }

