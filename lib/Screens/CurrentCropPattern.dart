// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'package:flinq/flinq.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:fms_mobileapp/Screens/CroppingPattern.dart';
import 'package:fms_mobileapp/classes/language.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternModel.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternDropDownModel.dart';
import 'package:fms_mobileapp/models/CroppingPattern/MstSeasonCropModel.dart';
import 'package:fms_mobileapp/models/LandHoldingRoRDesign/LandHoldingRoRDesignModel.dart';
import 'package:fms_mobileapp/models/MasterModel/MstCropModel.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/CropPatternDetails.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/models/MasterModel/MstSeasonModel.dart';
import 'package:fms_mobileapp/utils/CroppingPatternUtility/Operation.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:intl/intl.dart';
import 'package:translator/translator.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'dart:math';

class CurrentYrCropPatternScreen extends StatefulWidget {
  const CurrentYrCropPatternScreen({Key key}) : super(key: key);

  @override
  _CurrentYrCropPatternState createState() => _CurrentYrCropPatternState();
}

class _CurrentYrCropPatternState extends State<CurrentYrCropPatternScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  bool _isPlaying = true;
  bool _isBusy = false;
  String dropdownValue = 'One';
  String surveydropdownValue = "0";
  String cropdropdownValue = "0";
  String areadropdownValue = "0";
  String seasondropdownValue = "0";
  String yeardropdownValue = "0";
  double remainingBalanceArea = 0.0;
  @override
  void initState() {
    super.initState();
    ConvertCropPatternDetails();
    //getDropDownDataAsync();
    _controller = AnimationController(
      lowerBound: 0.3,
      vsync: this,
      duration: Duration(seconds: 3),
    )..repeat();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  List<RoRDesignResponse> landList;
  List<RoRDesignResponse> surveyList;

  Future<List<CroppingPatternModel>> crop;
  List<CroppingPatternModel> cp;
  Set<String> seasonList = new Set<String>();
  Set<String> yrList = new Set<String>();

  CropPatternDropDownModel _list;
  List<CommonList> yearList = new List<CommonList>();
  CommonList SelectedValue = new CommonList();

  CropPatternDropDownModel cpPattern;
  Future<CropPatternDropDownModel> cropPattern;
  CropPatternDropDownModel cpdropdown = new CropPatternDropDownModel();
  Future<CroppingPatternModel> cropPatternDetails;
  CroppingPatternModel cpDetails = new CroppingPatternModel();

  List<CropResponse> _cropList;
  List<CropResponse> cropList;

  List<SeasonResponse> SeasonCropList;
  // List areaList;

  ConvertDetails() async {
    var land = fetchLandHoldingRoRDesignDetailResponse();
    var crop = fetchCropListResponse();
  }

  getDropDownDataAsync() async {
    try {
      setState(() {
        _isBusy = true;
      });
      var land = fetchLandHoldingRoRDesignDetailResponse();
      var _ld = await land;
      var seasoncrop = fetchSeasonListResponse();
      var _scrop = await seasoncrop;
      var crop = fetchCropListResponse();
      var _crop = await crop;
      // var cpdropdown = fetchCropPatternDropDownResponse();
      // var _codropdown = await cpdropdown;
      await setState(() {
        surveyList = _ld.data.response;
        surveydropdownValue = surveyList.first.landId.toString();
        //yearList = _codropdown.yearList;
        yeardropdownValue = SelectedValue.year.toString();
        SeasonCropList = _scrop.data.response;
        _cropList = _crop.data.response;
        cropList = _crop.data.response
            .where(
                (element) => element.seasonId == SeasonCropList.first.seasonId)
            .toList();
        cropdropdownValue = cropList.first.cropId.toString();
        remainingBalanceArea = surveyList.first.totalArea;
        // areaList = _ld.data.response;
      });
    } on Exception catch (_, ex) {}
  }

  ErrorBuilder() {
    // ignore: prefer_const_constructors
    return Center(
      child: Text("Details Not Found "),
    );
  }

  String btnText = "ADD CROP";
  Future<void> getFormDialogBox(BuildContext context,
      {CroppingPatternModel values}) async {
    String area = '';
    final Size size = MediaQuery.of(context).size;
    String seadropdownValue = '0';
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy');
    final String formatted = formatter.format(now);
    int currYear = int.parse(formatted) - 1;
    var data = List<int>.generate(3, (int index) => currYear + index);
    try {
      if (values != null) {
        if (values.year == 0) values.year = currYear;
        var temp = await crop;
        await setState(() {
          btnText = "UPDATE";
          cropList = _cropList
              .distinctWhere((element) => element.seasonId == values.seasonId)
              .toList();
          double calculate = 0.0;
          temp
              .where((e) =>
                  e.year == values.year &&
                  e.seasonId == values.seasonId &&
                  e.landId == values.landId)
              .forEach((element) {
            calculate += double.parse(element.area);
          });
          remainingBalanceArea = surveyList
                  .where((element) => element.landId == values.landId)
                  .first
                  .totalArea -
              calculate;
        });
      } else {
        double calculate = 0.0;
        await setState(() {
          values = new CroppingPatternModel();
          btnText = "ADD";
          values.year = currYear + 1;
          values.landId = surveyList.first.landId;
          values.seasonId = SeasonCropList.first.seasonId;
          cropList = _cropList
              .where((element) => element.seasonId == values.seasonId)
              .toList();
          values.cropId = cropList.first.cropId;
          values.area = '0';
          remainingBalanceArea = surveyList.first.totalArea;
        });
      }
    } catch (_, ex) {}
    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return Dialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 10,
            child: Container(
              width: size.width * 0.60,
              height: size.height * 0.60,
              child: SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            width: size.width * 0.60,
                            color: Colors.blue[900],
                            child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(60.0, 10.0, 0.0, 10.0),
                                child: Text(
                                  btnText + ' CROPPING PATTERN',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                  softWrap: true,
                                ))),
                        DropdownButton<String>(
                          isExpanded: true,
                          value: surveyList != null
                              ? (values.landId == 0
                                  ? surveyList.first.landId.toString()
                                  : values.landId.toString())
                              : '0',
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: const TextStyle(color: Colors.deepPurple),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          onChanged: (String newValue) async {
                            try {
                              if (cp.length != 0) {
                                var temp = await crop;
                                double calculate = 0.0;

                                await setState(() {
                                  values.landId = int.parse(newValue);
                                  var tempList = surveyList
                                      .where((s) => s.landId == newValue)
                                      .toList();
                                  temp
                                      .where((e) =>
                                          e.year == values.year &&
                                          e.seasonId == values.seasonId &&
                                          e.landId == values.landId)
                                      .forEach((element) {
                                    calculate += double.parse(element.area);
                                  });

                                  remainingBalanceArea =
                                      tempList.first.totalArea - calculate;
                                });
                              } else {
                                var temp = await crop;
                                double calculate = 0.0;

                                await setState(() {
                                  values.landId = int.parse(newValue);
                                  var tempList = surveyList
                                      .where((s) => s.landId == newValue)
                                      .first;
                                  remainingBalanceArea = tempList.totalArea -
                                      double.parse(values.area);
                                });
                              }
                            } catch (_, ex) {}
                          },
                          items: surveyList != null
                              ? surveyList.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(
                                      item.khasaraNo.toString(),
                                      style: TextStyle(fontSize: 13.0),
                                    ),
                                    value: item.landId.toString(),
                                  );
                                }).toList()
                              : <String>[
                                  "0"
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                      child: Text(value), value: value);
                                }).toList(),
                        ),
                        DropdownButton<String>(
                          isExpanded: true,
                          value: values.year.toString(),
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: const TextStyle(color: Colors.deepPurple),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          items: data.map((item) {
                            return DropdownMenuItem<String>(
                              value: item.toString(),
                              child: Text(item.toString()),
                            );
                          }).toList(),
                          onChanged: (textvalue) async {
                            try {
                              if (cp.length != 0) {
                                var temp = await crop;
                                double calculate = 0.0;
                                await setState(() {
                                  values.year = int.parse(textvalue);

                                  var tempList = surveyList
                                      .where((s) => s.landId == values.landId)
                                      .toList();
                                  temp
                                      .where((e) =>
                                          e.year == values.year &&
                                          e.seasonId == values.seasonId &&
                                          e.landId == values.landId)
                                      .forEach((element) {
                                    calculate += double.parse(element.area);
                                  });

                                  remainingBalanceArea =
                                      tempList.first.totalArea - calculate;
                                });
                              } else {
                                await setState(() {
                                  values.year = int.parse(textvalue);
                                });
                              }
                            } catch (_, ex) {}
                          },
                        ),
                        DropdownButton<String>(
                          isExpanded: true,
                          value: SeasonCropList != null
                              ? (values.seasonId == 0
                                  ? SeasonCropList.first.seasonId.toString()
                                  : values.seasonId.toString())
                              : '0',
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: const TextStyle(color: Colors.deepPurple),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          onChanged: (String newValue) async {
                            try {
                              if (cp.length != 0) {
                                var temp = await crop;
                                double calculate = 0.0;
                                await setState(() {
                                  values.seasonId = int.parse(newValue);

                                  cropList = _cropList
                                      .where((element) =>
                                          element.seasonId == values.seasonId)
                                      .toList();
                                  values.cropId = cropList.first.cropId;

                                  var tempList = surveyList
                                      .where((s) => s.landId == values.landId)
                                      .toList();
                                  temp
                                      .where((e) =>
                                          e.year == values.year &&
                                          e.seasonId == values.seasonId &&
                                          e.landId == values.landId)
                                      .forEach((element) {
                                    calculate += double.parse(element.area);
                                  });

                                  remainingBalanceArea =
                                      tempList.first.totalArea - calculate;
                                });
                              } else {
                                await setState(() {
                                  values.seasonId = int.parse(newValue);

                                  cropList = _cropList
                                      .where((element) =>
                                          element.seasonId == values.seasonId)
                                      .toList();
                                  values.cropId = cropList.first.cropId;
                                });
                              }
                            } catch (_, ex) {}
                          },
                          items: SeasonCropList != null
                              ? SeasonCropList.distinct.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(
                                      item.seasonName.toString(),
                                      style: TextStyle(fontSize: 13.0),
                                    ),
                                    value: item.seasonId.toString(),
                                  );
                                }).toList()
                              : <String>[
                                  "0"
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                      child: Text(value), value: value);
                                }).toList(),
                        ),
                        DropdownButton<String>(
                          isExpanded: true,
                          value: cropList != null
                              ? (values.cropId == 0
                                  ? cropList.first.seasonId.toString()
                                  : values.cropId.toString())
                              : '0',
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: const TextStyle(color: Colors.deepPurple),
                          underline: Container(
                            height: 2,
                            color: Colors.deepPurpleAccent,
                          ),
                          onChanged: (String newValue) async {
                            try {
                              await setState(() {
                                values.cropId = int.parse(newValue);
                              });
                            } catch (_, ex) {}
                          },
                          items: cropList != null
                              ? cropList.distinct.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(
                                      item.cropName.toString(),
                                      style: TextStyle(fontSize: 13.0),
                                    ),
                                    value: item.cropId.toString(),
                                  );
                                }).toList()
                              : <String>[
                                  "0"
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                      child: Text(value), value: value);
                                }).toList(),
                        ),
                        Wrap(
                          direction: Axis.horizontal,
                          children: [
                            TextFormField(
                              initialValue: values.area,
                              decoration: const InputDecoration(
                                  hintText: 'Area(Ha): ',
                                  fillColor: Colors.white,
                                  filled: true),
                              keyboardType: TextInputType.number,
                              onChanged: (newValue) async {
                                try {
                                  if (cp.length != 0) {
                                    var temp = await crop;
                                    double calculate = 0.0;
                                    var tempList = surveyList
                                        .where((s) => s.landId == values.landId)
                                        .toList();
                                    temp
                                        .where((e) =>
                                            e.id != values.id &&
                                            e.year == values.year &&
                                            e.seasonId == values.seasonId &&
                                            e.landId == values.landId)
                                        .forEach((element) {
                                      calculate += double.parse(element.area);
                                    });

                                    await setState(() {
                                      values.area = newValue;
                                      if (!newValue.isEmpty) {
                                        remainingBalanceArea =
                                            tempList.first.totalArea -
                                                calculate -
                                                double.parse(newValue);
                                      } else {
                                        remainingBalanceArea =
                                            tempList.first.totalArea -
                                                calculate;
                                      }
                                    });
                                  } else {
                                    var temp = await crop;
                                    double calculate = 0.0;
                                    values.area = newValue;
                                    var tempList = surveyList
                                        .where((s) => s.landId == values.landId)
                                        .first;

                                    if (!newValue.isEmpty) {
                                      await setState(() {
                                        remainingBalanceArea =
                                            tempList.totalArea -
                                                double.parse(values.area);
                                      });
                                    } else
                                      await setState(() {
                                        remainingBalanceArea =
                                            tempList.totalArea - calculate;
                                      });
                                  }
                                } catch (_, ex) {}
                              },
                              // The validator receives the text that the user has entered.
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter the area';
                                } else if (remainingBalanceArea < 0)
                                  return 'Entered area is more than your land area';
                                else
                                  return value;
                              },
                            ),
                            Text(
                                "Remaining Balance Area: " +
                                    remainingBalanceArea.toStringAsFixed(2),
                                softWrap: true),
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.all(10.0),
                            child: ElevatedButton(
                              child: Center(child: Text(btnText)),
                              style: ElevatedButton.styleFrom(
                                primary: Colors.green,
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(32.0)),
                                padding: EdgeInsets.all(20),
                              ),
                              onPressed: () async {
                                try {
                                  if (remainingBalanceArea >= 0) {
                                    if (btnText == 'ADD') {
                                      if (cp.length != 0) {
                                        dynamic fetched =
                                            await SecureStorage.getUserData();
                                        int userid = fetched.data.userId;
                                        dynamic temp = await crop;
                                        await setState(() {
                                          cp = temp;
                                          cp.add(new CroppingPatternModel(
                                              id: cp.length + 1,
                                              year: values.year,
                                              cropId: values.cropId,
                                              seasonId: values.seasonId,
                                              area: values.area.toString(),
                                              landId: values.landId,
                                              farmerId: userid,
                                              marketRate: 'Y'));
                                        });
                                        ConvertCropPatternDetails();
                                        Navigator.pop(context);
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          const SnackBar(
                                              content: Text(
                                                  'Data added successfully')),
                                        );
                                      } else {
                                        dynamic fetched =
                                            await SecureStorage.getUserData();
                                        int userid = fetched.data.userId;
                                        await setState(() {
                                          cp.add(new CroppingPatternModel(
                                              id: cp.length + 1,
                                              year: values.year,
                                              cropId: values.cropId,
                                              seasonId: values.seasonId,
                                              area: values.area.toString(),
                                              landId: values.landId,
                                              farmerId: userid,
                                              marketRate: 'Y'));
                                        });
                                        ConvertCropPatternDetails();
                                        Navigator.pop(context);
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          const SnackBar(
                                              content: Text(
                                                  'Data added successfully')),
                                        );
                                      }
                                    } else if (btnText == 'UPDATE') {
                                      dynamic temp = await crop;

                                      await setState(() {
                                        cp = temp;
                                        var model = cp
                                            .where((e) => e.id == values.id)
                                            .first;
                                        model.cropId = values.cropId;
                                        model.seasonId = values.seasonId;
                                        model.year = values.year;
                                        model.area = values.area;
                                        model.landId = values.landId;
                                      });
                                      ConvertCropPatternDetails();
                                      Navigator.pop(context);
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text(
                                                'Data updated successfully')),
                                      );
                                    }
                                  } else {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                          content: Text(
                                              'Area you have enter is more than the remaining area ')),
                                    );
                                  }
                                } catch (_, ex) {}
                              },
                            )),
                      ],
                    )),
              ),
            ),
          );
        });
      },
    );
  }

  Widget yearComponentBuilder(BuildContext context, List<dynamic> values) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy');
    final String formatted = formatter.format(now);
    int currYear = int.parse(formatted) - 1;
    var data = List<int>.generate(3, (int index) => currYear + index);
    if (values.first.year == 0) values.first.year = currYear;
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: DropdownButtonFormField(
            value: values.first.year.toString(),
            decoration: InputDecoration(
              labelText: 'year',
              border: OutlineInputBorder(),
            ),
            items: data.map((item) {
              return DropdownMenuItem<String>(
                value: item.toString(),
                child: Text(item.toString()),
              );
            }).toList(),
            onChanged: (dropvalue) {
              setState(() {
                values.first.year = int.parse(dropvalue);
              });
            },
          ),
        ));
  }

  ConvertCropPatternDetails() async {
    try {
      String cpIds = '';
      String landIds = '';
      String years = '';
      String cropIds = '';
      String seasonIds = '';
      String areas = '';
      String mrate = '';
      crop = null;

      await getDropDownDataAsync();
      if (cp != null) {
        cp.forEach((element) {
          cpIds = cpIds + ',' + element.id.toString() ?? '';
          years = years + ',' + element.year.toString();
          landIds = landIds + ',' + element.landId.toString();
          cropIds = cropIds + ',' + element.cropId.toString();
          seasonIds = seasonIds + ',' + element.seasonId.toString();
          areas = areas + ',' + element.area.toString() ?? '0';
          mrate = mrate + ',' + element.marketRate ?? '';
        });

        crop = fetchCurrentYrCropPatternDetailResponse(
            cpIds: cpIds.substring(1),
            landIds: landIds.substring(1),
            years: years.substring(1),
            cropIds: cropIds.substring(1),
            seasonsIds: seasonIds.substring(1),
            areas: areas.substring(1),
            mrate: mrate.substring(1));

        await setState(() {
          cp = null;
        });
      } else
        crop = fetchCurrentYrCropPatternDetailResponse();

      cp = await crop;

      await setState(() {
        for (dynamic item in cp) {
          /*TotalestimateFloodExpenses += element.estimateFloodExpenses;
          TotalestimateFloodGrossIncome += element.estimateFloodGrossIncome;
          TotalestimateFloodNet += element.estimateFloodNet;
          TotalestimateSprinklerExpenses += element.estimateSprinklerExpenses;
          TotalestimateSprinklerGrossIncome +=
              element.estimateSprinklerGrossIncome;
          TotalestimateSprinklerNet += element.estimateSprinklerNet;
          */
          seasonList.add(item.seasonName);
          yrList.add(item.year.toString());
        }

        //print(seasonList);
        seasondropdownValue = seasonList.first;
        yeardropdownValue = yrList.first;

        cp = cp
            .where((element) => element.seasonName == seasondropdownValue)
            .toList();
        _isVisible = true;
      });

      setState(() {
        _isBusy = false;
      });
    } on Exception catch (_, ex) {}
  }

  double TotalestimateFloodNet = 0.0;
  double TotalestimateFloodGrossIncome = 0.0;
  double TotalestimateFloodExpenses = 0.0;
  double TotalestimateSprinklerNet = 0.0;
  double TotalestimateSprinklerGrossIncome = 0.0;
  double TotalestimateSprinklerExpenses = 0.0;
  bool _isVisible = false;

  getColor(String recommend) {
    MaterialColor color;
    switch (recommend) {
      case 'Recommended':
        color = Colors.green;
        break;
      case 'Not Recommended':
        // recommend = Colors.redAccent.toString();
        color = Colors.orange;
        break;
      default:
        color = Colors.red;
        break;
    }
    return color;
  }

  Widget tableRowBuilder(BuildContext context, List<dynamic> values) {
    Future.delayed(Duration(seconds: 2));
    return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(children: [
            Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              border: TableBorder.all(width: 1.0, color: Colors.black),
              children: [
                // TableRow(children: [
                //   Text(
                //     "Survey No",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Text(
                //     "Crop",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Text(
                //     "Area(Ha)",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Text(
                //     "Action",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                // ]),

                TableRow(children: [
                  Text(
                    values[index].khasaraNo ?? '',
                    textScaleFactor: 1.5,
                    style: TextStyle(
                      fontSize: 10,
                      // color: getColor(values[index].recommend)
                    ),
                    textAlign: TextAlign.center,
                  ),
                  // Text(
                  //   values[index].cropName ?? '',
                  //   textScaleFactor: 1.5,
                  //   style: TextStyle(fontSize: 10),
                  // ),
                  Container(
                    decoration: BoxDecoration(
                        color: getColor(values[index].recommendation)),
                    child: Wrap(
                      runSpacing: 0.0,
                      alignment: WrapAlignment.start,
                      spacing: 0.0,
                      children: [
                        values[index].imageLink != null
                            ? Image.network(
                                'http://fms.seprojects.in/' +
                                    values[index].imageLink.toString(),
                                width: 80,
                                height: 60,
                                fit: BoxFit.fitWidth,
                                alignment: Alignment.topLeft,
                              )
                            : Icon(Icons.eco),
                        // Text("Soybean")
                        Text(
                          values[index].cropName ?? '',
                          textScaleFactor: 1.5,
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    values[index].area ?? '',
                    textScaleFactor: 1.5,
                    style: TextStyle(fontSize: 10),
                    textAlign: TextAlign.center,
                  ),
                  Wrap(
                    spacing: 10.0,
                    children: [
                      Row(children: [
                        IconButton(
                            onPressed: () async {
                              // Text("Edit");
                              await getFormDialogBox(context,
                                  values: values[index]);
                            },
                            icon: Icon(
                              Icons.edit,
                              size: 25,
                              color: Colors.green,
                            )),

                        // Icon(
                        //   Icons.edit,
                        //   size: 15.0,
                        // ),
                        // Text("Edit"),
                      ]),
                      SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Row(children: [
                          IconButton(
                              onPressed: () async {
                                if (values[index].marketRate != 'Y') {
                                  await deleteCroppingPattern(values[index].id);
                                  setState(() {
                                    cp.remove(values[index]);
                                  });

                                  ConvertCropPatternDetails();
                                  SnackBar(
                                      content:
                                          Text("Data deleted successfully"));
                                }
                              },
                              icon: Icon(
                                Icons.delete,
                                size: 25,
                                color: Colors.redAccent,
                              )),
                          // Icon(
                          //   Icons.delete,
                          //   size: 15.0,
                          // ),
                          // Text("Delete"),
                        ]),
                      ),
                    ],
                  ),
                ]),
                // TableRow(children: [
                //   Text(
                //     "XYZ",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Row(
                //     children: [
                //       Image(
                //           width: 40,
                //           height: 40,
                //           image: AssetImage(
                //               "assets/images/soybeans.jpg")),
                //       Text("Soybean")
                //     ],
                //   ),
                //   Text(
                //     "1.5",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Row(
                //     children: [
                //       Icon(
                //         Icons.edit,
                //         size: 15.0,
                //       ),
                //       Text("Edit"),
                //       Icon(
                //         Icons.delete,
                //         size: 15.0,
                //       ),
                //       Text("Delete"),
                //     ],
                //   ),
                // ]),
                // TableRow(children: [
                //   Text(
                //     "ABC",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Row(
                //     children: [
                //       Image(
                //           width: 40,
                //           height: 40,
                //           image: AssetImage(
                //               "assets/images/soybeans.jpg")),
                //       Text("Soybean")
                //     ],
                //   ),
                //   Text(
                //     "2.5",
                //     textScaleFactor: 1.5,
                //     style: TextStyle(fontSize: 10),
                //   ),
                //   Row(
                //     children: [
                //       Icon(
                //         Icons.edit,
                //         size: 15.0,
                //       ),
                //       Text("Edit"),
                //       Icon(
                //         Icons.delete,
                //         size: 15.0,
                //       ),
                //       Text("Delete"),
                //     ],
                //   ),
                // ]),
              ],
            )
          ]);
        });
  }

  Widget economicsBuilder(BuildContext context, List<dynamic> values) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: values.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                        decoration: BoxDecoration(
                            color: Colors.grey,
                            border: Border(
                              top: BorderSide(width: 0.2, color: Colors.black),
                              bottom:
                                  BorderSide(width: 0.2, color: Colors.black),
                              right:
                                  BorderSide(width: 0.2, color: Colors.black),
                              left: BorderSide(width: 0.2, color: Colors.black),
                            )),
                        child: Table(
                            defaultVerticalAlignment:
                                TableCellVerticalAlignment.middle,
                            children: [
                              TableRow(children: [
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("Survey No: " +
                                            values[index].khasaraNo ??
                                        '')),
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Text(
                                        "Area: " + values[index].area ?? '')),
                              ]),
                              TableRow(children: [
                                Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Text("Chak/Subchak No: " +
                                            values[index].chakSubChakno ??
                                        '')),
                                Align(
                                    alignment: Alignment.bottomRight,
                                    child: Text(
                                        "Crop: " + values[index].cropName ??
                                            '')),
                              ]),
                              TableRow(children: [
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("Market Rate: " +
                                            values[index].marketRate ??
                                        '')),
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: Text('')),
                              ]),
                            ]),
                      )),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                      decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          border: Border(
                            top: BorderSide(width: 0.2, color: Colors.black),
                            bottom: BorderSide(width: 0.2, color: Colors.black),
                            right: BorderSide(width: 0.2, color: Colors.black),
                            left: BorderSide(width: 0.2, color: Colors.black),
                          )),
                      child: Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          children: [
                            TableRow(children: [
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Text("Irrigation Type: Flow/Flood")),
                              Align(
                                  alignment: Alignment.topRight,
                                  child: Text("Yield: " +
                                          values[index]
                                              .estimateFloodYield
                                              .toString() ??
                                      '')),
                            ]),
                            TableRow(children: [
                              Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text("Gross Income: " +
                                          values[index]
                                              .estimateFloodGrossIncome
                                              .toString() ??
                                      '')),
                              Align(),
                            ]),
                            TableRow(children: [
                              Align(),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text('')),
                            ]),
                          ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                      decoration: BoxDecoration(
                          color: Colors.greenAccent,
                          border: Border(
                            top: BorderSide(width: 0.2, color: Colors.black),
                            bottom: BorderSide(width: 0.2, color: Colors.black),
                            right: BorderSide(width: 0.2, color: Colors.black),
                            left: BorderSide(width: 0.2, color: Colors.black),
                          )),
                      child: Table(
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.middle,
                          children: [
                            TableRow(children: [
                              Align(
                                  alignment: Alignment.topLeft,
                                  child: Text("Irrigation Type: Sprinkler")),
                              Align(
                                  alignment: Alignment.topRight,
                                  child: Text("Yield: " +
                                          values[index]
                                              .estimateSprinklerYield
                                              .toString() ??
                                      '')),
                            ]),
                            TableRow(children: [
                              Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Text(" Gross Income: " +
                                          values[index]
                                              .estimateSprinklerGrossIncome
                                              .toString() ??
                                      '')),
                              Align(),
                            ]),
                            TableRow(children: [
                              Align(),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text('')),
                            ]),
                          ]),
                    ),
                  ),
                ],
              ));
        });
  }

  VoidCallback _changeLanguage(String language) {
    Locale _temp;
    switch (language) {
      case 'hi':
        _temp = Locale(language, 'IN');
        break;

      default:
        _temp = Locale(language, 'US');
    }

    // MyApp.setLocale(context, _temp);
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(DemoLocalization.of(context).translate('title')),
        backgroundColor: Colors.green,
      ),
      /*floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
        onPressed: () async {
          SnackBar(+++
            content: Text("Hello"),
          );

          await getFormDialogBox(context);
        },
      ),*/
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                  width: size.width,
                  color: Colors.blue[900],
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      child: Text(
                        'CURRENT YEAR CROPPING ' +
                            'PATTERN & ESTIMATED ECONOMICS',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                        softWrap: true,
                      ))),
              // Padding(
              //     padding: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
              //     child: FutureBuilder(
              //       future: null,
              //       builder: (BuildContext context, AsyncSnapshot snapshot) {},
              //     )),

              Wrap(
                direction: Axis.horizontal,
                spacing: MediaQuery.of(context).size.width * 0.20,
                runAlignment: WrapAlignment.center,
                children: [
                  if (!_isBusy)
                    DropdownButton<String>(
                      value: yrList != null && yrList.length != 0
                          ? yeardropdownValue
                          : '0',
                      icon: const Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        height: 2,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: (String newValue) async {
                        var temp = await crop;
                        await setState(() {
                          yeardropdownValue = newValue;

                          cp = temp
                              .where((element) =>
                                  element.year.toString() == newValue &&
                                  element.seasonName == seasondropdownValue)
                              .toList();
                          seasonList.clear();
                          for (dynamic item in cp) {
                            seasonList.add(item.seasonName);
                          }
                          seasondropdownValue = seasonList.first;
                        });
                        await setState(() {
                          TotalestimateFloodNet = 0.0;
                          TotalestimateFloodGrossIncome = 0.0;
                          TotalestimateFloodExpenses = 0.0;
                          TotalestimateSprinklerNet = 0.0;
                          TotalestimateSprinklerGrossIncome = 0.0;
                          TotalestimateSprinklerExpenses = 0.0;
                        });
                      },
                      items: yrList != null && yrList.length != 0
                          ? yrList
                              .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value), value: value);
                            }).toList()
                          : <String>["0"]
                              .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value), value: value);
                            }).toList(),
                    ),
                  if (!_isBusy)
                    DropdownButton<String>(
                      value: seasonList != null && seasonList.length != 0
                          ? seasondropdownValue
                          : '0',
                      icon: const Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 16,
                      style: const TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        height: 2,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: (String newValue) async {
                        var temp = await crop;
                        await setState(() {
                          seasondropdownValue = newValue;
                          cp = temp
                              .where((element) =>
                                  element.seasonName == newValue &&
                                  element.year.toString() == yeardropdownValue)
                              .toList();
                        });
                        await setState(() {
                          TotalestimateFloodNet = 0.0;
                          TotalestimateFloodGrossIncome = 0.0;
                          TotalestimateFloodExpenses = 0.0;
                          TotalestimateSprinklerNet = 0.0;
                          TotalestimateSprinklerGrossIncome = 0.0;
                          TotalestimateSprinklerExpenses = 0.0;
                        });
                      },
                      items: seasonList != null && seasonList.length != 0
                          ? seasonList
                              .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value), value: value);
                            }).toList()
                          : <String>["0"]
                              .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                  child: Text(value), value: value);
                            }).toList(),
                    ),
                  // FutureBuilder(
                  //     future: crop,
                  //     builder: (context, snapshot) {
                  //       try {
                  //         if (snapshot.hasData) {
                  //           if (snapshot.data != null) {
                  //             return yearComponentBuilder(
                  //                 context, snapshot.data);
                  //           }
                  //         } else if (snapshot.hasError) {
                  //           return Center(
                  //             child: Text("No data found"),
                  //           );
                  //         } else if (!snapshot.hasData)
                  //           return Container(
                  //               height: size.height * 0.50,
                  //               child: Center(
                  //                 child: Text("No Data Found"),
                  //               ));
                  //         else
                  //           return Container(
                  //             child: CircularProgressIndicator(),
                  //           );
                  //       } on Exception catch (_, ex) {
                  //         return Center(
                  //           child: Text("No data found"),
                  //         );
                  //       }
                  //     }),

                  ElevatedButton(
                    onPressed: () async {
                      if (cp == null || cp.length == 0)
                        await setState(() {
                          cp = new List<CroppingPatternModel>();
                        });
                      await getFormDialogBox(context);
                    },
                    child: Text('Add'),
                  ),
                ],
              ),

              Column(
                children: [
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                            width: size.width,
                            decoration: BoxDecoration(
                                border: Border(
                              top: BorderSide(width: 0.2, color: Colors.black),
                              right:
                                  BorderSide(width: 0.2, color: Colors.black),
                              left: BorderSide(width: 0.2, color: Colors.black),
                            )),
                            child: Column(children: [
                              Table(
                                  defaultVerticalAlignment:
                                      TableCellVerticalAlignment.middle,
                                  border: TableBorder.all(
                                      width: 1.0, color: Colors.black),
                                  children: [
                                    TableRow(
                                        decoration: BoxDecoration(
                                          color: Colors.blue[900],
                                        ),
                                        children: <Widget>[
                                          Text(
                                            "Survey No",
                                            textScaleFactor: 1.5,
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "Crop",
                                            textScaleFactor: 1.5,
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "Area(Ha)",
                                            textScaleFactor: 1.5,
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            "Action",
                                            textScaleFactor: 1.5,
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                        ]),
                                  ]),
                              FutureBuilder(
                                future: crop,
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (snapshot.hasData) {
                                    if (cp != null && crop != null)
                                      return tableRowBuilder(context, cp);
                                    else
                                      return Center(
                                          child: CircularProgressIndicator());
                                  } else if (snapshot.hasData == null) {
                                    return Text("No Details Found");
                                  } else if (snapshot.hasError) {
                                    return Center(child: CircularProgressIndicator());
                                  } else
                                    return Text("");
                                },
                              )
                            ]),
                          ))
                    ],
                  ),
                  if (cp != null)
                    Container(
                      padding: EdgeInsets.all(10),
                      width: size.width,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Submit'),
                                    content:
                                        Text("Are You Sure Want To Proceed ?"),
                                    actions: <Widget>[
                                      ElevatedButton(
                                        child: Text("YES"),
                                        onPressed: () async {
                                          //Put your code here which you want to execute on Yes button click.
                                          try {
                                            CircularProgressIndicator();
                                            List<CroppingPatternModel> temp =
                                                await crop;
                                            var tempRes =
                                                insertCroppingPattern(temp);
                                            var result = await tempRes;
                                            if (result) {
                                              cp = null;
                                              ConvertCropPatternDetails();
                                              Navigator.of(context).pop();
                                            } else
                                              new Exception();
                                          } on Exception catch (_, ex) {
                                            SnackBar(
                                                content: Text(
                                                    "Something went wrong!"));
                                          }
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("NO"),
                                        onPressed: () {
                                          //Put your code here which you want to execute on No button click.
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                });
                            // try {
                            //   CircularProgressIndicator();
                            //   dynamic temp = await crop;
                            //   var tempRes = insertCroppingPattern(temp);
                            //   var result = await tempRes;
                            //   if (result) {
                            //     cp = null;
                            //     ConvertCropPatternDetails();
                            //   } else
                            //     new Exception();
                            // } on Exception catch (_, ex) {
                            //   SnackBar(content: Text("Something went wrong!"));
                            // }
                          },
                          child: Text('Submit'),
                        ),
                      ),
                    ),
                  if (cp != null)
                    Container(
                        width: size.width,
                        color: Colors.blue[900],
                        child: Padding(
                            padding:
                                EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            child: Text(
                              'ESTIMATED CROP ECONOMICS',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ))),
                  Container(
                      margin: EdgeInsets.all(5),
                      width: size.width,
                      padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      child: FutureBuilder(
                          future: crop,
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              if (cp != null && crop != null)
                                return economicsBuilder(context, cp);
                            } else {
                              return Center(
                                child: new CircularProgressIndicator(),
                              );
                            }
                          })),
                  Container(
                      width: size.width,
                      padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                      child: FutureBuilder(
                          future: crop,
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              if (cp != null && crop != null) {
                                List<CroppingPatternModel> temp = cp;
                                temp.forEach((element) {
                                  TotalestimateFloodExpenses +=
                                      element.estimateFloodExpenses;
                                  TotalestimateFloodGrossIncome +=
                                      element.estimateFloodGrossIncome;
                                  TotalestimateFloodNet +=
                                      element.estimateFloodNet;
                                  TotalestimateSprinklerExpenses +=
                                      element.estimateSprinklerExpenses;
                                  TotalestimateSprinklerGrossIncome +=
                                      element.estimateSprinklerGrossIncome;
                                  TotalestimateSprinklerNet +=
                                      element.estimateSprinklerNet;
                                });
                                return Container(
                                  width: size.width,
                                  padding:
                                      EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                                  child: Table(
                                      defaultVerticalAlignment:
                                          TableCellVerticalAlignment.middle,
                                      border: TableBorder.all(
                                          width: 1.0, color: Colors.black),
                                      children: [
                                        TableRow(
                                            decoration: BoxDecoration(
                                              color: Colors.blue[900],
                                            ),
                                            children: [
                                              Text(
                                                "        ",
                                                textScaleFactor: 1.5,
                                                style: TextStyle(fontSize: 10),
                                                textAlign: TextAlign.center,
                                              ),
                                              Text(
                                                "Flood",
                                                textScaleFactor: 1.5,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                                textAlign: TextAlign.center,
                                              ),
                                              Text(
                                                "Sprinkler",
                                                textScaleFactor: 1.5,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10),
                                                textAlign: TextAlign.center,
                                              ),
                                            ]),
                                        TableRow(children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              "Total Gross Income",
                                              textScaleFactor: 1.5,
                                              style: TextStyle(fontSize: 10),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              TotalestimateFloodGrossIncome
                                                  .toString(),
                                              textScaleFactor: 1.5,
                                              style: TextStyle(fontSize: 10),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              TotalestimateSprinklerGrossIncome
                                                  .toStringAsFixed(2),
                                              textScaleFactor: 1.5,
                                              style: TextStyle(fontSize: 10),
                                            ),
                                          ),
                                        ]),
                                      ]),
                                );
                              } else {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                            } else {
                              return Center(
                                child: Text(""),
                              );
                            }
                          })),
                ],
              )
            ],
          )),
    );
  }
}

Widget ErrorViewBuilder(BuildContext context, String error) {
  return Center(
    child: Text(error),
  );
}

GifController controller;

// actions: <Widget>[
//   IconButton(
//       alignment: Alignment.center,
//       onPressed: () {
//         BoxFit;
//         // onButtonPressed(
//         //     'assets/images/Farmer_ani1.gif');
//         if (_isPlaying) {
//           speak();
//         } else {
//           stop();
//         }
//         setState(() => _isPlaying = !_isPlaying);
//       },
//       icon: Icon(!_isPlaying ? Icons.stop : Icons.play_arrow)),
//   // Padding(
//   //     padding: EdgeInsets.all(8.0),
//   //     child: DropdownButton(
//   //       onChanged: (Language language) {
//   //         _changeLanguage(language);
//   //       },
//   //       underline: SizedBox(),
//   //       icon: Icon(
//   //         Icons.language,
//   //         color: Colors.white,
//   //       ),
//   //       items: Language.languageList()
//   //           .map<DropdownMenuItem<Language>>((lang) =>
//   //               DropdownMenuItem(
//   //                 value: lang,
//   //                 child: Row(
//   //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//   //                   children: <Widget>[Text(lang.name)],
//   //                 ),
//   //               ))
//   //           .toList(),
//   //     ))
// ],

// drawer: Drawer(
//   child: ListView(
//     padding: EdgeInsets.zero,
//     children: [
//       const DrawerHeader(
//         padding: EdgeInsets.all(50.0),
//         decoration: BoxDecoration(
//           color: Colors.white,
//           image: DecorationImage(
//               image: AssetImage("assets/images/Logo.png"),
//               fit: BoxFit.fitHeight,
//               alignment: Alignment.center),
//         ),
//       ),
//       ListTile(
//         leading: Icon(
//           Icons.info_outline,
//           color: Colors.green,
//         ),
//         title:
//             Text(DemoLocalization.of(context).translate('mydetails')),
//         onTap: () {
//           // Update the state of the app.
//           // ...
//           Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => FarmerDetailsScreen()));
//         },
//       ),
//       ListTile(
//         leading: Icon(
//           Icons.crop,
//           color: Colors.green,
//         ),
//         title: Text(
//             DemoLocalization.of(context).translate('cropadvisory')),
//         onTap: () {
//           // Update the state of the app.
//           // ...
//           Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => CropAdvisoryScreen()));
//         },
//       ),
//       ExpansionTile(
//         leading: Icon(
//           Icons.language,
//           color: Colors.green,
//         ),
//         title: Text("Language"),
//         children: <Widget>[
//           ListTile(
//             title: const Text("English"),
//             onTap: () {
//               _changeLanguage('en');
//             },
//           ),
//           ListTile(
//             title: const Text("Hindi"),
//             onTap: () {
//               _changeLanguage('hi');
//             },
//           ),
//         ],
//       ),
//       ListTile(
//         leading: Icon(
//           Icons.logout,
//           color: Colors.green,
//         ),
//         title: Text(DemoLocalization.of(context).translate('logout')),
//         onTap: () {
//           // Update the state of the app.
//           // ...

//           SecureStorage.setUserData(null);
//           // Navigator.pushReplacement(
//           //     context,
//           //     MaterialPageRoute(
//           //         builder: (BuildContext context) => new MyCustomForm()));
//           Navigator.of(context).popUntil((route) => route.isFirst);
//         },
//       ),
//     ],
//   ),
// ),

// Row(
//   mainAxisAlignment: MainAxisAlignment.start,
//   children: <Widget>[
//     Wrap(children: [
//       FutureBuilder(
//         future: ConvertFarmerDetails(),
//         builder: (context, FarmerDetails) {
//           if (FarmerDetails.hasData) {
//             return Padding(
//               padding: const EdgeInsets.all(30.0),
//               child: Column(
//                 children: [
//                   Text(DemoLocalization.of(context)
//                           .translate('name') +
//                       ": " +
//                       fd.response.farmerName),
//                   Text(DemoLocalization.of(context)
//                           .translate('farmerreg') +
//                       ":" +
//                       fd.response.registrationNo),
//                   Text(DemoLocalization.of(context)
//                           .translate('contactno') +
//                       ": " +
//                       fd.response.contactNo),
//                   Text(DemoLocalization.of(context)
//                           .translate('address') +
//                       ": " +
//                       fd.response.address),
//                   Text(DemoLocalization.of(context)
//                           .translate('network') +
//                       ": " +
//                       fd.response.network),
//                   Text(DemoLocalization.of(context)
//                           .translate('project_name') +
//                       ":" +
//                       fd.response.projectName),
//                   // ElevatedButton(
//                   //   style: ElevatedButton.styleFrom(
//                   //     primary: Colors.blue,
//                   //     elevation: 3,
//                   //     shape: RoundedRectangleBorder(
//                   //         borderRadius:
//                   //             BorderRadius.circular(32.0)),
//                   //     padding: EdgeInsets.all(20),
//                   //   ),
//                   //   onPressed: () {
//                   //     speak();
//                   //   },
//                   //   child: const Text('PLAY VOICE',
//                   //       style: TextStyle()),
//                   // ),
//                   // ElevatedButton(
//                   //   style: ElevatedButton.styleFrom(
//                   //     primary: Colors.blue,
//                   //     elevation: 3,
//                   //     shape: RoundedRectangleBorder(
//                   //         borderRadius:
//                   //             BorderRadius.circular(32.0)),
//                   //     padding: EdgeInsets.all(20),
//                   //   ),
//                   //   onPressed: () {
//                   //     _stop();
//                   //   },
//                   //   child: const Text('STOP VOICE',
//                   //       style: TextStyle()),
//                   // ),
//                 ],
//               ),
//             );
//           } else if (FarmerDetails.hasError) {
//             return Text("${FarmerDetails.error}");
//           }
//           return CircularProgressIndicator();
//         },
//       ),
//     ]),
//     Expanded(
//       // width: MediaQuery.of(context).size.width/2,
//       child: Column(children: [
//         Image.asset(
//           imageLink,
//           fit: BoxFit.fitHeight,
//           height: 150,
//         ),
//          Padding(
//             padding: EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
//             child: ElevatedButton(
//               style: ElevatedButton.styleFrom(
//                 primary: Colors.blue,
//                 elevation: 2,
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(9.0)),
//               ),
//               child: Text(btnText, style: TextStyle(height: 0.2)),
//               onPressed: () async {
//                 if (_isPlaying) {
//                   setState(() {imageLink = "assets/images/Farmer_ani1.gif"; });
//                   setState(() => _isPlaying = false);
//                   getDataLength();
//                   setState(() => btnText = "Stop");
//                   await speak();
//                   //_controller.reset();
//                   //flutterTts.completionHandler;
//                 } else {
//                   setState(() => _isPlaying = true);
//                   setState(() {imageLink = "assets/images/Farmer_ani1.png"; });
//                   setState(() => btnText = "Play");
//                   await stop();
//                   //_controller.repeat();
//                 }
//               },
//             ))
//       ]),
//     ),
//   ],
// ),

// Widget listViewBuilder(BuildContext context, List<dynamic> values) {
//   return ListView.builder(
//       itemCount: values.length,
//       itemBuilder: (BuildContext context, int index) {
//         return new Padding(
//             padding: EdgeInsets.fromLTRB(0.0,0.0, 10.0, 10.0),
//             child: Card(
//               child: new InkWell(
//               child: Container(
//               height: 140,
//               color: Colors.green,
//               child: //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
//                   Container(
//                 decoration: BoxDecoration(
//                     border: Border(
//                   left: BorderSide(width: 3.0, color: Colors.green),
//                   bottom: BorderSide(width: 4.0, color: Colors.green),
//                 )),
//                 width: double.maxFinite,
//                 child: ListTile(
//                   onTap: () async {
//                     await SecureStorage.setLandId(values[index].landId);
//                   },
//                   title: Text(
//                     "Survey Number"+
//                         " : " +
//                         values[index].roRSurveyNo.toString() + "\t" + "             Area" + " : " + values[index].roRTotalArea.toString() +  "\n\n" + "Village" + " : " + values[index].roRTotalArea.toString() +
//                            "\t\t" + "                               Chak No" + " : " + values[index].chakno.toString(),
//                     style: TextStyle(
//                         color: Colors.black, fontWeight: FontWeight.w500),
//                   ),
//                   // subtitle: Text(
//                   //   DemoLocalization.of(context).translate('roRTotalArea') +
//                   //       " : " +
//                   //       values[index].roRTotalArea.toString() +
//                   //       "\n" +
//                   //       DemoLocalization.of(context)
//                   //           .translate('villageName') +
//                   //       " : " +
//                   //       values[index].villageName +
//                   //       "\n" +
//                   //       DemoLocalization.of(context)
//                   //           .translate('projectName') +
//                   //       " : " +
//                   //       values[index].projectName +
//                   //       "\n" +
//                   //       DemoLocalization.of(context).translate('chakno') +
//                   //       " : " +
//                   //       values[index].chakno.toString(),
//                   //   style: TextStyle(
//                   //       color: Colors.black, fontWeight: FontWeight.w500),
//                   // ),
//                   // leading: Icon(
//                   //   Icons.album,
//                   //   color: Colors.green,
//                   // ),
//                   // trailing: Icon(
//                   //   Icons.arrow_forward,
//                   //   size: 20,
//                   //   color: Colors.green,
//                   // ),
//                 ),
//                 alignment: Alignment.centerLeft,
//               ),
//             ))));

//         // ListTile(
//         //   leading: Text((values[index].id).toString()),
//         //   title: Text((values[index].stateName).toString()),
//         // );
//       });
// }

// FlutterTts flutterTts = FlutterTts();

// Future<FarmerDetailsModel> farmer;
// FarmerDetailsModel fd;
// var postData;

// var data = [];
// String strData = "";
// var l = null;

// ConvertFarmerDetails() async {
//   farmer = fetchFarmerDetailsResponse();
//   fd = await farmer;
//   return fd;
// }

// getDataLength() async {
//   farmer = fetchFarmerDetailsResponse();
//   fd = await farmer;
//   data.add(fd.response.farmerName +
//       " , " +
//       fd.response.registrationNo +
//       " , " +
//       fd.response.contactNo +
//       " , " +
//       fd.response.address +
//       " , " +
//       fd.response.network +
//       " , " +
//       fd.response.projectName);
//   print(data);
//   strData = DemoLocalization.of(context).translate('name') + " " + fd.response.farmerName + DemoLocalization.of(context).translate('farmerreg') + " " + fd.response.registrationNo + " , " + DemoLocalization.of(context).translate('contactno') + " " + fd.response.contactNo +" , " + DemoLocalization.of(context).translate('address') + " " + fd.response.address +" , " + DemoLocalization.of(context).translate('network') + " " +
//   fd.response.network +" , " + DemoLocalization.of(context).translate('projectName') + " " + fd.response.projectName;
//   print(strData);
//   // print(strData.length);
//   // print(data.length);
//   l = strData.split(',');
//   print(l.length);
// }

// String btnText = "Play";
// String imageLink = "assets/images/Farmer_ani1.png";

// Future speak() async {
//   // print(await flutterTts.getLanguages);

//   await flutterTts.setLanguage("hi-IN");
//   await flutterTts.setSpeechRate(0.2);
//   await flutterTts.awaitSpeakCompletion(true);
//   for (int i = 0; i < l.length; i++) {
//     await flutterTts.speak(l[i]);
//     print(l[i]);
//     if (i == (l.length - 1)) {
//       btnText = "Play";
//       setState(() {imageLink = "assets/images/Farmer_ani1.png"; });
//     }
//   }

//   return false;
// }

// Future speak() async {
//   // print(await flutterTts.getLanguages);
//   setState(() {
//     imageLink = "assets/images/Farmer_ani1.gif";
//   });

//   await flutterTts.setLanguage("hi-IN");
//   await flutterTts.setSpeechRate(0.2);
//   await flutterTts.speak(DemoLocalization.of(context).translate('name') +
//       ", " +
//       fd.response.farmerName +
//       ". " +
//       DemoLocalization.of(context).translate('farmerreg') +
//       ", " +
//       fd.response.registrationNo +
//       ". " +
//       DemoLocalization.of(context).translate('contactno') +
//       ", " +
//       fd.response.contactNo +
//       ". " +
//       DemoLocalization.of(context).translate('address') +
//       ", " +
//       fd.response.address +
//       ". " +
//       DemoLocalization.of(context).translate('network') +
//       ", " +
//       fd.response.network +
//       ". " +
//       DemoLocalization.of(context).translate('project_name') +
//       ", " +
//       fd.response.projectName);
// }

// Future stop() async {
//   await flutterTts.stop();
//   setState(() {
//     imageLink = "assets/images/Farmer_ani1.png";
//   });
// }

// GoogleTranslator translator = new GoogleTranslator();

// body: Container(
//   height: size.height,
//   // width: MediaQuery.of(context).size.width,
//   child: Column(
//     children: <Widget>[
//   Row(
//     children: [
//       Wrap(
//         children: [
//           FutureBuilder (
//     future: ConvertFarmerDetails(),
//     builder: (context,FarmerDetails) {
//      if(FarmerDetails.hasData){
//        return Padding(
//          padding: const EdgeInsets.all(30.0),
//          child: Column(
//            children: [
//             Text(DemoLocalization.of(context).translate('name') + ": " + fd.response.farmerName),
//             Text(DemoLocalization.of(context).translate('farmerreg') + ":" + fd.response.registrationNo),
//             Text(DemoLocalization.of(context).translate('contactno') + ": " + fd.response.contactNo),
//             Text(DemoLocalization.of(context).translate('address') + ": " + fd.response.address),
//             Text(DemoLocalization.of(context).translate('network') + ": " + fd.response.network),
//             Text(DemoLocalization.of(context).translate('project_name') + ":" + fd.response.projectName),
//             ElevatedButton(
//         style: ElevatedButton.styleFrom(
//           primary: Colors.blue,
//           elevation: 3,
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(32.0)),
//           padding: EdgeInsets.all(20),
//         ),
//         onPressed:  () {
//           speak();
//         },
//         child: const Text('PLAY VOICE', style: TextStyle()),

//       ),
//            ],
//          ),
//        );

//      }else if(FarmerDetails.hasError){
//        return Text("${FarmerDetails.error}");
//      }
//      return CircularProgressIndicator();
//     },
//   ),
//   ]),
//   Expanded(
//     // width: MediaQuery.of(context).size.width/2,
//     child: (
//       Image.asset("assets/images/Farmer_ani1.gif",
//       fit: BoxFit.cover,
//       height: 300,)
//     ),
//   ),
//    Expanded(
//             child: FutureBuilder(
//                 future: fetchFarmerLandResponse(),
//                 builder: (context, landDetails) {
//                   if (landDetails.hasData) {
//                     return listViewBuilder(context, landDetails.data);
//                   }
//                 }),
//       ),
//   ],
//   )
//     ],)

// ),

// child: FutureBuilder(
//   future: fetchFarmerLandResponse(),
//   builder: (context, landDetails) {
//     if (landDetails.hasData) {
//       return listViewBuilder(context, landDetails.data);
//     } else if (landDetails.hasError) {
//       return ErrorViewBuilder(
//           context, "No Land Details Found");
//     }
//     return Center(
//       child: CircularProgressIndicator(),
//     );
//   },
// ),

//    Expanded(child: FutureBuilder(
//   future: fetchFarmerLandResponse(),
//   builder: (context, landDetails) {
//     if (landDetails.hasData) {
//       return listViewBuilder(context, landDetails.data);
//     } else if (landDetails.hasError) {
//       return ErrorViewBuilder(
//           context, "No Land Details Found");
//     }
//     return Center(
//       child: CircularProgressIndicator(),
//     );
//   },
// ),
// )
