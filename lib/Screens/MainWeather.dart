import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/FarmWeatherScreen.dart';
import 'package:fms_mobileapp/Screens/JalpradhanDashboardScreen.dart';
import 'package:fms_mobileapp/Screens/dashboard.dart';
import 'package:fms_mobileapp/Screens/forecast_screen.dart';
import 'package:fms_mobileapp/Screens/location_screen.dart';
import 'package:fms_mobileapp/services/forecastInformer.dart';
import 'package:fms_mobileapp/services/weather_informer.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:fms_mobileapp/services/weather_informer.dart';
import 'package:provider/provider.dart';
import 'package:fms_mobileapp/utils/constants.dart';
import 'package:fms_mobileapp/Screens/city_screen.dart';
import 'package:fms_mobileapp/Screens/forecast_screen.dart';
import 'package:intl/intl.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:fms_mobileapp/services/second_weather_box.dart';
import 'package:fms_mobileapp/services/weather_informer.dart';
import 'package:provider/provider.dart';

import 'loading_screen.dart';

class MainWeatherScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new MainWeatherScreenState();
  }
}

class MainWeatherScreenState extends State<MainWeatherScreen> {
  // bool isLoading = false;
  // WeatherData weatherData;
  // ForecastData forecastData;
  // Location location;
  // LocationData currentLocation;
   List<Widget> cards = [];
  ForecastInformer fcst;
  String error;

  void initState() {
    super.initState();
    //cardBuilder();
    // location = new Location();
    // loadWeather();
  }

  // @override
  // void didChangeDependencies() {
  //   // TODO: implement didChangeDependencies
  //   super.didChangeDependencies();
  //   cardBuilder();
  // }




  void cardBuilder() {
    int length = ForecastInformer.weatherData['days'].length;
    int flag = 0;
    for (int i = 0; i < length; i++) {
      fcst = new ForecastInformer();
      fcst.buildForecast(i);
      flag = flag == 0 ? 1 : 0;
      print("flag :$flag");
      setState(() {
        cards.add(forecastCard(flag));
      });
    }
  }

  Widget forecastCard(int flag) {
    return Card(
      margin: EdgeInsets.only(top: 0.0, bottom: 2.0),
      // clipBehavior: Clip.antiAlias,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Ink.image(
            image: flag == 1
                ? AssetImage('assets/images/SunRising2.jpg')
                : AssetImage('assets/images/ForecastImg2.jpg'),
            height: 250,
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.6), BlendMode.dstATop),
          ),
          Padding(
            padding: const EdgeInsets.all(6.0),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.1,
              width: MediaQuery.of(context).size.width * 0.3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(fcst.date,
                      textAlign: TextAlign.left,
                      style: new TextStyle(color: Colors.black)),
                  // Image.network(
                  //     'https://openweathermap.org/img/w/${forecast.clouds}.png'),
                  Row(
                    children: [
                      IconButton(onPressed: null, icon: new Icon(WeatherIcons.thermometer,color: Colors.black)),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          fcst.temperature.toString() + " °C",
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://thumbs.dreamstime.com/b/vector-icon-thermometer-up-arrow-rise-temperature-temperature-sensor-simple-design-vector-icon-175423814.jpg',
                        height: 30,
                        width: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          fcst.maxTemp.toString() + " °C",
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset(
                        'assets/images/decreasing_temp.jpg',
                        height: 25,
                        width: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          fcst.minTemp.toString() + " °C",
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      // Image.network(
                      //   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-9zkVUhH0-kAEbNdT6Ei-4sC3dAe7nBDnodDyLLrJ4kb1H7ajeDJ5S9PnePRC7eM0uGw&usqp=CAU',
                      //   height: 28,
                      //   width: 30,
                      // ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Text(
                          fcst.humidity,
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  // Row(
                  //   children: [
                  //     // Image.network(
                  //     //     'https://th.bing.com/th/id/OIP.L8uzDXZg3bPW6PbBfXlp8AHaI5?pid=ImgDet&rs=1',height: 28,width: 30,),
                  //     Padding(
                  //       padding: const EdgeInsets.all(2.0),
                  //       child: Icon(Icons.cloud),
                  //     ),
                  //     Padding(
                  //       padding: const EdgeInsets.fromLTRB(2.0, 0.0, 8.0, 8.0),
                  //       child: Text(
                  //         forecast.main,
                  //         style: TextStyle(fontSize: 18),
                  //       ),
                  //     ),
                  //   ],
                  // ),
                ],
              ),
            )),
          // Padding(
          //   padding: const EdgeInsets.all(20.0),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       Row(
          //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //           children: [
          //             Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               mainAxisAlignment: MainAxisAlignment.start,
          //               children: [
          //                 Text(
          //                   fcst.date, // Time
          //                   style: TextStyle(
          //                       fontSize: 20.0, fontWeight: FontWeight.w500),
          //                 ),
          //               ],
          //             ),
          //             Column(
          //               children: [
          //                 Row(
          //                   mainAxisAlignment: MainAxisAlignment.spaceAround,
          //                   textBaseline: TextBaseline.ideographic,
          //                   crossAxisAlignment: CrossAxisAlignment.baseline,
          //                   children: [
          //                     Text(fcst.temperature,
          //                         style: TextStyle(
          //                             fontSize: 45.0,
          //                             fontWeight: FontWeight.w600)),
          //                     Text(
          //                       "°C",
          //                       style: TextStyle(
          //                           fontSize: 30.0,
          //                           fontWeight: FontWeight.w400),
          //                     )
          //                   ],
          //                 ),
          //               ],
          //             ),
          //             Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 Container(
          //                   padding: EdgeInsets.only(right: 5.0, bottom: 6.0),
          //                   child: Icon(
          //                     fcst.weatherIcon,
          //                     size: 50.0,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ]),
          //       SizedBox(
          //         height: 15,
          //       ),
          //       Divider(
          //         color: Colors.white,
          //         height: 20,
          //         thickness: 2,
          //       ),
          //       Row(
          //         children: [
          //           Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //             children: [
          //               Text(
          //                 fcst.humidity,
          //                 overflow: TextOverflow.ellipsis,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.preciption,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.uvIndex,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.windSpeed,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.minTemp,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //             ],
          //           ),
          //           SizedBox(
          //             width: 40.0,
          //           ),
          //           Column(
          //             crossAxisAlignment: CrossAxisAlignment.start,
          //             children: [
          //               Text(
          //                 fcst.dewPoint,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.cloudCover,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.pressure,
          //                 overflow: TextOverflow.ellipsis,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.windDirect,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //               SizedBox(
          //                 height: 5.0,
          //               ),
          //               Text(
          //                 fcst.maxTemp,
          //                 style: TextStyle(
          //                     fontSize: 16.0, fontWeight: FontWeight.w400),
          //               ),
          //             ],
          //           )
          //         ],
          //       )
          //     ],
          //   ),
          // )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Flutter Weather App",
        home: DefaultTabController(
          length: 2,
          child: Scaffold(
              appBar: AppBar(
                title: Text("Weather App"),
                backgroundColor: Colors.green,
                bottom: TabBar(
                  tabs: [
                    Text("Current Location", style: TextStyle(fontSize: 18),),
                    Text("Farm Location", style: TextStyle(fontSize: 18),)
                  ]
                ),
                leading: BackButton(
                    onPressed: () =>
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DynamicDashboardScreen()))),
              ),
              body: TabBarView(
                children: [
                  Container(child: LocationScreen()),
                  Container(child: FarmWeatherScreen())
                  // Column(
                  //   children: [
                  //     LoadingScreen(),
                  //     //ForecastViewer()
                  //    /* Card(child: Container(
                  //       child: Consumer<WeatherInformer>(
                  //         builder: (context, weatherData, child) {
                  //           // weatherData every time gets updates when they are any changes in the data and UI gets updated becasue of using provider architecture
                  //           // setFilter(weatherData.backgroundImage);
                  //           return SafeArea(
                  //             child: Stack(
                  //               children: [
                  //                 Container(
                  //                   // decoration: BoxDecoration(
                  //                   //   image: DecorationImage(
                  //                   //     image: AssetImage(weatherData
                  //                   //         .backgroundImage), // Setting background image

                  //                   //     fit: BoxFit.cover,
                  //                   //     colorFilter: ColorFilter.mode(
                  //                   //         Colors.white.withOpacity(0.8), BlendMode.dstATop),
                  //                   //   ),
                  //                   // ),
                  //                   // constraints: BoxConstraints.expand(),
                  //                   child: SafeArea(
                  //                     child: Container(
                  //                       child: Column(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.start,
                  //                         children: <Widget>[
                                         
                  //                       // ForecastViewer(),
                                         
                  //                        /*   Row(children: [
                  //                             Card(child: Container(child: ForecastViewer())),
                  //                             // Card(child: Container(height: 400,width: 185,child: Text("Card 2"))),
                  //                           ],),
                  //                           Column(children: [
                  //                             // Card(child: Container(child: Text("Date Time")),),
                                            

                                             
                  //                                         // SafeArea(
                  //                                         //   child: Padding(
                  //                                         //     padding: const EdgeInsets.all(0.0),
                  //                                         //     child: Container(
                  //                                         //       height: MediaQuery.of(context).size.height * 0.35,
                  //                                         //       child: ListView(children: cards,)
                  //                                         //     //   child:  ListView.builder(
                  //                                         //     //           itemCount: ForecastInformer.weatherData['days'].length,
                  //                                         //     //           scrollDirection: Axis.horizontal,
                  //                                         //     //           itemBuilder: (context, index) => 
                  //                                         //     //             Card(child: Container(child: Text(ForecastInformer.weatherData['days'].temperature),))
                  //                                         //     //             // WeatherItem(
                  //                                         //     //             //   forecast: forecastData.list.elementAt(index),
                  //                                         //     //             //   weather: weatherData,)
                  //                                         //     //             //   )
                                                              
                  //                                         //     // ),
                  //                                         //   ),
                  //                                         //   )
                  //                                         // )
                                              
                  //                             // ListView(
                  //                             //   children: cards,
                  //                             // )
                  //                           ],),
                  //                          */ 
                  //                           // Row(
                  //                           //   children: [
                  //                           //     Expanded(
                  //                           //       child: Card(
                  //                           //         child: Container(
                  //                           //           height: 300,
                  //                           //           width: 200,
                  //                           //           child: Column(
                  //                           //             children: [
                  //                           //               // Padding(
                  //                           //               //   padding: const EdgeInsets.all(8.0),
                  //                           //               //   child: Row(
                  //                           //               //           children: [
                  //                           //               //             IconButton(
                  //                           //               //               color: textAndIconColor,
                  //                           //               //               icon: Icon(Icons.location_on),
                  //                           //               //               iconSize: 25.0,
                  //                           //               //               onPressed: () {
                  //                           //               //                 weatherData.getLocationWeatherInfo();
                  //                           //               //               },
                  //                           //               //             ),
                  //                           //               //             // Text(
                  //                           //               //             //   weatherData.cityName,
                  //                           //               //             //   softWrap: true,
                  //                           //               //             //   style: TextStyle(
                  //                           //               //             //       fontWeight: FontWeight.bold,
                  //                           //               //             //       ),
                  //                           //               //             // ),
                  //                           //               //           ],
                  //                           //               //         ),
                  //                           //               // ),
                  //                           //               Row(
                  //                           //                 children: [
                  //                           //                   IconButton(
                  //                           //                       onPressed:
                  //                           //                           null,
                  //                           //                       icon:
                  //                           //                           new Icon(
                  //                           //                         WeatherIcons
                  //                           //                             .thermometer,
                  //                           //                         color: Colors
                  //                           //                             .black,
                  //                           //                       )),
                  //                           //                   Padding(
                  //                           //                     padding:
                  //                           //                         const EdgeInsets
                  //                           //                                 .all(
                  //                           //                             5.0),
                  //                           //                     child: Text(
                  //                           //                       weatherData
                  //                           //                           .temperature,
                  //                           //                       style:
                  //                           //                           TextStyle(),
                  //                           //                     ),
                  //                           //                   ),
                  //                           //                   Text(
                  //                           //                     weatherData
                  //                           //                         .tempMeteric,
                  //                           //                     style:
                  //                           //                         TextStyle(),
                  //                           //                   ),
                  //                           //                 ],
                  //                           //               ),
                  //                           //               Row(
                  //                           //                 children: [
                  //                           //                   Padding(
                  //                           //                     padding:
                  //                           //                         const EdgeInsets
                  //                           //                                 .all(
                  //                           //                             5.0),
                  //                           //                     child: Row(
                  //                           //                       children: [
                  //                           //                         IconButton(
                  //                           //                             onPressed:
                  //                           //                                 null,
                  //                           //                             icon:
                  //                           //                                 new Icon(
                  //                           //                               WeatherIcons
                  //                           //                                   .raindrop,
                  //                           //                               color:
                  //                           //                                   Colors.black,
                  //                           //                             )),
                  //                           //                         Text(
                  //                           //                           weatherData
                  //                           //                               .humidity
                  //                           //                               .toString(),
                  //                           //                           softWrap:
                  //                           //                               true,
                  //                           //                           style:
                  //                           //                               TextStyle(),
                  //                           //                         ),
                  //                           //                       ],
                  //                           //                     ),
                  //                           //                   ),
                  //                           //                 ],
                  //                           //               ),
                  //                           //               //  Row(
                  //                           //               //   children: [
                  //                           //               //     // Image.network(
                  //                           //               //     //     'https://th.bing.com/th/id/OIP.L8uzDXZg3bPW6PbBfXlp8AHaI5?pid=ImgDet&rs=1',height: 28,width: 30,),
                  //                           //               //     Padding(
                  //                           //               //       padding: const EdgeInsets.all(8.0),
                  //                           //               //       child: Icon(Icons.cloud),
                  //                           //               //     ),
                  //                           //               //     Padding(
                  //                           //               //       padding: const EdgeInsets.fromLTRB(2.0, 8.0, 8.0, 8.0),
                  //                           //               //       child: Text(
                  //                           //               //         widget.weather.main,
                  //                           //               //         softWrap: true,
                  //                           //               //         style: TextStyle(fontSize: 18),
                  //                           //               //       ),
                  //                           //               //     ),
                  //                           //               //   ],
                  //                           //               // ),
                  //                           //               Align(
                  //                           //                   alignment: Alignment
                  //                           //                       .bottomLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Row(
                  //                           //                       children: [
                  //                           //                         IconButton(
                  //                           //                             onPressed:
                  //                           //                                 null,
                  //                           //                             icon:
                  //                           //                                 new Icon(
                  //                           //                               WeatherIcons
                  //                           //                                   .sunrise,
                  //                           //                               color:
                  //                           //                                   Colors.black,
                  //                           //                             )),
                  //                           //                         Text(
                  //                           //                           weatherData
                  //                           //                                   .sunrise +
                  //                           //                               " AM",
                  //                           //                           softWrap:
                  //                           //                               true,
                  //                           //                         ),
                  //                           //                       ],
                  //                           //                     ),
                  //                           //                   )),
                  //                           //               Align(
                  //                           //                   alignment: Alignment
                  //                           //                       .bottomLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Row(
                  //                           //                       children: [
                  //                           //                         IconButton(
                  //                           //                             onPressed:
                  //                           //                                 null,
                  //                           //                             icon: new Icon(
                  //                           //                                 WeatherIcons
                  //                           //                                     .sunset,
                  //                           //                                 color:
                  //                           //                                     Colors.black)),
                  //                           //                         Text(
                  //                           //                           weatherData
                  //                           //                                   .sunset +
                  //                           //                               " PM",
                  //                           //                           softWrap:
                  //                           //                               true,
                  //                           //                         ),
                  //                           //                       ],
                  //                           //                     ),
                  //                           //                   )),

                  //                           //               // Padding(
                  //                           //               //   padding: const EdgeInsets.all(8.0),
                  //                           //               //   child: Text("99.99%"),
                  //                           //               // ),
                  //                           //               // Padding(
                  //                           //               //   padding: const EdgeInsets.all(8.0),
                  //                           //               //   child: Text("____%"),
                  //                           //               // ),
                  //                           //               // Padding(
                  //                           //               //   padding: const EdgeInsets.all(8.0),
                  //                           //               //   child: Text("SUN RISE :-"),
                  //                           //               // ),
                  //                           //               // Padding(
                  //                           //               //   padding: const EdgeInsets.all(8.0),
                  //                           //               //   child: Text("SUN SET :- "),
                  //                           //               // )
                  //                           //             ],
                  //                           //           ),
                  //                           //         ),
                  //                           //       ),
                  //                           //     ),
                  //                           //     Card(
                  //                           //         child:
                  //                           //             SingleChildScrollView(
                  //                           //       scrollDirection:
                  //                           //           Axis.vertical,
                  //                           //       child: Container(
                  //                           //           height: 300,
                  //                           //           width: 200,
                  //                           //           child: Padding(
                  //                           //             padding:
                  //                           //                 const EdgeInsets
                  //                           //                     .symmetric(),
                  //                           //             child: Column(
                  //                           //               children: [
                  //                           //                 // Align(
                  //                           //                 //     child: Padding(
                  //                           //                 //   padding: EdgeInsets.all(5.0),
                  //                           //                 //   child: Text(
                  //                           //                 //     "FARM lOCATION",
                  //                           //                 //     style: TextStyle(fontWeight: FontWeight.bold),
                  //                           //                 //   ),
                  //                           //                 // )

                  //                           //                 Align(
                  //                           //                   alignment:
                  //                           //                       Alignment
                  //                           //                           .topLeft,
                  //                           //                   child: Padding(
                  //                           //                       padding:
                  //                           //                           EdgeInsets
                  //                           //                               .all(
                  //                           //                                   5.0),
                  //                           //                       child: Text(weatherData
                  //                           //                               .tempMin
                  //                           //                               .toString() +
                  //                           //                           " °C")),
                  //                           //                 ),
                  //                           //                 Align(
                  //                           //                   alignment:
                  //                           //                       Alignment
                  //                           //                           .topLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Text(weatherData
                  //                           //                             .tempMax
                  //                           //                             .toString() +
                  //                           //                         " °C"),
                  //                           //                   ),
                  //                           //                 ),

                  //                           //                 Align(
                  //                           //                   alignment:
                  //                           //                       Alignment
                  //                           //                           .topLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Text(
                  //                           //                         weatherData
                  //                           //                             .pressure
                  //                           //                             .toString()),
                  //                           //                   ),
                  //                           //                 ),

                  //                           //                 // Align(
                  //                           //                 //   alignment: Alignment.topLeft,
                  //                           //                 //   child: Padding(padding: EdgeInsets.all(5.0),
                  //                           //                 //     child: Text("DEW POINT:-"),),
                  //                           //                 // ),

                  //                           //                 Align(
                  //                           //                   alignment:
                  //                           //                       Alignment
                  //                           //                           .topLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Text(
                  //                           //                         weatherData
                  //                           //                             .windSpeed
                  //                           //                             .toString()),
                  //                           //                   ),
                  //                           //                 ),

                  //                           //                 Align(
                  //                           //                   alignment:
                  //                           //                       Alignment
                  //                           //                           .topLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Text(weatherData
                  //                           //                         .windDirect
                  //                           //                         .toString()),
                  //                           //                   ),
                  //                           //                 ),

                  //                           //                 Align(
                  //                           //                   alignment:
                  //                           //                       Alignment
                  //                           //                           .topLeft,
                  //                           //                   child: Padding(
                  //                           //                     padding:
                  //                           //                         EdgeInsets
                  //                           //                             .all(
                  //                           //                                 5.0),
                  //                           //                     child: Text(
                  //                           //                         weatherData
                  //                           //                                 .visibility +
                  //                           //                             " KM"),
                  //                           //                   ),
                  //                           //                 ),

                  //                           //                 //  Align(
                  //                           //                 //    alignment: Alignment.topLeft,
                  //                           //                 //   child: Padding(padding: EdgeInsets.all(5.0),
                  //                           //                 //     child: Text("SOLAR RADIATION:-"),),
                  //                           //                 // ),

                  //                           //                 // Align(
                  //                           //                 //   alignment: Alignment.topLeft,
                  //                           //                 //   child: Padding(
                  //                           //                 //     padding: EdgeInsets.all(5.0),
                  //                           //                 //     child: Text("PRECIPITATION :-" +weather.),
                  //                           //                 //   ),
                  //                           //                 // ),
                  //                           //               ],
                  //                           //             ),
                  //                           //           )),
                  //                           //     )),
                                                 
                  //                           //   ],
                  //                           // ),
                  //                         //   Card(
                  //                         //   child: Container(
                  //                         //       child: SizedBox(
                  //                         //     height: 5.0,
                  //                         //     child: Padding(
                  //                         //       padding: EdgeInsets.all(0.0),
                  //                         //       child: Row(
                  //                         //         children: [
                  //                         //           Text("Hello ",
                  //                         //               textAlign: TextAlign.left,
                  //                         //               style: new TextStyle(color: Colors.white)),
                  //                         //           Text("Time" ,
                  //                         //               style: new TextStyle(color: Colors.white)),
                  //                         //         ],
                  //                         //       ),
                  //                         //     ),
                  //                         //   )),
                  //                         // )
                  //                           // Container(
                  //                           //   child: AppBar(
                  //                           //     // AppBar
                  //                           //     automaticallyImplyLeading: false,
                  //                           //     leading: IconButton(
                  //                           //       color: textAndIconColor,
                  //                           //       icon: Icon(Icons.location_on),
                  //                           //       iconSize: 25.0,
                  //                           //       onPressed: () {
                  //                           //         weatherData.getLocationWeatherInfo();
                  //                           //       },
                  //                           //     ),
                  //                           //     title: Text(
                  //                           //       weatherData.cityName,
                  //                           //       textAlign: TextAlign.center,
                  //                           //       overflow: TextOverflow.ellipsis,
                  //                           //       maxLines: 2,
                  //                           //       style: TextStyle(
                  //                           //           fontWeight: FontWeight.w900,
                  //                           //           fontSize: 20.0),
                  //                           //     ),
                  //                           //     centerTitle: true,
                  //                           //     backgroundColor: Colors.transparent,
                  //                           //     elevation: 0,
                  //                           //     actions: <Widget>[
                  //                           //       IconButton(
                  //                           //         icon: Icon(Icons.search),
                  //                           //         iconSize: 25.0,
                  //                           //         onPressed: () {
                  //                           //           Navigator.push(
                  //                           //               context,
                  //                           //               MaterialPageRoute(
                  //                           //                   builder: (context) =>
                  //                           //                       CityScreen()));
                  //                           //         },
                  //                           //         tooltip: 'Search',
                  //                           //       ),
                  //                           //     ],
                  //                           //   ),
                  //                           // ),
                  //                           // Expanded(
                  //                           //   child: Card(
                  //                           //       color:
                  //                           //           Colors.transparent.withOpacity(_opacityUp),
                  //                           //       margin: EdgeInsets.only(
                  //                           //           top: 10.0,
                  //                           //           left: 8.0,
                  //                           //           right: 8.0,
                  //                           //           bottom: 0.0),
                  //                           //       child: Row(
                  //                           //         children: [
                  //                           //           SizedBox(
                  //                           //             width: 18.0,
                  //                           //           ),
                  //                           //           Column(
                  //                           //             crossAxisAlignment:
                  //                           //                 CrossAxisAlignment.start,
                  //                           //             children: [
                  //                           //               SizedBox(
                  //                           //                 height: 35.0,
                  //                           //               ),
                  //                           //               Row(
                  //                           //                 children: [
                  //                           //                   Text(
                  //                           //                     weatherData.temperature,
                  //                           //                     style: TextStyle(fontSize: 70.0),
                  //                           //                   ),
                  //                           //                   Text(
                  //                           //                     weatherData.tempMeteric,
                  //                           //                     style: TextStyle(fontSize: 30.0),
                  //                           //                   ),
                  //                           //                 ],
                  //                           //               ),
                  //                           //               SizedBox(
                  //                           //                 height: 20.0,
                  //                           //               ),
                  //                           //               Column(
                  //                           //                 crossAxisAlignment:
                  //                           //                     CrossAxisAlignment.start,
                  //                           //                 children: [
                  //                           //                   Text(
                  //                           //                     weatherData.date,
                  //                           //                     style: TextStyle(fontSize: 18.0),
                  //                           //                   ),
                  //                           //                   Text(
                  //                           //                     weatherData.time,
                  //                           //                     style: TextStyle(fontSize: 31.0),
                  //                           //                   ),
                  //                           //                 ],
                  //                           //               ),
                  //                           //               Column(
                  //                           //                 // Wind direction and wind speed
                  //                           //                 crossAxisAlignment:
                  //                           //                     CrossAxisAlignment.start,
                  //                           //                 children: [
                  //                           //                   SizedBox(
                  //                           //                     height: 19.0,
                  //                           //                   ),
                  //                           //                   Text(
                  //                           //                     weatherData.windDirect,
                  //                           //                     style: TextStyle(fontSize: 18.0),
                  //                           //                   ),
                  //                           //                   Text(
                  //                           //                     weatherData.windSpeed,
                  //                           //                     style: TextStyle(fontSize: 18.0),
                  //                           //                   ),
                  //                           //                 ],
                  //                           //               ),
                  //                           //             ],
                  //                           //           ),
                  //                           //           SizedBox(
                  //                           //             width: 16.0,
                  //                           //           ),
                  //                           //           Expanded(
                  //                           //             flex: 1,
                  //                           //             child: Column(
                  //                           //               // Another Second Column in that row
                  //                           //               children: [
                  //                           //                 SizedBox(
                  //                           //                   height: 26.0,
                  //                           //                 ),
                  //                           //                 Icon(weatherData.icon, size: 80.0),
                  //                           //                 SizedBox(
                  //                           //                   height: 35.0,
                  //                           //                 ),
                  //                           //                 Text(
                  //                           //                   weatherData.weatherCondition,
                  //                           //                   overflow: TextOverflow.ellipsis,
                  //                           //                   maxLines: 2,
                  //                           //                   textAlign: TextAlign.center,
                  //                           //                   style: TextStyle(fontSize: 22.0),
                  //                           //                 ),
                  //                           //                 Column(
                  //                           //                   crossAxisAlignment:
                  //                           //                       CrossAxisAlignment.center,
                  //                           //                   children: [
                  //                           //                     SizedBox(
                  //                           //                       height: 26.0,
                  //                           //                     ),
                  //                           //                     Text(
                  //                           //                       weatherData.tempMax,
                  //                           //                       style:
                  //                           //                           TextStyle(fontSize: 20.0),
                  //                           //                     ),
                  //                           //                     Text(
                  //                           //                       weatherData.tempMin,
                  //                           //                       style:
                  //                           //                           TextStyle(fontSize: 20.0),
                  //                           //                     ),
                  //                           //                   ],
                  //                           //                 )
                  //                           //               ],
                  //                           //             ),
                  //                           //           ),
                  //                           //         ],
                  //                           //       )),
                  //                           // ),
                  //                           // Expanded(
                  //                           //   // Second weather Box
                  //                           //   child: Card(
                  //                           //     margin: EdgeInsets.only(
                  //                           //         top: 20.0,
                  //                           //         bottom: 20.0,
                  //                           //         left: 8.0,
                  //                           //         right: 8.0),
                  //                           //     color:
                  //                           //         Colors.transparent.withOpacity(_opacityDown),
                  //                           //     child: Padding(
                  //                           //       padding: const EdgeInsets.only(
                  //                           //           top: 0.0, bottom: 20.0),
                  //                           //       child: Row(
                  //                           //         children: [
                  //                           //           SizedBox(
                  //                           //             width: 18.0,
                  //                           //           ),
                  //                           //           Column(
                  //                           //             // SecondWeather box weather data
                  //                           //             mainAxisAlignment:
                  //                           //                 MainAxisAlignment.spaceEvenly,
                  //                           //             crossAxisAlignment:
                  //                           //                 CrossAxisAlignment.start,
                  //                           //             children: [
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.sunrise,
                  //                           //                   label: weatherData.sunrise),
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.sunset,
                  //                           //                   label: weatherData.sunset),
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.raindrop,
                  //                           //                   label: weatherData.humidity),
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.day_sunny,
                  //                           //                   label: weatherData.uvindex),
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.cloudy,
                  //                           //                   label: weatherData.visibility),
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.rain,
                  //                           //                   label: weatherData.preciption),
                  //                           //               SecondWeatherBox(
                  //                           //                   iconData: WeatherIcons.wind,
                  //                           //                   label: weatherData.pressure),
                  //                           //             ],
                  //                           //           ),
                  //                           //           SizedBox(
                  //                           //             width: 20.0,
                  //                           //           ),
                  //                           //         ],
                  //                           //       ),
                  //                           //     ),
                  //                           //   ),
                  //                           // ),
                  //                           // TextButton(
                  //                           //     style: TextButton.styleFrom(
                  //                           //         fixedSize: Size(
                  //                           //             MediaQuery.of(context).size.width - 30.0,
                  //                           //             50.0),
                  //                           //         side: BorderSide(
                  //                           //             color: Colors.white, width: 3.0)),
                  //                           //     onPressed: () {
                  //                           //       Navigator.push(
                  //                           //           context,
                  //                           //           MaterialPageRoute(
                  //                           //             builder: (context) => ForecastViewer(),
                  //                           //           ));
                  //                           //     },
                  //                           //     child: Text(
                  //                           //       'Forecast',
                  //                           //       style: TextStyle(
                  //                           //         color: Colors.white,
                  //                           //         fontSize: 30.0,
                  //                           //         fontWeight: FontWeight.w900,
                  //                           //       ),
                  //                           //     )),
                  //                           //       SizedBox(
                  //                           //         height: 5.0,
                  //                           //         child:   Padding(
                  //                           //  padding: EdgeInsets.all(0.0),
                  //                           //  child: Row(
                  //                           //    children: [

                  //                           //              Text("Hello" + weatherData.date,textAlign: TextAlign.left, style: new TextStyle(color: Colors.white)),
                  //                           //              Text(weatherData.time, style: new TextStyle(color: Colors.white)),
                  //                           //    ],
                  //                           //  ),
                  //                           //          ),
                  //                           //       )
                  //                         ],
                  //                       ),
                  //                     ),
                  //                   ),
                  //                 ),
                  //               ],
                  //             ),
                  //           );
                  //         },
                  //       ),
                  //     )),
                  //    */
                  //   ],
                  // ),
                  // Container(
                  //   child: Column(children: [
                  //     LocationScreen(),
                  //     LocationScreen()
                  //   ],),
                  //     // width: MediaQuery.of(context).size.width,
                  //     // height: MediaQuery.of(context).size.height,
                  //     // child: SingleChildScrollView(
                  //     //     child: Column(
                  //     //   mainAxisSize: MainAxisSize.min,
                  //     //   children: <Widget>[
                  //     //     Padding(child: LocationScreen()),
                  //     //     // Expanded(
                  //     //     //   child: Padding(
                  //     //     //     padding: const EdgeInsets.symmetric(
                  //     //     //         vertical: 0.0, horizontal: 8.0),
                  //     //     //     child: Container(
                  //     //     //       color: Colors.white,
                  //     //     //       child: Row(
                  //     //     //         children: [
                  //     //     //           Text("Date",
                  //     //     //               textAlign: TextAlign.left,
                  //     //     //               style:
                  //     //     //                   new TextStyle(color: Colors.black)),
                  //     //     //           Text("Time",
                  //     //     //               textAlign: TextAlign.right,
                  //     //     //               style:
                  //     //     //                   new TextStyle(color: Colors.black)),
                  //     //     //         ],
                  //     //     //       ),
                  //     //     //     ),
                  //     //     //   ),
                  //     //     // )
                  //     //   ],
                  //     // ))
                  //     ),
                  
                ],
              )),
        ));
  }

  // loadWeather() async {
  //   setState(() {
  //     isLoading = true;
  //   });

  //   print(location);
  //   try {
  //     currentLocation = await location.getLocation();

  //     error = null;
  //   } on PlatformException catch (e) {
  //     if (e.code == 'PERMISSION_DENIED') {
  //       error = 'Permission denied';
  //     } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
  //       error =
  //           'Permission denied - please ask the user to enable it from the app settings';
  //     }

  //     location = null;
  //   }
  //   if (location != null) {
  //     final lat = currentLocation.latitude;
  //     print(lat);
  //     final lon = currentLocation.longitude;
  //     print(lon);

  //     final weatherResponse = await http.get(Uri.parse(
  //         'https://api.openweathermap.org/data/2.5/weather?&units=metric&APPID=fdb21addeefaed324abd86332f58915a&lat=${lat.toString()}&lon=${lon.toString()}'));

  //     final forecastResponse = await http.get(Uri.parse(
  //         'https://api.openweathermap.org/data/2.5/onecall?lat=${lat.toString()}&lon=${lon.toString()}&exclude=hourly,minutely,current&appid=fdb21addeefaed324abd86332f58915a'));

  //     if (weatherResponse.statusCode == 200 &&
  //         forecastResponse.statusCode == 200) {
  //       setState(() {
  //         print('hello');
  //         print(weatherResponse.body);
  //         weatherData = WeatherData.fromJson(jsonDecode(weatherResponse.body));
  //         print('hello');
  //         print(forecastResponse.body);
  //         forecastData =
  //             ForecastData.fromJson(jsonDecode(forecastResponse.body));
  //         isLoading = false;
  //       });
  //     }
  //   }
  //   setState(() {
  //     isLoading = false;
  //   });
  // }

  // @override
  // Widget build(BuildContext context) {
  //   return MaterialApp(
  //     debugShowCheckedModeBanner: false,
  //     title: 'Flutter Weather App',
  //     theme: ThemeData(
  //       primarySwatch: Colors.blue,
  //     ),
  //     home: DefaultTabController(
  //       length: 2,
  //       child: Scaffold(
  //         backgroundColor: Colors.blueGrey,
  //         appBar: AppBar(
  //           title: Text('Weather App'),
  //           backgroundColor: Colors.green,
  //           bottom: TabBar(
  //             tabs: [
  //               Text("Current Location", style: TextStyle(fontSize: 18),),
  //               Text("Farm Location", style: TextStyle(fontSize: 18),)
  //             ],
  //           ),
  //           leading: BackButton(onPressed: () => Navigator.pop(context, DashBoardScreen())),
  //         ),
  //         body: TabBarView(
  //           children: [
  //             Container(
  //               width: MediaQuery.of(context).size.width,
  //               height: MediaQuery.of(context).size.height,

  //             child: SingleChildScrollView(child:
  //             Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
  //               Padding(
  //                   padding: const EdgeInsets.all(0.0),
  //                   child: weatherData != null
  //                       ? WeatherNew(weather: weatherData)
  //                       : Container(),
  //                 ),
  //            /*
  //             Expanded(
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: <Widget>[
  //                 Padding(
  //                   padding: const EdgeInsets.all(0.0),
  //                   child: weatherData != null
  //                       ? WeatherNew(weather: weatherData)
  //                       : Container(),
  //                 ),
  //                 // Padding(
  //                 //   padding: EdgeInsets.all(0.0),
  //                 //   child: Row(
  //                 //     children: [
  //                 //               if(weatherData.date == null){
  //                 //                 return CircularProgressIndicator();
  //                 //               }
  //                 //               Text(new DateFormat.yMMMd().format(weatherData.date),textAlign: TextAlign.left, style: new TextStyle(color: Colors.white)),
  //                 //               Text(new DateFormat.Hm().format(weatherData.date), style: new TextStyle(color: Colors.white)),
  //                 //     ],
  //                 //   ),

  //                 // )
  //                 Container(
  //                   height: MediaQuery.of(context).size.width * 0.2,
  //                   child:  Padding(
  //                       padding: const EdgeInsets.all(0.0),
  //                       child: isLoading
  //                           ? Center(
  //                             child: CircularProgressIndicator(
  //                                 strokeWidth: 2.0,
  //                                 valueColor:
  //                                     new AlwaysStoppedAnimation(Colors.white),
  //                               ),
  //                           )
  //                           :  Padding(
  //                             padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 8.0),
  //                             child: Container(

  //                               color: Colors.white,
  //                               child: Row(children: [
  //                                     Text("                 " + "Date: " + new DateFormat.yMMMd().format(weatherData.date),textAlign: TextAlign.left, style: new TextStyle(color: Colors.black)),
  //                                     Text("         " + "Time: " + new DateFormat.Hm().format(weatherData.date),textAlign: TextAlign.right, style: new TextStyle(color: Colors.black)),
  //                                     ],),
  //                             ),
  //                           )
  //                     ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //           */
  //                Container(
  //                   height: MediaQuery.of(context).size.width * 0.1,
  //                   child:  Padding(
  //                       padding: const EdgeInsets.all(0.0),
  //                       child: isLoading
  //                           ? Center(
  //                             child: CircularProgressIndicator(
  //                                 strokeWidth: 2.0,
  //                                 valueColor:
  //                                     new AlwaysStoppedAnimation(Colors.white),
  //                               ),
  //                           )
  //                           :  Padding(
  //                             padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 8.0),
  //                             child: Container(

  //                               color: Colors.white,
  //                               child: Row(children: [
  //                                     Text("      " + "Date: " + new DateFormat.yMMMd().format(weatherData.date),textAlign: TextAlign.left, style: new TextStyle(color: Colors.black)),
  //                                     Text("    " + "Time: " + new DateFormat.Hm().format(weatherData.date),textAlign: TextAlign.right, style: new TextStyle(color: Colors.black)),
  //                                     ],),
  //                             ),
  //                           )
  //                     ),
  //                 ),

  //           SafeArea(
  //             child: Padding(
  //               padding: const EdgeInsets.all(0.0),
  //               child: Container(
  //                 height: MediaQuery.of(context).size.height * 0.35,
  //                 child: forecastData != null
  //                     ? ListView.builder(
  //                         itemCount: forecastData.list.length,
  //                         scrollDirection: Axis.horizontal,
  //                         itemBuilder: (context, index) => WeatherItem(
  //                             forecast: forecastData.list.elementAt(index),
  //                             weather: weatherData,))
  //                     : Container(),
  //               ),
  //             ),
  //           )
  //         ]))),
  //         Weather(),
  //         /*Center(
  //             child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
  //           Expanded(
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: <Widget>[
  //                 Padding(
  //                   padding: const EdgeInsets.all(8.0),
  //                   child:Weather()
  //                 ),
  //                 // Expanded(
  //                 //   child: Padding(
  //                 //     padding: const EdgeInsets.all(0.0),
  //                 //     child: isLoading
  //                 //         ? Center(
  //                 //           child: CircularProgressIndicator(
  //                 //               strokeWidth: 2.0,
  //                 //               valueColor:
  //                 //                   new AlwaysStoppedAnimation(Colors.white),
  //                 //             ),
  //                 //         )
  //                 //         : Padding(
  //                 //           padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 8.0),
  //                 //           child: Container(
  //                 //             height: MediaQuery.of(context).size.width * 0.2,
  //                 //             color: Colors.white,
  //                 //             child: Row(children: [
  //                 //                   Text("                 " + "Date: " + new DateFormat.yMMMd().format(weatherData.date),textAlign: TextAlign.left, style: new TextStyle(color: Colors.black)),
  //                 //                   Text("         " + "Time: " + new DateFormat.Hm().format(weatherData.date),textAlign: TextAlign.right, style: new TextStyle(color: Colors.black)),
  //                 //                   ],),
  //                 //           ),
  //                 //         )
  //                 //   ),
  //                 // ),
  //               ],
  //             ),
  //           ),
  //           // SafeArea(
  //           //   child: Padding(
  //           //     padding: const EdgeInsets.all(0.0),
  //           //     child: Container(
  //           //       height: 300.0,
  //           //       child: forecastData != null
  //           //           ? ListView.builder(
  //           //               itemCount: forecastData.list.length,
  //           //               scrollDirection: Axis.horizontal,
  //           //               itemBuilder: (context, index) => WeatherItem(
  //           //                   forecast: forecastData.list.elementAt(index),
  //           //                   weather: weatherData,))
  //           //           : Container(),
  //           //     ),
  //           //   ),
  //           // )
  //         ]))
  //        */
  //         ],
  //         )
  //         ),)

  //   // ignore: empty_statements
  //   );
}
