import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/JalpradhanDashboardScreen.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/models/projectAuthorityModal.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:fms_mobileapp/utils/projectAuthorityoperation.dart';

class FarmProjectScreen extends StatefulWidget {
  FarmProjectScreen({Key key}) : super(key: key);
  @override
  State<FarmProjectScreen> createState() => _FarmProjectScreenState();
}

class _FarmProjectScreenState extends State<FarmProjectScreen> {
  void initState() {
    super.initState();
    ConvertProjectDetails();
  }

  int count = 0;
  //Converting data into display format
  List<AuthorityResponse> authority;
  ConvertProjectDetails() async {
    var _authority = fetchprojectAuthorityreponse();
    authority = await _authority;
    // count = authority.length;
    count = 0;
    if (count > 1) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => DynamicDashboardScreen()));
    }
    return _authority;
  }

  //Project data view
  Widget listViewBuilder(BuildContext context, List<dynamic> values) {
    return ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: InkWell(
              onTap: () async {
                await SecureStorage.setProjectId(
                    values[index].projectId.toString());
                print(values[index].projectId);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DynamicDashboardScreen()));
              },
              child: Container(
                color: Colors.green,
                width: 18.0,
                child: ListTile(
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  title: Text(
                    values[index].projectName,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 19.0,
                        color: Colors.white),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                            "State:" + values[index].stateName,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text("Project Group:" +
                              values[index].projectGroupName),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                              "Command Area:" + values[index].cCA.toString()),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(DemoLocalization.of(context).translate('title')),
          backgroundColor: Colors.green,
        ),
        body: FutureBuilder(
          future: ConvertProjectDetails(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return listViewBuilder(context, snapshot.data);
            } else if (snapshot.hasError) {
              return Center(child: Text("No Data Found"));
            } else {
              return Center(child: new CircularProgressIndicator());
            }
          },
        ));
  }
}
