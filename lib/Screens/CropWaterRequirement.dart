import 'package:flutter/material.dart';

class CropWaterScreen extends StatefulWidget {
  const CropWaterScreen({ Key key }) : super(key: key);

  @override
  _CropWaterScreenState createState() => _CropWaterScreenState();
}

class _CropWaterScreenState extends State<CropWaterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Crop Water Requirement"),
      backgroundColor: Colors.green,
      
      ),
       body: Container(
        child: Column(
          children: [
            Padding(padding:EdgeInsets.all(30)),
            Text("Crop Water Requirement" ,style: TextStyle(color: Colors.green,fontSize:25,fontWeight:FontWeight.bold  ),),
            Image.asset("assets/images/crop_water.jpg")
            
          ],
        ),
        
       ),
      
    );
  }
}