import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/FarmWeatherScreen.dart';
import 'package:fms_mobileapp/Screens/MainWeather.dart';
import 'package:fms_mobileapp/services/farmforecastinformer.dart';
import 'package:fms_mobileapp/services/farminfomer.dart';
import 'package:fms_mobileapp/services/weather.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fms_mobileapp/Screens/location_screen.dart';
import 'package:fms_mobileapp/services/weather_informer.dart';
import 'package:geocoder/geocoder.dart';
import 'package:provider/provider.dart';
import 'package:fms_mobileapp/services/forecastInformer.dart';
import 'package:http/http.dart' as http;

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  bool flag = false;
  @override
  void initState() {
    super.initState();
    getLocationData();
  }

  void getLocationData() async {
    var connectivityResult;

    print("connectivity result : $connectivityResult");

    var weatherData = await WeatherModel()
        .getLocationWeather(); // getting json data base on location(lattitude , longitude)of the device
    ForecastInformer.weatherData =
        weatherData; // passing json for ForecastInfomer

    

    Provider.of<WeatherInformer>(context, listen: false).getLocationWeatherInfo(
        weatherData: weatherData); // Passing Json data for making a data on UI
    Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return MainWeatherScreen(); //
      },
    ));
  }


  
  void getFarmLocationData() async {
    var connectivityResult;

    print("connectivity result : $connectivityResult");

    var weatherData = await WeatherModel()
        .getFarmLocationWeather(); // getting json data base on location(lattitude , longitude)of the device
    FarmForecastInformer.farmweatherData =
        weatherData; // passing json for ForecastInfomer

    Provider.of<FarmInformer>(context, listen: false).getFarmLocationWeatherInfo(
        farmweatherData: weatherData); // Passing Json data for making a data on UI
    Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return MainWeatherScreen(); //
      },
    ));
  }



 

  @override
  Widget build(BuildContext context) {
    return
        // ignore: missing_return
        Scaffold(
      body: Center(
        child: SpinKitDoubleBounce(
          color: Colors.black38,
          size: 100.0,
        ),
      ),
    );
  }
}
