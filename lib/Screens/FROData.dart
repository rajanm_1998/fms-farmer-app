import 'package:flutter/material.dart';
import 'package:fms_mobileapp/models/FRMModel/FROSurveyanalysismodel.dart';
import 'package:fms_mobileapp/models/FRMModel/GetFarmerListByFroIdModel.dart';
import 'package:fms_mobileapp/utils/FRMutils/farmerlistbyfrooperation.dart';
import 'package:fms_mobileapp/utils/FRMutils/frmsurveyoperation.dart';
import 'package:pie_chart/pie_chart.dart';

import 'FarmerListByFRO.dart';

class FROData extends StatefulWidget {
  const FROData({Key key}) : super(key: key);

  @override
  _FRODataState createState() => _FRODataState();
}

class _FRODataState extends State<FROData> {
  @override
  void initState() {
    super.initState();
  }

  Future<List<FROSurvey>> _survey;
  List<FROSurvey> survey;
  String Name;
  ConvertSurveyAnalysis() async {
    _survey = fetchFROListByFrmIdreponse();
    survey = await _survey;
    return _survey;
  }

  Widget listViewBuilder(BuildContext context, List<FROSurvey> values) {
    // final height = MediaQuery.of(context).size.height;
    return ListView.builder(
        
        shrinkWrap: true,
        itemCount: values.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: ()=>
              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) => FarmerListByFROScreen(values[index]))),
            
            child: Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.fromLTRB(10,5,5,5),
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(blurStyle: BlurStyle.outer)],
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Colors.black),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text("Name: ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700, fontSize: 18),
                                  textAlign: TextAlign.left),
                            ),
                            Expanded(
                                child: Text(
                              values[index].froname.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: Colors.black),
                            ))
                          ],
                        ),
                         Container(width: MediaQuery.of(context).size.width,
                        height: 5,
                        color: Colors.transparent,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text("Area Allocated :",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700, fontSize: 16),
                                  textAlign: TextAlign.left),
                            ),
                            Expanded(
                                child: Text(
                              values[index].areaAllocated.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: Colors.black),
                            ))
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text("No. Of Farmers :",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700, fontSize: 16),
                                  textAlign: TextAlign.left),
                            ),
                            Expanded(
                                child: Text(
                              values[index].noOfFarmer.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: Colors.black),
                            ))
                          ],
                        ),
                        Container(width: MediaQuery.of(context).size.width,
                        height: 1,
                        color: Colors.black,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text("Not Survey :",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700, fontSize: 16),
                                  textAlign: TextAlign.left),
                            ),
                            Expanded(
                                child: Text(
                              values[index].notSurvey.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: Colors.red),
                            ))
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text("Surveyed :",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700, fontSize: 16),
                                  textAlign: TextAlign.left),
                            ),
                            Expanded(
                                child: Text(
                              values[index].surveyed.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: Colors.orange),
                            ))
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text("Approved Surveyed :",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700, fontSize: 16),
                                  textAlign: TextAlign.left),
                            ),
                            Expanded(
                                child: Text(
                              values[index].surveyed.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 16,
                                  color: Colors.green),
                            ))
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: PieChart(
                      dataMap: values[index].sampleData,
                      chartType: ChartType.disc,
                      colorList: [Colors.red, Colors.orange, Colors.green],
                      legendOptions: LegendOptions(showLegends: false),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FRO Data"),
        backgroundColor: Colors.green,
        leading: BackButton(
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.grey[400],
          child: FutureBuilder(
              future: fetchFROListByFrmIdreponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return listViewBuilder(context, snapshot.data);
                } else if (snapshot.hasError) {
                  return Center(child: Text("No data found"));
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              })),
    );
  }
}
