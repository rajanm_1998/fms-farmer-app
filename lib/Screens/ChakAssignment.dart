import 'package:flutter/material.dart';

class ChakAssignment extends StatefulWidget {
  const ChakAssignment({ Key key }) : super(key: key);

  @override
  _ChakAssignmentState createState() => _ChakAssignmentState();
}

class _ChakAssignmentState extends State<ChakAssignment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Text("Chak Assignment"),
        backgroundColor: Colors.green,
        ),
        body: Container(
        child: Column(
          children: [
            Padding(padding:EdgeInsets.all(30)),
            Text("Chak Assignment" ,style: TextStyle(color: Colors.green,fontSize:25,fontWeight:FontWeight.bold  ),),
            Image.asset("assets/images/assignment_icons.png"),
            
            
          ],
        ),
        
       ),
    
      
    );
      
    
  }
}