

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';
import 'package:fms_mobileapp/Screens/CroppingPattern.dart';
import 'package:fms_mobileapp/classes/language.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:translator/translator.dart';
import 'package:fms_mobileapp/services/storage.dart';

class SubChakIrrigationCalendar extends StatefulWidget {
  SubChakIrrigationCalendar({Key key}) : super(key: key);

  @override
  SubChakIrrigationCalendarState createState() => SubChakIrrigationCalendarState();
}

class SubChakIrrigationCalendarState extends State<SubChakIrrigationCalendar>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  bool _isPlaying = true;

  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();

  }

  ErrorBuilder() {
    // ignore: prefer_const_constructors
    return Center(
      child: Text("SubChak Irrigation Details Not Found "),
    );
  }


  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: Text(DemoLocalization.of(context).translate('title')),
          backgroundColor: Colors.green,
        ),
        body: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Container(child: Text("SubChak Irrigation Schedule"),),
        ),
        );
  }
}