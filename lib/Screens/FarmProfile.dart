import 'dart:convert';
// import 'dart:js_util';
// import 'dart:html';
import 'dart:ui';
import 'package:easy_localization/easy_localization.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternDropDownModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/FamilyDetailModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/FarmerProflleModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/farmerMachineryDetailsModel.dart';
import 'package:fms_mobileapp/utils/CroppingPatternUtility/Operation.dart';
import 'package:zoom_widget/zoom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/utils/FarmerProfile/Operation.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:http/http.dart' as http;

class FarmerProfileScreen extends StatefulWidget {
  const FarmerProfileScreen({Key key}) : super(key: key);

  @override
  _FarmerProfileDetailScreenState createState() =>
      _FarmerProfileDetailScreenState();
}

class _FarmerProfileDetailScreenState extends State<FarmerProfileScreen> {
  Future<FarmerProfileDetailModel> farmer;
  FarmerProfileDetailModel fd;

  ConvertFarmerDetails() async {
    farmer = fetchEditFarmerProfileDetailsResponse();
    var temp = await farmer;
    setState(() {
      fd = temp;
    });
    return farmer;
  }

  Widget FarmerProfile(BuildContext context, dynamic values) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: Container(
        alignment: Alignment.centerLeft,
        child: Column(children: [
          Container(
            padding: EdgeInsets.fromLTRB(8, 5, 0, 0),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(
                DemoLocalization.of(context).translate('OWNERASPERROR') +
                    ": " +
                    values.fOwner.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('address') +
                    ": " +
                    values.address.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('contactDETAILS') +
                    ": " +
                    values.fContact.toString() +
                    "\n " +
                    DemoLocalization.of(context).translate('CAST') +
                    ": " +
                    values.casteName.toString() +
                    "\n " +
                    DemoLocalization.of(context).translate('RELIGION') +
                    ": " +
                    values.religionName.toString() +
                    " \n" +
                    DemoLocalization.of(context).translate('APL/BPL') +
                    ": " +
                    values.povertyLevel.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('FARMERNAME') +
                    ": " +
                    values.farmerName.toString() +
                    " \n" +
                    DemoLocalization.of(context).translate('contactno') +
                    ": " +
                    values.contactNo.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('FARMERTYPE') +
                    ": " +
                    values.ftype.toString() +
                    " \n" +
                    DemoLocalization.of(context).translate('PHONETYPE') +
                    ": " +
                    values.phoneType.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('network') +
                    ": " +
                    values.network.toString(),
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Future<void> farmerProfileDialog(
      BuildContext context, FarmerResponse value) async {
    // TextEditingController _textFieldController = TextEditingController();
    // Future.delayed(Duration(milliseconds: 1000));
    // ConvertFarmerDetails();

    String CasteDropDownvalue;
    var casteList = await CasteListDetails();
    if (value.casteId == 0)
      setState(() {
        CasteDropDownvalue = casteList.data.response.first.casteId.toString();
      });
    else
      setState(() {
        CasteDropDownvalue = casteList.data.response
            .where((element) => element.casteId == value.casteId)
            .first
            .casteId
            .toString();
      });
    ///// --------RELIGION  DROPDOWN ----------

    String Religion;
    String ReligionDropdownvalue;
    var ReligionList = await ReligionListDetails();
    if (value.religionId == 0)
      setState(() {
        ReligionDropdownvalue =
            ReligionList.data.response.first.religionId.toString();
      });
    else
      setState(() {
        ReligionDropdownvalue = ReligionList.data.response
            .where((element) => element.religionId == value.religionId)
            .first
            .religionId
            .toString();
      });

    return showGeneralDialog(
      context: context,
      pageBuilder: (context, animation, secondaryAnimation) => Center(
          child: value == null
              ? CircularProgressIndicator()
              : AlertDialog(
                  contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Farmer Profile',
                          style: TextStyle(color: Colors.green)),
                      Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.close,
                                color: Colors.red,
                                size: 20,
                              ))
                          // Icon(Icons.close, color: Colors.red, size: 20,)
                        ],
                      ),
                    ],
                  ),
                  content: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 500,
                            child: TextFormField(
                              initialValue: value.fOwner.toString(),

                              //  onChanged: (value) {
                              //    setState(() {
                              //    });
                              //  },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  enabled: false,
                                  labelText: "Owner Name",
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.address.toString(),
                              onChanged: (newvalue) {
                                setState(() {
                                  value.address = newvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                labelText: "address",
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.fContact.toString(),
                              //  onChanged: (value) {
                              //    setState(() {
                              //    });
                              //  },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  enabled: false,
                                  labelText: 'CONTACT DETAILS',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: DropdownButtonFormField(
                              value: CasteDropDownvalue,
                              decoration: InputDecoration(
                                labelText: 'CAST',
                                border: OutlineInputBorder(),
                              ),
                              items: casteList.data.response.map((value) {
                                return new DropdownMenuItem(
                                  child: new Text(
                                    value.casteName,
                                    style: TextStyle(fontSize: 13.0),
                                  ),
                                  value: value.casteId.toString(),
                                );
                              }).toList(),
                              onChanged: (dropvalue) {
                                setState(() {
                                  value.casteId = int.parse(dropvalue);
                                });
                              },
                            ),
                            // child: TextFormField(
                            //   initialValue: value.casteName.toString(),
                            //   onChanged: (textvalue) {
                            //     setState(() {
                            //       value.casteName = textvalue;
                            //     });
                            //   },
                            //   //  controller: _textFieldController,
                            //   decoration: InputDecoration(
                            //       labelText: 'CAST', border: OutlineInputBorder()),
                            // ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: DropdownButtonFormField(
                              value: ReligionDropdownvalue,
                              decoration: InputDecoration(
                                labelText: 'RELIGION',
                                border: OutlineInputBorder(),
                              ),
                              items: ReligionList.data.response.map((value) {
                                return new DropdownMenuItem(
                                  child: new Text(
                                    value.religionName,
                                    style: TextStyle(fontSize: 13.0),
                                  ),
                                  value: value.religionId.toString(),
                                );
                              }).toList(),
                              onChanged: (dropvalue) {
                                setState(() {
                                  value.religionId = int.parse(dropvalue);
                                });
                              },
                            ),
                            // child: TextFormField(
                            //   initialValue: value.religionName.toString(),
                            //   onChanged: (textvalue) {
                            //     setState(() {
                            //       value.religionName = textvalue;
                            //     });
                            //   },
                            //   //  controller: _textFieldController,
                            //   decoration: InputDecoration(
                            //       labelText: 'RELIGION',
                            //       border: OutlineInputBorder()),
                            // ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: DropdownButtonFormField(
                              value: value.povertyLevel.toString(),
                              decoration: InputDecoration(
                                labelText: 'APL/BPL',
                                border: OutlineInputBorder(),
                              ),
                              items: ['APL', 'BPL'].map((value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (dropvalue) {
                                setState(() {
                                  value.povertyLevel = dropvalue;
                                });
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.farmerName.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.farmerName = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'FARMER NAME',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.contactNo.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.contactNo = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'CONTACT NO.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue:
                                  fd.data.response.farmerType.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.farmerType = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'FARMER TYPE',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.phoneType.toString(),
                              onChanged: (newvalue) {
                                setState(() {
                                  value.phoneType = newvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'PHONE TYPE',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.network.toString(),
                              onChanged: (newvalue) {
                                setState(() {
                                  value.network = newvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'NETWORK',
                                  border: OutlineInputBorder(),
                                  focusColor: Colors.green),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    FlatButton(
                      color: Colors.green,
                      textColor: Colors.white,
                      child: Text('Update'),
                      onPressed: () async {
                        value.modifyBy = value.farmerId;
                        var temp = insertFarmerProfileDetails(value);
                        var isUpdate = await temp;
                        if (isUpdate) {
                          Navigator.of(context).pop();
                          SnackBar(content: Text("Data updated successfully"));
                        }
                        print(fd.data.response);
                      },
                    ),
                  ],
                )),
    );
  }

  DataRow FamilyDetailsList(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.memberName)),
        DataCell(Text(value.relation)),
        DataCell(Text(value.gender)),
        DataCell(Text(value.age.toString())),
        DataCell(Text(value.education)),
        DataCell(Text(value.occupation)),
        DataCell(
          IconButton(
              onPressed: () {
                FamilyDetailsDialog(context, value);
              },
              icon: Icon(
                Icons.edit,
                size: 15,
                color: Colors.green,
              )),
        ),
        // DataCell(
        //   IconButton(
        //       onPressed: () {},
        //       icon: Icon(
        //         Icons.delete,
        //         size: 15,
        //         color: Colors.red,
        //       )),
        // ),
      ],
    );
  }

  Future<void> FamilyDetailsDialog(BuildContext context, dynamic values) async {
    TextEditingController _textFieldController = TextEditingController();
    return showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) => Center(
            child: values == null
                ? CircularProgressIndicator()
                : AlertDialog(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('FAMILY DETAILS ',
                            style: TextStyle(color: Colors.green)),
                        Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.red,
                                  size: 20,
                                ))
                          ],
                        )
                      ],
                    ),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 500,
                              child: TextFormField(
                                initialValue: values.memberName.toString(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.memberName = textvalue;
                                  });
                                },
                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: 'Member Name',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: TextFormField(
                                initialValue: values.relation.toString(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.relation = textvalue;
                                  });
                                },
                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: 'Relation',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                child: DropdownButtonFormField(
                              value: values.gender.toString(),
                              decoration: InputDecoration(
                                labelText: 'Gender',
                                border: OutlineInputBorder(),
                              ),
                              items: ['Male', 'Female', 'Other'].map((value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  values.gender = value;
                                });
                              },
                            )
                                // child: TextFormField(
                                //   initialValue: values.gender.toString(),
                                //   onChanged: (textvalue) {
                                //     setState(() {
                                //       values.gender = textvalue;
                                //     });
                                //   },
                                //   //  controller: _textFieldController,
                                //   decoration: InputDecoration(
                                //       labelText: 'Gender', border: OutlineInputBorder()),
                                // ),
                                ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: TextFormField(
                                initialValue: values.age.toString(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.age = int.parse(textvalue);
                                  });
                                },
                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: "Age",
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: TextFormField(
                                initialValue: values.education.toString(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.education = textvalue;
                                  });
                                },
                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: 'Education',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: TextFormField(
                                initialValue: values.occupation,
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.occupation = textvalue;
                                  });
                                },

                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: "Occupation",
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                        ],
                      ),
                      //  else if (familyDetails.hasError) {
                      //   return Text("${familyDetails.error}");
                      // }
                      // return Center(child: CircularProgressIndicator());
                    ),
                    actions: <Widget>[
                      FlatButton(
                        color: Colors.green,
                        textColor: Colors.white,
                        child: Text('Update'),
                        onPressed: () async {
                          var temp = insertFamilyDetails(values);
                          var isUpdate = await temp;
                          if (isUpdate) {
                            Navigator.of(context).pop();
                            SnackBar(
                              content: Text('Date update successfully'),
                            );
                          }
                        },
                      ),
                    ],
                  )));
  }

  // Widget BankDetails(BuildContext context, dynamic values) {
  //   var loanTaken = '';
  //   if (values.loanTaken == 0 || values.loanTaken == "") {
  //     loanTaken = "No";
  //   } else {
  //     loanTaken = "Yes";
  //   }
  //   return Padding(
  //     padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
  //     child: Container(
  //       alignment: Alignment.centerLeft,
  //       child: Column(children: [
  //         Container(
  //           padding: EdgeInsets.fromLTRB(8, 5, 0, 0),
  //           child: Padding(
  //             padding: const EdgeInsets.all(5.0),
  //             child: Text(
  //               DemoLocalization.of(context)
  //                  .translate('BANKNAME') +
  //                   ": " +
  //                   values.bankName.toString() +
  //                   "\n" +
  //                    DemoLocalization.of(context)
  //                  .translate('BRANCHNAME') +
  //                  ":"+
  //                   values.bankBranch.toString() +
  //                   "\n" +
  //                    DemoLocalization.of(context)
  //                  .translate('ACCOUNTNO') +
  //                  ":"+
  //                   values.accountNo.toString() +
  //                   "\n" +
  //                    DemoLocalization.of(context)
  //                  .translate('LoanTaken') +
  //                  ":"+
  //                   values.loanTaken.toString(),
  //               style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
  //               textAlign: TextAlign.left,
  //             ),
  //           ),
  //         ),
  //       ]),
  //     ),
  //   );
  // }

  Widget BankDetails(BuildContext context, dynamic values) {
    var loanTaken = '';
    if (values.loanTaken == 0 || values.loanTaken == "") {
      loanTaken = "No";
    } else {
      loanTaken = "Yes";
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: Container(
        alignment: Alignment.centerLeft,
        child: Column(children: [
          Container(
            padding: EdgeInsets.fromLTRB(8, 5, 0, 0),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(
                DemoLocalization.of(context).translate('BANKNAME') +
                    ": " +
                    values.bankName.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('BRANCHNAME') +
                    ":" +
                    values.bankBranch.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('ACCOUNTNO') +
                    ":" +
                    values.accountNo.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('LoanTaken') +
                    ":" +
                    loanTaken.toString(),
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Future<void> BankDetailsDialog(
      BuildContext context, FarmerResponse value) async {
    TextEditingController _textFieldController = TextEditingController();
    var No = 0;
    var Yes = 1;
    return showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) => Center(
            child: value == null
                ? CircularProgressIndicator()
                : AlertDialog(
                    insetPadding: EdgeInsets.zero,
                    contentPadding: EdgeInsets.zero,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('BANK DETAILS',
                            style: TextStyle(color: Colors.green)),
                        Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.red,
                                  size: 20,
                                ))
                          ],
                        )
                      ],
                    ),
                    content: SingleChildScrollView(
                        child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.bankName.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.bankName = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'Bank Name',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.bankBranch.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.bankBranch = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'Bank Branch',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.accountNo.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.accountNo = textvalue;
                                });
                              },
                              //  controller: _textFieldController,

                              decoration: InputDecoration(
                                  labelText: 'Account No',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: DropdownButtonFormField(
                              value: value.loanTaken.toString(),
                              decoration: InputDecoration(
                                labelText: 'Loan Taken',
                                border: OutlineInputBorder(),
                              ),
                              items: ['1', '0'].map((value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.loanTaken = int.parse(textvalue);
                                });
                              },
                            ),
                            // child: TextFormField(
                            //   initialValue: value.loanTaken.toString(),
                            //   //  onChanged: (value) {
                            //   //    setState(() {
                            //   //    });
                            //   //  },
                            //   //  controller: _textFieldController,
                            //   decoration: InputDecoration(
                            //     labelText: 'Loan Taken',
                            //     border: OutlineInputBorder(),
                            //   ),
                            // ),
                          ),
                        ),
                      ],
                    )),
                    actions: <Widget>[
                      FlatButton(
                        color: Colors.green,
                        textColor: Colors.white,
                        child: Text('Update'),
                        onPressed: () async {
                          value.modifyBy = value.farmerId;
                          var temp = insertFarmerProfileDetails(value);
                          var isUpdate = await temp;
                          if (isUpdate) {
                            Navigator.of(context).pop();
                            SnackBar(
                                content: Text("Data updated successfully"));
                          }
                          print(fd.data.response);
                        },
                      ),
                    ],
                  )));
  }

  Widget OtherDetails(BuildContext context, dynamic values) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      child: Container(
        alignment: Alignment.centerLeft,
        child: Column(children: [
          Container(
            padding: EdgeInsets.fromLTRB(8, 5, 0, 0),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(
                DemoLocalization.of(context).translate('PANCARDNO') +
                    ": " +
                    values.pancardNo.toString() +
                    "\n" +
                    DemoLocalization.of(context).translate('AADHAARCARDNO') +
                    ": " +
                    values.aadharCardNo.toString(),
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Future<void> OtherDetailsDialog(BuildContext context, FarmerResponse value) async {
    TextEditingController _textFieldController = TextEditingController();
    return showGeneralDialog(
        context: context,
        
        barrierDismissible: false,
        pageBuilder: (context, animation, secondaryAnimation) => Center(
            child: value.hashCode == value
                ? CircularProgressIndicator()
                : AlertDialog(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('OTHER DETAILS',
                            style: TextStyle(color: Colors.green)),
                        Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.red,
                                  size: 20,
                                ))
                          ],
                        )
                      ],
                    ),
                    content: SingleChildScrollView(
                        child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 500,
                            child: TextFormField(
                              initialValue: value.pancardNo.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.pancardNo = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'PancardNo',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: TextFormField(
                              initialValue: value.aadharCardNo.toString(),
                              onChanged: (textvalue) {
                                setState(() {
                                  value.aadharCardNo = textvalue;
                                });
                              },
                              //  controller: _textFieldController,
                              decoration: InputDecoration(
                                  labelText: 'AadharCard No',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ),
                      ],
                    )),
                    actions: <Widget>[
                      FlatButton(
                        color: Colors.green,
                        textColor: Colors.white,
                        child: Text('Update'),
                        onPressed: () async {
                          value.modifyBy = value.farmerId;
                          var temp = insertFarmerProfileDetails(value);
                          var isUpdate = await temp;
                          if (isUpdate) {
                            Navigator.of(context).pop();
                            SnackBar(
                                content: Text("Data updated successfully"));
                          }
                          print(fd.data.response);
                        },
                      ),
                      Center(
                        child: CircularProgressIndicator(),
                      )
                    ],
                  )));
  }

  DataRow landDetailsROR(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.roRSurveyNo)),
        DataCell(Text(value.roRTotalArea.toStringAsFixed(2))),
        DataCell(Text(value.chakno)),
        DataCell(Text(value.soilType)),
      ],
    );
  }

  DataRow landDetailsRORDesign(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.khasaraNo)),
        DataCell(Text(value.totalArea.toStringAsFixed(2))),
        DataCell(Text(value.chakno)),
        DataCell(Text(value.subChakno)),
      ],
    );
  }

  DataRow cropPatternDetails(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.year.toString())),
        DataCell(Text(value.landName)),
        DataCell(Text(value.seasonName)),
        DataCell(Text(value.cropName)),
        DataCell(Text(value.area.toString())),
        DataCell(Text(value.variety)),
        DataCell(Text(value.inputCost)),
        DataCell(Text(value.yield)),
        DataCell(Text(value.sowingDate)),
        DataCell(Text(value.harvestingDate)),
      ],
    );
  }

  DataRow irrigationFacilityDetails(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.year.toString())),
        DataCell(Text(value.irrigationFacilityName)),
        DataCell(Text(value.khasaraNo)),
        // DataCell(Text(value.kharif)),
        DataCell(Text(value.pumpCapacity)),
        DataCell(Text(value.rate.toString())),
        DataCell(IconButton(
            onPressed: () {
              IrrigationFacilityDialog(context, value);
            },
            icon: Icon(
              Icons.edit,
              size: 15,
              color: Colors.green,
            ))),
      ],
    );
  }

  Future<void> IrrigationFacilityDialog(
      BuildContext context, dynamic values) async {
    TextEditingController _textFieldController = TextEditingController();
    ////------ Year dropdown ------

    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy');
    final String formatted = formatter.format(now);
    int currYear = int.parse(formatted) - 10;
    var data = List<int>.generate(30, (int index) => currYear + index);
    if (values.year == 0) values.year = currYear;

    /////-----IRRIGATION FACILITY dropdown-----

    String irrigation;
    String Irrigationdropdownvalue;
    var IrrigationList = await IrrigationFacilityList();
    if (values.irrigationFacilityId == 0)
      setState(() {
        Irrigationdropdownvalue =
            IrrigationList.data.response.first.irrigationFacilityId.toString();
      });
    else
      setState(() {
        Irrigationdropdownvalue = IrrigationList.data.response
            .where((element) =>
                element.irrigationFacilityId == values.irrigationFacilityId)
            .first
            .irrigationFacilityId
            .toString();
      });
    var landList = await fetchLandHoldingRoRDesignDetailResponse();
    String Landdropdownvalue;
    if (values.landId != 0)
      setState(() {
        if (landList.data.response
                .where((element) => element.landId == values.landId)
                .length ==
            0)
          Landdropdownvalue = landList.data.response.first.landId.toString();
        else
          Landdropdownvalue = landList.data.response
              .where((element) => element.landId == values.landId)
              .first
              .landId
              .toString();
      });
    else
      Landdropdownvalue = landList.data.response.first.landId.toString();
////-----------Survey No

    // String SurveyNo;
    // String Surveydropdownvalue;
    // var SurveyNoList = IrrigationFacilityList();
    // if(values.)
    // String Seasondropdownvalue;
    // var SeasonList = await fetchSeasonListResponse();
    // if (values.seasonId != 0)
    //   setState(() {
    //     if (SeasonList.data.response
    //             .where((element) => element.seasonId == values.seasonId)
    //             .length ==
    //         0)
    //       Seasondropdownvalue =
    //           SeasonList.data.response.first.seasonId.toString();
    //     else
    //       Seasondropdownvalue = SeasonList.data.response
    //           .where((element) => element.seasonId == values.seasonId)
    //           .first
    //           .seasonId
    //           .toString();
    //   });
    // else
    //   Seasondropdownvalue = SeasonList.data.response.first.seasonId.toString();

    return showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) => Center(
            child: values == null
                ? CircularProgressIndicator()
                : AlertDialog(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                    title: Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Text('IRRIGATION FACILITY',
                              style: TextStyle(color: Colors.green)),
                        ),
                        Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.close,
                                  color: Colors.red,
                                  size: 20,
                                ))
                          ],
                        )
                      ],
                    ),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 500,
                              child: DropdownButtonFormField(
                                value: values.year.toString(),
                                decoration: InputDecoration(
                                  labelText: 'year',
                                  border: OutlineInputBorder(),
                                ),
                                items: data.map((item) {
                                  return DropdownMenuItem<String>(
                                    value: item.toString(),
                                    child: Text(item.toString()),
                                  );
                                }).toList(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.year = int.parse(textvalue);
                                  });
                                },
                              ),
                              // child: TextFormField(
                              //   initialValue: values.year.toString(),
                              //   onChanged: (textvalue) {
                              //     setState(() {
                              //       values.year = textvalue;
                              //     });
                              //   },
                              //   //  controller: _textFieldController,
                              //   decoration: InputDecoration(
                              //       labelText: "year", border: OutlineInputBorder()),
                              // ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: DropdownButtonFormField(
                                value: Irrigationdropdownvalue,
                                decoration: InputDecoration(
                                  labelText: 'Irrigation Facility',
                                  border: OutlineInputBorder(),
                                ),
                                items:
                                    IrrigationList.data.response.map((value) {
                                  return new DropdownMenuItem(
                                    child: new Text(
                                      value.irrigationFacilityName,
                                      style: TextStyle(fontSize: 13.0),
                                    ),
                                    value:
                                        value.irrigationFacilityId.toString(),
                                  );
                                }).toList(),
                                onChanged: (dropvalue) {
                                  setState(() {
                                    values.irrigationFacilityId =
                                        int.parse(dropvalue);
                                  });
                                },
                              ),
                              //   child: DropdownButton(
                              //     hint: Text("-- Select Item --"),
                              //     value: valueChoose,
                              //     onChanged: (newValue) {
                              //       setState(() {
                              //         valueChoose = newValue;
                              //       });
                              //     },
                              //     items:listitem.map(values),
                              //     icon: Icon(
                              //       Icons.arrow_drop_down,
                              //       size: 30,
                              //     ),
                              //     // child: TextFormField(
                              //     //   initialValue: values.irrigationFacilityName.toString(),
                              //     //   onChanged: (textvalue) {
                              //     //     setState(() {
                              //     //       values.irrigationFacilityName = textvalue;
                              //     //     });
                              //     //   },
                              //     //   //  controller: _textFieldController,
                              //     //   decoration: InputDecoration(
                              //     //       labelText: 'Irrigation Facility',
                              //     //       border: OutlineInputBorder()),
                              //     // ),
                              //   ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: DropdownButtonFormField(
                                value: Landdropdownvalue,
                                decoration: InputDecoration(
                                  labelText: 'Survey No',
                                  border: OutlineInputBorder(),
                                ),
                                items: landList.data.response.map((value) {
                                  return new DropdownMenuItem(
                                    child: new Text(
                                      value.khasaraNo,
                                      style: TextStyle(fontSize: 13.0),
                                    ),
                                    value: value.landId.toString(),
                                  );
                                }).toList(),
                                onChanged: (dropvalue) {
                                  setState(() {
                                    values.landId = int.parse(dropvalue);
                                  });
                                },
                              ),
                              // child: TextFormField(
                              //   initialValue: values.khasaraNo.toString(),
                              //   onChanged: (textvalue) {
                              //     setState(() {
                              //       values.khasaraNo = textvalue;
                              //     });
                              //   },
                              //   //  controller: _textFieldController,
                              //   decoration: InputDecoration(
                              //       labelText: 'khasara No',
                              //       border: OutlineInputBorder()),
                              // ),
                            ),
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.all(8.0),
                          //   child: Container(
                          //     child: DropdownButtonFormField(
                          //       value: Seasondropdownvalue,
                          //       decoration: InputDecoration(
                          //         labelText: 'Season',
                          //         border: OutlineInputBorder(),
                          //       ),
                          //       items: SeasonList.data.response.map((value) {
                          //         return new DropdownMenuItem(
                          //           child: new Text(
                          //             value.seasonName,
                          //             style: TextStyle(fontSize: 13.0),
                          //           ),
                          //           value:value.seasonName.toString(),
                          //         );
                          //       }).toList(),
                          //       onChanged: (value) {
                          //         setState(() {
                          //           value.seasonId = int.parse(value);
                          //         });
                          //       },
                          //     ),
                          //     // child: TextFormField(
                          //     //   initialValue: values.kharif.toString(),
                          //     //   onChanged: (textvalue) {
                          //     //     setState(() {
                          //     //       values.kharif = textvalue;
                          //     //     });
                          //     //   },
                          //     //   //  controller: _textFieldController,
                          //     //   decoration: InputDecoration(
                          //     //       labelText: 'Season', border: OutlineInputBorder()),
                          //     // ),
                          //   ),
                          // ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: TextFormField(
                                initialValue: values.pumpCapacity.toString(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.pumpCapacity = textvalue;
                                  });
                                },
                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: 'PumpCapacity',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: TextFormField(
                                initialValue: values.rate.toString(),
                                onChanged: (textvalue) {
                                  setState(() {
                                    values.rate = textvalue;
                                  });
                                },
                                //  controller: _textFieldController,
                                decoration: InputDecoration(
                                    labelText: 'Rate',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      FlatButton(
                        color: Colors.green,
                        textColor: Colors.white,
                        child: Text('Update'),
                        onPressed: () async {
                          var temp = insertIrrigationFacility(values);
                          var isUpdate = await temp;
                          if (isUpdate) {
                            Navigator.of(context).pop();
                            SnackBar(content: Text("Date update successfully"));
                          }
                        },
                      ),
                    ],
                  )));
  }

  DataRow irrigationTypeDetails(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.year.toString())),
        DataCell(Text(value.irrigationTypeName)),
        DataCell(Text(value.khasaraNo)),
        DataCell(IconButton(
            onPressed: () {
              IrrigationTypeDialog(context, value);
            },
            icon: Icon(
              Icons.edit,
              size: 15,
              color: Colors.green,
            ))),
      ],
    );
  }

  Future<void> IrrigationTypeDialog(
      BuildContext context, dynamic values) async {
    // TextEditingController _textFieldController = TextEditingController();

///// -- Year-----

    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy');
    final String formatted = formatter.format(now);
    int currYear = int.parse(formatted) - 10;
    var data = List<int>.generate(30, (int index) => currYear + index);
    if (values.year == 0) values.year = currYear;

////----irrigationType-----

    String IrrigationType;
    String IrrigationTypeDropdownValue;
    var IrrigationList = await IrrigationTypeList();
    if (values.irrigationTypeId == 0)
      setState(() {
        IrrigationTypeDropdownValue =
            IrrigationList.data.response.first.irrigationTypeId.toString();
      });
    else
      setState(() {
        IrrigationTypeDropdownValue = IrrigationList.data.response
            .where((element) =>
                element.irrigationTypeId == values.irrigationTypeId)
            .first
            .irrigationTypeId
            .toString();
      });
//--------------------------

    var landList = await fetchLandHoldingRoRDesignDetailResponse();
    String Landdropdownvalue;
    if (values.landId != 0)
      setState(() {
        if (landList.data.response
                .where((element) => element.landId == values.landId)
                .length ==
            0)
          Landdropdownvalue = landList.data.response.first.landId.toString();
        else
          Landdropdownvalue = landList.data.response
              .where((element) => element.landId == values.landId)
              .first
              .landId
              .toString();
      });
    else
      Landdropdownvalue = landList.data.response.first.landId.toString();

    return showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) =>Center(
          child: values == null ? CircularProgressIndicator() :
           AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('IRRIGATION TYPE', style: TextStyle(color: Colors.green)),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.close,
                          color: Colors.red,
                          size: 20,
                        ))
                  ],
                )
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 500,
                      child: DropdownButtonFormField(
                        value: values.year.toString(),
                        decoration: InputDecoration(
                          labelText: 'year',
                          border: OutlineInputBorder(),
                        ),
                        items: data.map((item) {
                          return DropdownMenuItem<String>(
                            value: item.toString(),
                            child: Text(item.toString()),
                          );
                        }).toList(),
                        onChanged: (dropvalue) {
                          setState(() {
                            values.year = int.parse(dropvalue);
                          });
                        },
                      ),
                      // child: TextFormField(
                      //   initialValue: values.year.toString(),
                      //   onChanged: (textvalue) {
                      //     setState(() {
                      //       values.year = textvalue;
                      //     });
                      //   },
                      //   //  controller: _textFieldController,
                      //   decoration: InputDecoration(
                      //       labelText: 'year', border: OutlineInputBorder()),
                      // )
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: DropdownButtonFormField(
                        value: IrrigationTypeDropdownValue,
                        decoration: InputDecoration(
                          labelText: 'Irrigation Facility',
                          border: OutlineInputBorder(),
                        ),
                        items: IrrigationList.data.response.map((value) {
                          return new DropdownMenuItem(
                            child: new Text(
                              value.irrigationTypeName,
                              style: TextStyle(fontSize: 13.0),
                            ),
                            value: value.irrigationTypeId.toString(),
                          );
                        }).toList(),
                        onChanged: (dropvalue) {
                          setState(() {
                            values.irrigationTypeId = int.parse(dropvalue);
                          });
                        },
                      ),
                      // child: TextFormField(
                      //   initialValue: values.irrigationTypeName.toString(),
                      //   onChanged: (textvalue) {
                      //     setState(() {
                      //       values.sirrigationTypeName = textvalue;
                      //     });
                      //   },
                      //   //  controller: _textFieldController,
                      //   decoration: InputDecoration(
                      //       labelText: 'Irrigation Type',
                      //       border: OutlineInputBorder()),
                      // ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: DropdownButtonFormField(
                        value: Landdropdownvalue,
                        decoration: InputDecoration(
                          labelText: 'Survey No',
                          border: OutlineInputBorder(),
                        ),
                        items: landList.data.response.map((value) {
                          return new DropdownMenuItem(
                            child: new Text(
                              value.khasaraNo,
                              style: TextStyle(fontSize: 13.0),
                            ),
                            value: value.landId.toString(),
                          );
                        }).toList(),
                        onChanged: (dropvalue) {
                          setState(() {
                            values.landId = int.parse(dropvalue);
                          });
                        },
                      ),
                      // child: TextFormField(
                      //   initialValue: values.khasaraNo.toString(),
                      //   onChanged: (textvalue) {
                      //     setState(() {
                      //       values.khasaraNo = textvalue;
                      //     });
                      //   },
                      //   //  controller: _textFieldController,
                      //   decoration: InputDecoration(
                      //       labelText: 'khasara No',
                      //       border: OutlineInputBorder()),
                      // ),
                    ),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: Container(
                  //     child: DropdownButtonFormField(
                  //       decoration: InputDecoration(
                  //         labelText: 'Season',
                  //         border: OutlineInputBorder(),
                  //       ),
                  //       items: ['xxx', 'zxc'].map((value) {
                  //         return DropdownMenuItem<String>(
                  //           value: value,
                  //           child: Text(value),
                  //         );
                  //       }).toList(),
                  //       onChanged: (value) {
                  //         setState(() {});
                  //       },
                  //     ),
                  //     // child: TextFormField(
                  //     //   initialValue: values.kharif.toString(),
                  //     //   onChanged: (textvalue) {
                  //     //     setState(() {
                  //     //       values.kharif = textvalue;
                  //     //     });
                  //     //   },
                  //     //   //  controller: _textFieldController,
                  //     //   decoration: InputDecoration(
                  //     //       labelText: 'Season', border: OutlineInputBorder()),
                  //     // ),
                  //   ),
                  // ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('Update'),
                onPressed: () async {
                  var temp = insertIrrigationType(values);
                  var isUpdate = await temp;
                  if (isUpdate) {
                    Navigator.of(context).pop();
                    SnackBar(content: Text('Date update successfully'));
                  }
                  setState(() {});
                },
              ),
            ],
          ),
          )
        );
  }

  DataRow livestockDetails(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.livestockName)),
        DataCell(Text(value.number.toString())),
        DataCell(Text(value.use)),
        DataCell(Text(value.production)),
        DataCell(Text(value.rate + '' + value.unitName)),
        DataCell(IconButton(
            onPressed: () {
              LiveStockDetailsDialog(context, value);
            },
            icon: Icon(
              Icons.edit,
              size: 15,
              color: Colors.green,
            ))),
      ],
    );
  }

  Future<void> LiveStockDetailsDialog(
      BuildContext context, dynamic values) async {
    TextEditingController _textFieldController = TextEditingController();

    String LiveStock;
    String LiveStockDropdownvalue;
    var LivestockList = await LiveStockList();
    if (values.livestockId == 0)
      setState(() {
        LiveStockDropdownvalue =
            LivestockList.data.response.first.livestockId.toString();
      });
    else
      setState(() {
        LiveStockDropdownvalue = LivestockList.data.response
            .where((element) => element.livestockId == values.livestockId)
            .first
            .livestockId
            .toString();
      });

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Text('LIVE STOCK DETAILS',
                      style: TextStyle(color: Colors.green)),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.close,
                          color: Colors.red,
                          size: 20,
                        ))
                  ],
                )
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 500,
                      child: DropdownButtonFormField(
                        value: LiveStockDropdownvalue,
                        decoration: InputDecoration(
                          labelText: 'Live Stock',
                          border: OutlineInputBorder(),
                        ),
                        items: LivestockList.data.response.map((value) {
                          return new DropdownMenuItem(
                            child: new Text(
                              value.livestockName,
                              style: TextStyle(fontSize: 13.0),
                            ),
                            value: value.livestockId.toString(),
                          );
                        }).toList(),
                        onChanged: (dropvalue) {
                          setState(() {
                            values.livestockId = int.parse(dropvalue);
                          });
                        },
                      ),
                      // child: TextFormField(
                      //   initialValue: values.livestockName.toString(),
                      //   onChanged: (textvalue) {
                      //     setState(() {
                      //       values.livestockName = textvalue;
                      //     });
                      //   },
                      //   //  controller: _textFieldController,
                      //   decoration: InputDecoration(
                      //       labelText: 'Live Stock',
                      //       border: OutlineInputBorder()),
                      // ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextFormField(
                        initialValue: values.number.toString(),
                        onChanged: (textvalue) {
                          setState(() {
                            values.number = textvalue;
                          });
                        },
                        //  controller: _textFieldController,
                        decoration: InputDecoration(
                            labelText: 'Number', border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextFormField(
                        initialValue: values.use.toString(),
                        onChanged: (textvalue) {
                          setState(() {
                            values.use = textvalue;
                          });
                        },
                        //  controller: _textFieldController,
                        decoration: InputDecoration(
                            labelText: 'Use', border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextFormField(
                        initialValue: values.production.toString(),
                        onChanged: (textvalue) {
                          setState(() {
                            values.production = textvalue;
                          });
                        },
                        //  controller: _textFieldController,
                        decoration: InputDecoration(
                            labelText: 'Production',
                            border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextFormField(
                        initialValue: values.rate.toString(),
                        onChanged: (textvalue) {
                          setState(() {
                            values.rate = textvalue;
                          });
                        },
                        //  controller: _textFieldController,
                        decoration: InputDecoration(
                            labelText: 'Rate', border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('Update'),
                onPressed: () async {
                  var temp = await insertLiveStockDetails(values);
                  var isUpdate = temp;
                  if (isUpdate) {
                    Navigator.of(context).pop();
                    SnackBar(content: Text('Date update successfully'));
                  }
                },
              ),
            ],
          );
        });
  }

  DataRow farmMachineryDetails(BuildContext context, dynamic value) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(value.farmerMachineryName)),
        DataCell(Text(value.quantity.toString())),
        DataCell(Text(value.description)),
        DataCell(IconButton(
            onPressed: () {
              FarmerMachineryDetailsDialog(context, value);
            },
            icon: Icon(
              Icons.edit,
              size: 15,
              color: Colors.green,
            ))),
      ],
    );
  }

  Future<void> FarmerMachineryDetailsDialog(
      BuildContext context, dynamic values) async {
    // TextEditingController _textFieldController = TextEditingController();
    ///----- Machinery dropdown ----

    String FarmerMachinery;
    String FarmerMachineryDropdownvalue;
    var FarmerMachineryList = await FarmMachineryList();
    if (values.farmMachineryId == 0)
      setState(() {
        FarmerMachineryDropdownvalue = FarmerMachineryList
            .data.response.first.farmerMachineryId
            .toString();
      });
    else
      setState(() {
        FarmerMachineryDropdownvalue = FarmerMachineryList.data.response
            .where((element) =>
                element.farmerMachineryId == values.farmMachineryId)
            .first
            .farmerMachineryId
            .toString();
      });

    return showGeneralDialog(
        context: context,
      pageBuilder: (context, animation, secondaryAnimation) =>Center(
        child:values.hashCode == null ? CircularProgressIndicator() :
           AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Text('FARMER MACHINERY DETAILS',
                      style: TextStyle(color: Colors.green)),
                ),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(Icons.close, color: Colors.red, size: 20))
                  ],
                )
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 500,
                      child: DropdownButtonFormField(
                        value: FarmerMachineryDropdownvalue,
                        decoration: InputDecoration(
                          labelText: 'Machinery Details',
                          border: OutlineInputBorder(),
                        ),
                        items: FarmerMachineryList.data.response.map((value) {
                          return new DropdownMenuItem(
                            child: new Text(
                              value.farmerMachineryName,
                              style: TextStyle(fontSize: 13.0),
                            ),
                            value: value.farmerMachineryId.toString(),
                          );
                        }).toList(),
                        onChanged: (dropvalue) {
                          setState(() {
                            values.farmMachineryId = int.parse(dropvalue);
                          });
                        },
                      ),
                      // child: TextFormField(
                      //   initialValue: values.farmerMachineryName.toString(),
                      //   onChanged: (textvalue) {
                      //     setState(() {
                      //       values.farmerMachineryName = textvalue;
                      //     });
                      //   },
                      //   //  controller: _textFieldController,
                      //   decoration: InputDecoration(
                      //       labelText: 'Machinery Details',
                      //       border: OutlineInputBorder()),
                      // ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        initialValue: values.quantity.toString(),
                        onChanged: (textvalue) {
                          setState(() {
                            values.quantity = int.parse(textvalue);
                          });
                        },
                        //  controller: _textFieldController,
                        decoration: InputDecoration(
                            labelText: 'Quantity',
                            border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: TextFormField(
                        initialValue: values.description.toString(),
                        onChanged: (textvalue) {
                          setState(() {
                            values.description = int.parse(textvalue);
                          });
                        },
                        //  controller: _textFieldController,
                        decoration: InputDecoration(
                            labelText: 'Description',
                            border: OutlineInputBorder()),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text('Update'),
                onPressed: () async {
                  var temp = await insertFarmerMachineryDetails(values);
                  var isUpdate = temp;
                  if (isUpdate) {
                    Navigator.of(context).pop();
                    SnackBar(content: Text('Date update successfully'));
                  }
                },
              ),
            ],
          ),
          // Padding(
          //   child: FutureBuilder(
          //    future:ConvertFamilyDetails() ,
          //    builder: (context, FarmerDetails){
          //      if (FarmerDetails.hasData) {
          //        return AlertDialog()

          //      } else if (FarmerDetails.hasError) {
          //                   return Text("${FarmerDetails.error}");
          //                 }
          //                 return CircularProgressIndicator();

          //    }
          //      )
          // );
    )
        );
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(DemoLocalization.of(context).translate('FARMERPROFILE')),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //Farmer Profile
            Container(
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context).translate('FARMERPROFILE'),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  IconButton(
                      alignment: Alignment.centerRight,
                      onPressed: () {
                        farmerProfileDialog(context, fd.data.response);
                      },
                      icon: Icon(
                        Icons.edit,
                        size: 20,
                        color: Colors.white,
                      )),
                ],
              ),
            ),
            FutureBuilder(
              future: ConvertFarmerDetails(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return FarmerProfile(context, fd.data.response);
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //Family Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context).translate('FAMILYDETAILS'),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchFamilyDetailsResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data.data.response;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('MEMBERNAME'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('RELATION'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('GENDER'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('AGE'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('EDUCATION'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('OCCUPATION'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('Action'),
                          ),
                        ),
                        // DataColumn(
                        //   label: Text(
                        //     'Action',
                        //   ),
                        // ),
                      ], rows: [
                        for (dynamic data in list)
                          FamilyDetailsList(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //Bank Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context).translate('BANKDETAILS'),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  IconButton(
                      alignment: Alignment.centerRight,
                      onPressed: () {
                        BankDetailsDialog(context, fd.data.response);
                      },
                      icon: Icon(
                        Icons.edit,
                        size: 20,
                        color: Colors.white,
                      )),
                ],
              ),
            ),
            FutureBuilder(
              future: farmer,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return BankDetails(context, fd.data.response);
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //Other Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      DemoLocalization.of(context).translate('OTHERDETAILS'),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  IconButton(
                      alignment: Alignment.centerRight,
                      onPressed: () {
                        OtherDetailsDialog(context, fd.data.response);
                      },
                      icon: Icon(
                        Icons.edit,
                        size: 20,
                        color: Colors.white,
                      )),
                ],
              ),
            ),
            FutureBuilder(
              future: farmer,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return OtherDetails(context, fd.data.response);
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //Land Holding RoR Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("LANDHOLDINGDETAILS(ROR)"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                      softWrap: true,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchFarmerLandResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('khasaraNo'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('totalArea'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('chakno'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('soiltype'),
                          ),
                        )
                      ], rows: [
                        for (dynamic data in list) landDetailsROR(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //Land Holding RoR Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.80,
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("LANDHOLDINGDETAILS(RORDESIGN)"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchFarmerLandDetailsResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('khasaraNo'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('totalArea'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('chakno'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('subChakno'),
                          ),
                        )
                      ], rows: [
                        for (dynamic data in list)
                          landDetailsRORDesign(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //Cropping Pattern Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("CROPPINGPATTERNDETAILS"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchFCropPatternResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data.data.response;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('year'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('khasaraNo'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('Season'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('Crop'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('area'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('VARIETY'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('INPUTCOST'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('YIELD(Qt)'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('SOWINGDATE'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('HARVESTDATE'),
                          ),
                        ),
                      ], rows: [
                        for (dynamic data in list)
                          cropPatternDetails(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //IRRIGATION FACILITY Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("IRRIGATIONFACILITYDETAILS"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchFIrrigationFacilityResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data.data.response;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('year'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('IRRIGATIONFACILITYDETAILS'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('khasaraNo'),
                          ),
                        ),
                        // DataColumn(
                        //   label: Text(
                        //     DemoLocalization.of(context)
                        //     .translate('Season'),
                        //   ),
                        // ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('PUMPCAPACITY(HP)'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('RATE'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('Action'),
                          ),
                        )
                      ], rows: [
                        for (dynamic data in list)
                          irrigationFacilityDetails(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //IRRIGATION TYPE Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("IRRIGATIONTYPEDETAILS"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchFirrigationTypeResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data.data.response;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('year'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate('IrrigationType'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('khasaraNo'),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate('Action'),
                          ),
                        )
                      ], rows: [
                        for (dynamic data in list)
                          irrigationTypeDetails(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //LIVESTOCK Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("LIVESTOCKDETAILS"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchLiveStockDetailsResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data.data.response;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate("LivestockName"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate("NUMBERS"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate("USE"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate("PRODUCTION"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate("RATE"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate("Action"),
                          ),
                        )
                      ], rows: [
                        for (dynamic data in list)
                          livestockDetails(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),

            //FARM MACHINERY Details
            Container(
              margin: EdgeInsets.only(top: 15.0),
              padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
              color: Colors.blue[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Text(
                      DemoLocalization.of(context)
                          .translate("FARMERMACHINERYDETAILS"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: fetchfarmerMachineryDetailsResponse(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                //print(snapshot.data);
                if (snapshot.hasData) {
                  List<dynamic> list = snapshot.data.data.response;
                  return SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: DataTable(columns: <DataColumn>[
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate("FarmMachineryName"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate("QUANTITY"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context)
                                .translate("DESCRIPTION"),
                          ),
                        ),
                        DataColumn(
                          label: Text(
                            DemoLocalization.of(context).translate("Action"),
                          ),
                        )
                      ], rows: [
                        for (dynamic data in list)
                          farmMachineryDetails(context, data)
                      ]));
                } else if (snapshot.hasData == null) {
                  return Text("No Details Found");
                } else if (snapshot.hasError) {
                  return Text("Error Occurs");
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
