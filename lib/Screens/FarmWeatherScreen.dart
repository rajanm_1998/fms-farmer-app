import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/CroppingPattern.dart';
import 'package:fms_mobileapp/Screens/FarmForecastScreen.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/services/farminfomer.dart';
import 'package:fms_mobileapp/services/networking.dart';
import 'package:fms_mobileapp/services/weather.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/utils/constants.dart';
import 'package:fms_mobileapp/Screens/forecast_screen.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:fms_mobileapp/services/second_weather_box.dart';
import 'package:fms_mobileapp/services/weather_informer.dart';
import 'package:provider/provider.dart';

class FarmWeatherScreen extends StatefulWidget {
  @override
  State<FarmWeatherScreen> createState() => _FarmWeatherScreenState();
}

class _FarmWeatherScreenState extends State<FarmWeatherScreen> {
  Color textAndIconColor = Colors.white;

  IconData icon = WeatherIcons.night_alt_rain;

  double _opacityUp = 0.4;

  double _opacityDown = 0.4;

  void initState() {
    super.initState();
    ConvertLandDetails();
  }

  bool wedata = false;
  String villageName = '';
  getCoordinates() async {
    try {
      //Get Coordinates

      String query = "Borivali";
      // String query = "Madhya Pradesh";
      // final query = "Badhiya Madhya Pradesh";
      var addresses = await Geocoder.local.findAddressesFromQuery(query);
      if (addresses != null) {
        var first = addresses.first;
        if (first.coordinates != null) {
          print(
              "Latitude: ${first.coordinates.latitude} + Longitude: ${first.coordinates.longitude}");
          await setState(() {
            WeatherModel().setCoordinates(
                first.coordinates.latitude, first.coordinates.longitude);
            wedata = true;
          });
        } else {
          print("No data found");
        }
      }
    } on Exception catch (_, ex) {}
    // if (location != null) {
    //   final lat = currentLocation.latitude;
    //   print(lat);
    //   final lon = currentLocation.longitude;
    //   print(lon);

    //   final forecastResponse = await http.get(Uri.parse(
    //       'https://api.openweathermap.org/data/2.5/onecall?lat=${lat.toString()}&lon=${lon.toString()}&exclude=hourly,minutely,current&appid=fdb21addeefaed324abd86332f58915a'));

    //   if (weatherResponse.statusCode == 200 &&
    //       forecastResponse.statusCode == 200) {
    //     setState(() {
    //       print('hello');
    //       print(weatherResponse.body);
    //       weatherData = WeatherData.fromJson(jsonDecode(weatherResponse.body));
    //       // print('hello');
    //       // print(forecastResponse.body);
    //       // forecastData =
    //       //     ForecastData.fromJson(jsonDecode(forecastResponse.body));
    //       // isLoading = false;
    //     });
    //   }
    // }
  }

  double lat = 0.0, lon = 0.0;
  getCoordinates_New(String query) async {
    try {
      //Get Coordinates

      // String query = "Madhya Pradesh";
      // final query = "Badhiya Madhya Pradesh";
      var addresses = await Geocoder.local.findAddressesFromQuery(query);
      if (addresses != null) {
        var first = addresses.first;
        if (first.coordinates != null) {
          print(
              "Latitude: ${first.coordinates.latitude} + Longitude: ${first.coordinates.longitude}");
          await setState(() {
            //WeatherModel().setCoordinates(
            //  first.coordinates.latitude, first.coordinates.longitude);
            lat = first.coordinates.latitude;
            lon = first.coordinates.longitude;

            wedata = true;
          });
        } else {
          print("No data found");
        }
      }
    } on Exception catch (_, ex) {}
    // if (location != null) {
    //   final lat = currentLocation.latitude;
    //   print(lat);
    //   final lon = currentLocation.longitude;
    //   print(lon);

    //   final forecastResponse = await http.get(Uri.parse(
    //       'https://api.openweathermap.org/data/2.5/onecall?lat=${lat.toString()}&lon=${lon.toString()}&exclude=hourly,minutely,current&appid=fdb21addeefaed324abd86332f58915a'));

    //   if (weatherResponse.statusCode == 200 &&
    //       forecastResponse.statusCode == 200) {
    //     setState(() {
    //       print('hello');
    //       print(weatherResponse.body);
    //       weatherData = WeatherData.fromJson(jsonDecode(weatherResponse.body));
    //       // print('hello');
    //       // print(forecastResponse.body);
    //       // forecastData =
    //       //     ForecastData.fromJson(jsonDecode(forecastResponse.body));
    //       // isLoading = false;
    //     });
    //   }
    // }
  }

  List<FarmerLandModel> landResponse;
  Set<FarmInformer> weatherResponse = new Set<FarmInformer>();
  // ConvertLandDetails() async{
  //   List<FarmerLandModel> landResponse = new List<FarmerLandModel>();
  //   land = await landResponse;
  //   return land;
  //   }

  getVillageName(int index) {
    return landResponse[index].villageName;
  }

  Future<List<FarmerLandModel>> land;
  List<FarmerLandModel> ld;
  ConvertLandDetails() async {
    try {
      land = fetchFarmerLandResponse();
      var data = await land;
      await setState(() {
        ld = data;
      });
      weatherResponse.clear();
      for (int i = 0; i < ld.length; i++) {
        var temp = new FarmInformer();
        setState(() {
          lat = lon = 0.0;
        });
        await getCoordinates_New(ld[i].villageName +
            ' ' +
            ld[i].blockName +
            ' ' +
            ld[i].districtName +
            ' ' +
            ld[i].stateName);
        if (lat != 0.0 && lon != 0.0) {
          await temp.getFarmLocationWeatherInfo_New(
              latitude: lat, longitude: lon);
          await setState(() {
            weatherResponse.add(temp);
          });
        } else
          i--;
      }
      print('land: ' + ld.length.toString());
      print('weather: ' + weatherResponse.length.toString());
    } catch (Exception) {}
    return land;
  }

  // getLandCoordinates() async{
  //   List<FarmerLandModel> landDetails =  ConvertLandDetails();
  //   for(int i = 0; i < landDetails.length; i++){
  //     await getCoordinates(landDetails[i].villageName);
  //   }
  // }

  getLandCoordinates_New(FarmerLandModel values) async {
    //List<FarmerLandModel> landDetails =  ConvertLandDetails();
    //for(int i = 0; i < landDetails.length; i++){
    wedata = await getCoordinates();
  }

  // ConvertFarmLandWeatherResponse() async {
  //   List<FarmerLandModel> futureresp;
  //   try {
  //     futureresp = fetchFarmerLandResponse();
  //     landResponse = await futureresp;

  //     for (int i = 0; i < landResponse.length; i++)
  //       await getCoordinates(
  //           landResponse[i].villageName, landResponse[i].stateName);
  //   } on Exception catch (_, ex) {}
  //   return futureresp;
  // }

  visible(String visibleData) {
    var visible = int.parse(visibleData);
    var value = visible / 1000;
    return value.toString();
  }

  Widget listViewBuilder(
      BuildContext context, List<dynamic> values, FarmInformer weatherData) {
    Future.delayed(Duration(seconds: 5));
    return ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: SafeArea(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: Card(
                            child: Container(
                              height: 300,
                              width: 200,
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      // Icon(weatherData.icon, size: 45.0),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 20.0),
                                        child: Text("Farm Weather data" +
                                            weatherData.weatherCondition),
                                      )
                                    ],
                                  ),
                                  // Row(
                                  //   children: [
                                  //     IconButton(
                                  //         onPressed: null,
                                  //         icon: new Icon(
                                  //           WeatherIcons.thermometer,
                                  //           color: Colors.black,
                                  //         )),
                                  //     Padding(
                                  //       padding: const EdgeInsets.all(5.0),
                                  //       child: Text(
                                  //         weatherData.temperature,
                                  //         style: TextStyle(),
                                  //       ),
                                  //     ),
                                  //     Text(
                                  //       weatherData.tempMeteric,
                                  //       style: TextStyle(),
                                  //     ),
                                  //   ],
                                  // ),
                                  // Row(
                                  //   children: [
                                  //     Padding(
                                  //       padding: const EdgeInsets.all(5.0),
                                  //       child: Row(
                                  //         children: [
                                  //           IconButton(
                                  //               onPressed: null,
                                  //               icon: new Icon(
                                  //                 WeatherIcons.raindrop,
                                  //                 color: Colors.black,
                                  //               )),
                                  //           Text(
                                  //             weatherData.humidity.toString(),
                                  //             softWrap: true,
                                  //             style: TextStyle(),
                                  //           ),
                                  //         ],
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                  // //  Row(
                                  // //   children: [
                                  // //     // Image.network(
                                  // //     //     'https://th.bing.com/th/id/OIP.L8uzDXZg3bPW6PbBfXlp8AHaI5?pid=ImgDet&rs=1',height: 28,width: 30,),
                                  // //     Padding(
                                  // //       padding: const EdgeInsets.all(8.0),
                                  // //       child: Icon(Icons.cloud),
                                  // //     ),
                                  // //     Padding(
                                  // //       padding: const EdgeInsets.fromLTRB(2.0, 8.0, 8.0, 8.0),
                                  // //       child: Text(
                                  // //         widget.weather.main,
                                  // //         softWrap: true,
                                  // //         style: TextStyle(fontSize: 18),
                                  // //       ),
                                  // //     ),
                                  // //   ],
                                  // // ),
                                  // Align(
                                  //     alignment: Alignment.bottomLeft,
                                  //     child: Padding(
                                  //       padding: EdgeInsets.all(5.0),
                                  //       child: Row(
                                  //         children: [
                                  //           IconButton(
                                  //               onPressed: null,
                                  //               icon: new Icon(
                                  //                 WeatherIcons.sunrise,
                                  //                 color: Colors.black,
                                  //               )),
                                  //           Text(
                                  //             weatherData.sunrise + " AM",
                                  //             softWrap: true,
                                  //           ),
                                  //         ],
                                  //       ),
                                  //     )),
                                  // Align(
                                  //     alignment: Alignment.bottomLeft,
                                  //     child: Padding(
                                  //       padding: EdgeInsets.all(5.0),
                                  //       child: Row(
                                  //         children: [
                                  //           IconButton(
                                  //               onPressed: null,
                                  //               icon: new Icon(
                                  //                   WeatherIcons.sunset,
                                  //                   color: Colors.black)),
                                  //           Text(
                                  //             weatherData.sunset + " PM",
                                  //             softWrap: true,
                                  //           ),
                                  //         ],
                                  //       ),
                                  //     )),

                                  // Padding(
                                  //   padding: const EdgeInsets.all(8.0),
                                  //   child: Text("99.99%"),
                                  // ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8.0),
                                  //   child: Text("____%"),
                                  // ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8.0),
                                  //   child: Text("SUN RISE :-"),
                                  // ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8.0),
                                  //   child: Text("SUN SET :- "),
                                  // )
                                ],
                              ),
                            ),
                          ),
                        ),
                        // Card(
                        //     child: SingleChildScrollView(
                        //   scrollDirection: Axis.vertical,
                        //   child: Container(
                        //       height: 300,
                        //       width: 200,
                        //       child: Padding(
                        //         padding: const EdgeInsets.symmetric(),
                        //         child: Column(
                        //           children: [
                        //             // Align(
                        //             //     child: Padding(
                        //             //   padding: EdgeInsets.all(5.0),
                        //             //   child: Text(
                        //             //     "FARM lOCATION",
                        //             //     style: TextStyle(fontWeight: FontWeight.bold),
                        //             //   ),
                        //             // )

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //       padding: EdgeInsets.all(5.0),
                        //             //       child: Text(
                        //             //           weatherData.tempMin.toString() +
                        //             //               " °C")),
                        //             // ),
                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text(
                        //             //         weatherData.tempMax.toString() +
                        //             //             " °C"),
                        //             //   ),
                        //             // ),

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text(
                        //             //         weatherData.pressure.toString()),
                        //             //   ),
                        //             // ),

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text(weatherData.uvindex),
                        //             //   ),
                        //             // ),

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text(
                        //             //         weatherData.windSpeed.toString()),
                        //             //   ),
                        //             // ),

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text(weatherData.windDirect
                        //             //         .toString()),
                        //             //   ),
                        //             // ),

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text(
                        //             //         weatherData.visibility + " KM"),
                        //             //   ),
                        //             // ),

                        //             //  Align(
                        //             //    alignment: Alignment.topLeft,
                        //             //   child: Padding(padding: EdgeInsets.all(5.0),
                        //             //     child: Text("SOLAR RADIATION:-"),),
                        //             // ),

                        //             // Align(
                        //             //   alignment: Alignment.topLeft,
                        //             //   child: Padding(
                        //             //     padding: EdgeInsets.all(5.0),
                        //             //     child: Text("PRECIPITATION :-" +weather.),
                        //             //   ),
                        //             // ),
                        //           ],
                        //         ),
                        //       )),
                        // )),
                      ],
                    ),

                    //   Container(
                    //   height: MediaQuery.of(context).size.width * 0.1,
                    //   child: Row(
                    //   children: [
                    //     Padding(
                    //       padding: const EdgeInsets.only(left: 100.0),
                    //       child: Text("Date: " + weatherData.date),
                    //     ),
                    //     Text(" Time: " + weatherData.time)
                    //   ],
                    // )),
                    // ForecastViewer()

                    // Container(
                    //   child: AppBar(
                    //     // AppBar
                    //     automaticallyImplyLeading: false,
                    //     leading: IconButton(
                    //       color: textAndIconColor,
                    //       icon: Icon(Icons.location_on),
                    //       iconSize: 25.0,
                    //       onPressed: () {
                    //         weatherData.getLocationWeatherInfo();
                    //       },
                    //     ),
                    //     title: Text(
                    //       weatherData.cityName,
                    //       textAlign: TextAlign.center,
                    //       overflow: TextOverflow.ellipsis,
                    //       maxLines: 2,
                    //       style: TextStyle(
                    //           fontWeight: FontWeight.w900,
                    //           fontSize: 20.0),
                    //     ),
                    //     centerTitle: true,
                    //     backgroundColor: Colors.transparent,
                    //     elevation: 0,
                    //     actions: <Widget>[
                    //       IconButton(
                    //         icon: Icon(Icons.search),
                    //         iconSize: 25.0,
                    //         onPressed: () {
                    //           Navigator.push(
                    //               context,
                    //               MaterialPageRoute(
                    //                   builder: (context) =>
                    //                       CityScreen()));
                    //         },
                    //         tooltip: 'Search',
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // Expanded(
                    //   child: Card(
                    //       color:
                    //           Colors.transparent.withOpacity(_opacityUp),
                    //       margin: EdgeInsets.only(
                    //           top: 10.0,
                    //           left: 8.0,
                    //           right: 8.0,
                    //           bottom: 0.0),
                    //       child: Row(
                    //         children: [
                    //           SizedBox(
                    //             width: 18.0,
                    //           ),
                    //           Column(
                    //             crossAxisAlignment:
                    //                 CrossAxisAlignment.start,
                    //             children: [
                    //               SizedBox(
                    //                 height: 35.0,
                    //               ),
                    //               Row(
                    //                 children: [
                    //                   Text(
                    //                     weatherData.temperature,
                    //                     style: TextStyle(fontSize: 70.0),
                    //                   ),
                    //                   Text(
                    //                     weatherData.tempMeteric,
                    //                     style: TextStyle(fontSize: 30.0),
                    //                   ),
                    //                 ],
                    //               ),
                    //               SizedBox(
                    //                 height: 20.0,
                    //               ),
                    //               Column(
                    //                 crossAxisAlignment:
                    //                     CrossAxisAlignment.start,
                    //                 children: [
                    //                   Text(
                    //                     weatherData.date,
                    //                     style: TextStyle(fontSize: 18.0),
                    //                   ),
                    //                   Text(
                    //                     weatherData.time,
                    //                     style: TextStyle(fontSize: 31.0),
                    //                   ),
                    //                 ],
                    //               ),
                    //               Column(
                    //                 // Wind direction and wind speed
                    //                 crossAxisAlignment:
                    //                     CrossAxisAlignment.start,
                    //                 children: [
                    //                   SizedBox(
                    //                     height: 19.0,
                    //                   ),
                    //                   Text(
                    //                     weatherData.windDirect,
                    //                     style: TextStyle(fontSize: 18.0),
                    //                   ),
                    //                   Text(
                    //                     weatherData.windSpeed,
                    //                     style: TextStyle(fontSize: 18.0),
                    //                   ),
                    //                 ],
                    //               ),
                    //             ],
                    //           ),
                    //           SizedBox(
                    //             width: 16.0,
                    //           ),
                    //           Expanded(
                    //             flex: 1,
                    //             child: Column(
                    //               // Another Second Column in that row
                    //               children: [
                    //                 SizedBox(
                    //                   height: 26.0,
                    //                 ),
                    //                 Icon(weatherData.icon, size: 80.0),
                    //                 SizedBox(
                    //                   height: 35.0,
                    //                 ),
                    //                 Text(
                    //                   weatherData.weatherCondition,
                    //                   overflow: TextOverflow.ellipsis,
                    //                   maxLines: 2,
                    //                   textAlign: TextAlign.center,
                    //                   style: TextStyle(fontSize: 22.0),
                    //                 ),
                    //                 Column(
                    //                   crossAxisAlignment:
                    //                       CrossAxisAlignment.center,
                    //                   children: [
                    //                     SizedBox(
                    //                       height: 26.0,
                    //                     ),
                    //                     Text(
                    //                       weatherData.tempMax,
                    //                       style:
                    //                           TextStyle(fontSize: 20.0),
                    //                     ),
                    //                     Text(
                    //                       weatherData.tempMin,
                    //                       style:
                    //                           TextStyle(fontSize: 20.0),
                    //                     ),
                    //                   ],
                    //                 )
                    //               ],
                    //             ),
                    //           ),
                    //         ],
                    //       )),
                    // ),
                    // Expanded(
                    //   // Second weather Box
                    //   child: Card(
                    //     margin: EdgeInsets.only(
                    //         top: 20.0,
                    //         bottom: 20.0,
                    //         left: 8.0,
                    //         right: 8.0),
                    //     color:
                    //         Colors.transparent.withOpacity(_opacityDown),
                    //     child: Padding(
                    //       padding: const EdgeInsets.only(
                    //           top: 0.0, bottom: 20.0),
                    //       child: Row(
                    //         children: [
                    //           SizedBox(
                    //             width: 18.0,
                    //           ),
                    //           Column(
                    //             // SecondWeather box weather data
                    //             mainAxisAlignment:
                    //                 MainAxisAlignment.spaceEvenly,
                    //             crossAxisAlignment:
                    //                 CrossAxisAlignment.start,
                    //             children: [
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.sunrise,
                    //                   label: weatherData.sunrise),
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.sunset,
                    //                   label: weatherData.sunset),
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.raindrop,
                    //                   label: weatherData.humidity),
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.day_sunny,
                    //                   label: weatherData.uvindex),
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.cloudy,
                    //                   label: weatherData.visibility),
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.rain,
                    //                   label: weatherData.preciption),
                    //               SecondWeatherBox(
                    //                   iconData: WeatherIcons.wind,
                    //                   label: weatherData.pressure),
                    //             ],
                    //           ),
                    //           SizedBox(
                    //             width: 20.0,
                    //           ),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    // TextButton(
                    //     style: TextButton.styleFrom(
                    //         fixedSize: Size(
                    //             MediaQuery.of(context).size.width - 30.0,
                    //             50.0),
                    //         side: BorderSide(
                    //             color: Colors.white, width: 3.0)),
                    //     onPressed: () {
                    //       Navigator.push(
                    //           context,
                    //           MaterialPageRoute(
                    //             builder: (context) => ForecastViewer(),
                    //           ));
                    //     },
                    //     child: Text(
                    //       'Forecast',
                    //       style: TextStyle(
                    //         color: Colors.white,
                    //         fontSize: 30.0,
                    //         fontWeight: FontWeight.w900,
                    //       ),
                    //     )),
                    //       SizedBox(
                    //         height: 5.0,
                    //         child:   Padding(
                    //  padding: EdgeInsets.all(0.0),
                    //  child: Row(
                    //    children: [

                    //              Text("Hello" + weatherData.date,textAlign: TextAlign.left, style: new TextStyle(color: Colors.white)),
                    //              Text(weatherData.time, style: new TextStyle(color: Colors.white)),
                    //    ],
                    //  ),
                    //          ),
                    //       )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget FarmLandWidget(
      BuildContext context, dynamic values, FarmInformer weatherData) {
    Future.delayed(Duration(seconds: 5));
    return Container(
      child: SafeArea(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      // SizedBox(
                      //   height: 10.0,
                      // ),
                      Container(
                        height: 70,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.blue[900],
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "WEATHER DATA",
                                softWrap: true,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400),
                                textAlign: TextAlign.center,
                              ),
                              Text(""),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  
                                    Text("Date: " + weatherData.date,
                                        style: TextStyle(color: Colors.white)),
                                   Text("  Time: " +
                                          weatherData.time ,
                                        style: TextStyle(color: Colors.white)),
                                  Text(
                                          "  Location: " +
                                          weatherData.getLocation(),
                                      style: TextStyle(color: Colors.white))
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),

                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Card(
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.38,
                                width: MediaQuery.of(context).size.width * 0.50,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Row(
                                      children: [
                                        Icon(weatherData.icon, size: 45.0),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20.0),
                                          child: Text(
                                              weatherData.weatherCondition),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        IconButton(
                                            onPressed: null,
                                            icon: new Icon(
                                              WeatherIcons.thermometer,
                                              color: Colors.black,
                                            )),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              4.0, 0.0, 0.0, 0.0),
                                          child: Text(
                                            weatherData.temperature,
                                            style: TextStyle(),
                                          ),
                                        ),
                                        Text(
                                          weatherData.tempMeteric,
                                          style: TextStyle(),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              4.0, 0.0, 0.0, 0.0),
                                          child: Row(
                                            children: [
                                              IconButton(
                                                  onPressed: null,
                                                  icon: new Icon(
                                                    WeatherIcons.raindrop,
                                                    color: Colors.black,
                                                  )),
                                              Text(
                                                weatherData.humidity.toString(),
                                                style: TextStyle(),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    //  Row(
                                    //   children: [
                                    //     // Image.network(
                                    //     //     'https://th.bing.com/th/id/OIP.L8uzDXZg3bPW6PbBfXlp8AHaI5?pid=ImgDet&rs=1',height: 28,width: 30,),
                                    //     Padding(
                                    //       padding: const EdgeInsets.all(8.0),
                                    //       child: Icon(Icons.cloud),
                                    //     ),
                                    //     Padding(
                                    //       padding: const EdgeInsets.fromLTRB(2.0, 8.0, 8.0, 8.0),
                                    //       child: Text(
                                    //         widget.weather.main,
                                    //         softWrap: true,
                                    //         style: TextStyle(fontSize: 18),
                                    //       ),
                                    //     ),
                                    //   ],
                                    // ),
                                    Align(
                                        alignment: Alignment.bottomLeft,
                                        child: Padding(
                                          padding: EdgeInsets.all(5.0),
                                          child: Row(
                                            children: [
                                              IconButton(
                                                  onPressed: null,
                                                  icon: new Icon(
                                                    WeatherIcons.sunrise,
                                                    color: Colors.black,
                                                  )),
                                              Expanded(
                                                child: Text(
                                                  weatherData.sunrise + " AM",
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),
                                    Align(
                                        alignment: Alignment.bottomLeft,
                                        child: Padding(
                                          padding: EdgeInsets.all(5.0),
                                          child: Row(
                                            children: [
                                              IconButton(
                                                  onPressed: null,
                                                  icon: new Icon(
                                                      WeatherIcons.sunset,
                                                      color: Colors.black)),
                                              Expanded(
                                                child: Text(
                                                  weatherData.sunset + " PM",
                                                ),
                                              ),
                                            ],
                                          ),
                                        )),

                                    // Padding(
                                    //   padding: const EdgeInsets.all(8.0),
                                    //   child: Text("99.99%"),
                                    // ),
                                    // Padding(
                                    //   padding: const EdgeInsets.all(8.0),
                                    //   child: Text("____%"),
                                    // ),
                                    // Padding(
                                    //   padding: const EdgeInsets.all(8.0),
                                    //   child: Text("SUN RISE :-"),
                                    // ),
                                    // Padding(
                                    //   padding: const EdgeInsets.all(8.0),
                                    //   child: Text("SUN SET :- "),
                                    // )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Card(
                                child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.38,
                                  width:
                                      MediaQuery.of(context).size.width * 0.50,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        // Align(
                                        //     child: Padding(
                                        //   padding: EdgeInsets.all(5.0),
                                        //   child: Text(
                                        //     "FARM lOCATION",
                                        //     style: TextStyle(fontWeight: FontWeight.bold),
                                        //   ),
                                        // )

                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                              padding: EdgeInsets.all(5.0),
                                              child: Text(weatherData.tempMin
                                                  .toString())),
                                        ),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(
                                                weatherData.tempMax.toString()),
                                          ),
                                        ),

                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(weatherData.pressure
                                                .toString()),
                                          ),
                                        ),

                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(weatherData.uvindex),
                                          ),
                                        ),

                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(weatherData.windSpeed
                                                .toString()),
                                          ),
                                        ),

                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(weatherData.windDirect
                                                .toString()),
                                          ),
                                        ),

                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.all(5.0),
                                            child: Text(weatherData.visibility),
                                          ),
                                        ),

                                        //  Align(
                                        //    alignment: Alignment.topLeft,
                                        //   child: Padding(padding: EdgeInsets.all(5.0),
                                        //     child: Text("SOLAR RADIATION:-"),),
                                        // ),

                                        // Align(
                                        //   alignment: Alignment.topLeft,
                                        //   child: Padding(
                                        //     padding: EdgeInsets.all(5.0),
                                        //     child: Text("PRECIPITATION :-" +weather.),
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  )),
                            )),
                          ),
                        ],
                      ),

                      //   Container(
                      //   height: MediaQuery.of(context).size.width * 0.1,
                      //   child: Row(
                      //   children: [
                      //     Padding(
                      //       padding: const EdgeInsets.only(left: 50.0),
                      //       child: Text("Date: " + weatherData.date),
                      //     ),
                      //     Text(" Time: " + weatherData.time + "Location: " + weatherData.locationName)
                      //   ],
                      // )),
                      ForecastViewer()

                      // Container(
                      //   child: AppBar(
                      //     // AppBar
                      //     automaticallyImplyLeading: false,
                      //     leading: IconButton(
                      //       color: textAndIconColor,
                      //       icon: Icon(Icons.location_on),
                      //       iconSize: 25.0,
                      //       onPressed: () {
                      //         weatherData.getLocationWeatherInfo();
                      //       },
                      //     ),
                      //     title: Text(
                      //       weatherData.cityName,
                      //       textAlign: TextAlign.center,
                      //       overflow: TextOverflow.ellipsis,
                      //       maxLines: 2,
                      //       style: TextStyle(
                      //           fontWeight: FontWeight.w900,
                      //           fontSize: 20.0),
                      //     ),
                      //     centerTitle: true,
                      //     backgroundColor: Colors.transparent,
                      //     elevation: 0,
                      //     actions: <Widget>[
                      //       IconButton(
                      //         icon: Icon(Icons.search),
                      //         iconSize: 25.0,
                      //         onPressed: () {
                      //           Navigator.push(
                      //               context,
                      //               MaterialPageRoute(
                      //                   builder: (context) =>
                      //                       CityScreen()));
                      //         },
                      //         tooltip: 'Search',
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // Expanded(
                      //   child: Card(
                      //       color:
                      //           Colors.transparent.withOpacity(_opacityUp),
                      //       margin: EdgeInsets.only(
                      //           top: 10.0,
                      //           left: 8.0,
                      //           right: 8.0,
                      //           bottom: 0.0),
                      //       child: Row(
                      //         children: [
                      //           SizedBox(
                      //             width: 18.0,
                      //           ),
                      //           Column(
                      //             crossAxisAlignment:
                      //                 CrossAxisAlignment.start,
                      //             children: [
                      //               SizedBox(
                      //                 height: 35.0,
                      //               ),
                      //               Row(
                      //                 children: [
                      //                   Text(
                      //                     weatherData.temperature,
                      //                     style: TextStyle(fontSize: 70.0),
                      //                   ),
                      //                   Text(
                      //                     weatherData.tempMeteric,
                      //                     style: TextStyle(fontSize: 30.0),
                      //                   ),
                      //                 ],
                      //               ),
                      //               SizedBox(
                      //                 height: 20.0,
                      //               ),
                      //               Column(
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.start,
                      //                 children: [
                      //                   Text(
                      //                     weatherData.date,
                      //                     style: TextStyle(fontSize: 18.0),
                      //                   ),
                      //                   Text(
                      //                     weatherData.time,
                      //                     style: TextStyle(fontSize: 31.0),
                      //                   ),
                      //                 ],
                      //               ),
                      //               Column(
                      //                 // Wind direction and wind speed
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.start,
                      //                 children: [
                      //                   SizedBox(
                      //                     height: 19.0,
                      //                   ),
                      //                   Text(
                      //                     weatherData.windDirect,
                      //                     style: TextStyle(fontSize: 18.0),
                      //                   ),
                      //                   Text(
                      //                     weatherData.windSpeed,
                      //                     style: TextStyle(fontSize: 18.0),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ],
                      //           ),
                      //           SizedBox(
                      //             width: 16.0,
                      //           ),
                      //           Expanded(
                      //             flex: 1,
                      //             child: Column(
                      //               // Another Second Column in that row
                      //               children: [
                      //                 SizedBox(
                      //                   height: 26.0,
                      //                 ),
                      //                 Icon(weatherData.icon, size: 80.0),
                      //                 SizedBox(
                      //                   height: 35.0,
                      //                 ),
                      //                 Text(
                      //                   weatherData.weatherCondition,
                      //                   overflow: TextOverflow.ellipsis,
                      //                   maxLines: 2,
                      //                   textAlign: TextAlign.center,
                      //                   style: TextStyle(fontSize: 22.0),
                      //                 ),
                      //                 Column(
                      //                   crossAxisAlignment:
                      //                       CrossAxisAlignment.center,
                      //                   children: [
                      //                     SizedBox(
                      //                       height: 26.0,
                      //                     ),
                      //                     Text(
                      //                       weatherData.tempMax,
                      //                       style:
                      //                           TextStyle(fontSize: 20.0),
                      //                     ),
                      //                     Text(
                      //                       weatherData.tempMin,
                      //                       style:
                      //                           TextStyle(fontSize: 20.0),
                      //                     ),
                      //                   ],
                      //                 )
                      //               ],
                      //             ),
                      //           ),
                      //         ],
                      //       )),
                      // ),
                      // Expanded(
                      //   // Second weather Box
                      //   child: Card(
                      //     margin: EdgeInsets.only(
                      //         top: 20.0,
                      //         bottom: 20.0,
                      //         left: 8.0,
                      //         right: 8.0),
                      //     color:
                      //         Colors.transparent.withOpacity(_opacityDown),
                      //     child: Padding(
                      //       padding: const EdgeInsets.only(
                      //           top: 0.0, bottom: 20.0),
                      //       child: Row(
                      //         children: [
                      //           SizedBox(
                      //             width: 18.0,
                      //           ),
                      //           Column(
                      //             // SecondWeather box weather data
                      //             mainAxisAlignment:
                      //                 MainAxisAlignment.spaceEvenly,
                      //             crossAxisAlignment:
                      //                 CrossAxisAlignment.start,
                      //             children: [
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.sunrise,
                      //                   label: weatherData.sunrise),
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.sunset,
                      //                   label: weatherData.sunset),
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.raindrop,
                      //                   label: weatherData.humidity),
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.day_sunny,
                      //                   label: weatherData.uvindex),
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.cloudy,
                      //                   label: weatherData.visibility),
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.rain,
                      //                   label: weatherData.preciption),
                      //               SecondWeatherBox(
                      //                   iconData: WeatherIcons.wind,
                      //                   label: weatherData.pressure),
                      //             ],
                      //           ),
                      //           SizedBox(
                      //             width: 20.0,
                      //           ),
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      // TextButton(
                      //     style: TextButton.styleFrom(
                      //         fixedSize: Size(
                      //             MediaQuery.of(context).size.width - 30.0,
                      //             50.0),
                      //         side: BorderSide(
                      //             color: Colors.white, width: 3.0)),
                      //     onPressed: () {
                      //       Navigator.push(
                      //           context,
                      //           MaterialPageRoute(
                      //             builder: (context) => ForecastViewer(),
                      //           ));
                      //     },
                      //     child: Text(
                      //       'Forecast',
                      //       style: TextStyle(
                      //         color: Colors.white,
                      //         fontSize: 30.0,
                      //         fontWeight: FontWeight.w900,
                      //       ),
                      //     )),
                      //       SizedBox(
                      //         height: 5.0,
                      //         child:   Padding(
                      //  padding: EdgeInsets.all(0.0),
                      //  child: Row(
                      //    children: [

                      //              Text("Hello" + weatherData.date,textAlign: TextAlign.left, style: new TextStyle(color: Colors.white)),
                      //              Text(weatherData.time, style: new TextStyle(color: Colors.white)),
                      //    ],
                      //  ),
                      //          ),
                      //       )
                    ],
                  ),
                ),
              ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (ld != null && weatherResponse.length != 0)
      return ListView.builder(
        itemCount: ld.length,
        itemBuilder: (BuildContext context, int index) {
          return SafeArea(
              child: Stack(
            children: [
              /*FutureBuilder(
                              future: ConvertLandDetails(),
                              builder: (context, landDetails) {
                                if (landDetails.hasData) {
                                  Future.delayed(Duration(seconds: 5));
                                  return listViewBuilder(
                                      context, landResponse, weatherData);
                                } else if (landDetails.hasError) {
                                  return Text("No Farm Location Found");
                                } else {
                                  return Center(child: CircularProgressIndicator());
                                }
                              },
                            ),
                        */

              FarmLandWidget(
                  context, ld[index], weatherResponse.elementAt(index))

              // Container(child: ForecastViewer(),),
            ],
          ));
        },
      );
    else
      return Center(
        child: CircularProgressIndicator(),
      );
  }

  /* Widget build(BuildContext context) {
    if (ld != null) {
      return ListView.builder(
          itemCount: ld.length,
          itemBuilder: (BuildContext context, int index) {
            return Consumer<FarmInformer>(
              builder: (context, weatherData, child) {
                // weatherData every time gets updates when they are any changes in the data and UI gets updated becasue of using provider architecture
                // setFilter(weatherData.backgroundImage);
                return SafeArea(
                    child: Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.1,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.blue[900],
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "FARM WEATHER DATA",
                          softWrap: true,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w400),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    /*FutureBuilder(
                          future: ConvertLandDetails(),
                          builder: (context, landDetails) {
                            if (landDetails.hasData) {
                              Future.delayed(Duration(seconds: 5));
                              return listViewBuilder(
                                  context, landResponse, weatherData);
                            } else if (landDetails.hasError) {
                              return Text("No Farm Location Found");
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          },
                        ),
                    */
                    FarmLandWidget(context, ld[index], weatherData)

                    // Container(child: ForecastViewer(),),
                  ],
                ));
              },
            );
          });
    } else
      return Center(
        child: CircularProgressIndicator(),
      );
  }
*/
  // Method for setting opacity for two containers based on background
  void setFilter(String backgroundImage) {
    print("set filter called");
    switch (backgroundImage) {
      case kCloudyImage:
        {
          _opacityUp = 0.2;
          _opacityDown = 0.2;
          break;
        }
      case kNightImage:
        {
          _opacityUp = 0.1;
          _opacityDown = 0.1;
          break;
        }
      case kRainyImage:
        {
          _opacityUp = 0.1;
          _opacityDown = 0.2;
          break;
        }
      case kSunrisingImage:
        {
          _opacityUp = 0.4;
          _opacityDown = 0.4;
          break;
        }

      case kSnowyImage:
        {
          _opacityUp = 0.4;
          _opacityDown = 0.4;
          break;
        }
      case kFogImage:
        {
          _opacityUp = 0.2;
          _opacityDown = 0.2;
          break;
        }
      default:
        {
          _opacityUp = 0.4;
          _opacityDown = 0.4;
          break;
        }
    }
  }
}
