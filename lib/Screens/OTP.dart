import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/FrmProjectScreen.dart';
import 'package:fms_mobileapp/Screens/JalpradhanDashboardScreen.dart';
import 'package:fms_mobileapp/models/User.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/styles.dart';

class OTPScreen extends StatelessWidget {
  const OTPScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const appTitle = 'FMS Login';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
          backgroundColor: Colors.green,
        ),
        body: const MyOTPForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyOTPForm extends StatefulWidget {
  final String otpCode;
  final String mobileNo;
  const MyOTPForm({Key key, this.otpCode, this.mobileNo}) : super(key: key);
  @override
  MyOTPFormState createState() {
    return MyOTPFormState(otpCode: otpCode, mobileNo: mobileNo);
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyOTPFormState extends State<MyOTPForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>

  final otpController = TextEditingController();
  MyOTPFormState({this.otpCode, this.mobileNo});
  String otpCode;
  String mobileNo;
  bool isVisible = true;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      showFirstDialog(context, otpCode);
    }); //response = fetchResponse();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Verify OTP"),
      ),
      body: Form(
          key: _formKey,
          child: DecoratedBox(
            decoration: BoxDecoration(
              image: backgroundImage,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 200.0, 10.0, 0.0),
                  child: TextFormField(
                    controller: otpController,
                    decoration: const InputDecoration(
                        hintText: 'Enter OTP',
                        fillColor: Colors.white,
                        filled: true),
                    keyboardType: TextInputType.number,
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter OTP Code';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 16.0, horizontal: 130.0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.green,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                        minimumSize: const Size(350, 50)),
                    onPressed: () async {
                      try {
                        UserModel fetchedData;
                        Future<UserModel> res =
                            fetchOTPResponse(mobileNo, otpController.text);
                        UserModel result = await res;
                        await SecureStorage.setUserData(json.encode(result));
                        UserModel fetched = await SecureStorage.getUserData();
                        print(fetched.data.username);
                        print(result.data.username);
                        print(result.data.roleName);
                        print(fetched.data.userId);
                        if (result == null) {
                          throw Exception();
                        } else {
                          //  Navigator.push(context,MaterialPageRoute(builder: (context) => MyOTPForm(otpCode: result.data.code)));
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(builder: (context) =>  HomeScreen(username: result.data.username)));
                          if (compareOTP(otpCode, otpController.text)) {
                            // if(fetched.data.roleName.toLowerCase() == 'farmer'){
                            // showDialog(
                            //     context: context,
                            //     builder: (context) {
                            //       Future.delayed(Duration(milliseconds: 500),
                            //           () {
                            //         Navigator.of(context).pop(true);
                            //       });
                            //       return AlertDialog(
                            //         title: Center(
                            //             child: Text(DemoLocalization.of(context)
                            //                     .translate('role') +
                            //                 ": " +
                            //                 fetched.data.roleName)),
                            //       );
                            //     });

                            if (fetched.data.roleName == "FRM" ||
                                fetched.data.roleName == "FRO") {
  
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          FarmProjectScreen()));
                            }else{
                      
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          DynamicDashboardScreen()));
                            }

                            // }else if(fetched.data.roleName.toLowerCase() == 'frm' || fetched.data.roleName.toLowerCase() == 'fro'){
                            //     Navigator.of(context).pop();
                            //   Navigator
                            //     .of(context)
                            //     .pushReplacement(
                            //       MaterialPageRoute(
                            //         builder: (BuildContext context) => JalpradhanDashboardScreen()
                            //     )
                            //   );
                            // }
                          } else {
                            showErrorDialog(context, "Invalid OTP");
                          }
                        }
                      } on Exception catch (_) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text('Something went wrong!')),
                        );
                      }
                    },
                    child: Center(
                        child:
                            Text('Verify OTP', style: TextStyle(fontSize: 15))),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}

showFirstDialog(BuildContext context, String value) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("OTP is $value"),
        );
      });
}

showErrorDialog(BuildContext context, String value) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Error: $value"),
        );
      });
}

compareOTP(String otpCode1, String otpCode2) {
  if (otpCode1 == otpCode2) {
    return true;
  } else {
    return "OTP Did not match";
  }
}

_goBack(BuildContext context) {
  Navigator.pop(context);
}
