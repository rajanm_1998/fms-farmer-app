import 'package:flutter/material.dart';
import 'package:fms_mobileapp/Screens/CropAdvisoryWebView.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/styles.dart';
import 'dart:convert';
// ignore: file_names
import 'package:flutter/cupertino.dart';
import 'package:fms_mobileapp/Screens/Home.dart';
import 'package:fms_mobileapp/models/User.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/styles.dart';
import 'package:fms_mobileapp/Screens/OTP.dart';
import 'package:fms_mobileapp/Screens/FarmerDetails.dart';
import 'package:fms_mobileapp/models/CropAdvisoryModel.dart';
import 'package:fms_mobileapp/services/storage.dart';

class CropAdvisoryScreen extends StatelessWidget {
  CropAdvisoryScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              DemoLocalization.of(context).translate('CropAdvisoryScreen'),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.contact_support_outlined,
                  color: Colors.white,
                ),
                onPressed: () {
                  showColorsInformationDialog(context);
                },
              )
            ],
            backgroundColor: Colors.green),
        body: CropAdvisoryForm());
  }

  Future<void> showColorsInformationDialog(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Text(
                    DemoLocalization.of(context).translate('ColorsRecommend'),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          Icons.close,
                          color: Colors.red,
                          size: 20,
                        ))
                  ],
                ),
              ],
            ),
            actions: [
              const Divider(
                thickness: 3.0,
                color: Colors.grey,
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.green),
                title: Text(
                  DemoLocalization.of(context).translate('Recommended'),
                ),
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.orange),
                title: Text(
                  DemoLocalization.of(context).translate('NotRecommended'),
                ),
              ),
              ListTile(
                leading: Icon(Icons.circle, color: Colors.red),
                title: Text(
                  DemoLocalization.of(context).translate('Risky'),
                ),
              ),
            ],
          );
        });
  }
}

// Create a Form widget.
class CropAdvisoryForm extends StatefulWidget {
  CropAdvisoryForm({Key key}) : super(key: key);
  @override
  CropAdvisoryFormState createState() {
    return CropAdvisoryFormState();
  }
}

int sid;

// Create a corresponding State class.
// This class holds data related to the form.
class CropAdvisoryFormState extends State<CropAdvisoryForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>

  Widget listViewBuilder(BuildContext context, List<dynamic> values) {
    return ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: Container(
                child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CropAdvisoryWebView(
                                  url: values[index].link.toString())));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        // color: const Color(0xff7c94b6),
                        border: Border(
                            left: BorderSide(
                                width: 18.0,
                                color: getColor(values[index].recommendation))),
                      ),
                      child: ListTile(
                        leading: Material(
                            color: Colors.grey[50],
                            borderRadius:
                                BorderRadius.all(Radius.circular(150.0)),
                            child: Image.network(
                              'http://fms.seprojects.in/' +
                                  (values[index].path).toString(),
                            )),
                        title: Text(
                            (DemoLocalization.of(context).translate('Crop') +
                                " : " +
                                values[index].cropName.toString()),
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 19.0,
                                color: Colors.black),
                            textAlign: TextAlign.start),
                        subtitle: Text(
                          DemoLocalization.of(context).translate('Season') +
                              " : " +
                              values[index].seasonName.toString(),
                          style: TextStyle(
                              fontSize: 16,
                              color: getColor(values[index].recommendation)),
                        ),
                        trailing: Icon(Icons.arrow_forward,
                            color: getColor(values[index].recommendation)),
                      ),
                    ))),
          );

          // return new Row(children: [
          //   Card(
          //       child: new InkWell(
          //           onTap: () {
          //             Navigator.push(
          //                 context,
          //                 MaterialPageRoute(
          //                     builder: (context) => CropAdvisoryWebView(
          //                         url: values[index].link.toString())));
          //           },
          //           child: Column(children: [
          //             Container(
          //                 margin: EdgeInsets.fromLTRB(5.0, 0, 10.0, 0),
          //                 height: 140,
          //                 decoration: BoxDecoration(
          //                   color: const Color(0xff7c94b6),
          //                   border: Border(
          //                       left: BorderSide(
          //                           width: 20.0,
          //                           // color: getColor((values[index].recommendation))),
          //                           color: getColor(
          //                               values[index].recommendation))),
          //                   image: DecorationImage(
          //                     colorFilter: new ColorFilter.mode(
          //                         Colors.black.withOpacity(0.6),
          //                         BlendMode.dstATop),
          //                     image: NetworkImage('http://fms.seprojects.in/' +
          //                         (values[index].path).toString()),
          //                     fit: BoxFit.fill,
          //                   ),
          //           )),
          //             //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
          //             Container(
          //               // width: MediaQuery.of(context).size.width,
          //               // color: Colors.black.withOpacity(0.5),
          //               child: Text(
          //                   ("Crop Name: " +
          //                           values[index].cropName +
          //                           "\n" +
          //                           " Season: " +
          //                           values[index].seasonName)
          //                       .toString(),
          //                   style: TextStyle(
          //                       fontWeight: FontWeight.w600,
          //                       fontSize: 26.0,
          //                       color: Colors.black),
          //                   textAlign: TextAlign.start),
          //             ),
          //             // alignment: Alignment.center,
          //            ] )))
          //   ]  );

          // ListTile(
          //   leading: Text((values[index].id).toString()),
          //   title: Text((values[index].stateName).toString()),
          // );
        });
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
        future: fetchCropAdvisoryResponse(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text('Press button to start.');
            case ConnectionState.active:
              return Text('Awaiting result...');
            case ConnectionState.waiting:
              return Text('Awaiting result...');
            case ConnectionState.done:
              if (snapshot.hasError) return Text('Error: ${snapshot.error}');
              return listViewBuilder(context, snapshot.data);
            default:
              return Text('Some error occurred');
          }
        });

    final _formKey = GlobalKey<FormState>();

    return Scaffold(
        body: Scaffold(
            body: Center(
      child: futureBuilder,
    )));
  }
}

getColor(String recommend) {
  MaterialColor color;
  switch (recommend) {
    case 'Recommended':
      color = Colors.green;
      break;
    case 'Not Recommended':
      // recommend = Colors.redAccent.toString();
      color = Colors.orange;
      break;
    default:
      color = Colors.red;
      break;
  }
  return color;
}

// class CropAdvisoryScreen extends StatelessWidget {
//   CropAdvisoryScreen({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//             title: Text('Crop Advisory Screen'), backgroundColor: Colors.green),
//         body: CropAdvisoryForm());
//   }
// }

// // Create a Form widget.
// class CropAdvisoryForm extends StatefulWidget {
//   CropAdvisoryForm({Key key}) : super(key: key);
//   @override
//   CropAdvisoryFormState createState() {
//     return CropAdvisoryFormState();
//   }
// }

// int sid;

// // Create a corresponding State class.
// // This class holds data related to the form.
// class CropAdvisoryFormState extends State<CropAdvisoryForm> {
//   // Create a global key that uniquely identifies the Form widget
//   // and allows validation of the form.
//   //
//   // Note: This is a GlobalKey<FormState>,
//   // not a GlobalKey<MyCustomFormState>

//   Widget listViewBuilder(BuildContext context, List<dynamic> values) {
//     return ListView.builder(
//         itemCount: values.length,
//         itemBuilder: (BuildContext context, int index) {
//           return new Padding(
//               padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//               child: Card(
//                   child: new InkWell(
//                       onTap: () {
//                         Navigator.push(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => CropAdvisoryWebView(
//                                     url: values[index].link.toString())));
//                       },
//                       child: Container(
//                         height: 150,
//                         decoration: BoxDecoration(
//                           color: const Color(0xff7c94b6),
//                           border: Border(
//                               left: BorderSide(
//                                   width: 20.0,
//                                   //color: getColor((values[index].recommendation))),
//                                   color:
//                                       getColor(values[index].recommendation))),
//                           image: DecorationImage(
//                             colorFilter: new ColorFilter.mode(
//                                 Colors.black.withOpacity(0.6),
//                                 BlendMode.dstATop),
//                             //image: AssetImage("assets/images/Background_image.webp"),
//                             image: NetworkImage('http://fms.seprojects.in/' +
//                                 (values[index].path).toString()),
//                             fit: BoxFit.fill,
//                           ),
//                         ),
//                         child: //Text(("Crop Name: " + values[index].cropName + " Season: " + values[index].seasonName).toString(), style:  TextStyle(fontWeight: FontWeight.w600,fontSize: 15.0,color: Colors.white, backgroundColor: Colors.black.withOpacity(0.3))),
//                             Container(
//                           width: MediaQuery.of(context).size.width,
//                           color: Colors.black.withOpacity(0.5),
//                           child: Text(
//                               ("Crop Name: " +
//                                       values[index].cropName +
//                                       " Season: " +
//                                       values[index].seasonName)
//                                   .toString(),
//                               style: TextStyle(
//                                   fontWeight: FontWeight.w600,
//                                   fontSize: 25.0,
//                                   color: Colors.white),
//                               textAlign: TextAlign.center),
//                         ),
//                         alignment: Alignment.center,
//                       ))));

//           // ListTile(
//           //   leading: Text((values[index].id).toString()),
//           //   title: Text((values[index].stateName).toString()),
//           // );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     var futureBuilder = new FutureBuilder(
//         future: fetchCropAdvisoryResponse(),
//         builder: (BuildContext context, AsyncSnapshot snapshot) {
//           switch (snapshot.connectionState) {
//             case ConnectionState.none:
//               return Text('Press button to start.');
//             case ConnectionState.active:
//               return Text('Awaiting result...');
//             case ConnectionState.waiting:
//               return Text('Awaiting result...');
//             case ConnectionState.done:
//               if (snapshot.hasError) return Text('Error: ${snapshot.error}');
//               return listViewBuilder(context, snapshot.data);
//             default:
//               return Text('Some error occurred');
//           }
//         });

//     final _formKey = GlobalKey<FormState>();

//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//             body: Center(
//           child: futureBuilder,
//         )));
//   }
// }

// getColor(String recommend) {
//   MaterialColor color;
//   switch (recommend) {
//     case 'Recommended':
//       color = Colors.green;
//       break;
//     case 'Not Recommended':
//       // recommend = Colors.redAccent.toString();
//       color = Colors.orange;
//       break;
//     default:
//       color = Colors.red;
//       break;
//   }
//   return color;
// }
