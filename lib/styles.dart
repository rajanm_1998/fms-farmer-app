import 'package:flutter/material.dart';

DecorationImage backgroundImage = new DecorationImage(
  image: new ExactAssetImage('assets/images/Background_image.webp'),
  fit: BoxFit.cover,
);
