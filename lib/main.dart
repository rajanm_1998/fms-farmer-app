import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fms_mobileapp/arrey.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/services/farminfomer.dart';
import 'package:fms_mobileapp/services/weather_informer.dart';
import 'package:fms_mobileapp/styles.dart';
// ignore: file_names
// Hello
import 'package:fms_mobileapp/models/User.dart';
import 'package:fms_mobileapp/utils/Operation.dart';
import 'package:fms_mobileapp/Screens/OTP.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:fms_mobileapp/Screens/JalpradhanDashboardScreen.dart';
import 'package:provider/provider.dart';

void main() async {
  Provider.debugCheckInvalidValueType = null;
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    const appTitle = 'FMS Login';

    return MultiProvider(
      providers: [
        Provider(
          create: (context) => WeatherInformer(),
        ),
        Provider(
          create: (context) => FarmInformer(),
        )
      ],
      child: MaterialApp(
        theme: ThemeData(fontFamily: "Kanit"),
        debugShowCheckedModeBanner: false,
        title: appTitle,
        locale: _locale,
        supportedLocales: [Locale('en', 'US'), Locale('hi', 'IN')],
        localizationsDelegates: [
          DemoLocalization.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocale) {
          for (var locale in supportedLocale) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return deviceLocale;
            }
          }

          return supportedLocale.first;
        },
        home: Scaffold(
          appBar: AppBar(
            title: const Text(appTitle),
            backgroundColor: Colors.green,
          ),
          body: MyCustomForm(),
        ),
      ),
    );
  }
}

TextEditingController textFieldController = TextEditingController();

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key key, this.otpCode}) : super(key: key);
  final String otpCode;
  @override
  MyCustomFormState createState() {
    return MyCustomFormState(otpCode: otpCode);
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>
  final mobileNoController = TextEditingController();
  final otpController = TextEditingController();
  MyCustomFormState({this.otpCode});
  String otpCode;
  Future<Response> response;
  bool isVisible = true;
  UserModel data;

  @override
  void initState() {
    super.initState();
    getUser();

    //response = fetchResponse();
  }

  getUser() async {
    try {
      UserModel fetched = await SecureStorage.getUserData();
      if (fetched.data.username != null) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DynamicDashboardScreen()));
      }
    } on Exception catch (_) {}
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    // ignore: unnecessary_new
    return new Scaffold(
        body: Form(
            key: _formKey,
            child: DecoratedBox(
              decoration: BoxDecoration(
                image: backgroundImage,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 200.0, 10.0, 0.0),
                    child: TextFormField(
                      controller: mobileNoController,
                      decoration: const InputDecoration(
                          hintText: 'Mobile No',
                          fillColor: Colors.white,
                          suffixIcon: Icon(Icons.phone),
                          filled: true),
                      keyboardType: TextInputType.number,
                      // The validator receives the text that the user has entered.
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter mobile no';
                        }
                        return null;
                      },
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                      child: Center(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                            elevation: 3,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32.0)),
                            padding: EdgeInsets.all(20),
                          ),
                          onPressed: () async {
                            new CircularProgressIndicator();
                            try {
                              Future<Response> res =
                                  fetchResponse(mobileNoController.text);
                              Response result = await res;
                              if (result.status == 'Fail') {
                                throw Exception("Null Response");
                              } else {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyOTPForm(
                                              otpCode: result.data.code,
                                              mobileNo: mobileNoController.text,
                                            )));
                              }
                            } on Exception catch (_, ex) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text('Number not register!')),
                              );
                            }

                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(builder: (context) =>  OTPScreen()));

                            //   MaterialPageRoute(builder: (context) => HomeScreen(response: response)),
                            // );
                            // _sendDataToHomeScreen(context);
                            // Validate returns true if the form is valid, or false otherwise.
                            if (_formKey.currentState.validate()) {
                              // If the form is valid, display a snackbar. In the real world,
                              // you'd often call a server or save the information in a database.
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text('Processing Data')),
                              );
                            }
                          },
                          child: const Text('Request OTP', style: TextStyle()),
                        ),
                      ))
                ],
              ),
            )));
  }
}

// class CircularIndicator extends StatefulWidget {

//   CircularIndicatorWidget createState() => CircularIndicatorWidget();

// }

// class CircularIndicatorWidget extends State {

//   bool visible = true ;

//   loadProgress(){

//     if(visible == true){
//       setState(() {
//        visible = false;
//       });
//     }
//     else{
//       setState(() {
//        visible = true;
//       });
//     }

//   }

//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     throw UnimplementedError();
//   }
// }

// void _sendDataToHomeScreen(BuildContext context){
//   String texttoSend = textFieldController.text;
//   Navigator.push(
//     context,
//     MaterialPageRoute(
//       builder: (context) => HomeScreen(text: texttoSend,),
//     )
//   );
// }

showFirstDialog(BuildContext context, String value) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("OTP is $value"),
        );
      });
}
