class MstSeasonModel {
  String status;
  SeasonData data;

  MstSeasonModel({this.status, this.data});

  MstSeasonModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new SeasonData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class SeasonData {
  List<SeasonResponse> response;
  String status;
  Null message;

  SeasonData({this.response, this.status, this.message});

  SeasonData.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<SeasonResponse>();
      json['Response'].forEach((v) {
        response.add(new SeasonResponse.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class SeasonResponse {
  int seasonId;
  String seasonName;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;

  SeasonResponse(
      {this.seasonId,
      this.seasonName,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active});

  SeasonResponse.fromJson(Map<String, dynamic> json) {
    seasonId = json['SeasonId'];
    seasonName = json['SeasonName'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SeasonId'] = this.seasonId;
    data['SeasonName'] = this.seasonName;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    return data;
  }
}
