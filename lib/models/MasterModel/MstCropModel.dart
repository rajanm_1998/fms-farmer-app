class MstCropModel {
  String status;
  CropModelData data;

  MstCropModel({this.status, this.data});

  MstCropModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data =
        json['data'] != null ? new CropModelData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CropModelData {
  List<CropResponse> response;
  String status;
  Null message;

  CropModelData({this.response, this.status, this.message});

  CropModelData.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<CropResponse>();
      json['Response'].forEach((v) {
        response.add(new CropResponse.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class CropResponse {
  int cropId;
  String cropName;
  int seasonId;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;

  CropResponse(
      {this.cropId,
      this.cropName,
      this.seasonId,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active});

  CropResponse.fromJson(Map<String, dynamic> json) {
    cropId = json['CropId'];
    cropName = json['CropName'];
    seasonId = json['SeasonId'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CropId'] = this.cropId;
    data['CropName'] = this.cropName;
    data['SeasonId'] = this.seasonId;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    return data;
  }
}
