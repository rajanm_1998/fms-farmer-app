class ChakIrrigationModel {
  String status;
  ChakIrrigationData data;

  ChakIrrigationModel({this.status, this.data});

  ChakIrrigationModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null
        ? new ChakIrrigationData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ChakIrrigationData {
  List<ChakIrrigationResponse> response;
  String status;
  Null message;

  ChakIrrigationData({this.response, this.status, this.message});

  ChakIrrigationData.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<ChakIrrigationResponse>();
      json['Response'].forEach((v) {
        response.add(new ChakIrrigationResponse.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class ChakIrrigationResponse {
  int omsId;
  String omsCoordinate;
  String chakNo;
  int romsValveId;
  int area;
  String updateTime;
  int day;
  String displayStr;
  int valMin;
  String devStatus;
  double batterylevel;
  String door1;
  String door2;
  double inLetPressure;
  int outLetPressure;
  double calculatedFlowRate;
  double calculatedVolume;
  double valvePosition;
  int mode;
  int oMode;

  ChakIrrigationResponse(
      {this.omsId,
      this.omsCoordinate,
      this.chakNo,
      this.romsValveId,
      this.area,
      this.updateTime,
      this.day,
      this.displayStr,
      this.valMin,
      this.devStatus,
      this.batterylevel,
      this.door1,
      this.door2,
      this.inLetPressure,
      this.outLetPressure,
      this.calculatedFlowRate,
      this.calculatedVolume,
      this.valvePosition,
      this.mode,
      this.oMode});

  ChakIrrigationResponse.fromJson(Map<String, dynamic> json) {
    omsId = json['omsId'];
    omsCoordinate = json['omsCoordinate'];
    chakNo = json['chakNo'];
    romsValveId = json['romsValveId'];
    area = json['area'];
    updateTime = json['updateTime'];
    day = json['day'];
    displayStr = json['displayStr'];
    valMin = json['val_min'];
    devStatus = json['devStatus'];
    batterylevel = json['batterylevel'];
    door1 = json['door1'];
    door2 = json['door2'];
    inLetPressure = json['inLetPressure'];
    outLetPressure = json['outLetPressure'];
    calculatedFlowRate = json['calculatedFlowRate'];
    calculatedVolume = json['calculatedVolume'];
    valvePosition = json['valvePosition'];
    mode = json['mode'];
    oMode = json['o_mode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['omsId'] = this.omsId;
    data['omsCoordinate'] = this.omsCoordinate;
    data['chakNo'] = this.chakNo;
    data['romsValveId'] = this.romsValveId;
    data['area'] = this.area;
    data['updateTime'] = this.updateTime;
    data['day'] = this.day;
    data['displayStr'] = this.displayStr;
    data['val_min'] = this.valMin;
    data['devStatus'] = this.devStatus;
    data['batterylevel'] = this.batterylevel;
    data['door1'] = this.door1;
    data['door2'] = this.door2;
    data['inLetPressure'] = this.inLetPressure;
    data['outLetPressure'] = this.outLetPressure;
    data['calculatedFlowRate'] = this.calculatedFlowRate;
    data['calculatedVolume'] = this.calculatedVolume;
    data['valvePosition'] = this.valvePosition;
    data['mode'] = this.mode;
    data['o_mode'] = this.oMode;
    return data;
  }
}

/*class IrrigationSchedulingModels {
  String status;
  Data data;

  IrrigationSchedulingModels({this.status, this.data});

  IrrigationSchedulingModels.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<IrrigationScheduling> response;
  String status;
  String message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['response'] != null) {
      response = <IrrigationScheduling>[];
      json['response'].forEach((v) {
        response.add(new IrrigationScheduling.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class IrrigationScheduling {
  int day;
  String displayStr;
  int valMin;

  IrrigationScheduling({this.day, this.displayStr, this.valMin});

  IrrigationScheduling.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    displayStr = json['displayStr'];
    valMin = json['val_min'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['displayStr'] = this.displayStr;
    data['val_min'] = this.valMin;
    return data;
  }
}
*/