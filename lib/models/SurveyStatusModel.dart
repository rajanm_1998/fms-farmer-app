class SurveyStatusModel {
  String status;
  SurveyStatusData data;

  SurveyStatusModel({this.status, this.data});

  SurveyStatusModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new SurveyStatusData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class SurveyStatusData {
  List<SurveyStatusResponse> response;
  String status;
  Null message;

  SurveyStatusData({this.response, this.status, this.message});

  SurveyStatusData.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<SurveyStatusResponse>();
      json['Response'].forEach((v) {
        response.add(new SurveyStatusResponse.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class SurveyStatusResponse {
  int farmerId;
  String farmerName;
  int id;
  int statusId;
  String status;
  String active;

  SurveyStatusResponse(
      {this.farmerId,
      this.farmerName,
      this.id,
      this.statusId,
      this.status,
      this.active});

  SurveyStatusResponse.fromJson(Map<String, dynamic> json) {
    farmerId = json['FarmerId'];
    farmerName = json['FarmerName'];
    id = json['Id'];
    statusId = json['StatusId'];
    status = json['Status'];
    active = json['Active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerId'] = this.farmerId;
    data['FarmerName'] = this.farmerName;
    data['Id'] = this.id;
    data['StatusId'] = this.statusId;
    data['Status'] = this.status;
    data['Active'] = this.active;
    return data;
  }
}
