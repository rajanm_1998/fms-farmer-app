import 'dart:convert';
import 'package:http/http.dart' as http;

class Response {
  String status;
  Data data;

  Response({this.status, this.data});

  Response.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String mobileNumber;
  String code;
  String codeTime;

  Data({this.mobileNumber, this.code, this.codeTime});

  Data.fromJson(Map<String, dynamic> json) {
    mobileNumber = json['MobileNumber'];
    code = json['Code'];
    codeTime = json['CodeTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MobileNumber'] = this.mobileNumber;
    data['Code'] = this.code;
    data['CodeTime'] = this.codeTime;
    return data;
  }
}