class UserModel {
  String status;
  Data data;

  UserModel({this.status, this.data});

  UserModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int userId;
  String username;
  String mobileNumber;
  int roleId;
  String roleName;
  int StateId;
  String StateName;

  Data(
      {this.userId,
      this.username,
      this.mobileNumber,
      this.roleId,
      this.roleName,
      this.StateId,
      this.StateName,
      });

  Data.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    username = json['Username'];
    mobileNumber = json['MobileNumber'];
    roleId = json['RoleId'];
    roleName = json['RoleName'];
    StateId = json['stateId'];
    StateName = json['stateName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Username'] = this.username;
    data['MobileNumber'] = this.mobileNumber;
    data['RoleId'] = this.roleId;
    data['RoleName'] = this.roleName;
    data['stateId'] = this.StateId;
    data['stateName'] = this.StateName;
    return data;
  }
}