class CroppingPatternModel {
  int id = 0;
  int farmerId = 0;
  int year = 0;
  int seasonId = 0;
  String seasonName  = '';
  int cropId = 0;
  String cropName= '';
  String area = '';
  String variety= '';
  String yield= '';
  String totalYield= '';
  int createdBy= 0;
  String createdOn= '';
  int modifyBy= 0;
  String modifyOn= '';
  String active= '';
  String sowingDate= '';
  String harvestingDate= '';
  int landId= 0;
  String landName= '';
  dynamic inputCost= '';
  dynamic marketRate= '';
  dynamic marketPlace= '';
  dynamic marketDistance= '';

  CroppingPatternModel(
      {this.id,
      this.farmerId,
      this.year,
      this.seasonId,
      this.seasonName,
      this.cropId,
      this.cropName,
      this.area,
      this.variety,
      this.yield,
      this.totalYield,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.sowingDate,
      this.harvestingDate,
      this.landId,
      this.landName,
      this.inputCost,
      this.marketRate,
      this.marketPlace,
      this.marketDistance});

  CroppingPatternModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    farmerId = json['FarmerId'] ?? 0;
    year = json['Year'] ?? 0;
    seasonId = json['SeasonId'] ?? 0;
    seasonName = json['SeasonName'] ?? '';
    cropId = json['CropId'] ?? '';
    cropName = json['CropName'] ?? '';
    area = json['Area'] ?? '';
    variety = json['Variety'] ?? '';
    yield = json['Yield'] ?? '';
    totalYield = json['TotalYield'] ?? '';
    createdBy = json['CreatedBy'] ?? 0;
    createdOn = json['CreatedOn'] ?? '';
    modifyBy = json['ModifyBy'] ?? 0;
    modifyOn = json['ModifyOn'] ?? '';
    active = json['Active'] ?? '';
    sowingDate = json['SowingDate'] ?? '';
    harvestingDate = json['HarvestingDate'] ?? '';
    landId = json['LandId'] ?? 0;
    landName = json['LandName'] ?? '';
    inputCost = json['InputCost'] ?? '';
    marketRate = json['MarketRate'] ?? '';
    marketPlace = json['MarketPlace'] ?? '';
    marketDistance = json['MarketDistance'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['Year'] = this.year;
    data['SeasonId'] = this.seasonId;
    data['SeasonName'] = this.seasonName;
    data['CropId'] = this.cropId;
    data['CropName'] = this.cropName;
    data['Area'] = this.area;
    data['Variety'] = this.variety;
    data['Yield'] = this.yield;
    data['TotalYield'] = this.totalYield;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['SowingDate'] = this.sowingDate;
    data['HarvestingDate'] = this.harvestingDate;
    data['LandId'] = this.landId;
    data['LandName'] = this.landName;
    data['InputCost'] = this.inputCost;
    data['MarketRate'] = this.marketRate;
    data['MarketPlace'] = this.marketPlace;
    data['MarketDistance'] = this.marketDistance;
    return data;
  }
}
