class ChakIrrigationProjectDetails {
  ProjectDetailsResponse response;
  String status;
  

  ChakIrrigationProjectDetails({this.response, this.status});

  ChakIrrigationProjectDetails.fromJson(Map<String, dynamic> json) {
    response = json['Response'] != null
        ? new ProjectDetailsResponse.fromJson(json['Response'])
        : null;
    status = json['Status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.toJson();
    }
    data['Status'] = this.status;
    return data;
  }
}

class ProjectDetailsResponse {
  int cCA;
  double duty;
  double avgChakSize;
  int noHValves;
  int runningHValves;

  ProjectDetailsResponse(
      {this.cCA,
      this.duty,
      this.avgChakSize,
      this.noHValves,
      this.runningHValves});

  ProjectDetailsResponse.fromJson(Map<String, dynamic> json) {
    cCA = json['CCA'];
    duty = json['Duty'];
    avgChakSize = json['AvgChakSize'];
    noHValves = json['NoHValves'];
    runningHValves = json['RunningHValves'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CCA'] = this.cCA;
    data['Duty'] = this.duty;
    data['AvgChakSize'] = this.avgChakSize;
    data['NoHValves'] = this.noHValves;
    data['RunningHValves'] = this.runningHValves;
    return data;
  }
}