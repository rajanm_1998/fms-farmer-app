
class FarmWeatherData {
  final DateTime date;
  final String name;
  final double temp;
  final String main;
  final String icon;
  final String min;
  final String max;
  final String pressure;
  final String humidity;
  final String wind_speed;
  final String wind_direction;
  final DateTime sunrise;
  final DateTime sunset;
  final String uvindex;
  final String visibility;

  FarmWeatherData(
      {this.date,
      this.name,
      this.temp,
      this.main,
      this.icon,
      this.min,
      this.max,
      this.pressure,
      this.humidity,
      this.wind_speed,
      this.wind_direction,
      this.sunrise,
      this.sunset,
      this.uvindex,
      this.visibility});

  factory FarmWeatherData.fromJson(Map<String, dynamic> json) {
    return FarmWeatherData(
      date: new DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000,
          isUtc: false),
      name: json['name'],
      temp: json['main']['temp'].toDouble(),
      main: json['weather'][0]['main'],
      icon: json['weather'][0]['icon'],
      min: json['main']['temp_min'].toString(),
      max: json['main']['temp_max'].toString(),
      pressure: json['main']['pressure'].toString(),
      humidity: json['main']['humidity'].toString(),
      wind_speed: json['wind']['speed'].toString(),
      wind_direction: json['wind']['deg'].toString(),
      sunrise: new DateTime.fromMillisecondsSinceEpoch(
          json['sys']['sunrise'] * 1000,
          isUtc: false),
      sunset: new DateTime.fromMillisecondsSinceEpoch(
          json['sys']['sunset'] * 1000,
          isUtc: false),
      visibility: json['visibility'].toString()
    );
  }
}
