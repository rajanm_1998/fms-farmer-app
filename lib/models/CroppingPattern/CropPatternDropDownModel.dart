class CropPatternDropDownModel {
  int farmerId = 0;
  List<CommonList> list;
  List<CommonList> yearList;
  List<CommonList> cropList;
  List<CommonList> seasonList;
  List<CommonList> surveyList;

  CropPatternDropDownModel(
      {this.farmerId,
      this.list,
      this.yearList,
      this.cropList,
      this.seasonList,
      this.surveyList});

  CropPatternDropDownModel.fromJson(Map<String, dynamic> json) {
    farmerId = json['FarmerId'];
    if (json['_list'] != null) {
      list = new List<CommonList>();
      json['_list'].forEach((v) {
        list.add(new CommonList.fromJson(v));
      });
    }
    if (json['YearList'] != null) {
      yearList = new List<CommonList>();
      json['YearList'].forEach((v) {
        yearList.add(new CommonList.fromJson(v));
      });
    }
    if (json['CropList'] != null) {
      cropList = new List<CommonList>();
      json['CropList'].forEach((v) {
        cropList.add(new CommonList.fromJson(v));
      });
    }
    if (json['SeasonList'] != null) {
      seasonList = new List<CommonList>();
      json['SeasonList'].forEach((v) {
        seasonList.add(new CommonList.fromJson(v));
      });
    }
    if (json['SurveyList'] != null) {
      surveyList = new List<CommonList>();
      json['SurveyList'].forEach((v) {
        surveyList.add(new CommonList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerId'] = this.farmerId;
    if (this.list != null) {
      data['_list'] = this.list.map((v) => v.toJson()).toList();
    }
    if (this.yearList != null) {
      data['YearList'] = this.yearList.map((v) => v.toJson()).toList();
    }
    if (this.cropList != null) {
      data['CropList'] = this.cropList.map((v) => v.toJson()).toList();
    }
    if (this.seasonList != null) {
      data['SeasonList'] = this.seasonList.map((v) => v.toJson()).toList();
    }
    if (this.surveyList != null) {
      data['SurveyList'] = this.surveyList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CommonList {
  int year = 0;
  int cropId = 0;
  String cropName;
  int seasonId = 0;
  String seasonName;
  int landId = 0;
  String surveyNo;

  CommonList(
      {this.year,
      this.cropId,
      this.cropName,
      this.seasonId,
      this.seasonName,
      this.landId,
      this.surveyNo});

  CommonList.fromJson(Map<String, dynamic> json) {
    year = json['Year'];
    cropId = json['CropId'];
    cropName = json['CropName'];
    seasonId = json['SeasonId'];
    seasonName = json['SeasonName'];
    landId = json['LandId'];
    surveyNo = json['SurveyNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Year'] = this.year;
    data['CropId'] = this.cropId;
    data['CropName'] = this.cropName;
    data['SeasonId'] = this.seasonId;
    data['SeasonName'] = this.seasonName;
    data['LandId'] = this.landId;
    data['SurveyNo'] = this.surveyNo;
    return data;
  }
}
