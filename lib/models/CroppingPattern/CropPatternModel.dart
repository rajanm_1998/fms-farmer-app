class CroppingPatternModel {
  int id;
  int farmerId;
  int year;
  int cropId;
  String cropName;
  int seasonId;
  String seasonName;
  int landId;
  String khasaraNo;
  String chakSubChakno;
  String area;
  String yield;
  String marketPlace;
  String marketRate;
  String irrigationTypeName;
  String irrigationFacilityName;
  double actualIncome;
  double actualExpenses;
  double actualNetProfit;
  double estimateFloodNet;
  double estimateFloodGrossIncome;
  double estimateFloodExpenses;
  double estimateFloodYield;
  double estimateSprinklerNet;
  double estimateSprinklerGrossIncome;
  double estimateSprinklerExpenses;
  double estimateSprinklerYield;
  String imageLink;
  String recommendation;

  CroppingPatternModel(
      {this.id,
      this.farmerId,
      this.year,
      this.cropId,
      this.cropName,
      this.seasonId,
      this.seasonName,
      this.landId,
      this.chakSubChakno,
      this.area,
      this.yield,
      this.marketPlace,
      this.marketRate,
      this.irrigationTypeName,
      this.irrigationFacilityName,
      this.actualIncome,
      this.actualExpenses,
      this.actualNetProfit,
      this.estimateFloodNet,
      this.estimateFloodGrossIncome,
      this.estimateFloodExpenses,
      this.estimateFloodYield,
      this.estimateSprinklerNet,
      this.estimateSprinklerGrossIncome,
      this.estimateSprinklerExpenses,
      this.estimateSprinklerYield,
      this.imageLink,
      this.recommendation});

  CroppingPatternModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    farmerId = json['FarmerId'];
    year = json['Year'];
    cropId = json['CropId'] ?? 0;
    cropName = json['CropName'] ?? '';
    seasonId = json['SeasonId'] ?? 0;
    seasonName = json['SeasonName'] ?? '';
    landId = json['LandId'] ?? 0;
    khasaraNo = json['SurveyNo'] ?? '';
    chakSubChakno = json['ChakSubChakno'] ?? '';
    area = json['Area'] ?? '';
    yield = json['Yield'] ?? '';
    marketPlace = json['MarketPlace'] ?? '';
    marketRate = json['MarketRate'] ?? '';
    irrigationTypeName = json['IrrigationTypeName'] ?? '';
    irrigationFacilityName = json['IrrigationFacilityName'] ?? '';
    actualIncome = json['ActualIncome'] ?? 0;
    actualExpenses = json['ActualExpenses'] ?? 0;
    actualNetProfit = json['ActualNetProfit'] ?? 0;
    estimateFloodNet = json['EstimateFloodNet'] ?? 0;
    estimateFloodGrossIncome = json['EstimateFloodGrossIncome'] ?? 0;
    estimateFloodExpenses = json['EstimateFloodExpenses'] ?? 0;
    estimateFloodYield = json['EstimateFloodYield'] ?? 0;
    estimateSprinklerNet = json['EstimateSprinklerNet'] ?? 0;
    estimateSprinklerGrossIncome = json['EstimateSprinklerGrossIncome'] ?? 0;
    estimateSprinklerExpenses = json['EstimateSprinklerExpenses'] ?? 0;
    estimateSprinklerYield = json['EstimateSprinklerYield'] ?? 0;
    imageLink = json['ImageLink'] ?? '';
    recommendation = json['Recommendation'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['Year'] = this.year;
    data['CropId'] = this.cropId;
    data['CropName'] = this.cropName;
    data['SeasonId'] = this.seasonId;
    data['SeasonName'] = this.seasonName;
    data['LandId'] = this.landId;
    data['SurveyNo'] = this.khasaraNo;
    data['ChakSubChakno'] = this.chakSubChakno;
    data['Area'] = this.area;
    data['Yield'] = this.yield;
    data['MarketPlace'] = this.marketPlace;
    data['MarketRate'] = this.marketRate;
    data['IrrigationTypeName'] = this.irrigationTypeName;
    data['IrrigationFacilityName'] = this.irrigationFacilityName;
    data['ActualIncome'] = this.actualIncome;
    data['ActualExpenses'] = this.actualExpenses;
    data['ActualNetProfit'] = this.actualNetProfit;
    data['EstimateFloodNet'] = this.estimateFloodNet;
    data['EstimateFloodGrossIncome'] = this.estimateFloodGrossIncome;
    data['EstimateFloodExpenses'] = this.estimateFloodExpenses;
    data['EstimateFloodYield'] = this.estimateFloodYield;
    data['EstimateSprinklerNet'] = this.estimateSprinklerNet;
    data['EstimateSprinklerGrossIncome'] = this.estimateSprinklerGrossIncome;
    data['EstimateSprinklerExpenses'] = this.estimateSprinklerExpenses;
    data['EstimateSprinklerYield'] = this.estimateSprinklerYield;
    data['ImageLink'] = this.imageLink;
    data['Recommendation'] = this.recommendation;
    return data;
  }
}
