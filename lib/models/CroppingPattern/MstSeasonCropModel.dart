class MstSeasonCropModel {
  List<MstSeasonCropModelResp> response;
  String status;
  String message;

  MstSeasonCropModel({this.response, this.status, this.message});

  MstSeasonCropModel.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<MstSeasonCropModelResp>();
      json['Response'].forEach((v) {
        response.add(new MstSeasonCropModelResp.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class MstSeasonCropModelResp {
  int cropId;
  String cropName;
  int seasonId;
  String seasonName;
  String recommendation;
  String path;

  MstSeasonCropModelResp(
      {this.cropId,
      this.cropName,
      this.seasonId,
      this.seasonName,
      this.recommendation,
      this.path});

  MstSeasonCropModelResp.fromJson(Map<String, dynamic> json) {
    cropId = json['cropId'];
    cropName = json['cropName'];
    seasonId = json['seasonId'];
    seasonName = json['seasonName'];
    recommendation = json['recommendation'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cropId'] = this.cropId;
    data['cropName'] = this.cropName;
    data['seasonId'] = this.seasonId;
    data['seasonName'] = this.seasonName;
    data['recommendation'] = this.recommendation;
    data['path'] = this.path;
    return data;
  }
}
