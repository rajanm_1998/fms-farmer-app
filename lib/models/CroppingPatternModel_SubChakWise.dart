class CroppingPatternModel_SubChakWise {
  int landId;
  String khasaraNo;
  String roRSurveyNo;
  String subChakno;
  String chakno;
  String outlet;
  List<CroppingPatternsList> croppingPatternsList;

  CroppingPatternModel_SubChakWise(
      {this.landId,
      this.khasaraNo,
      this.roRSurveyNo,
      this.subChakno,
      this.chakno,
      this.outlet,
      this.croppingPatternsList});

  CroppingPatternModel_SubChakWise.fromJson(Map<String, dynamic> json) {
    landId = json['LandId'] ?? 0;
    khasaraNo = json['SurveyNo'] ?? '';
    roRSurveyNo = json['RoRSurveyNo'] ?? '';
    subChakno = json['SubChakno'] ?? '';
    chakno = json['Chakno'] ?? '';
    outlet = json['Outlet'] ?? '';
    if (json['croppingPatternsList'] != String) {
      croppingPatternsList = new List<CroppingPatternsList>();
      json['croppingPatternsList'].forEach((v) {
        croppingPatternsList.add(new CroppingPatternsList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LandId'] = this.landId;
    data['SurveyNo'] = this.khasaraNo;
    data['RoRSurveyNo'] = this.roRSurveyNo;
    data['SubChakno'] = this.subChakno;
    data['Chakno'] = this.chakno;
    data['Outlet'] = this.outlet;
    if (this.croppingPatternsList != String) {
      data['croppingPatternsList'] =
          this.croppingPatternsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CroppingPatternsList {
  int id;
  int farmerId;
  int year;
  int seasonId;
  String seasonName;
  int cropId;
  String cropName;
  String area;
  String variety;
  String yield;
  String totalYield;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  String sowingDate;
  String harvestingDate;
  int landId;
  String surveyNo;
  String inputCost;
  String marketRate;
  String marketPlace;
  String marketDistance;
  double totalArea;
  String roRSurveyNo;
  String subChakno;
  String chakno;
  String outlet;

  CroppingPatternsList(
      {this.id,
      this.farmerId,
      this.year,
      this.seasonId,
      this.seasonName,
      this.cropId,
      this.cropName,
      this.area,
      this.variety,
      this.yield,
      this.totalYield,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.sowingDate,
      this.harvestingDate,
      this.landId,
      this.surveyNo,
      this.inputCost,
      this.marketRate,
      this.marketPlace,
      this.marketDistance,
      this.totalArea,
      this.roRSurveyNo,
      this.subChakno,
      this.chakno,
      this.outlet});

  CroppingPatternsList.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    farmerId = json['FarmerId'] ?? 0;
    year = json['Year'] ?? 0;
    seasonId = json['SeasonId'] ?? 0;
    seasonName = json['SeasonName'] ?? '';
    cropId = json['CropId'] ?? 0;
    cropName = json['CropName'] ?? '';
    area = json['Area'] ?? 0;
    variety = json['Variety'] ?? '';
    yield = json['Yield'] ?? '';
    totalYield = json['TotalYield'] ?? '';
    createdBy = json['CreatedBy'] ?? 0;
    createdOn = json['CreatedOn'] ?? '';
    modifyBy = json['ModifyBy'] ?? 0;
    modifyOn = json['ModifyOn'] ?? '';
    active = json['Active'] ?? '';
    sowingDate = json['SowingDate'] ?? '';
    harvestingDate = json['HarvestingDate'] ?? '';
    landId = json['LandId'] ?? 0;
    surveyNo = json['SurveyNo'] ?? '';
    inputCost = json['InputCost'] ?? '';
    marketRate = json['MarketRate'] ?? '';
    marketPlace = json['MarketPlace'] ?? '';
    marketDistance = json['MarketDistance'] ?? '';
    totalArea = json['TotalArea'] ?? 0;
    roRSurveyNo = json['RoRSurveyNo'] ?? '';
    subChakno = json['SubChakno'] ?? '';
    chakno = json['Chakno'] ?? '';
    outlet = json['Outlet'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['Year'] = this.year;
    data['SeasonId'] = this.seasonId;
    data['SeasonName'] = this.seasonName;
    data['CropId'] = this.cropId;
    data['CropName'] = this.cropName;
    data['Area'] = this.area;
    data['Variety'] = this.variety;
    data['Yield'] = this.yield;
    data['TotalYield'] = this.totalYield;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['SowingDate'] = this.sowingDate;
    data['HarvestingDate'] = this.harvestingDate;
    data['LandId'] = this.landId;
    data['SurveyNo'] = this.surveyNo;
    data['InputCost'] = this.inputCost;
    data['MarketRate'] = this.marketRate;
    data['MarketPlace'] = this.marketPlace;
    data['MarketDistance'] = this.marketDistance;
    data['TotalArea'] = this.totalArea;
    data['RoRSurveyNo'] = this.roRSurveyNo;
    data['SubChakno'] = this.subChakno;
    data['Chakno'] = this.chakno;
    data['Outlet'] = this.outlet;
    return data;
  }
}
