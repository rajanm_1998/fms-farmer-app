class KhasaraChakList {
  String status;
  KhasaraChakData data;

  KhasaraChakList({this.status, this.data});

  KhasaraChakList.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null
        ? new KhasaraChakData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class KhasaraChakData {
  List<KhasaraChakResponse> response;
  String status;
  Null message;

  KhasaraChakData({this.response, this.status, this.message});

  KhasaraChakData.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<KhasaraChakResponse>();
      json['Response'].forEach((v) {
        response.add(new KhasaraChakResponse.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class KhasaraChakResponse {
  int id;
  String chakNo;
  String roRSurveyNo;
  int noOfFarmer;
  double area;
  int villageId;
  String villageName;
  int projectId;
  String projectName;
  int froId;
  String froName;

  KhasaraChakResponse(
      {this.id,
      this.chakNo,
      this.roRSurveyNo,
      this.noOfFarmer,
      this.area,
      this.villageId,
      this.villageName,
      this.projectId,
      this.projectName,
      this.froId,
      this.froName});

  KhasaraChakResponse.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    chakNo = json['ChakNo'];
    roRSurveyNo = json['RoRSurveyNo'];
    noOfFarmer = json['NoOfFarmer'];
    area = json['Area'];
    villageId = json['VillageId'];
    villageName = json['VillageName'];
    projectId = json['ProjectId'];
    projectName = json['ProjectName'];
    froId = json['FroId'];
    froName = json['FroName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['ChakNo'] = this.chakNo;
    data['RoRSurveyNo'] = this.roRSurveyNo;
    data['NoOfFarmer'] = this.noOfFarmer;
    data['Area'] = this.area;
    data['VillageId'] = this.villageId;
    data['VillageName'] = this.villageName;
    data['ProjectId'] = this.projectId;
    data['ProjectName'] = this.projectName;
    data['FroId'] = this.froId;
    data['FroName'] = this.froName;
    return data;
  }
}


