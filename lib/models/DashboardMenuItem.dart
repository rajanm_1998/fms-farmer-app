import 'package:flutter/cupertino.dart';
import 'package:fms_mobileapp/Screens/ChakAssignment.dart';
import 'package:fms_mobileapp/Screens/ChakIrrigationSchedule.dart';
import 'package:fms_mobileapp/Screens/CropAdvisory.dart';
import 'package:fms_mobileapp/Screens/CropWaterRequirement.dart';
import 'package:fms_mobileapp/Screens/CurrentCropPattern.dart';
import 'package:fms_mobileapp/Screens/FROData.dart';
import 'package:fms_mobileapp/Screens/FarmerDetails.dart';
import 'package:fms_mobileapp/Screens/FarmerProfileScreen/FarmerProfileScreen.dart';
import 'package:fms_mobileapp/Screens/KMLMap.dart';
import 'package:fms_mobileapp/Screens/PreviousYrCropPattern.dart';
import 'package:fms_mobileapp/Screens/SprinklerRequirement.dart';
import 'package:fms_mobileapp/Screens/FRMSurveyPage.dart';
import 'package:fms_mobileapp/Screens/SurveyPageDetails.dart';
import 'package:fms_mobileapp/Screens/WUA.dart';
import 'package:fms_mobileapp/Screens/loading_screen.dart';
import 'package:fms_mobileapp/localization/demo_localization.dart';

class DashboardMenuItem{
  List<MenuItem> farmermenuList;
  List<MenuItem> frmMenuList;
  List<MenuItem> froMenuList;
  DashboardMenuItem(BuildContext context)
  {
    farmermenuList = new List<MenuItem>();
    frmMenuList = new List<MenuItem>();
    froMenuList = new List<MenuItem>();
 
    farmermenuList.add(new MenuItem(id: 1, Menu: DemoLocalization.of(context)
                                            .translate('FARMERPROFILE'),ImagePath:'assets/images/F_Icon.png',ScreenName: FarmerProfileDetailScreen(),SeqNo: 1));
    farmermenuList.add(new MenuItem(id: 2, Menu: DemoLocalization.of(context)
                                              .translate('LandHoldingDetails'),ImagePath:'assets/images/LandHolding.png',ScreenName: FarmerDetailsScreen(),SeqNo: 2));
    farmermenuList.add(new MenuItem(id: 3, Menu: DemoLocalization.of(context).translate(
                                              'PreviousYearCroppingPattern&EstimatedEconomics'),ImagePath:'assets/images/cropping.png',ScreenName: PreviousYrCropPatternScreen(),SeqNo: 3));
    farmermenuList.add(new MenuItem(id: 4, Menu: DemoLocalization.of(context).translate(
                                              'CurrentYearCroppingPattern&EstimatedEconomics'),ImagePath:'assets/images/cropping.png',ScreenName: CurrentYrCropPatternScreen(),SeqNo: 4));
    farmermenuList.add(new MenuItem(id: 5, Menu: DemoLocalization.of(context)
                                          .translate('CROPAdvisory'),ImagePath:'assets/images/cropadvi.png',ScreenName: CropAdvisoryScreen(),SeqNo: 5));
    farmermenuList.add(new MenuItem(id: 6, Menu: DemoLocalization.of(context)
                                          .translate('SprinklerRequirement'),ImagePath:'assets/images/SPRINKLER_icon.png',ScreenName: SprinklerRequirementScreen(),SeqNo: 6));
    farmermenuList.add(new MenuItem(id: 7, Menu:DemoLocalization.of(context)
                                          .translate('ChakIrrigationSchedule'),ImagePath:'assets/images/CHAK_IRRIGATION_icon.jpg',ScreenName: SubChakIrrigationScheduleScreen(),SeqNo: 7));
    farmermenuList.add(new MenuItem(id: 8, Menu: DemoLocalization.of(context)
                                          .translate('FarmerMAP'),ImagePath:'assets/images/map.png',ScreenName: KMLAPP(),SeqNo: 8));
    farmermenuList.add(new MenuItem(id: 9, Menu: DemoLocalization.of(context)
                                          .translate('Weather'),ImagePath:'assets/images/weather.png',ScreenName: LoadingScreen(),SeqNo: 9));
    farmermenuList.add(new MenuItem(id: 10, Menu: DemoLocalization.of(context)
                                          .translate('WaterUserAssociation'),ImagePath:'assets/images/WUA_icon.jpg',ScreenName: WUAScreen(),SeqNo: 10));
    farmermenuList.add(new MenuItem(id: 11, Menu: DemoLocalization.of(context)
                                          .translate('CropCalendar'),ImagePath:'assets/images/calender.png',ScreenName: WUAScreen(),SeqNo: 11));
    farmermenuList.add(new MenuItem(id: 12, Menu: DemoLocalization.of(context)
                                          .translate('CropWaterRequirement'),ImagePath:'assets/images/WATER_REQU.png',ScreenName: CropWaterScreen(),SeqNo: 12));
    frmMenuList.add(new MenuItem(id: 1, Menu: 'FRM Survey  Analysis',ImagePath:'assets/images/survey_icon.png',ScreenName: SurveyPage(),SeqNo: 1));
    frmMenuList.add(new MenuItem(id: 2, Menu: 'FRO Data',ImagePath:'assets/images/survey_icon.png',ScreenName: FROData(),SeqNo: 2));
    //frmMenuList.add(new MenuItem(id: 3, Menu: 'Chak Assignment',ImagePath:'assets/images/assignment_icons.png',ScreenName: FarmerListByFROScreen(),SeqNo: 3));
    frmMenuList.add(new MenuItem(id: 4, Menu: DemoLocalization.of(context)
                                          .translate('Weather'),ImagePath:'assets/images/weather.png',ScreenName: LoadingScreen(),SeqNo: 4));
  
    froMenuList.add(new MenuItem(id: 1, Menu: 'Survey Page Details',ImagePath:'assets/images/survey_icon.png',ScreenName: SurveyPageDetails(),SeqNo: 1));
    froMenuList.add(new MenuItem(id: 2, Menu: 'Farmer List',ImagePath:'assets/images/F_Icon.png',ScreenName: FarmerProfileDetailScreen(),SeqNo: 2));
    froMenuList.add(new MenuItem(id: 3, Menu: 'Crop Advisiory',ImagePath:'assets/images/cropadvi.png',ScreenName: CropAdvisoryScreen(),SeqNo: 3));

  }
  Map<String, dynamic> toJson(String role) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (role.toLowerCase() == 'farmer') {
      data['List'] = this.farmermenuList.map((v) => v.toJson()).toList();
    }
    if (role.toLowerCase() == 'frm') {
      data['List'] = this.frmMenuList.map((v) => v.toJson()).toList();
    }
    if (role.toLowerCase() == 'fro') {
      data['List'] = this.froMenuList.map((v) => v.toJson()).toList();
    }
    return data;
  }
   DashboardMenuItem.fromJson(Map<String, dynamic> json, String role) {
   if (role.toLowerCase() == 'farmer') {
     farmermenuList = new List<MenuItem>();
     json['List'].forEach((v) {
        farmermenuList.add(new MenuItem.fromJson(v));
      });
      
    }
    if (role.toLowerCase() == 'frm') {
      frmMenuList = new List<MenuItem>();
      json['List'].forEach((v) {
        frmMenuList.add(new MenuItem.fromJson(v));
      });
    }
    if (role.toLowerCase() == 'fro') {
      froMenuList = new List<MenuItem>();
      json['List'].forEach((v) {
        froMenuList.add(new MenuItem.fromJson(v));
      });
    }
  }
}
class MenuItem{
  int id;
  String Menu;
  String ImagePath;
  Widget ScreenName;
  int SeqNo;
  MenuItem({this.id, this.Menu='', this.ImagePath='',this.ScreenName= null, this.SeqNo});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['Menu'] = this.Menu;
    data['ImagePath'] = this.ImagePath;
    data['ScreenName'] = this.ScreenName;
    data['SeqNo'] = this.SeqNo;
    return data;
  }
  MenuItem.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.Menu = json['Menu'];
    this.ImagePath = json['ImagePath'];
    this.ScreenName = json['ScreenName'];
    this.SeqNo = json['SeqNo'];
  }

}