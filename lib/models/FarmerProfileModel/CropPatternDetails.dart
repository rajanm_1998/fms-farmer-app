class CropPatternDetailsModel {
  String status;
  Data data;

  CropPatternDetailsModel({this.status, this.data});

  CropPatternDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != String ? new Data.fromJson(json['data']) : String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != String) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  String message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != String) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != String) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int id;
  int farmerId;
  int year;
  int seasonId;
  String seasonName;
  int cropId;
  String cropName;
  String area;
  String variety;
  String yield;
  String totalYield;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  String sowingDate;
  String harvestingDate;
  int landId;
  String landName;
  String inputCost;
  String marketRate;
  String marketPlace;
  String marketDistance;

  Response(
      {this.id,
      this.farmerId,
      this.year,
      this.seasonId,
      this.seasonName,
      this.cropId,
      this.cropName,
      this.area,
      this.variety,
      this.yield,
      this.totalYield,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.sowingDate,
      this.harvestingDate,
      this.landId,
      this.landName,
      this.inputCost,
      this.marketRate,
      this.marketPlace,
      this.marketDistance});

  Response.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    farmerId = json['FarmerId'];
    year = json['Year'];
    seasonId = json['SeasonId'];
    seasonName = json['SeasonName'] ?? '';
    cropId = json['CropId'];
    cropName = json['CropName'] ?? '';
    area = json['Area'] ?? '';
    variety = json['Variety'] ?? '';
    yield = json['Yield'] ?? '';
    totalYield = json['TotalYield'] ?? '';
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'] ?? '';
    sowingDate = json['SowingDate'] ?? '';
    harvestingDate = json['HarvestingDate'] ?? '';
    landId = json['LandId'];
    landName = json['LandName'] ?? '';
    inputCost = json['InputCost'] ?? '';
    marketRate = json['MarketRate'] ?? '';
    marketPlace = json['MarketPlace'] ?? '';
    marketDistance = json['MarketDistance'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['Year'] = this.year;
    data['SeasonId'] = this.seasonId;
    data['SeasonName'] = this.seasonName;
    data['CropId'] = this.cropId;
    data['CropName'] = this.cropName;
    data['Area'] = this.area;
    data['Variety'] = this.variety;
    data['Yield'] = this.yield;
    data['TotalYield'] = this.totalYield;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['SowingDate'] = this.sowingDate;
    data['HarvestingDate'] = this.harvestingDate;
    data['LandId'] = this.landId;
    data['LandName'] = this.landName;
    data['InputCost'] = this.inputCost;
    data['MarketRate'] = this.marketRate;
    data['MarketPlace'] = this.marketPlace;
    data['MarketDistance'] = this.marketDistance;
    return data;
  }
}
