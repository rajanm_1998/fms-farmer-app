class LiveStockDetailsModel {
  String status;
  Data data;

  LiveStockDetailsModel({this.status, this.data});

  LiveStockDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int id;
  int farmerId;
  int livestockId;
  String livestockName;
  int number;
  String use;
  String production;
  String rate;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  int unitId;
  String unitName;
  String productionUnit;
  String rateUnit;

  Response(
      {this.id,
      this.farmerId,
      this.livestockId,
      this.livestockName,
      this.number,
      this.use,
      this.production,
      this.rate,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.unitId,
      this.unitName,
      this.productionUnit,
      this.rateUnit});

  Response.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    farmerId = json['FarmerId'];
    livestockId = json['LivestockId'];
    livestockName = json['LivestockName'];
    number = json['Number'];
    use = json['Use'];
    production = json['Production'];
    rate = json['Rate'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
    unitId = json['UnitId'];
    unitName = json['UnitName'];
    productionUnit = json['ProductionUnit'];
    rateUnit = json['RateUnit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['LivestockId'] = this.livestockId;
    data['LivestockName'] = this.livestockName;
    data['Number'] = this.number;
    data['Use'] = this.use;
    data['Production'] = this.production;
    data['Rate'] = this.rate;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['UnitId'] = this.unitId;
    data['UnitName'] = this.unitName;
    data['ProductionUnit'] = this.productionUnit;
    data['RateUnit'] = this.rateUnit;
    return data;
  }
}
