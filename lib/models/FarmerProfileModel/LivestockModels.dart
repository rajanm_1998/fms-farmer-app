class LivestockModels {
  String status;
  Data data;

  LivestockModels({this.status, this.data});

  LivestockModels.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int livestockId;
  String livestockName;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  String productionUnit;
  String rateUnit;

  Response(
      {this.livestockId,
      this.livestockName,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.productionUnit,
      this.rateUnit});

  Response.fromJson(Map<String, dynamic> json) {
    livestockId = json['LivestockId'];
    livestockName = json['LivestockName'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
    productionUnit = json['ProductionUnit'];
    rateUnit = json['RateUnit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LivestockId'] = this.livestockId;
    data['LivestockName'] = this.livestockName;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['ProductionUnit'] = this.productionUnit;
    data['RateUnit'] = this.rateUnit;
    return data;
  }
}
