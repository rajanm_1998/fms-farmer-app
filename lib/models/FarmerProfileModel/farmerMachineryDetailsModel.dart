class farmerMachineryDetailsModel {
  String status;
  Data data;

  farmerMachineryDetailsModel({this.status, this.data});

  farmerMachineryDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int id;
  int farmerId;
  int farmMachineryId;
  String farmerMachineryName;
  int quantity;
  String description;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;

  Response(
      {this.id,
      this.farmerId,
      this.farmMachineryId,
      this.farmerMachineryName,
      this.quantity,
      this.description,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active});

  Response.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    farmerId = json['FarmerId'];
    farmMachineryId = json['FarmMachineryId'];
    farmerMachineryName = json['FarmerMachineryName'];
    quantity = json['Quantity'];
    description = json['Description'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['FarmMachineryId'] = this.farmMachineryId;
    data['FarmerMachineryName'] = this.farmerMachineryName;
    data['Quantity'] = this.quantity;
    data['Description'] = this.description;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    return data;
  }
}
