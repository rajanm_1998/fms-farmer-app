class FamilyDetailModel {
  String status;
  Data data;

  FamilyDetailModel({this.status, this.data});

  FamilyDetailModel.fromJson(Map<dynamic, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<FamilyDetails> response;
  String status;
  String message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<dynamic, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<FamilyDetails>();
      json['Response'].forEach((v) {
        response.add(new FamilyDetails.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class FamilyDetails {
  int id;
  int farmerId;
  String memberName;
  String relation;
  String gender;
  int age;
  String education;
  String occupation;
  String sHGFC;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;

  FamilyDetails(
      {this.id,
      this.farmerId,
      this.memberName,
      this.relation,
      this.gender,
      this.age,
      this.education,
      this.occupation,
      this.sHGFC,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active});

  FamilyDetails.fromJson(Map<dynamic, dynamic> json) {
    id = json['Id'];
    farmerId = json['FarmerId'];
    memberName = json['MemberName'];
    relation = json['Relation'];
    gender = json['Gender'];
    age = json['Age'];
    education = json['Education'];
    occupation = json['Occupation'];
    sHGFC = json['SHGFC'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<dynamic, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['MemberName'] = this.memberName;
    data['Relation'] = this.relation;
    data['Gender'] = this.gender;
    data['Age'] = this.age;
    data['Education'] = this.education;
    data['Occupation'] = this.occupation;
    data['SHGFC'] = this.sHGFC;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    return data;
  }
}
