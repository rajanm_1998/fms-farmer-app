// import 'dart:collection';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

// class KMLMAP extends StatefulWidget {
//   KMLMAP({Key key}) : super(key: key);

//   @override
//   _GMapState createState() => _GMapState();
// }

// class _GMapState extends State<KMLMAP> {
//   Set<Marker> _markers = HashSet<Marker>();
//   Set<Polygon> _polygons = HashSet<Polygon>();
//   List<LatLng> polygonLatLongs = List<LatLng>();
//   int _polygonIdCounter = 1;

//   static final CameraPosition _cameraPosition = CameraPosition(
//       bearing: 192.8334901395799,
//       target: LatLng(76.18521944699553, 24.32527449600105),
//       tilt: 59.440717697143555,
//       zoom: 19.151926040649414);

//   bool _showMapStyle = false;
//   bool _isPolygon = true;

//   GoogleMapController _mapController;
//   BitmapDescriptor _markerIcon;

//   @override
//   void initState() {
//     super.initState();
//     _setMarkerIcon();
//     _setPolygons();
//   }

//   void _setMarkerIcon() async {
//     _markerIcon = await BitmapDescriptor.fromAssetImage(
//         ImageConfiguration(), 'assets/noodle_icon.png');
//   }

//   void _toggleMapStyle() async {
//     //String style = await DefaultAssetBundle.of(context).loadString('assets/map_style.json');

//     {
//       _mapController.setMapStyle(null);
//     }
//   }

//   void _setPolygons() {
//     polygonLatLongs.add(LatLng(76.18521944699553, 24.32527449600105));
//     polygonLatLongs.add(LatLng(76.18522485724836, 24.3253397605078));
//     polygonLatLongs.add(LatLng(76.18522636239433, 24.32562108749878));
//     polygonLatLongs.add(LatLng(76.18522254368726, 24.32569187030174));
//     polygonLatLongs.add(LatLng(76.18520554743729, 24.32583987968718));
//     polygonLatLongs.add(LatLng(76.18520409295442, 24.32592960596005));
//     final String polygonIdVal = 'polygon_id_$_polygonIdCounter';
//     _polygons.add(
//       Polygon(
//         polygonId: PolygonId(polygonIdVal),
//         points: polygonLatLongs,
//         fillColor: Colors.yellow.withOpacity(0.15),
//         strokeWidth: 2,
//         strokeColor: Colors.yellow,
//       ),
//     );
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     _mapController = controller;

//     setState(() {
//       _markers.add(
//         Marker(
//             markerId: MarkerId("0"),
//             position: LatLng(76.18521944699553, 24.32527449600105),
//             infoWindow: InfoWindow(
//               title: "Borivali",
//               snippet: "National Park",
//             ),
//             icon: _markerIcon),
//       );
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         children: <Widget>[
//           GoogleMap(
//             onMapCreated: _onMapCreated,
//             initialCameraPosition: CameraPosition(
//               target: LatLng(76.18521944699553, 24.32527449600105),
//               zoom: 12,
//             ),
//             mapType: MapType.hybrid,
//             markers: _markers,
//             polygons: _polygons,
//             myLocationEnabled: true,
//             onTap: (point){
//               if(_isPolygon){
//                 setState(() {
//                   polygonLatLongs.add(point);
//                   _setPolygons();
//                 });
//               }
//             },
//           ),
//         ],
//       ),
//       floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//       floatingActionButton: FloatingActionButton(
//         tooltip: 'Increment',
//         child: Icon(Icons.map),
//         onPressed: () {
//           setState(() {
//             _showMapStyle = !_showMapStyle;
//           });

//           _toggleMapStyle();
//         },
//       ),
//     );
//   }
// }

import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class KMLAPP extends StatefulWidget {
  @override
  _KMLAPPState createState() => _KMLAPPState();
}

class _KMLAPPState extends State<KMLAPP> {
  
  @override
  void initState() {
    _setPolygon();
    super.initState();
  }
  GoogleMapController myController;
  // String chaknoValue = "0";
  // String khasaranoValue  = "0";
 
  String alphabateValue = "A";
   
  void _onMapCreated(GoogleMapController controller) {
    myController = controller;
  }

  // Maps

  Set<Polygon> _polygons = HashSet<Polygon>();
    List<LatLng> polygonLatLngs = List<LatLng>();

  double radius;

  //ids
  int _polygonIdCounter = 1;
  
  // Type controllers
  bool _isPolygon = true; //Default

  // Draw Polygon to the map
  void _setPolygon() async{
    String CoordinateStr = "76.18528848631438,24.32666385747874,76.18521457338524,24.32667988226268,76.18500270040481,24.32672423242942,76.18479044348985,24.3267586983303,76.1846335186095,24.32678714974971,76.18461247087339,24.32678936170064,76.1844855860223,24.3268018667299,76.18446491252419,24.32680503783424,76.18440599070104,24.32661637620478,76.18439835152695,24.32659886108882,76.18447888121418,24.32656107616781,76.18457074172711,24.32650291542018,76.18458938320479,24.3265131799622,76.18461863953411,24.32652928934341,76.18465130198901,24.32653912875821,76.18482331015947,24.32657046026661,76.18499021380734,24.32657421376393,76.18513777270297,24.32657130837811,76.18516819720065,24.32656518070965,76.18524314605588,24.3265475398219,76.18524984214166,24.32658034244534,76.18526160885179,24.32661226897847,76.18528848631438,24.32666385747874";
   List<String> list = CoordinateStr.split(',').toList();
   print(CoordinateStr.split(','));
   print(list);
   print(list.length);
    int i=0;
   while( i<list.length){
     setState(() {
    polygonLatLngs.add(LatLng(double.parse(list[i+1]), double.parse(list[i])));   
     });
    
    //print(i.toString());
    i+=2;
   }
    await setState(() {      
      final String polygonIdVal = 'polygon_id_$_polygonIdCounter';
      _polygons.add(Polygon(
        polygonId: PolygonId(polygonIdVal),
        points: polygonLatLngs,
        strokeWidth: 1,
        strokeColor: Colors.yellow,
        fillColor: Colors.yellow.withOpacity(0.15),
      ));
    
    });
  }

  // Widget _fabPolygon() {
  //   return FloatingActionButton.extended(
  //     onPressed: () {
  //       setState(() {
  //         polygonLatLngs.removeLast();
  //       });
  //     },
  //     icon: Icon(Icons.undo),
  //     label: Text('Undo point'),
  //     backgroundColor: Colors.orange,
  //   );
  // }
static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(24.32527449600105, 76.18521944699553),
    zoom: 14.4746,
  );
  String dropdownValue = "One";
  
  
  @override
  Widget build(BuildContext context) {
    String KhasraDropdownvalue;
    

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
        title:Text("Farmer Map"),
        backgroundColor: Colors.green,
        ),
        // floatingActionButton:
        //     polygonLatLngs.length > 0 && _isPolygon ? _fabPolygon() : null,
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                
                children: [
                      // DropdownButtonFormField(
                      //   // value: CasteDropDownvalue,
                      //   decoration: InputDecoration(
                      //     labelText: 'CAST',
                      //     border: OutlineInputBorder(),
                      //   ),
                      //   items: [].map((value) {
                      //     return new DropdownMenuItem(
                      //       child: new Text(
                      //         value.casteName,
                      //         style: TextStyle(fontSize: 13.0),
                      //       ),
                      //       value: value.casteId.toString(),
                      //     );
                      //   }).toList(),
                      //   onChanged: (dropvalue) {
                      //     setState(() {
                      //       // value.casteId = int.parse(dropvalue);
                      //     });
                      //   },
                      // ),
                  DropdownButton<String>(
                    value: dropdownValue,
                    icon: const Icon(Icons.arrow_downward),
                    elevation: 16,
                    style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: <String>['One', 'Two', 'Free', 'Four']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                  DropdownButton<String>(
                    value: alphabateValue,
                    icon: const Icon(Icons.arrow_downward),
                    elevation: 16,
                    style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (String newValue) {
                      setState(() {
                        alphabateValue = newValue;
                      });
                    },
                    items: <String>['A', 'B', 'C', 'C']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                  DropdownButton<String>(
                    value: alphabateValue,
                    icon: const Icon(Icons.arrow_downward),
                    elevation: 16,
                    style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (String newValue) {
                      setState(() {
                        alphabateValue = newValue;
                      });
                    },
                    items: <String>['A', 'B', 'C', 'C']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ],
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.75,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  border: Border.all(
                    style: BorderStyle.solid,
                  ),
                ),
                child: GoogleMap(
                  mapType: MapType.hybrid,
                initialCameraPosition: _kGooglePlex,
                    polygons: _polygons,
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
