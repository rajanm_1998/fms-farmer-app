class LandHoldingModel {
  String status;
  Data data;

  LandHoldingModel({this.status, this.data});

  LandHoldingModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int landId;
  int fileId;
  int fileSrNo;
  int farmerId;
  String roRSurveyNo;
  String khasaraNo;
  double totalArea;
  String lowland;
  String upland;
  String orchard;
  String chakno;
  String subChakno;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  String address;
  int villageId;
  String villageName;
  int blockId;
  String blockName;
  int districtId;
  String districtName;
  int stateId;
  String stateName;
  int projectId;
  String projectName;
  String scheme;
  String distributary;
  String wua;
  String terrconst;
  String minor;
  String subminor;
  String outlet;
  int soiledTypeId;
  String soilType;

  Response(
      {this.landId,
      this.fileId,
      this.fileSrNo,
      this.farmerId,
      this.roRSurveyNo,
      this.khasaraNo,
      this.totalArea,
      this.lowland,
      this.upland,
      this.orchard,
      this.chakno,
      this.subChakno,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.address,
      this.villageId,
      this.villageName,
      this.blockId,
      this.blockName,
      this.districtId,
      this.districtName,
      this.stateId,
      this.stateName,
      this.projectId,
      this.projectName,
      this.scheme,
      this.distributary,
      this.wua,
      this.terrconst,
      this.minor,
      this.subminor,
      this.outlet,
      this.soiledTypeId,
      this.soilType});

  Response.fromJson(Map<String, dynamic> json) {
    landId = json['LandId'];
    fileId = json['FileId'];
    fileSrNo = json['FileSrNo'];
    farmerId = json['FarmerId'];
    roRSurveyNo = json['RoRSurveyNo'];
    khasaraNo = json['SurveyNo'];
    totalArea = json['TotalArea'];
    lowland = json['Lowland'];
    upland = json['Upland'];
    orchard = json['Orchard'];
    chakno = json['Chakno'];
    subChakno = json['SubChakno'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
    address = json['Address'];
    villageId = json['VillageId'];
    villageName = json['VillageName'];
    blockId = json['BlockId'];
    blockName = json['BlockName'];
    districtId = json['DistrictId'];
    districtName = json['DistrictName'];
    stateId = json['StateId'];
    stateName = json['StateName'];
    projectId = json['ProjectId'];
    projectName = json['ProjectName'];
    scheme = json['scheme'];
    distributary = json['distributary'];
    wua = json['wua'];
    terrconst = json['terrconst'];
    minor = json['minor'];
    subminor = json['subminor'];
    outlet = json['outlet'];
    soiledTypeId = json['SoiledTypeId'];
    soilType = json['SoilType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LandId'] = this.landId;
    data['FileId'] = this.fileId;
    data['FileSrNo'] = this.fileSrNo;
    data['FarmerId'] = this.farmerId;
    data['RoRSurveyNo'] = this.roRSurveyNo;
    data['SurveyNo'] = this.khasaraNo;
    data['TotalArea'] = this.totalArea;
    data['Lowland'] = this.lowland;
    data['Upland'] = this.upland;
    data['Orchard'] = this.orchard;
    data['Chakno'] = this.chakno;
    data['SubChakno'] = this.subChakno;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['Address'] = this.address;
    data['VillageId'] = this.villageId;
    data['VillageName'] = this.villageName;
    data['BlockId'] = this.blockId;
    data['BlockName'] = this.blockName;
    data['DistrictId'] = this.districtId;
    data['DistrictName'] = this.districtName;
    data['StateId'] = this.stateId;
    data['StateName'] = this.stateName;
    data['ProjectId'] = this.projectId;
    data['ProjectName'] = this.projectName;
    data['scheme'] = this.scheme;
    data['distributary'] = this.distributary;
    data['wua'] = this.wua;
    data['terrconst'] = this.terrconst;
    data['minor'] = this.minor;
    data['subminor'] = this.subminor;
    data['outlet'] = this.outlet;
    data['SoiledTypeId'] = this.soiledTypeId;
    data['SoilType'] = this.soilType;
    return data;
  }
}
