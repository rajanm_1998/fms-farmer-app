class IrrigationTypeModels {
  String status;
  Data data;

  IrrigationTypeModels({this.status, this.data});

  IrrigationTypeModels.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int irrigationTypeId;
  String irrigationTypeName;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;

  Response(
      {this.irrigationTypeId,
      this.irrigationTypeName,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active});

  Response.fromJson(Map<String, dynamic> json) {
    irrigationTypeId = json['IrrigationTypeId'];
    irrigationTypeName = json['IrrigationTypeName'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IrrigationTypeId'] = this.irrigationTypeId;
    data['IrrigationTypeName'] = this.irrigationTypeName;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    return data;
  }
}
