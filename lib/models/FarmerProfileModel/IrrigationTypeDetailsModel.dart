class IrrigationTypeDetailsModel {
  String status;
  Data data;

  IrrigationTypeDetailsModel({this.status, this.data});

  IrrigationTypeDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int id;
  int farmerId;
  int irrigationTypeId;
  String irrigationTypeName;
  String kharif;
  String rabi;
  String summer;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  int year;
  int landId;
  String khasaraNo;

  Response(
      {this.id,
      this.farmerId,
      this.irrigationTypeId,
      this.irrigationTypeName,
      this.kharif,
      this.rabi,
      this.summer,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.year,
      this.landId,
      this.khasaraNo});

  Response.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    farmerId = json['FarmerId'];
    irrigationTypeId = json['IrrigationTypeId'];
    irrigationTypeName = json['IrrigationTypeName'];
    kharif = json['Kharif'];
    rabi = json['Rabi'];
    summer = json['Summer'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
    year = json['Year'];
    landId = json['LandId'];
    khasaraNo = json['SurveyNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['IrrigationTypeId'] = this.irrigationTypeId;
    data['IrrigationTypeName'] = this.irrigationTypeName;
    data['Kharif'] = this.kharif;
    data['Rabi'] = this.rabi;
    data['Summer'] = this.summer;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['Year'] = this.year;
    data['LandId'] = this.landId;
    data['SurveyNo'] = this.khasaraNo;
    return data;
  }
}
