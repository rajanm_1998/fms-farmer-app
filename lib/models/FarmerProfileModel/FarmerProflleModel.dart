class FarmerProfileDetailModel {
  String status;
  FarmerData data;

  FarmerProfileDetailModel({this.status, this.data});

  FarmerProfileDetailModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data =
        json['data'] != String ? new FarmerData.fromJson(json['data']) : String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != String) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class FarmerData {
  FarmerResponse response;
  String status;
  String message;

  FarmerData({this.response, this.status, this.message});

  FarmerData.fromJson(Map<String, dynamic> json) {
    response = json['Response'] != String
        ? new FarmerResponse.fromJson(json['Response'])
        : String;
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != String) {
      data['Response'] = this.response.toJson();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class FarmerResponse {
  int farmerId;
  String farmerName;
  String contactNo;
  String address;
  String registrationNo;
  int casteId;
  int religionId;
  String povertyLevel;
  String bankName;
  String bankBranch;
  String iFSCCode;
  String accountNo;
  int loanTaken;
  String payeeType;
  String pancardNo;
  String aadharCardNo;
  String network;
  String phoneType;
  String farmerType;
  int cropChangePattern;
  String cropPhisibilityType;
  int tryAnyCrop;
  int interestForCropChange;
  String cropType;
  String seedCollectionFrom;
  int meritOfSeedChange;
  int loanTakenForAgriculture;
  String loanTakenFromWhere;
  int tieUpWithCompany;
  String forWhichCrop;
  int treatmentOfSeeds;
  int soilTesting;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  int projectId;
  String projectName;
  String fOwner;
  String fContact;
  int role;
  String roleName;
  int ftype;
  String farmerTypeName;
  String password;
  String oTPCode;
  String oTPCodeTime;
  String email;
  int villageId;
  String villageName;
  String casteName;
  String religionName;
  String isRented;
  int statusId;
  String status;
  int submittedBy;
  String submittedByName;
  String submittedOn;
  int approvedBy;
  String approvedByName;
  String approvedOn;
  String remark;

  FarmerResponse(
      {this.farmerId,
      this.farmerName,
      this.contactNo,
      this.address,
      this.registrationNo,
      this.casteId,
      this.religionId,
      this.povertyLevel,
      this.bankName,
      this.bankBranch,
      this.iFSCCode,
      this.accountNo,
      this.loanTaken,
      this.payeeType,
      this.pancardNo,
      this.aadharCardNo,
      this.network,
      this.phoneType,
      this.farmerType,
      this.cropChangePattern,
      this.cropPhisibilityType,
      this.tryAnyCrop,
      this.interestForCropChange,
      this.cropType,
      this.seedCollectionFrom,
      this.meritOfSeedChange,
      this.loanTakenForAgriculture,
      this.loanTakenFromWhere,
      this.tieUpWithCompany,
      this.forWhichCrop,
      this.treatmentOfSeeds,
      this.soilTesting,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.projectId,
      this.projectName,
      this.fOwner,
      this.fContact,
      this.role,
      this.roleName,
      this.ftype,
      this.farmerTypeName,
      this.password,
      this.oTPCode,
      this.oTPCodeTime,
      this.email,
      this.villageId,
      this.villageName,
      this.casteName,
      this.religionName,
      this.isRented,
      this.statusId,
      this.status,
      this.submittedBy,
      this.submittedByName,
      this.submittedOn,
      this.approvedBy,
      this.approvedByName,
      this.approvedOn,
      this.remark});

  FarmerResponse.fromJson(Map<String, dynamic> json) {
    farmerId = json['FarmerId'];
    farmerName = json['FarmerName'] ?? '';
    contactNo = json['ContactNo'] ?? '';
    address = json['Address'] ?? '';
    registrationNo = json['RegistrationNo'] ?? '';
    casteId = json['CasteId'];
    religionId = json['ReligionId'];
    povertyLevel = json['PovertyLevel'] ?? '';
    bankName = json['BankName'] ?? '';
    bankBranch = json['BankBranch'] ?? '';
    iFSCCode = json['IFSCCode'] ?? '';
    accountNo = json['AccountNo'] ?? '';
    loanTaken = json['LoanTaken'];
    payeeType = json['PayeeType'] ?? '';
    pancardNo = json['PancardNo'] ?? '';
    aadharCardNo = json['AadharCardNo'] ?? '';
    network = json['Network'] ?? '';
    phoneType = json['PhoneType'] ?? '';
    farmerType = json['FarmerType'] ?? '';
    cropChangePattern = json['CropChangePattern'];
    cropPhisibilityType = json['CropPhisibilityType'];
    tryAnyCrop = json['TryAnyCrop'];
    interestForCropChange = json['InterestForCropChange'];
    cropType = json['CropType'];
    seedCollectionFrom = json['SeedCollectionFrom'];
    meritOfSeedChange = json['MeritOfSeedChange'];
    loanTakenForAgriculture = json['LoanTakenForAgriculture'];
    loanTakenFromWhere = json['LoanTakenFromWhere'];
    tieUpWithCompany = json['TieUpWithCompany'];
    forWhichCrop = json['ForWhichCrop'];
    treatmentOfSeeds = json['TreatmentOfSeeds'];
    soilTesting = json['SoilTesting'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
    projectId = json['ProjectId'];
    projectName = json['ProjectName'] ?? '';
    fOwner = json['FOwner'] ?? '';
    fContact = json['FContact'] ?? '';
    role = json['Role'];
    roleName = json['RoleName'] ?? '';
    ftype = json['Ftype'];
    farmerTypeName = json['FarmerTypeName'] ?? '';
    password = json['Password'] ?? '';
    oTPCode = json['OTPCode'] ?? '';
    oTPCodeTime = json['OTPCodeTime'];
    email = json['Email'];
    villageId = json['VillageId'];
    villageName = json['VillageName'] ?? '';
    casteName = json['CasteName'] ?? '';
    religionName = json['ReligionName'] ?? '';
    isRented = json['IsRented'] ?? '';
    statusId = json['StatusId'];
    status = json['Status'] ?? '';
    submittedBy = json['SubmittedBy'];
    submittedByName = json['SubmittedByName'] ?? '';
    submittedOn = json['SubmittedOn'];
    approvedBy = json['ApprovedBy'];
    approvedByName = json['ApprovedByName'] ?? '';
    approvedOn = json['ApprovedOn'];
    remark = json['Remark'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerId'] = this.farmerId;
    data['FarmerName'] = this.farmerName;
    data['ContactNo'] = this.contactNo;
    data['Address'] = this.address;
    data['RegistrationNo'] = this.registrationNo;
    data['CasteId'] = this.casteId;
    data['ReligionId'] = this.religionId;
    data['PovertyLevel'] = this.povertyLevel;
    data['BankName'] = this.bankName;
    data['BankBranch'] = this.bankBranch;
    data['IFSCCode'] = this.iFSCCode;
    data['AccountNo'] = this.accountNo;
    data['LoanTaken'] = this.loanTaken;
    data['PayeeType'] = this.payeeType;
    data['PancardNo'] = this.pancardNo;
    data['AadharCardNo'] = this.aadharCardNo;
    data['Network'] = this.network;
    data['PhoneType'] = this.phoneType;
    data['FarmerType'] = this.farmerType;
    data['CropChangePattern'] = this.cropChangePattern;
    data['CropPhisibilityType'] = this.cropPhisibilityType;
    data['TryAnyCrop'] = this.tryAnyCrop;
    data['InterestForCropChange'] = this.interestForCropChange;
    data['CropType'] = this.cropType;
    data['SeedCollectionFrom'] = this.seedCollectionFrom;
    data['MeritOfSeedChange'] = this.meritOfSeedChange;
    data['LoanTakenForAgriculture'] = this.loanTakenForAgriculture;
    data['LoanTakenFromWhere'] = this.loanTakenFromWhere;
    data['TieUpWithCompany'] = this.tieUpWithCompany;
    data['ForWhichCrop'] = this.forWhichCrop;
    data['TreatmentOfSeeds'] = this.treatmentOfSeeds;
    data['SoilTesting'] = this.soilTesting;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['ProjectId'] = this.projectId;
    data['ProjectName'] = this.projectName;
    data['FOwner'] = this.fOwner;
    data['FContact'] = this.fContact;
    data['Role'] = this.role;
    data['RoleName'] = this.roleName;
    data['Ftype'] = this.ftype;
    data['FarmerTypeName'] = this.farmerTypeName;
    data['Password'] = this.password;
    data['OTPCode'] = this.oTPCode;
    data['OTPCodeTime'] = this.oTPCodeTime;
    data['Email'] = this.email;
    data['VillageId'] = this.villageId;
    data['VillageName'] = this.villageName;
    data['CasteName'] = this.casteName;
    data['ReligionName'] = this.religionName;
    data['IsRented'] = this.isRented;
    data['StatusId'] = this.statusId;
    data['Status'] = this.status;
    data['SubmittedBy'] = this.submittedBy;
    data['SubmittedByName'] = this.submittedByName;
    data['SubmittedOn'] = this.submittedOn;
    data['ApprovedBy'] = this.approvedBy;
    data['ApprovedByName'] = this.approvedByName;
    data['ApprovedOn'] = this.approvedOn;
    data['Remark'] = this.remark;
    return data;
  }
}
