class IrrigationFacilityDetailsModel {
  String status;
  Data data;

  IrrigationFacilityDetailsModel({this.status, this.data});

  IrrigationFacilityDetailsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Response> response;
  String status;
  Null message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Response>();
      json['Response'].forEach((v) {
        response.add(new Response.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Response {
  int id;
  int farmerId;
  int irrigationFacilityId;
  String irrigationFacilityName;
  String kharif;
  String rabi;
  String summer;
  String rate;
  int createdBy;
  String createdOn;
  int modifyBy;
  String modifyOn;
  String active;
  int year;
  int landId;
  String khasaraNo;
  String pumpCapacity;

  Response(
      {this.id,
      this.farmerId,
      this.irrigationFacilityId,
      this.irrigationFacilityName,
      this.kharif,
      this.rabi,
      this.summer,
      this.rate,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.modifyOn,
      this.active,
      this.year,
      this.landId,
      this.khasaraNo,
      this.pumpCapacity});

  Response.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    farmerId = json['FarmerId'];
    irrigationFacilityId = json['IrrigationFacilityId'];
    irrigationFacilityName = json['IrrigationFacilityName'];
    kharif = json['Kharif'];
    rabi = json['Rabi'];
    summer = json['Summer'];
    rate = json['Rate'];
    createdBy = json['CreatedBy'];
    createdOn = json['CreatedOn'];
    modifyBy = json['ModifyBy'];
    modifyOn = json['ModifyOn'];
    active = json['Active'];
    year = json['Year'];
    landId = json['LandId'];
    khasaraNo = json['SurveyNo'];
    pumpCapacity = json['PumpCapacity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['FarmerId'] = this.farmerId;
    data['IrrigationFacilityId'] = this.irrigationFacilityId;
    data['IrrigationFacilityName'] = this.irrigationFacilityName;
    data['Kharif'] = this.kharif;
    data['Rabi'] = this.rabi;
    data['Summer'] = this.summer;
    data['Rate'] = this.rate;
    data['CreatedBy'] = this.createdBy;
    data['CreatedOn'] = this.createdOn;
    data['ModifyBy'] = this.modifyBy;
    data['ModifyOn'] = this.modifyOn;
    data['Active'] = this.active;
    data['Year'] = this.year;
    data['LandId'] = this.landId;
    data['SurveyNo'] = this.khasaraNo;
    data['PumpCapacity'] = this.pumpCapacity;
    return data;
  }
}
