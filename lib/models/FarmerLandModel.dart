

class FarmerLandModel {
  int landId;
  int farmerId;
  String roRSurveyNo;
  double roRTotalArea;
  String khasaraNo;
  double totalArea;
  String address;
  int villageId;
  String villageName;
  int blockId;
  String blockName;
  int districtId;
  String districtName;
  int stateId;
  String stateName;
  int projectId;
  String projectName;
  String chakno;
  String subChakno;
  String outlet;
  int soiledTypeId;
  String soilType;

  FarmerLandModel(
      {this.landId,
      this.farmerId,
      this.roRSurveyNo,
      this.roRTotalArea,
      this.khasaraNo,
      this.totalArea,
      this.address,
      this.villageId,
      this.villageName,
      this.blockId,
      this.blockName,
      this.districtId,
      this.districtName,
      this.stateId,
      this.stateName,
      this.projectId,
      this.projectName,
      this.chakno,
      this.subChakno,
      this.outlet,
      this.soiledTypeId,
      this.soilType});

  FarmerLandModel.fromJson(Map<String, dynamic> json) {
    landId = json['LandId'] ?? 0;
    farmerId = json['FarmerId'] ?? 0;
    roRSurveyNo = json['RoRSurveyNo'] ?? '';
    roRTotalArea = json['RoRTotalArea'] ?? 0.0;
    khasaraNo = json['SurveyNo'] ?? '';
    totalArea = json['TotalArea'] ?? 0.0;
    address = json['Address'] ?? '';
    villageId = json['VillageId'] ?? 0;
    villageName = json['VillageName'] ?? '';
    blockId = json['BlockId'] ?? 0;
    blockName = json['BlockName'] ?? '';
    districtId = json['DistrictId'] ?? 0;
    districtName = json['DistrictName'] ?? '';
    stateId = json['StateId'] ?? 0;
    stateName = json['StateName'] ?? '';
    projectId = json['ProjectId'] ?? 0;
    projectName = json['ProjectName'] ?? '';
    chakno = json['Chakno'] ?? '';
    subChakno = json['SubChakno'] ?? '';
    outlet = json['outlet'] ?? '';
    soiledTypeId = json['SoiledTypeId'] ?? 0;
    soilType = json['SoilType'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['LandId'] = this.landId;
    data['FarmerId'] = this.farmerId;
    data['RoRSurveyNo'] = this.roRSurveyNo;
    data['RoRTotalArea'] = this.roRTotalArea;
    data['SurveyNo'] = this.khasaraNo;
    data['TotalArea'] = this.totalArea;
    data['Address'] = this.address;
    data['VillageId'] = this.villageId;
    data['VillageName'] = this.villageName;
    data['BlockId'] = this.blockId;
    data['BlockName'] = this.blockName;
    data['DistrictId'] = this.districtId;
    data['DistrictName'] = this.districtName;
    data['StateId'] = this.stateId;
    data['StateName'] = this.stateName;
    data['ProjectId'] = this.projectId;
    data['ProjectName'] = this.projectName;
    data['Chakno'] = this.chakno;
    data['SubChakno'] = this.subChakno;
    data['outlet'] = this.outlet;
    data['SoiledTypeId'] = this.soiledTypeId;
    data['SoilType'] = this.soilType;
    return data;
  }
}
