class FarmerDetailsModel {
  FarmerDetails response;
  String status;
  String message;

  FarmerDetailsModel({this.response, this.status, this.message});

  FarmerDetailsModel.fromJson(Map<String, dynamic> json) {
    response = json['Response'] != null
        ? new FarmerDetails.fromJson(json['Response'])
        : null;
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.toJson();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class FarmerDetails {
  int farmerId;
  String farmerName;
  String contactNo;
  String address;
  String registrationNo;
  int casteId;
  int religionId;
  String povertyLevel;
  String network;
  String phoneType;
  String farmerType;
  int projectId;
  String projectName;
  String fOwner;
  String fContact;
  int role;
  String roleName;
  int ftype;
  String farmerTypeName;
  int villageId;
  String villageName;

  FarmerDetails(
      {this.farmerId,
      this.farmerName,
      this.contactNo,
      this.address,
      this.registrationNo,
      this.casteId,
      this.religionId,
      this.povertyLevel,
      this.network,
      this.phoneType,
      this.farmerType,
      this.projectId,
      this.projectName,
      this.fOwner,
      this.fContact,
      this.role,
      this.roleName,
      this.ftype,
      this.farmerTypeName,
      this.villageId,
      this.villageName});

  FarmerDetails.fromJson(Map<String, dynamic> json) {
    farmerId = json['FarmerId'] ?? 0;
    farmerName = json['FarmerName'] ?? '';
    contactNo = json['ContactNo'] ?? '';
    address = json['Address'] ?? '';
    registrationNo = json['RegistrationNo'] ?? '';
    casteId = json['CasteId'] ?? 0;
    religionId = json['ReligionId'] ?? 0;
    povertyLevel = json['PovertyLevel'] ?? '';
    network = json['Network'] ?? '';
    phoneType = json['PhoneType'] ?? '';
    farmerType = json['FarmerType'] ?? '';
    projectId = json['ProjectId'] ?? 0;
    projectName = json['ProjectName'] ?? '';
    fOwner = json['FOwner'] ?? '';
    fContact = json['FContact'] ?? '';
    role = json['Role'] ?? 0;
    roleName = json['RoleName'] ?? '';
    ftype = json['Ftype'] ?? 0;
    farmerTypeName = json['FarmerTypeName'] ?? '';
    villageId = json['VillageId'] ?? 0;
    villageName = json['VillageName'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerId'] = this.farmerId;
    data['FarmerName'] = this.farmerName;
    data['ContactNo'] = this.contactNo;
    data['Address'] = this.address;
    data['RegistrationNo'] = this.registrationNo;
    data['CasteId'] = this.casteId;
    data['ReligionId'] = this.religionId;
    data['PovertyLevel'] = this.povertyLevel;
    data['Network'] = this.network;
    data['PhoneType'] = this.phoneType;
    data['FarmerType'] = this.farmerType;
    data['ProjectId'] = this.projectId;
    data['ProjectName'] = this.projectName;
    data['FOwner'] = this.fOwner;
    data['FContact'] = this.fContact;
    data['Role'] = this.role;
    data['RoleName'] = this.roleName;
    data['Ftype'] = this.ftype;
    data['FarmerTypeName'] = this.farmerTypeName;
    data['VillageId'] = this.villageId;
    data['VillageName'] = this.villageName;
    return data;
  }
}
