

class CropAdvisoryModel {
  int id;
  int stateId;
  String stateName;
  int cropId;
  String cropName;
  int seasonId;
  String seasonName;
  String link;
  String recommendation;
  String filename;
  String path;
  int createdBy;
  String createdOn;
  int modifyBy;
  String active;


  CropAdvisoryModel(
      {this.id,
      this.stateId,
      this.stateName,
      this.cropId,
      this.cropName,
      this.seasonId,
      this.seasonName,
      this.link,
      this.recommendation,
      this.filename,
      this.path,
      this.createdBy,
      this.createdOn,
      this.modifyBy,
      this.active,
      });

  factory CropAdvisoryModel.fromJson(Map<String, dynamic> json) {

    return CropAdvisoryModel(
    id : json['id'] as int,
    stateId : json['stateId'] as int,
    stateName : json['stateName'] as String,
    cropId : json['cropId'] as int,
    cropName : json['cropName'] as String,
    seasonId: json['seasonId'] as int,
    seasonName: json['seasonName'] as String,
    link : json['link'] as String,
    recommendation : json['recommendation'] as String,
    filename : json['filename'] as String,
    path : json['path'] as String,
    createdBy : json["createdBy"] as int,
    createdOn : json['createdOn'] as String,
    modifyBy : json['modifyBy'] as int,
    active : json['active'] as String);
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['stateId'] = this.stateId;
    data['stateName'] = this.stateName;
    data['cropId'] = this.cropId;
    data['cropName'] = this.cropName;
    data['seasonId'] = this.seasonId;
    data['seasonName'] = this.seasonName;
    data['link'] = this.link;
    data['recommendation'] = this.recommendation;
    data['filename'] = this.filename;
    data['path'] = this.path;
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['modifyBy'] = this.modifyBy;
    data['active'] = this.active;
    return data;
  }
}
