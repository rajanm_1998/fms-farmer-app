class FROsurveyanalysis {
  String status;
  FROSurveyData data;

  FROsurveyanalysis({this.status, this.data});

  FROsurveyanalysis.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new FROSurveyData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class FROSurveyData {
  List<FROSurvey> response;
  String status;
  Null message;

  FROSurveyData({this.response, this.status, this.message});

  FROSurveyData.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<FROSurvey>();
      json['Response'].forEach((v) {
        response.add(new FROSurvey.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class FROSurvey {
  int froid;
  String froname;
  int noOfFarmer;
  double areaAllocated;
  int surveyed;
  int notSurvey;
  int surveyedApproved;
  int frmId;
  String frmName;
  Map<String, double> sampleData;
  FROSurvey(
      {this.froid,
      this.froname,
      this.noOfFarmer,
      this.areaAllocated,
      this.surveyed,
      this.notSurvey,
      this.surveyedApproved,
      this.frmId,
      this.frmName});

  FROSurvey.fromJson(Map<String, dynamic> json) {
    froid = json['froid'];
    froname = json['froname'];
    noOfFarmer = json['NoOfFarmer'];
    areaAllocated = json['AreaAllocated'];
    surveyed = json['Surveyed'];
    notSurvey = json['NotSurvey'];
    surveyedApproved = json['SurveyedApproved'];
    frmId = json['FrmId'];
    frmName = json['FrmName'];
    sampleData = {
      "Not surveyed": this.notSurvey.toDouble(),
      "Surveyed": this.surveyed.toDouble(),
      "Approved": this.surveyedApproved.toDouble(),
    };
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['froid'] = this.froid;
    data['froname'] = this.froname;
    data['NoOfFarmer'] = this.noOfFarmer;
    data['AreaAllocated'] = this.areaAllocated;
    data['Surveyed'] = this.surveyed;
    data['NotSurvey'] = this.notSurvey;
    data['SurveyedApproved'] = this.surveyedApproved;
    data['FrmId'] = this.frmId;
    data['FrmName'] = this.frmName;
    sampleData = {
      "Not surveyed": this.notSurvey.toDouble(),
      "Surveyed": this.surveyed.toDouble(),
      "Approved": this.surveyedApproved.toDouble(),
    };
    return data;
  }
}
