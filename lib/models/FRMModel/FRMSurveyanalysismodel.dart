class FRMsurveyanalysis {
  String status;
  Data data;

  FRMsurveyanalysis({this.status, this.data});

  FRMsurveyanalysis.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Surveyanalysis> response;
  String status;
  String message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<Surveyanalysis>();
      json['Response'].forEach((v) {
        response.add(new Surveyanalysis.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class Surveyanalysis {
  int projectId;
  String projectName;
  int stateId;
  String stateName;
  String projectGroupName;
  int cCA;
  int noOfFarmer;
  int noOfFROs;
  double areaAllocated;
  int surveyed;
  int notSurvey;
  int surveyedApproved;
  Map<String, double> sampleData;

  Surveyanalysis(
      {this.projectId,
      this.projectName,
      this.stateId,
      this.stateName,
      this.projectGroupName,
      this.cCA,
      this.noOfFarmer,
      this.noOfFROs,
      this.areaAllocated,
      this.surveyed,
      this.notSurvey,
      this.surveyedApproved});

  Surveyanalysis.fromJson(Map<String, dynamic> json) {
    projectId = json['ProjectId'];
    projectName = json['ProjectName'];
    stateId = json['StateId'];
    stateName = json['StateName'];
    projectGroupName = json['ProjectGroupName'];
    cCA = json['CCA'];
    noOfFarmer = json['NoOfFarmer'];
    noOfFROs = json['NoOfFROs'];
    areaAllocated = json['AreaAllocated'];
    surveyed = json['Surveyed'];
    notSurvey = json['NotSurvey'];
    surveyedApproved = json['SurveyedApproved'];
     sampleData = {
      "Not surveyed": this.notSurvey.toDouble(),
      "Surveyed": this.surveyed.toDouble(),
      "Approved": this.surveyedApproved.toDouble(),
    };
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProjectId'] = this.projectId;
    data['ProjectName'] = this.projectName;
    data['StateId'] = this.stateId;
    data['StateName'] = this.stateName;
    data['ProjectGroupName'] = this.projectGroupName;
    data['CCA'] = this.cCA;
    data['NoOfFarmer'] = this.noOfFarmer;
    data['NoOfFROs'] = this.noOfFROs;
    data['AreaAllocated'] = this.areaAllocated;
    data['Surveyed'] = this.surveyed;
    data['NotSurvey'] = this.notSurvey;
    data['SurveyedApproved'] = this.surveyedApproved;
    sampleData = {
      "Not surveyed": this.notSurvey.toDouble(),
      "Surveyed": this.surveyed.toDouble(),
      "Approved": this.surveyedApproved.toDouble(),
    };
    return data;
  }
}
