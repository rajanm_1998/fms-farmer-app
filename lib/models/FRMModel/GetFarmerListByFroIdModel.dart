class GetFarmerListByFroId {
  String status;
  Data data;

  GetFarmerListByFroId({this.status, this.data});

  GetFarmerListByFroId.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<FarmerListByFroId> response;
  String status;
  String message;

  Data({this.response, this.status, this.message});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = new List<FarmerListByFroId>();
      json['Response'].forEach((v) {
        response.add(new FarmerListByFroId.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class FarmerListByFroId {
  int farmerId;
  String farmerName;
  int id;
  int statusId;
  String status;
  String active;

  FarmerListByFroId(
      {this.farmerId,
      this.farmerName,
      this.id,
      this.statusId,
      this.status,
      this.active});

  FarmerListByFroId.fromJson(Map<String, dynamic> json) {
    farmerId = json['FarmerId'];
    farmerName = json['FarmerName'];
    id = json['Id'];
    statusId = json['StatusId'];
    status = json['Status'];
    active = json['Active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FarmerId'] = this.farmerId;
    data['FarmerName'] = this.farmerName;
    data['Id'] = this.id;
    data['StatusId'] = this.statusId;
    data['Status'] = this.status;
    data['Active'] = this.active;
    return data;
  }
}
