class KmlMapModel {
  List<KmlMapResponse> response;
  String status;
  String message;

  KmlMapModel({this.response, this.status, this.message});

  KmlMapModel.fromJson(Map<String, dynamic> json) {
    if (json['Response'] != null) {
      response = <KmlMapResponse>[];
      json['Response'].forEach((v) {
        response.add(new KmlMapResponse.fromJson(v));
      });
    }
    status = json['Status'];
    message = json['Message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['Response'] = this.response.map((v) => v.toJson()).toList();
    }
    data['Status'] = this.status;
    data['Message'] = this.message;
    return data;
  }
}

class KmlMapResponse {
  String coordinates;
  int outerBoundaryIsId;
  int villageId;
  int fileId;
  int surveyNo;
  int chakno;
  int totalArea;
  int farmerName;

  KmlMapResponse(
      {this.coordinates,
      this.outerBoundaryIsId,
      this.villageId,
      this.fileId,
      this.surveyNo,
      this.chakno,
      this.totalArea,
      this.farmerName});

  KmlMapResponse.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'];
    outerBoundaryIsId = json['outerBoundaryIs_Id'];
    villageId = json['VillageId'];
    fileId = json['FileId'];
    surveyNo = json['SurveyNo'];
    chakno = json['Chakno'];
    totalArea = json['TotalArea'];
    farmerName = json['FarmerName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['coordinates'] = this.coordinates;
    data['outerBoundaryIs_Id'] = this.outerBoundaryIsId;
    data['VillageId'] = this.villageId;
    data['FileId'] = this.fileId;
    data['SurveyNo'] = this.surveyNo;
    data['Chakno'] = this.chakno;
    data['TotalArea'] = this.totalArea;
    data['FarmerName'] = this.farmerName;
    return data;
  }
}
