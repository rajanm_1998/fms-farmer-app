import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

class PieChartView extends StatelessWidget {
  
  const PieChartView({this.jsonData});

  final Map<String, double> jsonData;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: PieChart(
      dataMap: jsonData,
      chartType: ChartType.disc,
      chartRadius: MediaQuery.of(context).size.width / 1.8,
      colorList: [Colors.red, Colors.orange, Colors.green],
      legendOptions: LegendOptions( 
      showLegendsInRow: false,
      legendPosition: LegendPosition.bottom,
      showLegends: true,
      legendTextStyle: TextStyle(
      fontWeight: FontWeight.bold,
          )
      )
    )
    );
   }
 }