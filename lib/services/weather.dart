import 'package:fms_mobileapp/services/location.dart';
import 'package:fms_mobileapp/services/networking.dart';
import 'package:fms_mobileapp/utils/constants.dart';
import 'package:geocoder/geocoder.dart';

class WeatherModel {
  Future<dynamic> getCityWeather(String cityName) async {
    NetworkHelper networkHelper1 = new NetworkHelper(
        url:
            '$kweatherApiUrl$cityName?unitGroup=metric&key=$kweatherApiKey&include=fcst%2Ccurrent');

    var weatherData = await networkHelper1.getData();
    print(weatherData);
    return weatherData;
  }

  Future<dynamic> getLocationWeather() async {
    Location location = new Location();
    await location.getCurrentPostion();
    print("latitiude : " + location.latitude.toString());
    print("longitude : " + location.longitude.toString());

    NetworkHelper networkHelper = new NetworkHelper(
        url:
            '$kweatherApiUrl${location.latitude},${location.longitude}?unitGroup=metric&key=$kweatherApiKey&include=fcst%2Ccurrent');
    var weatherData = await networkHelper.getData();

    print(weatherData);

    return weatherData;
  }

  Future<dynamic> getFarmWeather(double latitude, double longitude) async {
    // String query = "Madhya Pradesh";
    // final query = "Badhiya Madhya Pradesh";
   
        NetworkHelper networkHelper = new NetworkHelper(
            url:
                '$kweatherApiUrl${latitude},${longitude}?unitGroup=metric&key=$kweatherApiKey&include=fcst%2Ccurrent');
        var weatherData = await networkHelper.getData();

        print(weatherData);

        return weatherData;
   }

  double latitude, longitude;
  void setCoordinates(double latitude, double longitude) {
    latitude = 24.3302293;
    longitude = 76.4388139;
  }

  Future<dynamic> getFarmLocationWeather() async {
    // await location.getCurrentPostion();
    // print("latitiude : " + location.latitude.toString());
    // print("longitude : " + location.longitude.toString());

    print(latitude + longitude);
    NetworkHelper networkHelper = new NetworkHelper(
        url:
            '$kweatherApiUrl${latitude},${longitude}?unitGroup=metric&key=$kweatherApiKey&include=fcst%2Ccurrent');
    var farmweatherData = await networkHelper.getData();

    print(farmweatherData);

    return farmweatherData;
  }
}
