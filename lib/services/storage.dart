import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/LandHoldingModel.dart';
import 'package:fms_mobileapp/models/User.dart';

class SecureStorage{
  static final _storage = FlutterSecureStorage();

  static const _keyUserData = 'userData';
  static const _keyLanguage = 'userLanguage';
  static const _keyLandId = 'landRoRId';
  static const _keyProjectId = 'projectId';

  static Future setUserData(String userModel) async => 
    await _storage.write(key: _keyUserData, value: userModel);


  static Future getUserData() async {
   dynamic value = _storage.read(key: _keyUserData);
   var val = await value;
    UserModel user = UserModel.fromJson(jsonDecode(val));
    // LandHoldingModel Land = LandHoldingModel.fromJson(jsonDecode(val));
   return user;
   
  }


  static Future setProjectId(String projectId) async =>
      await _storage.write(key: _keyProjectId, value: projectId);

  static Future getProjectId() async {
    String projectid = await _storage.read(key: _keyProjectId);
    return projectid;
  }
  
  static Future setLanguage(String lang) async =>
    await _storage.write(key: _keyLanguage, value: lang);

  static Future getLanguage() async =>
    await _storage.read(key: _keyLanguage);

  static Future setLandId(int LandId) async => 
  await _storage.write(key: _keyLandId, value: LandId.toString());

    
  static Future getLandId() async {
   dynamic value = _storage.read(key: _keyLandId);
   var val = await value;
    return value;
  }
}