import 'dart:convert';
import 'package:fms_mobileapp/models/FarmerMap/KmlFileTypeModel';
import 'package:fms_mobileapp/models/FarmerMap/KmlMapModel.dart';
import 'package:http/http.dart' as http;

Future<KmlFileTypeModel> fetchKmlFileType() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetFmlFileType'));
    if (response.statusCode == 200) {
      KmlFileTypeModel model = KmlFileTypeModel.fromJson(jsonDecode(response.body));
      return model;
    } else {
      throw new Exception();
    }
  } on Exception catch (_, ex) {
    return null;
  }
}
Future<KmlMapModel> fetchKmlMapCoordinates(int LandId, int KmlFileTypeId) async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetKmlMapCoordinate?LandId=1441370&FmlFileTypeId=$KmlFileTypeId'));
    if (response.statusCode == 200) {
      KmlMapModel model = KmlMapModel.fromJson(jsonDecode(response.body));
      print(response.body);
      return model;
    } else {
      throw new Exception();
    }
  } on Exception catch (_, ex) {
    return null; 
  }
}