import 'dart:convert';
import 'package:fms_mobileapp/models/FRMModel/FRMSurveyanalysismodel.dart';
import 'package:fms_mobileapp/models/FRMModel/FROSurveyanalysismodel.dart';
import 'package:fms_mobileapp/models/KhasaraChakModel.dart';
import 'package:fms_mobileapp/models/SurveyStatusModel.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:http/http.dart' as http;

Future<Surveyanalysis> fetchFRMsurveyanalysisreponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    String projectfetched = await SecureStorage.getProjectId();
    int pid = int.parse(projectfetched);
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/SurveyAnalysis/GetFRMSurveyAnalysisDetail_New?FrmId=555219'));
    if (response.statusCode == 200) {
      FRMsurveyanalysis farmer =
          FRMsurveyanalysis.fromJson(jsonDecode(response.body));
      //  print(farmer);
        var survey = farmer.data.response.where((e) => e.projectId == 13).first;
      return survey;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}
Future<List<FROSurvey>> fetchFROListByFrmIdreponse() async {
  try {
    /*dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    String projectfetched = await SecureStorage.getProjectId();
    int pid = int.parse(projectfetched);*/
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/SurveyAnalysis/GetFROSurveyAnalysisDetailList_New?FrmId=555219&ProjectId=13'));
    if (response.statusCode == 200) {
      FROsurveyanalysis farmer =
          FROsurveyanalysis.fromJson(jsonDecode(response.body));
      //  print(farmer);
        var survey = farmer.data.response;
        print(survey.length);
      return survey;
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}
Future<List<KhasaraChakResponse>> fetchFRMKhasaraChakList() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/SurveyAnalysis/GetFRMKhasaraChakList_New?FrmId=555219&ProjectId=13'));
    if (response.statusCode == 200) {
      KhasaraChakList farmer =
          KhasaraChakList.fromJson(jsonDecode(response.body));
      //  print(farmer);
      var survey = farmer.data.response;
      return survey;
    } else
      throw Exception();
  } catch (_, ex) {
    throw Exception();
  }
}
Future<List<KhasaraChakResponse>> fetchFROKhasaraChakList() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/SurveyAnalysis/GetFROKhasaraChakList_New?FroId=555220&FrmId=0&ProjectId=13'));
    if (response.statusCode == 200) {
      KhasaraChakList farmer =
          KhasaraChakList.fromJson(jsonDecode(response.body));
      //  print(farmer);
      var survey = farmer.data.response;
      return survey;
    } else
      throw Exception();
  } catch (_, ex) {
    throw Exception();
  }
}

Future<List<SurveyStatusResponse>> fetchSurveyStatusList() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/SurveyAnalysis/GetSurveyStatusList'));
    if (response.statusCode == 200) {
      SurveyStatusModel farmer =
          SurveyStatusModel.fromJson(jsonDecode(response.body));
      //  print(farmer);
      var survey = farmer.data.response;
      return survey;
    } else
      throw Exception();
  } catch (_, ex) {
    throw Exception();
  }
}


