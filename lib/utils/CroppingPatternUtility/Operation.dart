import 'package:fms_mobileapp/models/CroppingPattern/MstSeasonCropModel.dart';
import 'package:fms_mobileapp/models/MasterModel/MstCropModel.dart';
import 'package:fms_mobileapp/models/MasterModel/MstSeasonModel.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:fms_mobileapp/models/LandHoldingRoRDesign/LandHoldingRoRDesignModel.dart';

Future<LandHoldingRoRDesignModel>
    fetchLandHoldingRoRDesignDetailResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetLandHoldingDetailsRoRDesign?FarmerId=${uid}&RSurveyNo='));
    if (response.statusCode == 200) {
      // print(response.body);
      dynamic l = jsonDecode(response.body);
      /*print(response.body);*/
      if (l['Status'].toString().toLowerCase() == 'ok') {
        LandHoldingRoRDesignModel list = LandHoldingRoRDesignModel.fromJson(l);
        return list;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<MstSeasonModel> fetchSeasonListResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http
        .get(Uri.parse('http://fmsapi.seprojects.in/api/farmer/GetSeasonList'));
    if (response.statusCode == 200) {
      // print(response.body);
      dynamic l = jsonDecode(response.body);
      if (l['Status'].toString().toLowerCase() == 'ok') {
        MstSeasonModel list = MstSeasonModel.fromJson(l);
        return list;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<MstCropModel> fetchCropListResponse() async {
  try {
    final response = await http
        .get(Uri.parse('http://fmsapi.seprojects.in/api/farmer/GetCropList'));
    if (response.statusCode == 200) {
      // print(response.body);
      dynamic l = jsonDecode(response.body);
      if (l['Status'].toString().toLowerCase() == 'ok') {
        MstCropModel list = MstCropModel.fromJson(l);
        return list;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<List<MstSeasonCropModelResp>> fetchSeasonCropListResponse() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetSeasonCropList'));
    if (response.statusCode == 200) {
      // print(response.body);
      dynamic l = jsonDecode(response.body);
      if (l['Status'].toString().toLowerCase() == 'ok') {
        MstSeasonCropModel list = MstSeasonCropModel.fromJson(l);
        return list.response;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}
