import 'dart:convert';
import 'dart:async';
// import 'package:flutter_session/flutter_session.dart';
import 'package:fms_mobileapp/main.dart';
import 'package:fms_mobileapp/models/ChakIrrigationProjectDetails.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternModel.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/models/LandHoldingRoRDesign/LandHoldingRoRDesignModel.dart';
import 'package:fms_mobileapp/models/subchakIrrigationSchedulingModel.dart';
import 'package:fms_mobileapp/services/storage.dart';
import 'package:http/http.dart' as http;
import 'package:fms_mobileapp/models/CropAdvisoryModel.dart';
import 'package:fms_mobileapp/models/User.dart';
import 'package:fms_mobileapp/models/Login.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/CroppingPatternModel_SubChakWise.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternModel.dart';
import 'package:fms_mobileapp/models/CroppingPatternModel_SubChakWise.dart';
import 'package:fms_mobileapp/models/CroppingPattern/CropPatternDropDownModel.dart';

String mobileNo;
Future<Response> fetchResponse(String text) async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/login/GenerateOTP_New?MobileNumber=$text'));
    if (response.statusCode == 200) {
      Response user = Response.fromJson(jsonDecode(response.body));
      //  print(user.data.code);
      //  print(response.body);
      //  String otpCode = user.data.code;
      //  MyCustomForm(otpCode: otpCode);
      return user;
    } else {
      return new Response(status: "Fail");
    }
  } on Exception catch (_, ex) {
    return new Response(status: "Fail");
  }
}

Future<UserModel> fetchOTPResponse(String mobileNo, String otpCode) async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/login/VerifyOTP_New?MobileNumber=$mobileNo&OTPCode=$otpCode'));
    if (response.statusCode == 200) {
      UserModel user = UserModel.fromJson(jsonDecode(response.body));
      // await FlutterSession().set('UserModel', user.data.StateId);
      //  print(response.body);
      return user;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}

Future<List<CropAdvisoryModel>> fetchCropAdvisoryResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int sid = fetched.data.StateId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/CropAdvisory/GetCropAdvisoryList?sid=$sid'));
    if (response.statusCode == 200) {
      //  print(response.body);
      dynamic cropObject = jsonDecode(response.body);
      final convertedCropObject = cropObject.cast<Map<String, dynamic>>(); //4
      List<CropAdvisoryModel> list = convertedCropObject
          .map<CropAdvisoryModel>((json) => CropAdvisoryModel.fromJson(json))
          .toList();
      //  print(convertedCropObject);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}

Future<FarmerDetailsModel> fetchFarmerDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetFarmerDetails_New?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      FarmerDetailsModel farmer =
          FarmerDetailsModel.fromJson(jsonDecode(response.body));
      //  print(farmer);
      //  print(response.body);
      return farmer;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}

Future<List<FarmerLandModel>> fetchFarmerLandResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetLandDetailList?fid=${uid}'));
    if (response.statusCode == 200) {
      // print(response.body);
      if (response.body.length > 2) {
        dynamic l = jsonDecode(response.body);
        //  print(convertedCropObject);]
        final convertedCropObject = l.cast<Map<String, dynamic>>(); //4
        List<FarmerLandModel> list = convertedCropObject
            .map<FarmerLandModel>((model) => FarmerLandModel.fromJson(model))
            .toList();
        // print(list);
        // print(response.body);
        return list;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<List<FarmerLandModel>> fetchFarmerLandDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetLandDetailRoRDesignList?fid=$uid&RSurveyno='));
    if (response.statusCode == 200) {
      // print(response.body);

      dynamic l = jsonDecode(response.body);
      //  print(convertedCropObject);]
      final convertedCropObject = l.cast<Map<String, dynamic>>(); //4
      List<FarmerLandModel> list = convertedCropObject
          .map<FarmerLandModel>((model) => FarmerLandModel.fromJson(model))
          .toList();
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}

Future<List<CroppingPatternModel>> fetchCroppingPatternResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetCroppingPatternList?FarmerId=${uid}&LandId=590389'));
    if (response.statusCode == 200) {
      // print(response.body);
      if (response.body.length != 2) {
        dynamic l = jsonDecode(response.body);
        //  print(convertedCropObject);]
        final convertedCropObject = l.cast<Map<String, dynamic>>(); //4
        List<CroppingPatternModel> list = convertedCropObject
            .map<CroppingPatternModel>(
                (model) => CroppingPatternModel.fromJson(model))
            .toList();
        //  print(list);
        //  print(response.body);
        return list;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<List<CroppingPatternModel_SubChakWise>> fetchCroppingPatternListResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int userid = fetched.data.userId;
    String lid = await SecureStorage.getLandId();
    int landId = int.tryParse(lid);
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetCroppingPatternList_New?FarmerId=$userid&LandId=$landId'));
    if (response.statusCode == 200) {
      // print(response.body);
      if (response.body.length != 2) {
        dynamic l = jsonDecode(response.body);
        //  print(convertedCropObject);]
        final convertedCropObject = l.cast<Map<String, dynamic>>(); //4
        List<CroppingPatternModel_SubChakWise> list = convertedCropObject
            .map<CroppingPatternModel_SubChakWise>(
                (model) => CroppingPatternModel_SubChakWise.fromJson(model))
            .toList();
        //  print(list);
        //  print(response.body);
        return list;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<CropPatternDropDownModel> fetchCropPatternDropDownResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetPreviousYrCropPatternDropDownList?FarmerId=$uid'));
    if (response.statusCode == 200) {
      // print(response.body);
      CropPatternDropDownModel pattern =
          CropPatternDropDownModel.fromJson(jsonDecode(response.body));
      //  print(response.body);
      return pattern;
    } else {
      throw new Exception();
    }
  } on Exception catch (_, ex) {}
  throw new Exception();
}

Future<CroppingPatternModel> fetchCropPatternDetailResponse(int Year, int Sid, int Lid, int Cid) async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int userid = fetched.data.userId;
    //print(userid);
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetPreviousYrCropPatternList?FarmerId=$userid&Year=$Year&SeasonId=$Sid&LandId=$Lid&CropId=$Cid'));
    if (response.statusCode == 200) {
      // print(response.body);
      CroppingPatternModel pattern =
          CroppingPatternModel.fromJson(jsonDecode(response.body));
      //  print(response.body);
      return pattern;
    } else {
      throw new Exception();
    }
  } on Exception catch (_, ex) {
    throw new Exception();
  }
}

Future<List<CroppingPatternModel>> fetchCurrentYrCropPatternDetailResponse(
    {String cpIds = '',
    String landIds = '',
    String years = '',
    String seasonsIds = '',
    String cropIds = '',
    String areas = '',
    String mrate = ''}) async 
    {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int userid = fetched.data.userId;
    //print(userid);
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetCurrentYrCropPatternListNew?FarmerId=$userid&CpIds=' +
            cpIds.replaceAll('null', '') +
            '&landIds=' +
            landIds.replaceAll('null', '') +
            '&years=' +
            years.replaceAll('null', '') +
            ' &seasonIds=' +
            seasonsIds.replaceAll('null', '') +
            '&cropIds=' +
            cropIds.replaceAll('null', '') +
            '&areas=' +
            areas.replaceAll('null', '') +
            '&mrate=' +
            mrate.replaceAll('null', '')));

    if (response.statusCode == 200) {
      // print(response.body);
      if (response.body.length > 2) {
        dynamic l = jsonDecode(response.body);
        final convertedCropObject = l.cast<Map<String, dynamic>>();
        List<CroppingPatternModel> pattern = convertedCropObject
            .map<CroppingPatternModel>(
                (model) => CroppingPatternModel.fromJson(model))
            .toList();
        // print(convertedCropObject);
        //print(response.body);
        //print(pattern);
        return pattern;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {}
}

/*Future<http.Response> createPost(Response response) async {
  final data = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/InsertFarmMachineryDetails'),
      headers: {"Content-type": "appliction/json"},
      body: jsonEncode(response.toJson()));
  return data;
}
*/
/*
Future<bool> insertCroppingPattern(List<CroppingPatternModel> cplist) async {
 var headers = {
  'Content-Type': 'application/json',
  "Access-Control-Allow-Origin": "*", // Required for CORS support to work
  "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
  "Access-Control-Allow-Headers": "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
  "Access-Control-Allow-Methods": "POST, OPTIONS"
};
var request = http.Request('POST', Uri.parse('http://fmsapi.seprojects.in/api/Farmer_FMobile/InsertCurrentYearCroppingPattern'));
request.body = jsonEncode(cplist);
// request.headers.addAll(headers);

http.StreamedResponse response = await request.send();

if (response.statusCode == 200) {
  
  print(await response.stream.bytesToString());
  return true;
}
else {
  print(response.reasonPhrase);
  return false;
}
}*/


Future<bool> insertCroppingPattern(List<CroppingPatternModel> cplist) async {
  final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/Farmer_FMobile/InsertCurrentYearCroppingPattern'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(cplist));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to insert.');
  }
}

Future<bool> deleteCroppingPattern(int cpid) async {
  /*final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/DeleteCroppingPatternDetails?id=${cpid}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      });
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to delete.');
  }
  */
  var request = http.Request(
      'POST',
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/DeleteCroppingPatternDetails?id=$cpid'));

  http.StreamedResponse response = await request.send();

  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to delete.');
  }
}






Future<LandHoldingRoRDesignModel> fetchDropdownLandHoldingRoRDesignDetailResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetLandHoldingDetailsRoRDesign?FarmerId=${uid}&RSurveyNo='));
    if (response.statusCode == 200) {
      // print(response.body);
      dynamic l = jsonDecode(response.body);
      print(response.body);
      if (l['Status'].toString().toLowerCase() == 'ok') {
        LandHoldingRoRDesignModel land = LandHoldingRoRDesignModel.fromJson(l);
        return land;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}



String username = 'smp';
String password = 'smp@123';
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));
Future<ChakIrrigationModel> fetchSubChakIrrigationDetailsResponse() async {
  // Map headers = {
  //   'content-type': 'application/json',
  //   'accept': 'application/json',
  //   'authorization': basicAuth
  // };
  try {
    // dynamic fetched = await SecureStorage.getUserData();
    // int userid = fetched.data.userId;
    // String lid = await SecureStorage.getLandId();
    // int landId = int.tryParse(lid);
    final response = await http.get(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetRomsSchedulingwithChakNo?ChakNo=9A&ProjectId=4'),
      //headers: {'authorization': basicAuth},
    );
    if (response.statusCode == 200) {
      // print(response.body);
      if (response.body.length != 2) {
        dynamic l = jsonDecode(response.body);
        //  print(convertedCropObject);]
        // final jsonResponse =
        //     json.decode(response.body).cast<Map<String, dynamic>>();
        // ChakIrrigationModel list =
        //     jsonResponse.map<ChakIrrigationModel>((json) async {
        //   return IrrigationSchedulingModels.fromJson(json);
        // }).toList();

        ChakIrrigationModel farmer =
            ChakIrrigationModel.fromJson(jsonDecode(response.body));

         print(response.body);
        return farmer;
      } else
        throw Exception();
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}

Future<ProjectDetailsResponse> fetchChakProjectDetailsResponse(int projectId) async {

  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetProjectDetail?ProjectId=${projectId}'));
    if (response.statusCode == 200) {
      ChakIrrigationProjectDetails data = ChakIrrigationProjectDetails.fromJson(jsonDecode(response.body));
      ProjectDetailsResponse user = data.response;
      print(response.body);
      
      return user;
    } else {
      throw Exception();
    }
  } on Exception catch (_, ex) {
    throw Exception();
  }
}





