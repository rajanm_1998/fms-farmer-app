import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:fms_mobileapp/models/FarmerDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerLandModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/CastsModels.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/CropPatternDetails.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/FamilyDetailModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/FarmMachineryListModels.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/FarmerProflleModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/IrrigationFacility.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/IrrigationFacilityModels.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/IrrigationTypeDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/IrrigationTypeModels.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/LandHoldingModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/LiveStockDetailsModel.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/LivestockModels.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/ReligionModels.dart';
import 'package:fms_mobileapp/models/FarmerProfileModel/farmerMachineryDetailsModel.dart';
import 'package:http/http.dart' as http;
import 'package:fms_mobileapp/services/storage.dart';

Future<FamilyDetailModel> fetchFamilyDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetFamilyDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      FamilyDetailModel list = FamilyDetailModel.fromJson(l);

      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}

// Future<List<FarmerDetails>> fetchFarmerLandResponse() async {
//   try {
//     // dynamic fetched = await SecureStorage.getUserData();
//     // int uid = fetched.data.userId;
//     final response = await http.get(Uri.parse(
//         'http://fmsapi.seprojects.in/api/Farmer_FMobile/GetLandDetailList?fid=28900'));
//     if (response.statusCode == 200) {
//       // print(response.body);

//       dynamic l = jsonDecode(response.body);

//       //  print(convertedCropObject);]
//       final convertedCropObject = l.cast<Map<String, dynamic>>(); //4
//       List<FarmerLandModel> list = convertedCropObject
//           .map<FarmerLandModel>((model) => FarmerLandModel.fromJson(model))
//           .toList();
//       print(list);
//       // print(response.body);
//       return list;
//     } else {
//       return null;
//     }
//   } on Exception catch (_, ex) {
//     return null;
//   }
// }

Future<CropPatternDetailsModel> fetchFCropPatternResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetCroppingPatternDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      // print(response.body);

      dynamic l = jsonDecode(response.body);
      //print(l);
      //print(l);
      /*
      dynamic obj = l.cast<Map<String, dynamic>>(); //4

      List<FamilyDetailModel> list = obj
          .map<FamilyDetailModel>((model) => FamilyDetailModel.fromJson(model))
          .toList();*/
      CropPatternDetailsModel list = CropPatternDetailsModel.fromJson(l);
//      print(list);
      //R    print("object");
      // print(response.body);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    //print(ex);

    return null;
  }
}

Future<IrrigationFacilityDetailsModel>
    fetchFIrrigationFacilityResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetIrrigationFacilityDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      IrrigationFacilityDetailsModel list =
          IrrigationFacilityDetailsModel.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<IrrigationTypeDetailsModel> fetchFirrigationTypeResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetIrrigationTypeDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      IrrigationTypeDetailsModel list = IrrigationTypeDetailsModel.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}


Future<LiveStockDetailsModel> fetchLiveStockDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetLivestockDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      LiveStockDetailsModel list = LiveStockDetailsModel.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<farmerMachineryDetailsModel>
    fetchfarmerMachineryDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetFarmMachineryDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      farmerMachineryDetailsModel list =
          farmerMachineryDetailsModel.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<LandHoldingModel> fetchLandHoldDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetLandHoldingDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      LandHoldingModel list = LandHoldingModel.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

// Future<FarmerDetailsModel> fetchFarmerProfileDetailsResponse() async {
//   try {
//     // dynamic fetched = await SecureStorage.getUserData();
//     // int uid = fetched.data.userId;
//     final response = await http.get(Uri.parse(
//         'http://fmsapi.seprojects.in/api/farmer/GetFarmerDetails_New?FarmerId=28900'));
//     if (response.statusCode == 200) {
//       FarmerDetailsModel farmer =
//           FarmerDetailsModel.fromJson(jsonDecode(response.body));
//       //  print(farmer);
//       //  print(response.body);
//       return farmer;
//     } else {
//       return null;
//     }
//   } on Exception catch (_, ex) {
//     return null;
//   }
// }

Future<FarmerProfileDetailModel> fetchFarmerProfileDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetFarmerDetails?FarmerId=${uid}'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      FarmerProfileDetailModel list = FarmerProfileDetailModel.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<FarmerProfileDetailModel> fetchEditFarmerProfileDetailsResponse() async {
  try {
    dynamic fetched = await SecureStorage.getUserData();
    int uid = fetched.data.userId;
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetFarmerDetails?FarmerId=$uid'));
    if (response.statusCode == 200) {
      FarmerProfileDetailModel farmer =
          FarmerProfileDetailModel.fromJson(jsonDecode(response.body));
      return farmer;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    return null;
  }
}

Future<bool> insertFarmerProfileDetails(FarmerResponse value) async {
  var jsonString = value.toJson();
  var code = jsonEncode(jsonString);
  final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/Farmer_FMobile/UpdateFarmerProfileDetails'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(jsonString));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to update.');
  }
}

Future<bool> insertFamilyDetails(FamilyDetails cplist) async {
  final response = await http.post(
      Uri.parse('http://fmsapi.seprojects.in/api/farmer/UpdateFamilyDetails'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(cplist.toJson()));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to insert.');
  }
}

Future<bool> insertIrrigationFacility(dynamic cplist) async {
  final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/UpdateIrrigationFacilityDetails'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(cplist.toJson()));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to insert.');
  }
}

Future<bool> insertIrrigationType(dynamic cplist) async {
  final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/UpdateIrrigationTypeDetails'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(cplist.toJson()));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to insert.');
  }
}

Future<bool> insertLiveStockDetails(dynamic cplist) async {
  final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/UpdateLivestockDetails'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(cplist.toJson()));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to insert.');
  }
}

Future<bool> insertFarmerMachineryDetails(dynamic cplist) async {
  final response = await http.post(
      Uri.parse(
          'http://fmsapi.seprojects.in/api/farmer/UpdateFarmMachineryDetails'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(cplist.toJson()));
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return true;
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to insert.');
  }
}

Future<CastsModels> CasteListDetails() async {
  try {
    final response = await http
        .get(Uri.parse('http://fmsapi.seprojects.in/api/farmer/GetCasteList'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      CastsModels list = CastsModels.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<ReligionModels> ReligionListDetails() async {
  try {
    final response = await http.get(
        Uri.parse('http://fmsapi.seprojects.in/api/farmer/GetReligionList'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      ReligionModels list = ReligionModels.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<IrrigationFacilityModels> IrrigationFacilityList() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetIrrigationFacilityList'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      IrrigationFacilityModels list = IrrigationFacilityModels.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<IrrigationTypeModels> IrrigationTypeList() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetIrrigationTypeList'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      IrrigationTypeModels list = IrrigationTypeModels.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<LivestockModels> LiveStockList() async {
  try {
    final response = await http.get(
        Uri.parse('http://fmsapi.seprojects.in/api/farmer/GetLivestockList'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      LivestockModels list = LivestockModels.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}

Future<FarmMachineryListModels> FarmMachineryList() async {
  try {
    final response = await http.get(Uri.parse(
        'http://fmsapi.seprojects.in/api/farmer/GetFarmMachineryList'));
    if (response.statusCode == 200) {
      dynamic l = jsonDecode(response.body);
      FarmMachineryListModels list = FarmMachineryListModels.fromJson(l);
      return list;
    } else {
      return null;
    }
  } on Exception catch (_, ex) {
    print(ex);

    return null;
  }
}



// Future<bool> FarmMachineryList(dynamic cplist) async {
//   final response = await http.post(
//       Uri.parse('http://fmsapi.seprojects.in/api/farmer/GetFarmMachineryList'),
//       headers: <String, String>{
//         'Content-Type': 'application/json; charset=UTF-8',
//       },
//       body: jsonEncode(cplist.toJson()));
//   if (response.statusCode == 200) {
//     // If the server did return a 201 CREATED response,
//     // then parse the JSON.
//     return true;
//   } else {
//     // If the server did not return a 201 CREATED response,
//     // then throw an exception.
//     throw Exception('Failed to insert.');
//   }
// }
